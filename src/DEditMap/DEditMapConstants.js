/* global Descartes, VERSION */

function checkDescartesContext(context) {
	return true;
}

function applyDescartesContext(context) {

	if (checkDescartesContext(context)) {
		try {
		if (context.editionManager && context.editionManager.configureOptions) {
			Descartes.EditionManager.configure(context.editionManager.configureOptions);
		}

		var contenuCarte = new Descartes.MapContent(context.mapContent.mapContentParams);

		contenuCarte.populate(context.mapContent.items);

		var carte = null;

		if (context.map.type === Descartes.Map.MAP_TYPES.CONTINUOUS) {
			carte = new Descartes.Map.ContinuousScalesMap(context.map.div, contenuCarte, context.map.mapParams);
		} else if (context.map.type === Descartes.Map.MAP_TYPES.DISCRETE) {
			carte = new Descartes.Map.DiscreteScalesMap(context.map.div, contenuCarte, context.map.mapParams);
		}

		if (context.features) {
			if (context.features.toolBars) {
				for (var i = 0, iLen = context.features.toolBars.length; i < iLen; i++) {
					var toolBar = carte.addNamedToolBar(context.features.toolBars[i].div, context.features.toolBars[i].tools, context.features.toolBars[i].options);
					if (context.features.toolBars[i].toolBarId) {
						toolBar.toolBarName = context.features.toolBars[i].toolBarId;
					}
				}
			}
		}

		if (context.mapContent.mapContentManager) {
            carte.addContentManager(context.mapContent.mapContentManager.div, context.mapContent.mapContentManager.contentTools, context.mapContent.mapContentManager.options);
		}

		if (context.mapContent.mapContentManagerSimple) {
            carte.addContentManagerSimple(context.mapContent.mapContentManagerSimple.div);
		}

		carte.show();

		if (context.features) {
			if (context.features.editionToolBars) {
				for (var ii = 0, iiLen = context.features.editionToolBars.length; ii < iiLen; ii++) {
					var options = context.features.editionToolBars[ii].options;
					if (context.features.editionToolBars[ii].options && context.features.editionToolBars[ii].options.toolBarId) {
						options.toolBar = carte.getNamedToolBar(context.features.editionToolBars[ii].options.toolBarId);
					}
					carte.addEditionToolBar(context.features.editionToolBars[ii].div, context.features.editionToolBars[ii].tools, options);
				}
			}
			if (context.features.annotationToolBars) {
				for (var jj = 0, jjLen = context.features.annotationToolBars.length; jj < jjLen; jj++) {
					var opts = context.features.annotationToolBars[jj].options;
					if (context.features.annotationToolBars[jj].options && context.features.annotationToolBars[jj].options.toolBarId) {
						opts.toolBar = carte.getNamedToolBar(context.features.annotationToolBars[jj].options.toolBarId);
					}
					carte.addAnnotationToolBar(context.features.annotationToolBars[jj].div, context.features.annotationToolBars[jj].tools, opts);
				}
			}
			if (context.features.infos) {
				carte.addInfos(context.features.infos);
			}
			if (context.features.actions) {
				carte.addActions(context.features.actions);
			}

			if (context.features.directionalPanPanel) {
				carte.addDirectionalPanPanel(context.features.directionalPanPanel.options);
			}
			if (context.features.miniMap) {
                carte.addMiniMap(context.features.miniMap.resourceUrl, context.features.miniMap.options);
			}
			if (context.features.toolTip) {
				var toolTipLayers = [];
				for (var m = 0, mLen = context.features.toolTip.toolTipLayers.length; m < mLen; m++) {
                    toolTipLayers.push({
                        layer: contenuCarte.getLayerById(context.features.toolTip.toolTipLayers[m].layerId),
                        fields: context.features.toolTip.toolTipLayers[m].fields
                    });
				}
				carte.addToolTip(context.features.toolTip.div, toolTipLayers, context.features.toolTip.options);
			}
			if (context.features.selectToolTip) {
				var selectToolTipLayers = [];
				for (var n = 0, nLen = context.features.selectToolTip.selectToolTipLayers.length; n < nLen; n++) {
					selectToolTipLayers.push({
                        layer: contenuCarte.getLayerById(context.features.selectToolTip.selectToolTipLayers[n].layerId),
                        fields: context.features.selectToolTip.selectToolTipLayers[n].fields
                    });
				}
				carte.addSelectToolTip(selectToolTipLayers, context.features.selectToolTip.options);
			}
			if (context.features.bookmarksManager) {
				carte.addBookmarksManager(context.features.bookmarksManager.div, context.features.bookmarksManager.mapName, context.features.bookmarksManager.options);
			}
			if (context.features.defaultGazetteer) {
				carte.addDefaultGazetteer(context.features.defaultGazetteer.div, context.features.defaultGazetteer.initValue, context.features.defaultGazetteer.startlevel, context.features.defaultGazetteer.options);
			}
			if (context.features.gazetteer) {
				carte.addGazetteer(context.features.gazetteer.div, context.features.gazetteer.initValue, context.features.gazetteer.levels, context.features.gazetteer.options);
			}
			if (context.features.requestManager) {
				var requestManager = carte.addRequestManager(context.features.requestManager.div, context.features.requestManager.options);
				for (var j = 0, jLen = context.features.requestManager.requests.length; j < jLen; j++) {
					var descartesLayer = contenuCarte.getLayerById(context.features.requestManager.requests[j].layerId);
					var requestObj = context.features.requestManager.requests[j];
					var request = new Descartes.Request(descartesLayer, requestObj.title, requestObj.geometryType, requestObj.options);
					for (var k = 0, kLen = context.features.requestManager.requests[j].requestMembers.length; k < kLen; k++) {
						var critereType = new Descartes.RequestMember(context.features.requestManager.requests[j].requestMembers[k].title, context.features.requestManager.requests[j].requestMembers[k].field, context.features.requestManager.requests[j].requestMembers[k].operator, context.features.requestManager.requests[j].requestMembers[k].value, context.features.requestManager.requests[j].requestMembers[k].visible);
						request.addMember(critereType);
					}
					requestManager.addRequest(request);
				}
			}

			if (context.features.openlayersFeatures) {
				if (context.features.openlayersFeatures.controls) {
					carte.addOpenLayersControls(context.features.openlayersFeatures.controls);
				}
				if (context.features.openlayersFeatures.interactions) {
					carte.addOpenLayersInteractions(context.features.openlayersFeatures.interactions);
				}
			}
		}

        return carte;
		} catch (e) {
			alert("Problème de chargement du context Descartes");
		}
	} else {
		alert("Problème de chargement du context Descartes");
	}
	return null;

}

/**
 * Class: Descartes
 * L'objet Descartes est l'objet technique principal de la librairie.
 *
 * :
 * Il fournit :
 * - un espace de nommage pour toutes les classes de la librairie.
 * - des propriétés et méthodes statiques de configuration générale.
 *
 * :
 * La classe fonctionnelle principale de la librairie est la classe <Descartes.Map>.
 */

var constants = {
    applyDescartesContext: applyDescartesContext
};
module.exports = constants;

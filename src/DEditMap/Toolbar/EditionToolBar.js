/* global Descartes */

var Utils = Descartes.Utils;
var ToolBar = Descartes.ToolBar;
var ToolBarConstants = require('./ToolBarConstants');

require('./css/editionToolbar.css');

/**
 * Class: Descartes.ToolBar.EditionToolBar
 * Classe définissant une barre d'outils d'édition, pouvant accueillir des boutons de type <Descartes.Tool.Edition>.
 *
 * Hérite de:
 *  - <Descartes.ToolBar>
 */
var Class = Utils.Class(ToolBar, {
    /**
     * Methode: addControl
     * Ajoute un outil de type <Descartes.Tool.Edition> à la barre d'outils.
     *
     * Paramètres:
     * control - {Descartes.Tool.Edition} Bouton à ajouter.
     */
    addControl: function (control) {
        if (control.CLASS_NAME.indexOf('Descartes.Tool.Edition') === 0 ||
                control.CLASS_NAME.indexOf('Descartes.Button.ToolBarOpener') === 0 ||
                      control.CLASS_NAME.indexOf('Descartes.Button.ContentTask.ExportVectorLayer') === 0) {
            ToolBar.prototype.addControl.apply(this, arguments);
        }
    },

    _computeToolBarName: function () {
        return ToolBarConstants.EDITION_TEMPLATE_NAME + ToolBarConstants.ID;
    },

    CLASS_NAME: "Descartes.ToolBar.EditionToolBar"
});

module.exports = Class;

/* global MODE, Descartes */

var _ = require('lodash');
var ToolBar = Descartes.ToolBar;
var EditionToolBar = require('./EditionToolBar');
var Constants = require('./ToolBarConstants');

var namespace = {
	EditionToolBar: EditionToolBar
};
_.extend(namespace, Constants);
_.extend(ToolBar, namespace);

module.exports = ToolBar;

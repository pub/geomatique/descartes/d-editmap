var ol = require('openlayers');

require('ol-ext');
// require('ol-ext-css');

ol.interaction.Transform.prototype.setDefaultStyle = function () {	// Style
	var stroke = new ol.style.Stroke({ color: [0, 0, 0, 1], width: 1 });
	var strokedash = new ol.style.Stroke({ color: [0, 0, 0, 1], width: 1, lineDash: [4, 4] });
	var fill0 = new ol.style.Fill({ color: [0, 0, 0, 0.01] });
	var fill = new ol.style.Fill({ color: [0, 0, 0, 0.8] });
	var circle = new ol.style.RegularShape({
		fill: fill,
		stroke: stroke,
		radius: this.isTouch ? 12 : 6,
		points: 15
	});
	circle.getAnchor()[0] = this.isTouch ? -10 : -5;
	var bigpt = new ol.style.RegularShape({
		fill: fill,
		stroke: stroke,
		radius: this.isTouch ? 16 : 8,
		points: 4,
		angle: Math.PI / 4
	});
	var smallpt = new ol.style.RegularShape({
		fill: fill,
		stroke: stroke,
		radius: this.isTouch ? 12 : 6,
		points: 4,
		angle: Math.PI / 4
	});
	function createStyle(img, stroke, fill) {
		return [
			new ol.style.Style({
			image: img,
			stroke: stroke,
			fill: fill
		}) ];
	}
	/** Style for handles */
	this.style = {
		'default': createStyle(bigpt, strokedash, fill0),
		'translate': createStyle(bigpt, stroke, fill),
		'rotate': createStyle(circle, stroke, fill),
		'rotate0': createStyle(bigpt, stroke, fill),
		'scale': createStyle(bigpt, stroke, fill),
		'scale1': createStyle(bigpt, stroke, fill),
		'scale2': createStyle(bigpt, stroke, fill),
		'scale3': createStyle(bigpt, stroke, fill),
		'scalev': createStyle(smallpt, stroke, fill),
		'scaleh1': createStyle(smallpt, stroke, fill),
		'scalev2': createStyle(smallpt, stroke, fill),
		'scaleh3': createStyle(smallpt, stroke, fill)
	};
	this.drawSketch_();
};

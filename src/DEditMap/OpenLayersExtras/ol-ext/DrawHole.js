var ol = require('openlayers');

require('ol-ext');
// require('ol-ext-css');

////correction pb multipolygon
//https://github.com/Viglino/ol-ext/issues/112
ol.interaction.DrawHole.prototype._startDrawing = function (e) {
    var map = this.getMap();
    var layersFilter = this.layers_;
    this._feature = e.feature;
    var coord = e.feature.getGeometry().getCoordinates()[0][0];
    // Check object under the pointer
    var features = map.getFeaturesAtPixel(
            map.getPixelFromCoordinate(coord), {
        layerFilter: layersFilter
    });
    var current = null;
    if (features) {
        if (features[0].getGeometry().getType().indexOf('Polygon') === -1) {
            current = null;
        } else if (features[0].getGeometry().intersectsCoordinate(coord)) {
            current = features[0];
        } else {
            current = null;
        }
    } else {
        current = null;
    }
    if (!current) {
        this.setActive(false);
        this.setActive(true);
        this._select.getFeatures().clear();
    } else {
        this._select.getFeatures().push(current);
    }
};

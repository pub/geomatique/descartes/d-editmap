var _ = require('lodash');

var messages = {
    Descartes_EDITION_NOT_CONFORM_GEOMETRY: "La géométrie n'est pas conforme.",
    Descartes_EDITION_NOT_CONFORM_TOPO: "La topologie n'est pas conforme : la géométrie intersecte une géométrie existante dans la couche.",
    Descartes_EDITION_NOT_CONFORM_TOPO_SUPPORT: "La topologie n'est pas conforme : la géométrie intersecte une géométrie de la couche ",
    Descartes_EDITION_NOT_CONFORM_APP: "Vérification applicative: la géométrie n'est pas conforme.",
    Descartes_EDITION_NOT_VALID_GEOMETRY: "La géométrie n'est pas valide.",
    Descartes_EDITION_NOT_AVAILABLE_GEOMETRY: "Il n'y a aucune géométrie disponible pour cette action.",
    Descartes_EDITION_NOT_SNAPPING_LAYER_PART_ONE: "La couche avec id ",
    Descartes_EDITION_NOT_SNAPPING_LAYER_PART_TWO: " n'a pas de couche permettant d'utiliser le magnétisme.",
    Descartes_EDITION_NOT_AIDE: "Aucune aide n'est renseignée pour cet outil.",
    Descartes_READ_ERROR_WRONG_GEOMETRY_TYPE: "Une ou plusieurs features n'ont pas pu être créés car elles n'étaient pas du type : ",
    Descartes_ANNOTATIONSLAYER_REMOVE_IMPOSSIBLE: "Cette couche ne peut pas être supprimée car elle est liée aux outils d'annotations.'",
    Descartes_ANNOTATIONSLAYER_GROUP_REMOVE_IMPOSSIBLE: "Ce groupe ne peut pas être supprimé car il contient une couche d'annotations.'",

    Descartes_Messages_UI_LayersTree: {
        ROOT_TITLE: "Contenu de la carte",
        CLOSE_GROUP: "Cliquez pour fermer le groupe",
        OPEN_GROUP: "Cliquez pour ouvrir le groupe",
        HIDE_GROUP: "Cliquez pour masquer les thèmes du groupe",
        SHOW_GROUP: "Cliquez pour afficher les thèmes du groupe",
        HIDE_LAYER: "Cliquez pour masquer le thème",
        SHOW_LAYER: "Cliquez pour afficher le thème",
        STOP_EDIT_LAYER: "Cliquez pour terminer l'édition du thème",
        START_EDIT_LAYER: "Cliquez pour commencer l'édition du thème",
        UNDER_EDITION_LAYER: "Edition en cours",
        NOT_UNDER_EDITION_LAYER: "Edition non active",
        UNAVAILABLE_EDIT_LAYER: "L'édition n'est pas disponible",
        ZOOM_OUT_EDIT_LAYER: "Il faut dézoomer pour pouvoir activer l'édition du thème",
        ZOOM_IN_EDIT_LAYER: "Il est nécessaire de zoomer pour pouvoir activer l'édition du thème",
        INACTIVE_EDIT_LAYER: "Le thème n'est pas visible : Edition impossible",
        MIN_SCALE: "Visible du ",
        MAX_SCALE: " au ",
        MIN_SCALE_ONLY: "Visible à partir du ",
        MAX_SCALE_ONLY: "Visible jusqu\'au ",
        DISABLE_QUERY: "Cliquez pour désactiver le thème lors des interrogations",
        ENABLE_QUERY: "Cliquez pour activer le thème lors des interrogations",
        FORBIDDEN_QUERY: "Le thème n'est pas visible : Interrogations impossibles",
        ACTIVE_SHEET: "Cliquez pour afficher les données du thème",
        INACTIVE_SHEET: "Le thème n'est pas visible : Affichage des données impossible",
        NO_QUERY: "",
        LIEN_METADATA: "Cliquez pour afficher les métadonnées associées dans une nouvelle fenêtre",
        NO_LIEN_METADATA: "Aucune métadonnée sur cette couche",
        LOADING: "Couche en cours de chargement",
        MORE_INFOS: "Plus d'informations",
        MORE_INFO_LEGEND: "Légende"
    },
    Descartes_Messages_UI_LayersTreeSimple: {
        ROOT_TITLE: "Contenu de la carte",
        HIDE_LAYER: "Cliquez pour masquer le thème",
        SHOW_LAYER: "Cliquez pour afficher le thème",
        ZOOM_OUT_EDIT_LAYER: "Il faut dézoomer pour pouvoir activer l'édition du thème",
        ZOOM_IN_EDIT_LAYER: "Il est nécessaire de zoomer pour pouvoir activer l'édition du thème",
        INACTIVE_EDIT_LAYER: "Le thème n'est pas visible : Edition impossible",
        MIN_SCALE: "Visible du ",
        MAX_SCALE: " au ",
        MIN_SCALE_ONLY: "Visible à partir du ",
        MAX_SCALE_ONLY: "Visible jusqu\'au ",
        DISABLE_QUERY: "Cliquez pour désactiver le thème lors des interrogations",
        ENABLE_QUERY: "Cliquez pour activer le thème lors des interrogations",
        FORBIDDEN_QUERY: "Le thème n'est pas visible : Interrogations impossibles",
        NO_QUERY: "",
        MORE_INFOS: "Plus d'informations",
        MORE_INFO_LEGEND: "Légende"
    },

    Descartes_Forms_AttributesEditor: {
        EMPTY_ATTRIBUTES: "Pas d'attributs configurés pour la couche",
        INVITE_LAYER: "Attributs de la couche",
        TITLE_MESSAGE: "Edition des attributs"
    },
    Descartes_Messages_UI_AttributesEditorInPlace: {
        EMPTY_SELECTION: "Pas de sélection",
        BUTTON_MESSAGE: "Créer une géométrie et enregistrer"
    },
    Descartes_Messages_UI_AttributesEditorDialog: {
        SEND_BUTTON: "Envoyer",
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_Action_MapPreview: {
        PREVIEW_MAP_NO_ATTRIBUTES: "Aucun attribut disponible",
        WARNING_NO_GEOMETRY: "Veuillez choisir au moins une géométrie.",
        WARNING_ONLY_ONE_GEOMETRY: "Veuillez choisir une seule géométrie."
    },
    Descartes_Messages_UI_MapPreviewDialog: {
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler",
        TITLE_MESSAGE: "Veuillez selectionner les géométries."
    },
    Descartes_Messages_UI_HomotheticEditorDialog: {
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler",
        BUFFER_LABEL: "Coefficient d'homothétie ",
        X_LABEL: "X ",
        Y_LABEL: "Y ",
        TITLE_MESSAGE: "Veuillez entrer les caractéristiques de l'homothétie",
        CENTER_MESSAGE: "Coordonnées du centre d'homothétie:",
        CLICK_MESSAGE: "Veuillez ensuite cliquer sur la carte pour choisir le centre d'homothétie.",
        FORMAT_BUFFER_ERROR: "Saisissez des valeurs numériques."
    },
    Descartes_Messages_UI_BufferEditorDialog: {
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler",
        BUFFER_LABEL: "Distance (m) ",
        NB_POINTS_LABEL: "Nombre de segments pour créer l'arc de cercle (facultatif)",
        TITLE_MESSAGE: "Veuillez entrer les caractéristiques du buffer",
        FORMAT_BUFFER_ERROR: "Saisissez une valeur numérique pour la distance.",
        FORMAT_NBPOINTS_ERROR: "Saisissez une valeur numérique pour le nombre de points."
    },

    Descartes_Messages_Tool_Edition_Selection: {
        TITLE: "Sélectionner un objet",
        NO_ATTRIBUTES: "Aucun attribut disponible",
        TITLE_AIRE_UNITE: "<sup>2</sup>",
        SHOW_INFO_TITLE: 'Objet sélectionné',
        SHOW_INFO_IDENTIFIER: 'Identifiant',
        SHOW_INFO_AREA: 'Surface',
        SHOW_INFO_PERIMETER: 'Périmètre'
    },
    Descartes_Messages_Tool_Edition_CompositeSelection: {
        TITLE: "Sélectionner une géométrie d'un objet composite"
    },
    Descartes_Messages_Tool_Edition_Attribute: {
        TITLE: "Editer les attributs d'un objet"
    },
    Descartes_Messages_Tool_Edition_AttributeAnnotation: {
        TITLE: "Editer les attributs d'une annotation&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Editer les attributs d'une annotation</br></br>Fonctionnement:</br>1- cliquer pour sélectionner une annotation</br>2- saisir (ou modifier) les attributs dans la boîte de dialogue et cliquer sur 'OK'"
    },
    Descartes_Messages_Tool_Edition_SelectionAttribut: {
        TITLE: "Editer les attributs de la sélection",
        ERROR_NO_SELECT_GEOMETRY: "Aucune géométrie sélectionnée"
    },
    Descartes_Messages_Tool_Edition_Creation_DrawCreation: {
        TITLE: "Tracer une géométrie"
    },
    Descartes_Messages_Tool_Edition_Creation_DrawAnnotation: {
        TITLE: "Créer une annotation",
        TITLE_POINT: " de type point&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        TITLE_LINE: " de type ligne&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        TITLE_POLYGON: " de type polygone&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        TITLE_RECTANGLE: " de type rectangle&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        TITLE_CIRCLE: " de type cercle&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Créer une annotation",
        AIDE_POINT: " de type point</br></br>Fonctionnement:</br>- cliquer sur la carte pour créer un ou plusieurs points",
        AIDE_LINE: " de type ligne</br></br>Fonctionnement:</br>1- cliquer sur la carte pour créer les sommets intermédiaires de la ligne</br>2- double-cliquer pour terminer la création de la ligne</br></br>Compléments (en cours de construction - étape 1):</br>- la touche ECHAP permet d'effacer la constuction</br>- la combinaison CTRL+Z permet d'effacer le dernier sommet créé",
        AIDE_POLYGON: " de type polygone</br></br>Fonctionnement:</br>1- cliquer sur la carte pour créer les sommets intermédiaires du polygone</br>2- double-cliquer pour terminer la création du polygone</br></br>Compléments (en cours de construction - étape 1):</br>- la touche ECHAP permet d'effacer la constuction</br>- la combinaison CTRL+Z permet d'effacer le dernier sommet créé",
        AIDE_RECTANGLE: " de type rectangle</br></br>Fonctionnement:</br>1- cliquer sur la carte pour créer le premier sommet du rectangle</br>2- déplacer la souris</br>3- cliquer une deuxième fois pour créer le sommet opposé du rectangle et finaliser la construction</br></br>Complément (pendant l'étape 2):</br>- la touche ECHAP permet d'effacer la constuction",
        AIDE_CIRCLE: " de type cercle</br></br>Fonctionnement:</br>1- cliquer sur la carte pour définir le centre du cercle</br>2- déplacer la souris selon le rayon voulu</br>3- cliquer une deuxième fois pour finaliser la construction</br></br>Complément (pendant l'étape 2):</br>- la touche ECHAP permet d'effacer la constuction"
    },
    Descartes_Messages_Tool_Edition_Creation_TextAnnotation: {
        TITLE: "Créer une annotation texte&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Créer une annotation texte</br></br>Fonctionnement:</br>1- cliquer sur la carte pour choisir l'emplacement du texte</br>2- renseigner le texte dans la boîte de dialogue et cliquer sur 'OK'",
        PROMPT_MESSAGE: "Veuillez saisir le texte"
    },
    Descartes_Messages_Tool_Edition_Creation_ArrowAnnotation: {
        TITLE: "Créer une annotation de type flèche&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Créer une annotation de type flèche</br></br>Fonctionnement:</br>1- cliquer sur la carte pour créer les sommets intermédiaires de la flèche</br>2- double-cliquer pour terminer la création de la flèche</br></br>Compléments (en cours de construction - étape 1):</br>- la touche ECHAP permet d'effacer la constuction</br>- la combinaison CTRL+Z permet d'effacer le dernier sommet créé"
    },
    Descartes_Messages_Tool_Edition_Creation_FreehandAnnotation: {
        TITLE: "Créer une annotation libre&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Créer une annotation libre</br></br>Fonctionnement:</br>1- cliquer sur la carte et garder le bouton gauche de la souris enfoncé</br>2- déplacer la souris librement </br>3- relâcher le bouton pour finaliser la construction"
    },
    Descartes_Messages_Tool_Edition_Creation_BufferHaloAnnotation: {
        TITLE_DIALOG: "Créer une annotation de type buffer",
        TITLE: "Créer une annotation de type buffer à partir d'une autre annotation&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Créer une annotation de type buffer à partir d'une autre annotation</br></br>Fonctionnement:</br>1- renseigner les éléments de la boîte de dialogue et cliquer sur 'OK'</br>2- sélectionner une ou plusieurs annotations mises en surbrillance</br>3- cliquer sur la carte en dehors des annotations pour créer les buffers</br></br>Compléments (options au niveau de la boîte de dialogue):</br>- la case à cocher 'Agréger les buffers' permet de réunir les buffers qui s'intersectent</br>- la case à cocher 'Supprimer les annotations supports aux buffers' supprime les annotations sélectionnées à l'étape 2",
        TEXTE_WARNING: "<div class='DescartesRemoveGroupWarning'><i class='fa fa-warning'></i>Cet outil ne peut actuellement pas être utilisé car aucune annotation support n'est disponible</div></br>",
        BUFFER_LABEL: "Distance (m) ",
        CHECKBOX_AGGREGATE_LABEL: "Agréger les buffers",
        CHECKBOX_DELETE_ORIG_LABEL: "Supprimer les annotations supports aux buffers"
    },
    Descartes_Messages_Tool_Edition_Creation_CopyCreation: {
        TITLE: "Copier une géométrie",
        CONFIRM: "Etes-vous sûr de vouloir copier cette géométrie ?"
    },
    Descartes_Messages_Tool_Edition_Creation_CloneCreation: {
        TITLE: "Cloner un objet",
        CONFIRM: "Etes-vous sûr de vouloir cloner cet objet ?",
        NO_OBJECT: "Il n'y a aucun objet disponible pour cette action"
    },
    Descartes_Messages_Tool_Edition_Creation_SplitCreation: {
        TITLE: "Scinder par rapport à une géométrie dessinée",
        PREVIEW_MAP_TITLE_CREATION: "Veuillez sélectionner les géométries à conserver pour la création de l'objet.",
        PREVIEW_MAP_TITLE_MODIFICATION: "Veuillez sélectionner les géométries à conserver pour la modification de l'objet",
        WARNING_NO_ONE_GEOMETRY: "Veuillez sélectionner une géométrie"
    },
    Descartes_Messages_Tool_Edition_Creation_DivideCreation: {
        TITLE: "Scinder par rapport à une géométrie existante",
        PREVIEW_MAP_TITLE_CREATION: "Veuillez sélectionner les géométries à conserver pour la création de l'objet.",
        PREVIEW_MAP_TITLE_MODIFICATION: "Veuillez sélectionner les géométries à conserver pour la modification de l'objet",
        WARNING_NO_RESULT: "Pas de resultat possible. Veuillez choisir une autre géométrie",
        WARNING_NO_COMPOSITE: "Impossible de transformer la géométrie de l'objet en géométrie composite."
    },
    Descartes_Messages_Tool_Edition_Creation_BufferHaloCreation: {
        TITLE: "Extension - Buffer de type halo",
        WARNING_NO_GEOMETRY: "Veuillez sélectionner au moins une géométrie.",
        WARNING_MULTIPLE_GEOMETRY: "Le résultat est une géométrie multiple. Veuillez choisir d'autres géométries."
    },
    Descartes_Messages_Tool_Edition_Creation_BufferNormalCreation: {
        TITLE: "Extension - Buffer normal",
        WARNING_NO_GEOMETRY: "Veuillez sélectionner au moins une géométrie.",
        WARNING_MULTIPLE_GEOMETRY: "Le résultat est une géométrie multiple. Veuillez choisir d'autres géométries."
    },
    Descartes_Messages_Tool_Edition_Creation_HomotheticCreation: {
        TITLE: "Extension/Réduction  par transformation homothétique"
    },
    Descartes_Messages_Tool_Edition_Creation_AggregationCreation: {
        TITLE: "Agréger des géométries",
        PREVIEW_MAP_TITLE_CREATION: "Résultat de l'agrégation : confirmez-vous la création de cet objet ?",
        PREVIEW_MAP_TITLE_MODIFICATION: "Résultat de l'agrégation : êtes vous sûr de vouloir poursuivre la modification ?",
        WARNING_AT_LEAST_TWO_GEOMETRY: "Veuillez selectionner au moins deux géométries.",
        WARNING_MULTIPLE_GEOMETRY: "Le résultat est une géométrie multiple. Veuillez choisir une autre géométrie.",
        WARNING_INTERSECT_GEOMETRY: "Veuillez choisir des géométries s'intersectant.",
        WARNING_UPDATE_INTO_MULTIPLE_GEOMETRY: "Impossible de modifier la géométrie de l'objet en géométrie multiple.",
        ERROR_ALTER_GEOMETRY: "La géométrie à modifier n'est plus sélectionnée."
    },
    Descartes_Messages_Tool_Edition_Creation_UnAggregationCreation: {
        TITLE: "Désagréger"
    },
    Descartes_Messages_Tool_Edition_Deletion_RubberDeletion: {
        TITLE: "Effacer un objet"
    },
    Descartes_Messages_Tool_Edition_Deletion_RubberAnnotation: {
        TITLE: "Effacer une annotation&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Effacer une annotation</br></br>Fonctionnement:</br>- cliquer directement sur l'annotation à supprimer"
    },
    Descartes_Messages_Tool_Edition_Deletion_EraseAnnotation: {
        TITLE: "Effacer toutes les annotations&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Effacer toutes les annotations</br></br>Fonctionnement:</br>- cliquer sur le bouton 'supprimer' dans la boîte de dialogue pour confirmer la suppression de toutes les annotations",
        DIALOG_TITLE: "Suppression des annotations",
        DIALOG_MSG: "Confirmez-vous la suppression de toutes les annotations?"
    },
    Descartes_Messages_Tool_Edition_Deletion_CompositeRubberDeletion: {
        TITLE: "Effacer une géométrie d'un objet composite"
    },
    Descartes_Messages_Tool_Edition_Deletion_SelectionDeletion: {
        TITLE: "Supprimer la sélection",
        ERROR_NO_SELECT_GEOMETRY: "Aucune géométrie sélectionnée"
    },
    Descartes_Messages_Tool_Edition_Modification_GlobalModification: {
        TITLE: "Modifier un objet"
    },
    Descartes_Messages_Tool_Edition_Modification_GlobalModificationAnnotation: {
        TITLE: "Modifier une annotation&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Modifier une annotation</br></br>Fonctionnement:</br>1- cliquer sur l'annotation à modifier</br>(elle est mise en surbrillance -bleu- et des puces noires apparaissent)</br>2- cliquer sur une des puces (au survol le pointer change) et garder le bouton gauche de la souris enfoncé</br>3- déplacer la souris puis relâcher le bouton</br>4- cliquer en dehors de l'annotation pour finaliser la modification</br></br>Compléments (puces):</br>- la puce centrale permet de déplacer l'annotation</br>- les puces externes permettent de redimensionner l'annotation</br>- la puce ronde en bas à droite permet de faire pivoter l'annotation"
    },
    Descartes_Messages_Tool_Edition_Modification_CompositeGlobalModification: {
        TITLE: "Modifier une géométrie d'un objet composite"
    },
    Descartes_Messages_Tool_Edition_Modification_VerticeModification: {
        TITLE: "Modifier les sommets d'un objet"
    },
    Descartes_Messages_Tool_Edition_Modification_VerticeModificationAnnotation: {
        TITLE: "Modifier les sommets d'une annotation&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Modifier les sommets d'une annotation</br></br>Fonctionnement:</br></br>Pour déplacer les sommets existants:</br>1- cliquer sur l'annotation à modifier</br>(elle est mise en surbrillance -bleu- et des croix noires apparaissent sur les sommets existants)</br>2- cliquer sur une des croix et garder le bouton gauche de la souris enfoncé</br>3- déplacer la souris puis relâcher le bouton</br>4- cliquer en dehors de l'annotation pour finaliser la modification</br></br>Pour ajouter des sommets:</br>1- cliquer sur l'annotation à modifier</br>2- positionner le curseur de la souris sur un des côtés de l'annotation</br>(une croix noire apparaît)</br>3- cliquer sur cette croix et garder le bouton gauche de la souris enfoncé</br>4- déplacer la souris puis relâcher le bouton pour ajouter le sommet</br>5- cliquer en dehors de l'annotation pour finaliser la modification</br></br>Pour supprimer des sommets:</br>1- cliquer sur l'annotation à modifier</br>(elle est mise en surbrillance -bleu- et des croix noires apparaissent sur les sommets existants)</br>2- cliquer sur une des croix pour supprimer le sommet associé</br>3- cliquer en dehors de l'annotation pour finaliser la modification"
    },
    Descartes_Messages_Tool_Edition_Modification_AddTextAnnotation: {
        TITLE: "Ajouter un texte à une annotation&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Ajouter un texte à une annotation</br></br>Fonctionnement:</br>1- cliquer pour sélectionner une annotation</br>2- renseigner le texte dans la boîte de dialogue et cliquer sur 'OK'",
        PROMPT_MESSAGE: "Veuillez saisir le texte",
        FEATURE_STYLE_ERROR: "Erreur: Impossible d'ajouter un texte car une fonction de style est utilisée pour cet objet."
    },
    Descartes_Messages_Tool_Edition_Modification_StyleAnnotation: {
        TITLE: "Modifier le style des annotations&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Modifier le style des annotations</br></br>Fonctionnement:</br></br>Pour modifier le style des futures annotations qui seront créées</br>1- cliquer sur l'outil (le bouton s'active)</br>2- configurer le style dans la boîte de dialogue et cliquer sur 'OK'</br>(le bouton est encore en position activé)</br>3- cliquer sur l'outil</br>(le bouton se désactive et les futures annotations auront le style défini)</br></br>Pour modifier le style d'une sélection d'annotations existantes:</br>1- cliquer sur l'outil (le bouton s'active)</br>2- configurer le style dans la boîte de dialogue et cliquer sur 'OK'</br>(le bouton est encore en position activé)</br>3- cliquer sur les annotations une à une pour changer le style (tant que le bouton est en position activé)</br></br>Pour modifier le style de toutes les annotations existantes:</br>1- cliquer sur l'outil (le bouton s'active)</br>2- configurer le style et cocher la case 'Appliquer le style à toutes les annotations' dans la boîte de dialogue et cliquer sur 'OK'</br>(le style de toutes les annotations est directement modifié)",
        TITLE_DIALOG: "Modifier le style des annotations"
    },
    Descartes_Messages_Tool_Edition_ExportAnnotation: {
        TITLE: "Exporter les annotations&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Exporter les annotations</br></br>Fonctionnement:</br>- sélectionner le format d'export dans la boîte de dialogue et cliquer sur 'OK'",
        DIALOG_TITLE_EXPORT_FORMAT: "Exporter les annotations"
    },
    Descartes_Messages_Tool_Edition_ImportAnnotation: {
        TITLE: "Importer des annotations&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Importer des annotations</br></br>Fonctionnement:</br>1- sélectionner le format d'import dans la boîte de dialogue</br>2- sélectionner le fichier à importer</br>3- cliquer sur le bouton 'OK'</br></br>Compléments:</br>- par défaut, les annotations déjà existantes dans la carte sont écrasées</br>- pour les conserver, cocher la case 'Importer en conservant les annotations déjà existantes'",
        TITLE_DIALOG: "Importer des annotations",
        FORMAT: "Sélectionner le format d'import :"
    },
    Descartes_Messages_Tool_Edition_AideAnnotation: {
        TITLE: "Aide à l'utilisation des outils d'annotations&#x0A;&#x0A;Fonctionnement:&#x0A;1- cliquer pour ouvrir la fenêtre d'aide à l'utilisation&#x0A;2- cliquer sur un outil d'annotations pour afficher l'aide associée dans la fenêtre",
        TITLE_DIALOG: "Aide à l'utilisation des outils d'annotations",
        TEXTE_DIALOG: "Cliquer sur un outil d'annotations pour afficher l'aide associée"
    },
    Descartes_Messages_Tool_Edition_Creation_GeolocationSimpleAnnotation: {
        TITLE: "Création d'une annotations relative à ma position courante&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Création d'une annotations relative à ma position courante</br></br>Fonctionnement:</br>1- cliquer sur l'outil (le navigateur demande de valider l'autorisation d'accès à la localisation)</br>2- Accepter la demande (la recherche de la position se lance)"
    },
    Descartes_Messages_Tool_Edition_Creation_GeolocationTrackingAnnotation: {
        TITLE: "Création d'annotations avec la trace de mes positions courantes&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Création d'annotations avec la trace de mes positions courantes</br></br>Fonctionnement:</br>1- cliquer sur l'outil (le navigateur demande de valider l'autorisation d'accès à la localisation)</br>2- Accepter la demande (la recherche des positions se lance)</br>3- cliquer sur l'outil pour arrêter la recherche des positions"
    },
    Descartes_Messages_Tool_Edition_SnappingAnnotation: {
        TITLE: "Configurer l'accroche des annotations&#x0A;&#x0A;Pour plus de détails, activer l'outil 'Aide à l'utilisation des outils d'annotations'",
        AIDE: "Configurer l'accroche des annotations</br></br>Fonctionnement:</br>- cocher les couches d'accroche dans la boîte de dialogue et cliquer sur 'OK'</br>(les couches sélectionnées apparaîtront dans l'arbre des couches avec un picto 'aimant' qui s'activera selon les outils utilisés)</br></br>Compléments:</br>- les couches proposées sont uniquement les couches ayant un service WFS associé</br>- les pictos 'aimant' peuvent être désactivés/réactivés directement par clic dans l'arbre des couches lorsqu'un outil utilise l'accroche</br>- la case à cocher 'Activer le tracé automatique' permet de sélectionner automatiquement des sommets intermédiaires</br>(un picto 'eclair' apparaîtra au niveau de l'arbre des couches pour les couches concernées)",
        TITLE_DIALOG: "Configurer l'accroche des annotations"
    },
    Descartes_Messages_Tool_Edition_Modification_MergeModification: {
        TITLE: "Fusionner des objets",
        PREVIEW_MAP_TITLE_MESSAGE: "Veuillez choisir l'objet à conserver.",
        CONFIRM_MERGE: "Etes-vous sûr de vouloir fusionner ces objets ?",
        CONFIRM_MERGE_RESULT: "Résultat de la fusion : &#x0A;êtes vous sûr de vouloir poursuivre la modification ?",
        WARNING_AT_LEAST_TWO_GEOM: "Veuillez selectionner au moins deux géométrie.",
        WARNING_MULTI_GEOM_RESULT: "Le résultat est une géométrie multiple. Veuillez choisir une autre géométrie.",
        WARNING_NO_GEOM_RESULT: "Le résultat n'est pas une géométrie. Veuillez choisir une autre géométrie.",
        WARNING_NO_INTERSECTION: "Veuillez choisir des objets qui s'intersectent."
    },
    Descartes_Messages_Tool_Edition_Modification_SelectionSubstractModification: {
        TITLE: "Soustraire une géométrie",
        PREVIEW_MAP_TITLE_MESSAGE: "Le résultat de la soustraction est le suivant. Etes-vous sûr de vouloir poursuivre la modification ?",
        WARNING_MULTIPLE_GEOMETRY: "Le résultat est une géométrie multiple. Veuillez choisir une autre géométrie.",
        WARNING_NO_GEOMETRY: "Veuillez selectionner au moins une géométrie.",
        ERROR_NO_SELECT_GEOMETRY: "Aucune géométrie sélectionnée"
    },
    Descartes_Messages_Tool_Edition_Save: {
        TITLE: "Sauvegarder les modifications"
    },
    Descartes_Messages_Tool_Edition_Information_IntersectInformation: {
        TITLE: "Informations sur l'intersection des objets",
        TITLE_OBJET_SELECT: 'Objet sélectionné: ',
        INTERSECT_PREFIX: 'Cet objet intersecte ',
        INTERSECT_SUFFIX: ' objet(s)',
        IDENTIFIER: 'Identifiant',
        AREA: 'Surface',
        PERIMETER: 'Périmètre',
        NO_ATTRIBUTES: "Aucun attribut disponible",
        TITLE_AIRE_UNITE: "<sup>2</sup>"
    }
};

messages.Descartes_Messages_UI_AttributesEditorInPlace = _.extend(messages.Descartes_Messages_UI_AttributesEditorInPlace, messages.Descartes_Forms_AttributesEditor);
messages.Descartes_Messages_UI_AttributesEditorDialog = _.extend(messages.Descartes_Messages_UI_AttributesEditorDialog, messages.Descartes_Forms_AttributesEditor);

module.exports = messages;


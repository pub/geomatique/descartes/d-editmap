/* global Descartes*/

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;

var Vector = require('./Vector');
var GenericVector = require('./GenericVector');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../EditionLayerConstants');
var Symbolizers = require('../../Symbolizers');
var EditionManager = require('../../Core/EditionManager');
var log = require('loglevel');

/**
 * Class: Descartes.Layer.EditionLayer.GenericVector
 * Objet "métier" correspondant à une couche d'édition pour le contenu de la carte, basée sur des *ressources génériques* (alimentation manuelle).
 *
 * Hérite de:
 * - <Descartes.Layer.EditionLayer.Vector>
 */
var Class = Utils.Class(GenericVector, {

	_loaderWithRemoveFeatures: true,

     annotationsFeatures: [],

     autoHide: false,

    /**
     * Constructeur: Descartes.EditionLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WFS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = EditionLayerConstants.TYPE_Annotations;
        if (_.isNil(options)) {
             options = {};
        }
        options.geometryType = LayerConstants.GENERIC_GEOMETRY;
        options.halo = {
            enable: true
        };
        if (_.isNil(options.id)) {
            options.id = "dAnnotationsLayer";
        }
        if (_.isNil(options.attributes) || (!_.isNil(options.attributes) && _.isNil(options.attributes.attributesEditable))) {
            options.attributes = {
                attributesEditable: [
                    {fieldName: 'nom', label: 'Nom'},
                    {fieldName: 'description', label: 'Description'}
                ],
                attributesAlias: [
                    {fieldName: 'nom', label: 'Nom'},
                    {fieldName: 'description', label: 'Description'}
                ]
            };
        }
        if (_.isNil(options.autoHide)) {
            options.autoHide = true;
        }
        if (Descartes.EditionManager.globalEditionMode) {
            options.autoHide = false;
        }

        this.symbolizers = {
            'default': _.extend({}, Symbolizers.Descartes_Symbolizers_DefaultEdition_Annotations),
            'select': _.extend({}, Symbolizers.Descartes_Symbolizers_SelectionEdition_Annotations),
            'create': _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_Annotations),
            'temporary': _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_Annotations),
            'support': _.extend({}, Symbolizers.Descartes_Symbolizers_SupportEdition_Annotations),
            'modify': _.extend({}, Symbolizers.Descartes_Symbolizers_ModifyEdition_Annotations),
            'delete': _.extend({}, Symbolizers.Descartes_Symbolizers_DeletionEdition_Annotations),
            'intersect': _.extend({}, Symbolizers.Descartes_Symbolizers_IntersectEdition_Annotations),
            'autotracing': _.extend({}, Symbolizers.Descartes_Symbolizers_AutoTracingEdition_Annotations)
        };
        //if (_.isNil(layersDefinition)) {
            var that = this;
            layersDefinition = [{
                 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
                     that.displayIconSupport = false;
                     this.clear();
                     if (that.annotationsFeatures === null) {
                          that.annotationsFeatures = [];
                     }
                      this.addFeatures(that.getAnnotationsFeatures());
                      _.each(this.getFeatures(), function (feature) {
                           if (feature.get("dAnnotationType") && (feature.get("dAnnotationType") === "drawAnnotation" || feature.get("dAnnotationType") === "drawAndTexteAnnotation" || feature.get("dAnnotationType") === "importAnnotation") && feature.getGeometry() && feature.getGeometry().getType() === "Point") {
                               var typeStyle = typeof feature.getStyle();
                               var featureStyle = feature.getStyle();
                               var style = null;
                               if (featureStyle && !(featureStyle instanceof Array) && typeStyle !== "function") {
                                  style = that.checkFeatureStyle(featureStyle, feature.get("dAnnotationType"));
                               } else if (featureStyle && featureStyle instanceof Array && typeStyle !== "function") {
                                  style = [];
                                  for (var i = 0, len = featureStyle.length; i < len; i++) {
                                    if (typeof featureStyle[i] !== "function") {
                                      var styl = that.checkFeatureStyle(featureStyle[i], feature.get("dAnnotationType"));
                                      style.push(styl);
                                   }
                                 }
                               } else {
                                  var olStyles = Symbolizers.getOlStyle(that.symbolizers['default']);
                                  style = olStyles[feature.getGeometry().getType()];
                               }
                               feature.setStyle(style);
                           }
                           /*if (!feature.get("dAnnotationType")) {
                                feature.setStyle(null);
                           }*/
                      });

                      /*if (that.autoHide && this.getFeatures().length === 0) {
                         that.hide = true;
                      } else {
                         that.hide = false;
                      }*/
                      that.setAutoHide();
                 }
            }];
        //}

        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);

        this.queryable = false;
        this.activeToQuery = false;
        this.sheetable = false;
        this.displayIconSupport = false;

        if (this.annotationsSerialized) {
           this.setAnnotationsFeaturesSerialized(this.annotationsSerialized, false);
        }

        /*if (this.annotationsSerializedKML) {
           var optionsFormat = {
               extractStyles: true,
               showPointNames: false,
               //defaultStyle: [],
               defaultDataProjection: 'EPSG:4326' //flux kml toujours en EPSG:4326
           };

           var features = new ol.format.KML(optionsFormat).readFeatures(this.annotationsSerializedKML, {
               dataProjection: 'EPSG:4326',
               featureProjection: 'EPSG:4326'
          });
          features.forEach(function (feature) {
              if (feature.get("dAnnotationType") === "arrowAnnotation" || feature.get("dAnnotationType") === "texteAnnotation" || feature.get("labelTextAnnotion")) {
                  that.refreshFeatureStyle(feature);
              }
          });
          this.setAnnotationsFeatures(features);
        }*/

        this.setAutoHide();

        Descartes.AnnotationsLayer = this;

    },
    checkFeature: function (feature) {
        if (_.isNil(feature.getId())) {
             feature.setId('dAnnotObjectId_' + Math.random().toString(36).slice(2));
        }
        if (!_.isNil(feature.getStyle()) && typeof feature.getStyle() === 'function') {
             var style = feature.getStyle().bind(feature)(feature);
             if (style instanceof Array) {
                for (var j = 0, len = style.length; j < len; j++) {
                   if (style[j].getText && style[j].getText()) {
                      if (feature.get("labelTextAnnotion")) {
                         style[j].getText().setText(feature.get("labelTextAnnotion"));
                      } else {
                         style[j].getText().setFont('20px sans-serif');
                         style[j].getText().getFill().setColor("#000000");
                         style[j].getText().setScale(2);
                      }
                   }
                }
             }
             feature.setStyle(style);
        }
        if (_.isNil(feature.getStyle()) && feature.get("labelTextAnnotion")) {
             var olStyles = Symbolizers.getOlStyle(this.symbolizers["default"]);
             var olStyle = olStyles[feature.getGeometry().getType()];
             var styleText = this.getTextStyle(feature);
             olStyle.setText(styleText.getText());
             feature.setStyle(olStyle);
        }
	},
	checkFeatureStyle: function (featureStyle, type) {
          var style = null;
          var fillColor = "#000000";
          var strokeColor = "#000000";
          var strokeWidth = 2;
          if (featureStyle.getImage && featureStyle.getImage() && featureStyle.getImage().getFill && featureStyle.getImage().getFill()) {
             fillColor = featureStyle.getImage().getFill().getColor();
          }
          if(featureStyle.getImage && featureStyle.getImage() && featureStyle.getImage().getStroke && featureStyle.getImage().getStroke()) {
             strokeColor = featureStyle.getImage().getStroke().getColor();
             strokeWidth = featureStyle.getImage().getStroke().getWidth();
          }
          style = new ol.style.Style({
             image: new ol.style.Circle({
                fill: new ol.style.Fill({
                   color: fillColor
                }),
                stroke: new ol.style.Stroke({
                    color: strokeColor,
                    width: strokeWidth
                }),
                radius: 6
            })
          });
          if (type === "drawAndTexteAnnotation") {
              style.setText(featureStyle.getText());
          }
          return style;
	},
    setAnnotationsFeatures: function (features) {
        for (var i = 0, len = features.length; i < len; i++) {
            this.checkFeature(features[i]);
        }
        this.annotationsFeatures = features;
	},
    getAnnotationsFeatures: function () {
        return this.annotationsFeatures;
	},
    addAnnotationsFeatures: function (features) {
        for (var i = 0, len = features.length; i < len; i++) {
            this.checkFeature(features[i]);
            if (this.checkPresentFeatureId(features[i].getId())) {
                 features[i].setId(features[i].getId() + "_" + Math.random().toString(36).slice(2).substring(4));
            }
        }
        this.annotationsFeatures = this.annotationsFeatures.concat(features);
	},
    setAnnotationsFeaturesSerialized: function (featuresSerialized, keepAnnotations) {
           if (!keepAnnotations) {
               this.annotationsFeatures = [];
           }
           var that = this;
           featuresSerialized.forEach(function (annot) {
             var geom = null;
             if (annot.geometry && annot.geometry.type === "Point") {
                 geom = new ol.geom.Point(annot.geometry.coordinates);
             } else if (annot.geometry && annot.geometry.type === "LineString") {
                 geom = new ol.geom.LineString(annot.geometry.coordinates);
             } else if (annot.geometry && annot.geometry.type === "Polygon") {
                 geom = new ol.geom.Polygon(annot.geometry.coordinates);
             }
             var style = new ol.style.Style({
                   fill: new ol.style.Fill({
                       color: annot.style.fillColor
                   }),
                   stroke: new ol.style.Stroke({
                       color: annot.style.strokeColor,
                       width: annot.style.strokeWidth
                   })
             });
             if (annot.geometry && annot.geometry.type === "Point" && annot.properties && (annot.properties.dAnnotationType === "drawAnnotation" || annot.properties.dAnnotationType === "drawAndTexteAnnotation")) {
                style = new ol.style.Style({
                   image: new ol.style.Circle({
                       fill: new ol.style.Fill({
                           color: annot.style.fillColor
                       }),
                       stroke: new ol.style.Stroke({
                           color: annot.style.strokeColor,
                           width: annot.style.strokeWidth
                       }),
                       radius: 6
                   })
                });
             }
             if (annot.properties && (annot.properties.dAnnotationType === "texteAnnotation" || annot.properties.dAnnotationType === "drawAndTexteAnnotation")) {
                var text = new ol.style.Text({
                     fill: new ol.style.Fill({
                         color: annot.style.textFillColor
                     }),
                     text: annot.properties.labelTextAnnotion,
                     scale: annot.style.textScale
                });
                style.setText(text);
             }
             var myfeature = new ol.Feature({
               geometry: geom
             });
             if (!_.isNil(annot.id)) {
                 myfeature.setId(annot.id);
             } else {
                 myfeature.setId('dAnnotObjectId_' + Math.random().toString(36).slice(2));
             }
             myfeature.setStyle(style);
             myfeature.setProperties(annot.properties);

             if (myfeature.get("dAnnotationType") === "arrowAnnotation" || myfeature.get("dAnnotationType") === "texteAnnotation" || myfeature.get("labelTextAnnotion")) {
                 that.refreshFeatureStyle(myfeature);
             }
             if (that.checkPresentFeatureId(myfeature.getId())) {
                 myfeature.setId(myfeature.getId() + "_" + Math.random().toString(36).slice(2).substring(4));
             }
             that.annotationsFeatures.push(myfeature);
           });
	},
    checkPresentFeatureId: function (featureId) {
          var isPresent = false;
          for (var i = 0, len = this.annotationsFeatures.length; i < len; i++) {
              if (this.annotationsFeatures[i].getId() === featureId) {
                  isPresent = true;
                  break;
              }
          }
          return isPresent;
	},
    setAutoHide: function () {
        if (this.autoHide && this.getAnnotationsFeatures() && this.getAnnotationsFeatures().length === 0) {
            this.hide = true;
        } else if (this.autoHide) {
            this.hide = false;
        }
	},
    refreshFeatureStyle: function (feature) {
       if (feature.get("dAnnotationType") === "arrowAnnotation" || feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
            var color = '#000000';
            var width = 2;
            var styl = null;
            if (feature.getStyle() instanceof Array) {
                styl = feature.getStyle()[0];
            } else {
                styl = feature.getStyle();
            }
            if (styl && styl.getStroke()) {
                color = styl.getStroke().getColor();
                width = styl.getStroke().getWidth();
            }
            var geometry = feature.getGeometry();
            var styles = [
               new ol.style.Style({
                   stroke: new ol.style.Stroke({
                       color: color,
                       width: width
                   })
               })
            ];
            /*geometry.forEachSegment(function (start, end) {
                var dx = end[0] - start[0];
                var dy = end[1] - start[1];
                var rotation = Math.atan2(dy, dx);

                var lineStr1 = new ol.geom.LineString([end, [end[0] - 0.5, end[1] + 0.5]]);
                lineStr1.rotate(rotation, end);
                var lineStr2 = new ol.geom.LineString([end, [end[0] - 0.5, end[1] - 0.5]]);
                lineStr2.rotate(rotation, end);

                var stroke = new ol.style.Stroke({
                    color: 'green',
                    width: 1
                });

                styles.push(new ol.style.Style({
                    geometry: lineStr1,
                    stroke: stroke
                }));
                styles.push(new ol.style.Style({
                    geometry: lineStr2,
                    stroke: stroke
                }));
            });*/
            /*geometry.forEachSegment(function (start, end) {
                var dx = end[0] - start[0];
                var dy = end[1] - start[1];
                var rotation = Math.atan2(dy, dx);
                // arrows
                styles.push(
                    new ol.style.Style({
                        geometry: new ol.geom.Point(end),
                        image: new ol.style.Icon({
                            src: 'arrow.png',
                            anchor: [0.75, 0.5],
                            rotateWithView: true,
                            rotation: -rotation
                        })
                    })
                );
            });*/
            var coordinates = geometry.getCoordinates();
            var dx = coordinates[coordinates.length - 1][0] - coordinates[coordinates.length - 2][0];
            var dy = coordinates[coordinates.length - 1][1] - coordinates[coordinates.length - 2][1];
            var rotation = Math.atan2(dy, dx);
            // arrows
            styles.push(
                new ol.style.Style({
                    geometry: new ol.geom.Point(coordinates[coordinates.length - 1]),
                    image: new ol.style.Icon({
                       src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAATCAYAAACk9eypAAAAiUlEQVQokWNgYGBg+H/R0JmBgeH//xd63AyEwP8zxm4MDAz/Yfj/ET9egnrQ8f8zxvy4Vf8PZcOq6ZKuID5nsWLVdNJZGLem/Q0sWDXd0RPDrWlVKDNWTVdNJfD4iYEJq6ab+tL4NDFi1XTKTpZkmyi3gSQ/kBRKJMUDSTFNTloiMbWSkR9IynEA8kSc9pvbGQ4AAAAASUVORK5CYII=',
                       anchor: [0.75, 0.5],
                       rotateWithView: true,
                       rotation: -rotation
                    })
                })
            );
            feature.setStyle(styles);
      }

     /* if (feature.get("dAnnotationType") === "texteAnnotation") {
        var style = this.getTextStyle(feature);
        feature.setStyle(style);
      }*/
      if (feature.get("labelTextAnnotion")) {
        var styleFeature = feature.getStyle();
        var styleText = this.getTextStyle(feature);
        if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
            styleFeature[0].setText(styleText.getText());
        } else{
            styleFeature.setText(styleText.getText());
        }
        feature.setStyle(styleFeature);
      }
	},
    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde
     * d'un contexte de consultation.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour: {Object} Objet JSON.
     */
    serialize: function () {
        var annotations = this.serializeAnnotations(this.annotationsFeatures);

        /*var optionsFormat = {};
        //if (selectedFormat === "kml") {
            optionsFormat.featureProjection = this.OL_layers[0].map.getView().getProjection().getCode();
            optionsFormat.defaultDataProjection = 'EPSG:4326'; //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
        //}
        var format = new ol.format.KML();
        var features = this.OL_layers[0].getSource().getFeatures();
        var flux = format.writeFeatures(features, optionsFormat);
        */
        var json = GenericVector.prototype.serialize.apply(this);
        json.definition = [{}];
        json.options.autoHide = this.autoHide;
        json.options.annotationsSerialized = annotations;
        //json.options.annotationsSerializedKML = flux;
        return json;
    },
    serializeAnnotations: function (annotations) {
        var annotationsSerialized = [];
        var that = this;
        annotations.forEach(function (annot) {
             var properties = annot.getProperties();
             delete properties.geometry;
             delete properties.selected;
             delete properties.state;
             var geometry = annot.getGeometry();
             var fillColor = null;
             var strokeColor = null;
             var strokeWidth = null;
             var textFillColor = null;
             var textScale = null;
             var style = annot.getStyle();
             var typeStyle = typeof style;
             if (style && typeStyle !== "function") {
                 if (style instanceof Array) {
                    style = style[0];
                 }
             } else {
                 var olStyles = Symbolizers.getOlStyle(that.symbolizers['default']);
                 style = olStyles[geometry.getType()];
             }
             if (style && style.getFill()) {
                 fillColor = style.getFill().getColor();
             }
             if (style && style.getStroke()) {
                 strokeColor = style.getStroke().getColor();
                 strokeWidth = style.getStroke().getWidth();
             }
             if (style && style.getImage()) {
                 fillColor = style.getImage().getFill().getColor();
                 strokeColor = style.getImage().getStroke().getColor();
                 strokeWidth = style.getImage().getStroke().getWidth();
             }
             if (style && style.getText()) {
                 textFillColor = style.getText().getFill().getColor();
                 textScale = style.getText().getScale();
             }
             var myannot = {
                id: annot.getId(),
                properties: properties,
                geometry: {
                   type: geometry.getType(),
                   coordinates: geometry.getCoordinates()
                },
                style: {
                   fillColor: fillColor,
                   strokeColor: strokeColor,
                   strokeWidth: strokeWidth,
                   textFillColor: textFillColor,
                   textScale: textScale
                }
             };
             annotationsSerialized.push(myannot);
        });
        return annotationsSerialized;
    },
    getTextStyle: function (feature) {
      var color = '#000000';
      var scale = 2;
      var style = null;
      if (feature.getStyle() instanceof Array) {
           style = feature.getStyle()[0];
      } else {
           style = feature.getStyle();
      }
      if (style && style.getText && style.getText() && style.getText().getFill()) {
          color = style.getText().getFill().getColor();
          scale = style.getText().getScale();
      }
      var text = feature.get("labelTextAnnotion");
      var offsetX = 0;
      var offsetY = 0;
      if (feature.getGeometry().getType() === "Point" && feature.get("dAnnotationType") === "drawAndTexteAnnotation") {
        offsetY = 12;
      }
      var textStyle = new ol.style.Style({
         text: new ol.style.Text({
            text: text,
            overflow: true,
            //font: 'normal 50px',
            scale: scale,
            /*stroke: new ol.style.Stroke({
              color: '#FFFF99',
              width: 5
            }),*/
            fill: new ol.style.Fill({color: color}),
            offsetX: offsetX,
            offsetY: offsetY
        })/*,
        image: new ol.style.Icon({
            src: 'marker2.png',
            anchorOrigin: "bottom-right"
        })*/
        /*image: new ol.style.Circle({
          radius: 5,
          fill: new ol.style.Fill({
           color: 'red'
          })
        })*/
      });
      return textStyle;
    },
    getGeolocationPositionFeatureStyle: function () {
        var style = new ol.style.Style({
            text: new ol.style.Text({
                    text: '\uf041', // fa-map-marker
                    font: 'normal 24px FontAwesome',
                    fill: new ol.style.Fill({color: '#3399CC'})
            })
        });
        return style;
    },
    getGeolocationAccuracyFeatureStyle: function () {
        var style = new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: '#3399CC',
            width: 2
          })
        });
        return style;
    },
    CLASS_NAME: 'Descartes.Layer.EditionLayer.Annotations'
});

module.exports = Class;

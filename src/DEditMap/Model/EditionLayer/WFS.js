/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;

var Vector = require('./Vector');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../EditionLayerConstants');
var Symbolizers = require('../../Symbolizers');
var EditionManager = require('../../Core/EditionManager');
var log = require('loglevel');

/**
 * Class: Descartes.Layer.EditionLayer.WFS
 * Objet "métier" correspondant à une couche d'édition pour le contenu de la carte, basée sur des *ressources WFS*.
 *
 * Hérite de:
 * - <Descartes.Layer.EditionLayer.Vector>
 */
var Class = Utils.Class(Vector, {

    /**
     * Constructeur: Descartes.EditionLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WFS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = EditionLayerConstants.TYPE_WFS;
        this.symbolizers = {
            'default': _.extend({}, Symbolizers.Descartes_Symbolizers_DefaultEdition_WFS),
            'select': _.extend({}, Symbolizers.Descartes_Symbolizers_SelectionEdition_WFS),
            'create': _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_WFS),
            'temporary': _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_WFS),
            'support': _.extend({}, Symbolizers.Descartes_Symbolizers_SupportEdition_WFS),
            'modify': _.extend({}, Symbolizers.Descartes_Symbolizers_ModifyEdition_WFS),
            'delete': _.extend({}, Symbolizers.Descartes_Symbolizers_DeletionEdition_WFS),
            'intersect': _.extend({}, Symbolizers.Descartes_Symbolizers_IntersectEdition_WFS),
            'autotracing': _.extend({}, Symbolizers.Descartes_Symbolizers_AutoTracingEdition_WFS)
        };
        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },
    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche WFS.
     *
     * Retour:
     * {OpenLayers.Layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams();
        params.layerName = layerDefinition.layerName;

        var version = '1.1.0';
        if (!_.isNil(layerDefinition.serverVersion)) {
            version = layerDefinition.serverVersion;
        }
        var gmlFormat = new ol.format.GML3();
        if (version === '1.0.0') {
            gmlFormat = new ol.format.GML2();
        }
        this.olFormat = new ol.format.WFS({
            featureNS: layerDefinition.featureNameSpace,
            gmlFormat: gmlFormat
        });
        this.olFormat.wfsVersion = version;

        if (!_.isNil(layerDefinition.internalProjection)) {
            this.olFormat.defaultDataProjection = layerDefinition.internalProjection;
        }

        //this.url = this.getUrl(Utils.adaptationUrl(layerDefinition.serverUrl));
        this.url = Utils.adaptationUrl(layerDefinition.serverUrl);
        var saveStrategy = EditionManager.initSaveStrategy(layerDefinition);

        var loader = null;
        if (_.isNil(layerDefinition.featureLoaderMode) || layerDefinition.featureLoaderMode === LayerConstants.GET_REQUEST_LOADER) {
            var url = function (extent, resolution, projection) {

                var finalBbox = extent;
                var finalSrsName = projection.getCode();
                if (!_.isNil(layerDefinition.internalProjection)) {
                    finalBbox = ol.proj.transformExtent(extent, projection, layerDefinition.internalProjection);
                    finalSrsName = layerDefinition.internalProjection;
                }

                if (layerDefinition.featureReverseAxisOrientation) {
                    finalBbox = [finalBbox[1], finalBbox[0], finalBbox[3], finalBbox[2]];
                }

                finalBbox = finalBbox.join(',');

                if (layerDefinition.useBboxSrsProjection) {
                    finalBbox += ',' + finalSrsName;
                }

                var typeName = layerDefinition.layerName;
                if (layerDefinition.featurePrefix) {
                    typeName = layerDefinition.featurePrefix + ':' + layerDefinition.layerName;
                }

                var labelTypeName = 'TYPENAME=';
                if (version === '2.0.0') {
                    labelTypeName = 'TYPENAMES=';
                }

                //var finalUrl = Utils.adaptationUrl(this.url);
                var finalUrl = this.url;
                finalUrl += 'SERVICE=WFS';
                finalUrl += '&VERSION=' + version;
                finalUrl += '&REQUEST=GetFeature';
                finalUrl += '&' + labelTypeName + typeName;
                finalUrl += '&SRSNAME=' + finalSrsName;
                finalUrl += '&BBOX=' + finalBbox;

                if (layerDefinition.displayMaxFeatures) {
                    finalUrl += '&MAXFEATURES=' + layerDefinition.displayMaxFeatures;
                }
                if (version === '2.0.0') {//force gml version cause ol 4 not support gml32
                    finalUrl += '&OUTPUTFORMAT=gml3';
                }

                finalUrl = this.getUrl(finalUrl);

                return finalUrl;
            }.bind(this);
            loader = this.getRequestGetLoader(url, this.olFormat, this.type, this._loaderWithRemoveFeatures);
        } else {
            //comme ol2 (voir protocol wfs)
            var geometryName = Utils.DEFAULT_FEATURE_GEOMETRY_NAME;
            if (!_.isNil(layerDefinition.featureGeometryName)) {
                geometryName = layerDefinition.featureGeometryName;
            }
            var prefix = Utils.DEFAULT_FEATURE_PREFIX;
            if (!_.isNil(layerDefinition.featurePrefix)) {
                prefix = layerDefinition.featurePrefix;
            }
            var maxFeatures = null;
            if (!_.isNil(layerDefinition.displayMaxFeatures)) {
                maxFeatures = layerDefinition.displayMaxFeatures;
            }
            this.url = this.getUrl(this.url);
            loader = this.getRequestPostLoader(this.url, this.olFormat, layerDefinition.layerName, geometryName, prefix, maxFeatures, this._loaderWithRemoveFeatures);
        }

        params.source = new ol.source.Vector({
            format: this.olFormat,
            loader: loader,
            strategy: ol.loadingstrategy.bbox,
            saveStrategy: saveStrategy
        });
        params.source.set('saveStrategy', saveStrategy);

        var vectorLayer = new ol.layer.Vector(params);
        saveStrategy.setLayer(vectorLayer);

        if (!EditionManager.autoSave) {
            vectorLayer.on('loadstart', this, this.checkModificationToSave.bind(this));
        }

        this._checkZoomModification(vectorLayer);
        this._createOLImageResourceLayer(vectorLayer);
        this.createCompositeFeatureTemporaryLayer(vectorLayer, layerDefinition);

        return vectorLayer;
    },
    /*
     * Méthode createCompositeFeatureTemporaryLayer
     * Pour gérer les layers contenant des objets composites,
     * on créé une deuxieme couche Vecteur initialement vide
     * mais destinée à être remplie de manière temporaire par les outils
     * ayant besoin de selectionné une géométrie dans un objet composite
     */
    createCompositeFeatureTemporaryLayer: function (vectorLayer, layerDefinition) {
        if (this.isCompositeGeometry()) {
            this.compositeVectorLayer = vectorLayer;
            var styleMapConfig = {};
            for (var key in this.symbolizers) {
                // TODO Style
                //var style = new OpenLayers.Style();
                //style.addRules([new OpenLayers.Rule({symbolizer: this.symbolizers[key]})]);
                //styleMapConfig[key] = style;
            }

            //var styleMap = new OpenLayers.StyleMap(styleMapConfig);

            var params = {
                //styleMap: styleMap
                title: this.title + "_composite_selection_layer",
                source: new ol.source.Vector()
            };

            this.compositeSelectionLayer = new ol.layer.Vector(params);
        }
    },
    /*
     * Méthode checkModificationToSave
     * Listener de l'événement "loadStart" sur la couche.
     * Vérifie que la couche possède des éléments à sauvegarder ou non.
     * S'il y en existe, alors une confirmation pour la sauvegarde est demandé à l'utilisateur.
     */
    checkModificationToSave: function (e) {
        if (e.object === this.getFeatureOL_layers()[0] && this.isDirty()) {

            var confirm = function () {
                this.getSaveStrategy().save();
            }.bind(this);

            this.confirmOrLoseModification(confirm);
        }
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    toJSON: function () {
        var json = Vector.prototype.toJSON.apply(this);
        json.attributes = this.properties;
        return json;
    },
    CLASS_NAME: 'Descartes.Layer.EditionLayer.WMS'
});

module.exports = Class;

/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;

var EditionLayer = require('../EditionLayer');
var defaultSymbolizers = require('../../Symbolizers');

/**
 * Class: Descartes.Layer.EditionLayer.Vector
 * Objet "métier" correspondant à une couche d'édition pour le contenu de la carte, basée sur des *Vecteur*.
 *
 * Hérite de:
 * - <Descartes.Layer.EditionLayer>
 */
var Class = Utils.Class(EditionLayer, {
    /**
     * Propriete: useProxy
     * Utilisation ou non d'un proxy pour récupérer les données.
     */
    useProxy: true,
    /**
     * Constructeur: Descartes.Layer.EditionLayer.Vector
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        if (!_.isNil(options) && !_.isNil(options.symbolizers)) {
            for (var key in options.symbolizers) {
                _.extend(this.symbolizers[key], options.symbolizers[key]);
            }
            delete options.symbolizers;
        }

        EditionLayer.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },
    /**
     * Methode: getCommonParams
     * Fournit les paramètres communs à tous les types de couche vectorielle.
     *
     * Retour:
     * {Object} Objet JSON contenant les paramètres communs.
     */
    getCommonParams: function () {
        var olStyles = defaultSymbolizers.getOlStyle(this.symbolizers.default);
        var params = {
            title: this.title,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            zIndex: this.zIndex,
            minResolution: this.minResolution,
            maxResolution: this.maxResolution,
            style: function (feature, resolution) {
                var featureStyleFunction = feature.getStyleFunction();
                if (featureStyleFunction) {
                    return featureStyleFunction.call(feature, resolution);
                } else {
                    var style = olStyles[feature.getGeometry().getType()];
                    return style;
                }
            }
        };
        return params;
    },
    CLASS_NAME: 'Descartes.Layer.EditionLayer.Vector'
});

module.exports = Class;

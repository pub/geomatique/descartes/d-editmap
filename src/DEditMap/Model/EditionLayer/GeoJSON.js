/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;

var Vector = require('./Vector');
var EditionLayerConstants = require('../EditionLayerConstants');
var Symbolizers = require('../../Symbolizers');
var EditionManager = require('../../Core/EditionManager');

/**
 * Class: Descartes.Layer.EditionLayer.GeoJSON
 * Objet "métier" correspondant à une couche d'édition pour le contenu de la carte,
 * basée sur des *ressources GeoJSON*.
 *
 * Hérite de:
 * - <Descartes.Layer.EditionLayer.Vector>
 */
var Class = Utils.Class(Vector, {
    /**
     * Constructeur: Descartes.EditionLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches GeoJSON constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = EditionLayerConstants.TYPE_GeoJSON;
        this.symbolizers = {
            'default': _.extend({}, Symbolizers.Descartes_Symbolizers_DefaultEdition_GEOJSON),
            select: _.extend({}, Symbolizers.Descartes_Symbolizers_SelectionEdition_GEOJSON),
            create: _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_GEOJSON),
            temporary: _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_GEOJSON),
            support: _.extend({}, Symbolizers.Descartes_Symbolizers_SupportEdition_GEOJSON),
            modify: _.extend({}, Symbolizers.Descartes_Symbolizers_ModifyEdition_GEOJSON),
            'delete': _.extend({}, Symbolizers.Descartes_Symbolizers_DeletionEdition_GEOJSON),
            intersect: _.extend({}, Symbolizers.Descartes_Symbolizers_IntersectEdition_GEOJSON),
            autotracing: _.extend({}, Symbolizers.Descartes_Symbolizers_AutoTracingEdition_GEOJSON)
        };
        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },
    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche GeoJSON.
     *
     * Retour:
     * {OpenLayers.Layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams();
        params.layerName = layerDefinition.layerName;

        //default data projection EPSG:4326
        var optionsFormat = {
            defaultDataProjection: 'EPSG:4326'
        };

        this.olFormat = new ol.format.GeoJSON(optionsFormat);

        if (!_.isNil(layerDefinition.internalProjection)) {
            this.olFormat.defaultDataProjection = layerDefinition.internalProjection;
        }

        this.url = this.getUrl(layerDefinition.serverUrl);

        params.source = new ol.source.Vector({
            loader: this.getRequestGetLoader(this.url, this.olFormat, this.type, this._loaderWithRemoveFeatures),
            strategy: ol.loadingstrategy.bbox,
            format: this.olFormat
        });
        var saveStrategy = EditionManager.initSaveStrategy(layerDefinition);
        params.source.set('saveStrategy', saveStrategy);

        var vector = new ol.layer.Vector(params);
        saveStrategy.setLayer(vector);

        if (!EditionManager.autoSave) {
            vector.on('loadstart', this, this.checkModificationToSave.bind(this));
        }

        this._checkZoomModification(vector);
        this._createOLImageResourceLayer(vector);
        this.createCompositeFeatureTemporaryLayer(vector, layerDefinition);

        return vector;
    },

    /*
     * Méthode createCompositeFeatureTemporaryLayer
     * Pour gérer les layers contenant des objets composites,
     * on créé une deuxieme couche Vecteur initialement vide
     * mais destinée à être remplie de manière temporaire par les outils
     * ayant besoin de selectionné une géométrie dans un objet composite
     */
    createCompositeFeatureTemporaryLayer: function (vectorLayer, layerDefinition) {
        if (this.isCompositeGeometry()) {
            this.compositeVectorLayer = vectorLayer;
            var styleMapConfig = {};
            for (var key in this.symbolizers) {
                // TODO Style
                //var style = new OpenLayers.Style();
                //style.addRules([new OpenLayers.Rule({symbolizer: this.symbolizers[key]})]);
                //styleMapConfig[key] = style;
            }

            //var styleMap = new OpenLayers.StyleMap(styleMapConfig);

            var params = {
                title: this.title + "_composite_selection_layer",
                source: new ol.source.Vector()
            };

            this.compositeSelectionLayer = new ol.layer.Vector(params);
        }
    },
    /*
     * Méthode checkModificationToSave
     * Listener de l'événement "loadStart" sur la couche.
     * Vérifie que la couche possède des éléments à sauvegarder ou non.
     * S'il y en existe, alors une confirmation pour la sauvegarde est demandé à l'utilisateur.
     */
    checkModificationToSave: function (e) {
        if (e.object === this.getFeatureOL_layers()[0] && this.isDirty()) {
            var confirm = function () {
                this.getSaveStrategy().save();
            }.bind(this);

            this.confirmOrLoseModification(confirm);
        }
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    toJSON: function () {
        var json = Vector.prototype.toJSON.apply(this);
        json.attributes = this.properties;
        return json;
    },
    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Doit être surchargée par les classes dérivées.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        return Vector.prototype.serialize.apply(this);
    },
    CLASS_NAME: 'Descartes.Layer.EditionLayer.GeoJSON'
});

module.exports = Class;

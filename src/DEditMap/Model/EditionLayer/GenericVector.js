/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;

var Vector = require('./Vector');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../EditionLayerConstants');
var Symbolizers = require('../../Symbolizers');
var EditionManager = require('../../Core/EditionManager');
var log = require('loglevel');

/**
 * Class: Descartes.Layer.EditionLayer.GenericVector
 * Objet "métier" correspondant à une couche d'édition pour le contenu de la carte, basée sur des *ressources génériques* (alimentation manuelle).
 *
 * Hérite de:
 * - <Descartes.Layer.EditionLayer.Vector>
 */
var Class = Utils.Class(Vector, {

	_loaderWithRemoveFeatures: true,

    /**
     * Constructeur: Descartes.EditionLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WFS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = EditionLayerConstants.TYPE_GenericVector;
        options.geometryType = LayerConstants.GENERIC_GEOMETRY;
        this.symbolizers = {
            'default': _.extend({}, Symbolizers.Descartes_Symbolizers_DefaultEdition_GenericVector),
            'select': _.extend({}, Symbolizers.Descartes_Symbolizers_SelectionEdition_GenericVector),
            'create': _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_GenericVector),
            'temporary': _.extend({}, Symbolizers.Descartes_Symbolizers_CreateEdition_GenericVector),
            'support': _.extend({}, Symbolizers.Descartes_Symbolizers_SupportEdition_GenericVector),
            'modify': _.extend({}, Symbolizers.Descartes_Symbolizers_ModifyEdition_GenericVector),
            'delete': _.extend({}, Symbolizers.Descartes_Symbolizers_DeletionEdition_GenericVector),
            'intersect': _.extend({}, Symbolizers.Descartes_Symbolizers_IntersectEdition_GenericVector),
            'autotracing': _.extend({}, Symbolizers.Descartes_Symbolizers_AutoTracingEdition_GenericVector)
        };
        if (_.isNil(layersDefinition)) {
            layersDefinition = [{}];
        }
        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },
    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche générique.
     *
     * Retour:
     * {OpenLayers.Layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams();
        var saveStrategy = EditionManager.initSaveStrategy(layerDefinition);

        var opts = {
            strategy: ol.loadingstrategy.bbox,
            saveStrategy: saveStrategy
        };
        var that = this;
        var loader = function (extent, resolution, projection) {
            this.dispatchEvent('featureloadstart');
            if (that._loaderWithRemoveFeatures && (layerDefinition.displayFeatures || layerDefinition.loaderFunction)) {
                this.clear();
            }
            if (layerDefinition.loaderFunction) {
                 layerDefinition.loaderFunction.call(this, extent, resolution, projection);
            }
            if (layerDefinition.displayFeatures) {
                this.addFeatures(layerDefinition.displayFeatures);
            }
            this.dispatchEvent('featureloadend');
        };
        opts.loader = loader;

        params.source = new ol.source.Vector(opts);
        params.source.set('saveStrategy', saveStrategy);

        var vectorLayer = new ol.layer.Vector(params);
        saveStrategy.setLayer(vectorLayer);

        if (!EditionManager.autoSave) {
            vectorLayer.on('loadstart', this, this.checkModificationToSave.bind(this));
        }

        this._checkZoomModification(vectorLayer);
        this._createOLImageResourceLayer(vectorLayer);
        this.createCompositeFeatureTemporaryLayer(vectorLayer, layerDefinition);

        return vectorLayer;
    },
    /*
     * Méthode createCompositeFeatureTemporaryLayer
     * Pour gérer les layers contenant des objets composites,
     * on créé une deuxieme couche Vecteur initialement vide
     * mais destinée à être remplie de manière temporaire par les outils
     * ayant besoin de selectionné une géométrie dans un objet composite
     */
    createCompositeFeatureTemporaryLayer: function (vectorLayer, layerDefinition) {
        if (this.isCompositeGeometry()) {
            this.compositeVectorLayer = vectorLayer;
            var styleMapConfig = {};
            for (var key in this.symbolizers) {
                // TODO Style
                //var style = new OpenLayers.Style();
                //style.addRules([new OpenLayers.Rule({symbolizer: this.symbolizers[key]})]);
                //styleMapConfig[key] = style;
            }

            //var styleMap = new OpenLayers.StyleMap(styleMapConfig);

            var params = {
                //styleMap: styleMap
                title: this.title + "_composite_selection_layer",
                source: new ol.source.Vector()
            };

            this.compositeSelectionLayer = new ol.layer.Vector(params);
        }
    },
    /*
     * Méthode checkModificationToSave
     * Listener de l'événement "loadStart" sur la couche.
     * Vérifie que la couche possède des éléments à sauvegarder ou non.
     * S'il y en existe, alors une confirmation pour la sauvegarde est demandé à l'utilisateur.
     */
    checkModificationToSave: function (e) {
        if (e.object === this.getFeatureOL_layers()[0] && this.isDirty()) {

            var confirm = function () {
                this.getSaveStrategy().save();
            }.bind(this);

            this.confirmOrLoseModification(confirm);
        }
    },
    /**
     * Methode: addFeatures
     * Permet d'ajouter des objets dans la couche.
     *
     */
    addFeatures: function (features) {
        var addObjects = features;
        if (!(addObjects instanceof Array)) {
            addObjects = [addObjects];
        }
        this.OL_layers[0].getSource().addFeatures(addObjects);
    },

    /**
     * Methode: getFeatures
     * Permet de récupérer les objets de la couche.
     *
     */
    getFeatures: function () {
        return this.OL_layers[0].getSource().getFeatures();
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    toJSON: function () {
        var json = Vector.prototype.toJSON.apply(this);
        json.attributes = this.properties;
        return json;
    },
    CLASS_NAME: 'Descartes.Layer.EditionLayer.GenericVector'
});

module.exports = Class;

var _ = require('lodash');

var EditionLayer = require('./EditionLayer');

//var Vector = require('./EditionLayer/Vector');
var GeoJSON = require('./EditionLayer/GeoJSON');
var WFS = require('./EditionLayer/WFS');
var KML = require('./EditionLayer/KML');
var GenericVector = require('./EditionLayer/GenericVector');
var Annotations = require('./EditionLayer/Annotations');

var constants = require('./EditionLayerConstants');

var namespace = {
    GeoJSON: GeoJSON,
    //Vector: Vector,
    KML: KML,
    WFS: WFS,
    GenericVector: GenericVector,
    Annotations: Annotations
};

_.extend(namespace, constants);
_.extend(EditionLayer, namespace);

module.exports = EditionLayer;

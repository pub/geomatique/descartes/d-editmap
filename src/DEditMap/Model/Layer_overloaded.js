/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;

var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('./EditionLayerConstants');

var Layer = Descartes.Layer;

/**
 * Class: Descartes.Layer
 * Objet "métier" correspondant à une couche pour le contenu de la carte.
 *
 * :
 * Une couche peut comporter plusieurs couches OpenLayers. Ces dernières sont alors manipulées globalement.
 *
 * :
 * C'est en fait un agrégat de couches OpenLayers.
 *
 * :
 * Par exemple, rendre la couche invisible masque simultanément toutes les couches OpenLayers qu'elle comporte.
 *
 * Classes dérivées:
 * - <Descartes.Layer.WMS>
 * - <Descartes.Layer.Tiled>
 * - <Descartes.Layer.Vector>
 */
var Class = Utils.Class(Layer, {

    /*
     * Private
     */
    displayIconSnap: false,
    /*
     * Private
     */
    displayIconAutoTracing: false,
    /*
     * Private
     */
    displayIconSupport: false,

    getRequestGetLoader: function (url, format, type, withRemoveFeatures) {
        return function (extent, resolution, projection) {
            this.dispatchEvent('featureloadstart');

            var finalUrl = url;
            if (_.isFunction(url)) {
                finalUrl = url(extent, resolution, projection);
            }

            if (type === LayerConstants.TYPE_KML || type === LayerConstants.TYPE_GeoJSON || type === EditionLayerConstants.TYPE_KML || type === EditionLayerConstants.TYPE_GeoJSON) {
                //if (finalUrl.indexOf('?') !== -1) {
                //    finalUrl += '&ver=' + new Date().getTime();
                //} else {
                finalUrl += '?ver=' + new Date().getTime();
                //}
            }

            var xhr = new XMLHttpRequest();
            xhr.open('GET', finalUrl);
            if (format.getType() === 'arraybuffer') {
                xhr.responseType = 'arraybuffer';
            }

            if (withRemoveFeatures) {
                this.clear();
            }

            xhr.onload = function (event) {
                if (!xhr.status || xhr.status >= 200 && xhr.status < 300) {
                    var type = format.getType();
                    /** @type {Document|Node|Object|string|undefined} */
                    var source;
                    if (type === 'json' ||
                            type === 'text') {
                        if (xhr.responseText.indexOf('html') === -1) {
                            source = xhr.responseText;
                        } else {
                            source = null;
                        }
                    } else if (type === 'xml') {
                        source = xhr.responseXML;
                        if (!source) {
                            if (xhr.responseText.indexOf('html') === -1) {
                                source = ol.xml.parse(xhr.responseText);
                            } else {
                                source = null;
                            }
                        }
                    } else if (type === 'arraybuffer') {
                        source = (xhr.response);
                    }
                    if (source) {
                        var readOpts = {
                            featureProjection: projection
                        };
                        if (!_.isNil(format.defaultDataProjection)) {
                            readOpts.dataProjection = format.defaultDataProjection;
                        }
                        var features = format.readFeatures(source, readOpts);
                        this.addFeatures(features);
                        this.dispatchEvent('featureloadend');
                    } else {
                        this.dispatchEvent('featureloaderror');
                    }
                } else {
                    this.dispatchEvent('featureloaderror');
                }
            }.bind(this);
            xhr.onerror = function () {
                this.dispatchEvent('featureloaderror');
            }.bind(this);

            //send request
            xhr.send();
        };
    },

    CLASS_NAME: 'Descartes.Layer'
});

module.exports = Class;

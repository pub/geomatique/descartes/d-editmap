module.exports = {
    /**
     * Constante: TYPE_WFS
     * Code du layer d'édition de type WFS.
     */
    TYPE_WFS: 10,
    /**
     * Constante: TYPE_KML
     * Code du layer d'édition de type KML.
     */
    TYPE_KML: 11,
    /**
     * Constante: TYPE_GeoJSON
     * Code du layer d'édition de type GeoJSON.
     */
    TYPE_GeoJSON: 12,
    /**
     * Constante: TYPE_GenericVector
     * Code du layer d'édition de type Generic.
     */
    TYPE_GenericVector: 13,
    /**
     * Constante: TYPE_Annotations
     * Code du layer d'édition de type Annotations.
     */
    TYPE_Annotations: 14
};

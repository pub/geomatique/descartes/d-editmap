/* global GEOREF */

var _ = require('lodash');

var Layer_overloaded = require('./Layer_overloaded');

var EditionLayer = require('./EditionLayerNamespace');


var namespace = {
    EditionLayer: EditionLayer
};

_.extend(Layer_overloaded, namespace);

module.exports = Layer_overloaded;

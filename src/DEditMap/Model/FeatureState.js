module.exports = {
    /**
     * Constante: UNKNOWN
     * Etat inconnu.
     */
    UNKNOWN: 'Unknown',
    /**
     * Constante: INSERT
     * Etat inséré.
     */
    INSERT: 'Insert',
    /**
     * Constante: UPDATE
     * Etat mis à jour.
     */
    UPDATE: 'Update',
    /**
     * Constante: DELETE
     * Etat supprimé.
     */
    DELETE: 'Delete'
};

/* global Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var EditionLayerConstants = require('./EditionLayerConstants');

var Group = Descartes.Group;

/**
 * Class: Descartes.Group
 * Objet "métier" correspondant à un groupe d'éléments pour le contenu de la carte.
 *
 * :
 * Les éléments du groupe peuvent être des instances de <Descartes.Group> ou <Descartes.Layer>.
 *
 * :
 * Aucune limite de profondeur "groupe / sous-groupe / sous-sous-groupe / ..." n'existe.
 */
var Class = Utils.Class(Group, {

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié du groupe sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON sous la forme {title: <Titre>, opened: <Déplié ou non>}.
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.opened = this.opened;
        json.infosContent = {};
        if (this.addedByUser) {
           json.infosContent.nbLayer = this.countLayerInGroup(this.items);
           json.infosContent.nbLayerAddedByUser = this.countLayerAddedByUserInGroup(this.items);
        }
        json.infosContent.isPresentAnnotationsLayer = this.isPresentAnnotationsLayerInGroup(this.items);
        return json;
    },

    isPresentAnnotationsLayerInGroup: function (items) {
        var isPresent = false;
        for(var i = 0; i < items.length; i++) {
           if (items[i] instanceof Descartes.Layer) {
              if (items[i].type === EditionLayerConstants.TYPE_Annotations) {
                  isPresent = true;
                  break;
              }
           } else if (items[i] instanceof Descartes.Group) {
              isPresent = this.isPresentAnnotationsLayerInGroup(items[i].items);
           }
        }
        return isPresent;
    },

    CLASS_NAME: 'Descartes.Group'
});

module.exports = Class;

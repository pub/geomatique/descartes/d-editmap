/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Layer = require('./Layer_overloaded');
var LayerConstants = Descartes.Layer;
var EditionManager = require('../Core/EditionManager');
var FeatureState = require('./FeatureState');

var Symbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Layer.EditionLayer
 * Objet "métier" correspondant à une couche d'édition pour le contenu de la carte.
 *
 * Hérite de:
 * - <Descartes.Layer>
 */
var Class = Utils.Class(Layer, {
    /**
     * Propriete: maxScale
     * {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     */
    maxScale: null,
    /**
     * Propriete: minScale
     * {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     */
    minScale: null,
    /**
     * Propriete: symbolizers
     * {Object} Objet JSON surchargeant les styles vectoriels par défaut.
     */
    symbolizers: null,
    /**
     * Propriete: maxScale
     * {Float} Dénominateur de l'échelle maximale d'édition de la couche.
     */
    maxEditionScale: null,
    /**
     * Propriete: minScale
     * {Float} Dénominateur de l'échelle minimale d'édition de la couche.
     */
    minEditionScale: null,
    /**
     * Propriete: underEdition
     * {Boolean} Indique si la couche est en cours d'édition ou non.
     */
    underEdition: false,

    /**
     * Propriete: geometryType
     * {String} Indique le type de géométrie de la couche.
     *
     *  La valeur doit être une des suivantes :
     * - <Descartes.Layer.POINT_GEOMETRY>
     * - <Descartes.Layer.LINE_GEOMETRY>
     * - <Descartes.Layer.POLYGON_GEOMETRY>
     * - <Descartes.Layer.MULTI_POINT_GEOMETRY>
     * - <Descartes.Layer.MULTI_LINE_GEOMETRY>
     * - <Descartes.Layer.MULTI_POLYGON_GEOMETRY>
     */
    geometryType: null,
    /**
     * Propriete: attributes
     * {Objet} Définit des attributs éditables des objets et de l'attribut permettant
     * l'identification d'un objet de la couche.
     *
     * (start code)
     * attribute: {
     *   attributeId: {
     *     fieldName: "xxx"
     *   },
     *   attributesEditable: [{
     *     fieldName: 'name',
     *     label: 'Nom'
     *   }]
     * }
     * (end)
     */
    attributes: null,
    attributesEditable: [],
    /**
     * Propriete: snapping
     * {Objet} Configuration du magnétisme.
     *
     * (start code)
     * snapping: {
     *   tolerance: xx,
     *   enable: true | false
     * }
     * (end)
     */
    snapping: {
        tolerance: 15,
        enable: false,
        autotracing: false
    },
    /**
     * Propriete: clone
     * {Objet} Configuration du clonage d'un objet.
     *
     * (start code)
     * clone: {
     *   supportLayers: [{id:"xxx",attributes:[{from:"...", to:"..."}]},{...}],
     *   enable: true | false,
     *   attributsNotClonable:[{fieldName :'...'}]
     * }
     * (end)
     */
    clone: null,

    /**
     * Propriete: copy
     * {Objet} Configuration de la copie d'une géométrie.
     *
     * (start code)
     * copy: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    copy: null,

    /**
     * Propriete: split
     * {Objet} Configuration de la scission par dessin d'une géométrie.
     *
     * (start code)
     * split: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    split: null,

    /**
     * Propriete: divide
     * {Objet} Configuration de la scission d'une géométrie par rapport à une géométrie support.
     *
     * (start code)
     * divide: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    divide: null,

    /**
     * Propriete: aggregate
     * {Objet} Configuration de l'agrégation d'une géométrie.
     *
     * (start code)
     * aggregate: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    aggregate: null,

    /**
     * Propriete: unaggregate
     * {Objet} Configuration de la désagrégation d'une géométrie.
     *
     * (start code)
     * unaggregate: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    unaggregate: null,

    /**
     * Propriete: buffer
     * {Objet} Configuration de l'extension (buffer normal) d'une géométrie.
     *
     * (start code)
     * buffer: {
     *   supportLayers: [{id:"xxx", attribute:"..."},{...}],
     *   distance: ...,
     *   enable: true | false
     * }
     * (end)
     */
    buffer: null,

    /**
     * Propriete: halo
     * {Objet} Configuration de l'extension (buffer halo) d'une géométrie.
     *
     * (start code)
     * halo: {
     *   supportLayers: [{id:"xxx", attribute:"..."},{...}],
     *   distance: ...,
     *   enable: true | false
     * }
     * (end)
     */
    halo: null,

    /**
     * Propriete: homothetic
     * {Objet} Configuration de l'extension/réduction par homothétie d'une géométrie.
     *
     * (start code)
     * homothetic: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    homothetic: null,

    /**
     * Propriete: substract
     * {Objet} Configuration de la soustraction d'une géométrie.
     *
     * (start code)
     * substract: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    substract: null,

    /**
     * Propriete: intersect
     * {Objet} Configuration de l'affichage d'informations sur l'interserction d'une géométrie.
     *
     * (start code)
     * intersect: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false
     * }
     * (end)
     */
    intersect: null,

    /**
     * Propriete: verifAppConformity
     * {Boolean} Activation ou non de la vérification des règles de conformité métier. (béta)
     */
    verifAppConformity: false,

    /**
     * Propriete: verifTopoConformity
     * {Objet} Configuration de la vérification de la conformité topologique. (béta)
     *
     * (start code)
     * verifTopoConformity: {
     *   supportLayersIdentifier: ["xxx"],
     *   enable: true | false,
     *   strict: true | false
     * }
     * (end)
     */
    verifTopoConformity: {
        supportLayersIdentifier: [],
        enable: false,
        strict: false
    },

    /*
     * Private
     */
    snappingEnable: null,
    /*
     * Private
     */
    autotracingEnable: null,

    _parser: null,
    /**
     * Constructeur: Descartes.EditionLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WFS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * maxScale - {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     * minScale - {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     * maxEditionScale - {Float} Dénominateur de l'échelle maximale d'édition de la couche.
     * minEditionScale - {Float} D�nominateur de l'échelle minimale d'édition de la couche.
     * symbolizers - {Object} Objet JSON surchargeant les styles par défaut pour la couche.
     * geometryType - {<Descartes.Layer.POINT_GEOMETRY>|<Descartes.Layer.LINE_GEOMETRY>|<Descartes.Layer.POLYGON_GEOMETRY>} Type de géométrie de la couche
     * attributes - {Object} Objet JSON permettant de définir les attributs éditables des objets et de l'attribut permettant l'identification d'un objet de la couche.
     * snapping - {Object} Objet JSON permettant de configurer le magnétisme pour la couche.
     *
     */
    initialize: function (title, layersDefinition, options) {
        if (options !== undefined) {
            _.extend(this, options);
        }

        this._parser = new jsts.io.OL3Parser();

        Layer.prototype.initialize.apply(this, [title, layersDefinition, options]);

        //calcul max/min edition resolution si nécéssaire
        if (!_.isNil(this.minEditionScale) && _.isNil(this.maxEditionResolution)) {
            this.maxEditionResolution = Utils.getResolutionForScale(this.minEditionScale, 'm');
        }
        if (!_.isNil(this.maxEditionScale) && _.isNil(this.minEditionResolution)) {
            this.minEditionResolution = Utils.getResolutionForScale(this.maxEditionScale, 'm');
        }

    },
    /**
     * Methode: acceptMaxScaleRange
     * Indique si l'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     */
    acceptMaxScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var currentResolution = view.getResolution();
            var units = view.getProjection().getUnits();
            var currentScale = Utils.getScaleFromResolution(currentResolution, units);
            return (this.maxScale === null || (this.maxScale <= currentScale));
        }
        return true;
    },
    /**
     * Methode: acceptMinScaleRange
     * Indique si l'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     */
    acceptMinScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var currentResolution = view.getResolution();
            var units = view.getProjection().getUnits();
            var currentScale = Utils.getScaleFromResolution(currentResolution, units);
            return (this.minScale === null || (this.minScale >= currentScale));
        }
        return true;
    },
    /*
     * Méthode: _checkZoomModification
     * Lorsque la couche passée en paramètre est ajouté à la carte,
     * alors il faut mettre à jour la visibilité de la couche en fonction du niveau de zoom.
     * Lorsque la couche est supprimé de la carte, ce n'est plus la peine d'écouter les changements de niveau de zoom.
     */
    _checkZoomModification: function (layer) {
        //Avec OL>3, layer.map n'est plus disponible.
        //Descartes le rend disponible mais ce n'est pas immédiatement.
        //il faut donc s'enregistrer lorsque la couche est liée à la carte.
        //Lors du premier chargement c'est possible.

        var source = layer.getSource();

        var register = function (e) {
            layer.map.getView().on('change:resolution', this.updateVisibilityOnZoomChanged.bind(this), this);
            //maj lorsque la couche est chargée.
            this.updateVisibilityOnZoomChanged();
        };

        if (source instanceof ol.source.Image) {
            if (this._sourceImageListenersMethod.indexOf(register.name) === -1) {
            source.once('imageloadstart', register, this);
                this._sourceImageListenersMethod.push(register.name);
            }
        } else if (source instanceof ol.source.Tile) {
            if (this._sourceTileListenersMethod.indexOf(register.name) === -1) {
            source.once('tileloadstart', register, this);
                this._sourceTileListenersMethod.push(register.name);
            }
        } else if (source instanceof ol.source.Vector) {
            if (this._sourceFeatureListenersMethod.indexOf(register.name) === -1) {
            source.once('featureloadstart', register, this);
                this._sourceFeatureListenersMethod.push(register.name);
            }
            if (this._sourceFeatureListenersMethod.indexOf(this.checkForModificationToSave.name) === -1) {
            source.on('featureloadstart', this.checkForModificationToSave.bind(this), this);
                this._sourceFeatureListenersMethod.push(this.checkForModificationToSave.name);
            }
        }
    },
    /*
     * Méthode: _createOLImageResourceLayer
     * Si un layer de resource image est défini alors un layer wms est créer.
     */
    _createOLImageResourceLayer: function (vectorLayer) {
        var lastResourceLayer = this.resourceLayers[this.resourceLayers.length - 1];
        if (!_.isNil(lastResourceLayer) && !_.isNil(lastResourceLayer.getImageServerParams())) {
            var imageServerParams = lastResourceLayer.getImageServerParams();
            var params = {
                layers: imageServerParams.layerName,
                isBaseLayer: false
            };

            if (!_.isNil(imageServerParams.serverVersion)) {
                params.version = imageServerParams.serverVersion;
            }

            if (!_.isNil(imageServerParams.layerStyles)) {
                params.styles = imageServerParams.layerStyles;
            }

            var projection = null;
            if (!_.isNil(imageServerParams.internalProjection)) {
                projection = ol.proj.get(imageServerParams.internalProjection);
                //params.SRS = projection.getCode();
            }

            var crossOrigin = null;
            if (!_.isNil(imageServerParams.crossOrigin)) {
                crossOrigin = imageServerParams.crossOrigin;
            }

            var wms = new ol.layer.Image({
                title: this.title + '_image',
                opacity: this.opacity / 100.0,
                visible: this.visible,
                extent: this.extent,
                minResolution: this.minResolution,
                maxResolution: this.maxResolution,
                zIndex: this.zIndex,
                source: new ol.source.ImageWMS({
                    url: imageServerParams.serverUrl,
                    serverType: this.serverType,
                    ratio: this.ratio,
                    projection: projection,
                    params: params,
                    attributions: this.attribution,
                    crossOrigin: crossOrigin
                })
            });

            this.checkLayerLoading(wms);
            this.OL_layers.push(wms);
            this._checkZoomModification(wms);
            //this.updateVisibilityOnZoomChanged();
        } else {
            //s'il n y a pas de couche à afficher en dehors de la plage d'édition
            //alors la plage d'édition est la plage de visualisation.
            this.minScale = this.minEditionScale;
            this.maxScale = this.maxEditionScale;
        }
    },
    /**
     * Methode: getImageOL_Layers
     * Retourne les couches images (wms) de cette couche d'édition.
     *
     * Retour:
     * {OpenLayers.Layer.WMS} couche image.
     */
    getImageOL_Layers: function () {
        var result = [];
        for (var i = 0, len = this.OL_layers.length; i < len; i++) {
            var aLayer = this.OL_layers[i];
            if (aLayer instanceof ol.layer.Image) {
                result.push(aLayer);
            }
        }
        return result;
    },
    /*
     * Methode: updateVisibilityOnZoomChanged
     * Change la visibilité des layers en fonction du niveau de zoom et de la plage d'édition.
     * Si le zoom est en dehors de la plage d'édition alors les layers de type wms sont affichés,
     * sinon dans la plage d'édition les autres type de layer sont affichés.
     */
    updateVisibilityOnZoomChanged: function () {
        if (this.visible) {
            var isInEditionScalesRange = this.isInEditionScalesRange();

            for (var i = 0, len = this.OL_layers.length; i < len; i++) {
                var aLayer = this.OL_layers[i];
                if (aLayer instanceof ol.layer.Image) {
                    aLayer.setVisible(!isInEditionScalesRange);
                } else {
                    aLayer.setVisible(isInEditionScalesRange);
                }
            }
        }
    },
    /**
     * Methode: setVisibility
     * Modifie la visibilité de la couche.
     *
     * Paramètres:
     * isVisible - {Boolean} Visibilité souhaitée.
     */
    setVisibility: function (isVisible) {
        if (this.visible !== isVisible) {
            this.visible = isVisible;
            for (var i = 0, len = this.OL_layers.length; i < len; i++) {
                var aLayer = this.OL_layers[i];
                if (isVisible) {
                    if (aLayer instanceof ol.layer.Image) {
                        aLayer.setVisible(!this.isInEditionScalesRange());
                    } else {
                        aLayer.setVisible(this.isInEditionScalesRange());
                    }
                } else {
                    aLayer.setVisible(false);
                }
            }
        }
    },
    /**
     * Méthode: isSameGeometryType
     * Retourne true si le layer ou la géométrie passée en paramètre est du même type que la géométrie du layer courant
     * Exemples :
     * Polygon <--> Polygon = true
     * Polygon <--> Multipolygon = true
     * Polygon <--> Line = false
     * Polygon <--> MultiLine = false
     */
    isSameGeometryType: function (other) {
        if (other instanceof Layer) {
            switch (this.geometryType) {
                case LayerConstants.POINT_GEOMETRY:
                case LayerConstants.MULTI_POINT_GEOMETRY:
                    return (other.geometryType === LayerConstants.POINT_GEOMETRY ||
                            other.geometryType === LayerConstants.MULTI_POINT_GEOMETRY);
                case LayerConstants.LINE_GEOMETRY:
                case LayerConstants.MULTI_LINE_GEOMETRY:
                    return (other.geometryType === LayerConstants.LINE_GEOMETRY ||
                            other.geometryType === LayerConstants.MULTI_LINE_GEOMETRY);
                case LayerConstants.POLYGON_GEOMETRY:
                case LayerConstants.MULTI_POLYGON_GEOMETRY:
                    return (other.geometryType === LayerConstants.POLYGON_GEOMETRY ||
                            other.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY);
            }
        } else if (other instanceof ol.geom.Geometry) {
            switch (this.geometryType) {
                case LayerConstants.POINT_GEOMETRY:
                case LayerConstants.MULTI_POINT_GEOMETRY:
                    return (other instanceof ol.geom.Point ||
                            other instanceof ol.geom.MultiPoint);
                case LayerConstants.LINE_GEOMETRY:
                case LayerConstants.MULTI_LINE_GEOMETRY:
                    return (other instanceof ol.geom.LineString ||
                            other instanceof ol.geom.MultiLineString);
                case LayerConstants.POLYGON_GEOMETRY:
                case LayerConstants.MULTI_POLYGON_GEOMETRY:
                    return (other instanceof ol.geom.Polygon ||
                            other instanceof ol.geom.MultiPolygon);
            }
        }
        return false;
    },
    /**
     * Methode: showCompositeSelectionLayer
     * permet d'afficher la couche contenant les géometries des objets composites gérés de manières indépendantes
     */
    showCompositeSelectionLayer: function () {
        //on affiche la couche avec les objets composites séparés
        this.compositeSelectionLayer.setVisible(true);

        //on cache la couche avec les objets composites regroupés
        this.compositeVectorLayer.setVisible(false);

        if (this.compositeVectorLayer.map && !this.compositeSelectionLayer.map) {
            //On le supprime d'abord s'il a déjà été ajouté.
            this.compositeVectorLayer.map.removeLayer(this.compositeSelectionLayer);
            this.compositeVectorLayer.map.addLayer(this.compositeSelectionLayer);
        }
    },

    /**
     * Methode: hideCompositeSelectionLayer
     * permet de revenir dans le mode ou l'on affiche la couche originale des objets composites
     *
     */
    hideCompositeSelectionLayer: function () {
        this.compositeVectorLayer.setVisible(true);
        if (this.compositeSelectionLayer) {
            this.compositeSelectionLayer.setVisible(false);
        }
    },
    /**
     * Methode: clearCompositeSelectionLayer
     * permet de supprimer les geometries temporaires présente dans la couche dédiée à cet effet "compositeSelectionLayer"
     */
    clearCompositeSelectionLayer: function () {
        this.compositeSelectionLayer.getSource().clear();
    },
    /**
     * Methode: splitCompositeFeatures
     * permet de décomposer les objets composites et d'ajouter les geometrie
     * dans la couche dédiée à cet effet "compositeSelectionLayer"
     *
     */
    splitCompositeFeatures: function () {
        //On verifie que la couche temporaire est vide
        this.compositeSelectionLayer.getSource().clear();

        var featuresToClone = this.compositeVectorLayer.getSource().getFeatures();
        var simpleFeatures = [];

        _.each(featuresToClone, function (feature) {
            var multiGeom = feature.getGeometry();

            var style = Utils.getStyleByState(feature);
            var olStyles = Symbolizers.getOlStyle(this.symbolizers[style]);

            var olStyle = olStyles[feature.getGeometry().getType()];

            var geoms = Utils.getSubGeom(multiGeom);
            var features = [];
            _.each(geoms, function (geom) {
                var simpleFeature = new ol.Feature(feature.getProperties());
                simpleFeature.setGeometry(geom);
                simpleFeature.set('parent', feature);
                simpleFeature.setStyle(olStyle);
                features.push(simpleFeature);
            });
            feature.set('children', features);

            simpleFeatures = simpleFeatures.concat(features);
        }.bind(this));

        this.compositeSelectionLayer.getSource().addFeatures(simpleFeatures);
    },

    /**
     * Methode: updateParentCompositeFeature
     * permet de mettre à jour l'objet composite parent de la geométrie ciblé
     *
     * Paramètres:
     * feature - {Feature} objet qui a été modifié dans la couche temporaire
     *
     */
    updateParentCompositeFeature: function (featureWithGeomToRemove) {
        var geomToRemove = featureWithGeomToRemove.getGeometry();
        var geomToRemoveJsts = this._parser.read(geomToRemove);

        var deleteOlStyles = Symbolizers.getOlStyle(this.symbolizers.delete);
        var modifyOlStyles = Symbolizers.getOlStyle(this.symbolizers.modify);


        var olStyle = deleteOlStyles[featureWithGeomToRemove.getGeometry().getType()];
        featureWithGeomToRemove.setStyle(olStyle);

        var parentFeature = featureWithGeomToRemove.get('parent');
        parentFeature.set('state', FeatureState.UPDATE);

        olStyle = modifyOlStyles[parentFeature.getGeometry().getType()];
        parentFeature.setStyle(olStyle);

        var parentGeom = parentFeature.getGeometry();
        var parentGeomJsts = this._parser.read(parentGeom);

        var updatedGeomJsts = parentGeomJsts.difference(geomToRemoveJsts);
        if (updatedGeomJsts.isGeometryCollectionOrDerived()) {
            var updatedGeom = this._parser.write(updatedGeomJsts);
            parentFeature.setGeometry(updatedGeom);
        } else if (updatedGeomJsts.isEmpty()) {
            //plus de geometrie
            parentFeature.set('state', FeatureState.DELETE);
            olStyle = deleteOlStyles[parentFeature.getGeometry().getType()];
            parentFeature.setStyle(olStyle);
        } else {
            //la géométrie résultante n'est plus une géométrie multiple.
            var updatedSimpleGeom = this._parser.write(updatedGeomJsts);
            var coordinates = [];
            if (parentGeom instanceof ol.geom.MultiPolygon) {
                coordinates = [updatedSimpleGeom.getCoordinates()];
            }
            parentGeom.setCoordinates(coordinates);
        }

        this.compositeSelectionLayer.getSource().removeFeature(featureWithGeomToRemove);
    },
    /**
     * Methode: isEditable
     * Indique si la couche est editable ou non.
     * Une couche éditable est une couche visible et dont le niveau de zoom est dans la plage d'édition.
     *
     * Retour:
     * {Boolean} La couche est éditable ou non.
     *
     */
    isEditable: function () {
        return this.isInEditionScalesRange() && this.isVisible();
    },
    /**
     * Methode: isUnderEdition
     * Indique si la couche est en cours d'édition ou non.
     *
     * Retour:
     * {Boolean} La couche est en cours d'édition ou non.
     */
    isUnderEdition: function () {
        return this.underEdition;
    },
    /**
     * Methode: toggleEdition
     * Change le statut d'édition de la couche.
     *
     */
    toggleEdition: function () {
        if (this.isUnderEdition()) {
            this.disableEdition();
        } else {
            this.enableEdition();
        }
    },
    /**
     * Methode: enableEdition
     * Active le statut d'édition de la couche.
     */
    enableEdition: function () {
        if (!this.isUnderEdition()) {
            this.underEdition = true;
        }
    },
    /**
     * Methode: disableEdition
     * Desactive le statut d'édition de la couche.
     */
    disableEdition: function () {
        if (this.isUnderEdition()) {
            this.checkForModificationToSave();
            this.underEdition = false;
        }
    },
    checkForModificationToSave: function () {
        if (this.isUnderEdition()) {
            if (this.isDirty() && EditionManager.isGlobalEditonMode()) {
                var confirm = function () {
                    this.getSaveStrategy().save();
                    //Inutile de concerver l'état des features.
                    //l'état est conservé coté serveur.
                    this.cancelChange();
                }.bind(this);

                var lose = function () {
                    this.cancelChange();
                    this.refresh();
                }.bind(this);

                this.confirmOrLoseModification(confirm, lose);
            }
        }
    },
    /**
     * Methode: confirmOrLoseModification
     * Demande à l'utilisateur s'il veut conserver ces modifications.
     * Si l'utilisateur confirme alors on sauvegarde les données,
     * sinon on rafraichit le layer si le paramêtre est true, sinon on ne fait rien
     *
     */
    confirmOrLoseModification: function (confirmHandler, loseHandler) {
        if (confirm('Il reste des éléments à sauvegarder.\nVoulez-vous sauvegarder les modifications en cours ou annuler les modifications.')) {
            if (confirmHandler) {
                confirmHandler();
            }
            return true;
        } else if (loseHandler) {
            return loseHandler();
        }
        return false;
    },
    /**
     * Methode refresh
     * Refresh the first OL Layer
     */
    refresh: function () {
        var layer = this.getFeatureOL_layers()[0];
        layer.getSource().clear();

        if (this.isCompositeGeometry()) {
            this.splitCompositeFeatures();
            this.compositeSelectionLayer.getSource().clear();
        }
    },
    /**
     * Methode: isInScalesRange
     * Indique si la couche est potientiellement éditable au regard de l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} La couche est potentiellement éditable ou non.
     */
    isInEditionScalesRange: function () {
        return this.acceptEditionMaxScaleRange() && this.acceptEditionMinScaleRange();
    },
    /**
     * Methode: acceptEditionMaxScaleRange
     * Indique si l'échelle maximale d'édition de la couche est supérieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle maximale d'édition de la couche est supérieure à l'échelle courante de la carte.
     */
    acceptEditionMaxScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var units = view.getProjection().getUnits();
            var currentResolution = view.getResolution();
            var currentScale = Utils.getScaleFromResolution(currentResolution, units);
            return (this.maxEditionScale === null || (this.maxEditionScale <= currentScale));
        }
        return false;
    },
    /**
     * Methode: acceptEditionMinScaleRange
     * Indique si l'échelle minimale d'édition de la couche est inférieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle minimale d'édition de la couche est inférieure à l'échelle courante de la carte.
     */
    acceptEditionMinScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var units = view.getProjection().getUnits();
            var currentResolution = view.getResolution();
            var currentScale = Utils.getScaleFromResolution(currentResolution, units);
            return (this.minEditionScale === null || (this.minEditionScale >= currentScale));
        }
        return false;
    },
    /**
     * Methode: getSelectedFeatures
     * Retourne la liste des features selectionnées dans la carte pour cette couche.
     *
     * Retour:
     * {Array de OpenLayers.Feature} Un tableau contenant des features openlayers ou un tableau vide.
     */
    getSelectedFeatures: function () {
        var selection = [];
        for (var i = 0; i < this.OL_layers.length; i++) {
            var olLayer = this.OL_layers[i];
            if (!_.isNil(olLayer.getSource().getFeatures)) {
                var features = olLayer.getSource().getFeatures();
                for (var j = 0; j < features.length; j++) {
                    var feature = features[j];
                    if (feature.get('selected') === true) {
                        selection.push(feature);
                    }
                }
            }
        }
        return selection;
    },
    getUnselectedFeatures: function () {
        var selection = [];
        for (var i = 0; i < this.OL_layers.length; i++) {
            var olLayer = this.OL_layers[i];
            if (!_.isNil(olLayer.getSource().getFeatures)) {
                var features = olLayer.getSource().getFeatures();
                for (var j = 0; j < features.length; j++) {
                    var feature = features[j];
                    var selected = feature.get('selected');
                    if (_.isNil(selected) || selected === false) {
                        selection.push(feature);
                    }
                }
            }
        }
        return selection;
    },
    /**
     * Methode: getcompositeSelectionLayerSelectedFeatures
     * Retourne la liste des features selectionnées dans le layer composite dans la carte pour cette couche.
     *
     * Retour:
     * {Array de OpenLayers.Feature} Un tableau contenant des features openlayers ou un tableau vide.
     */
    getcompositeSelectionLayerSelectedFeatures: function (featureIdToDelete) {
        var selection = [];
        if (!_.isNil(this.compositeSelectionLayer)) {
            var features = this.compositeSelectionLayer.getSource().getFeatures();
            _.each(features, function (feature) {
                if (feature.get('selected') === true || (!_.isNil(featureIdToDelete) && feature.getId() === featureIdToDelete)) {
                    selection.push(feature);
                }
            });
        }
        return selection;
    },
    /**
     * Methode: getFeatures
     * Retourne l'ensemble des features de cette couche d'édition.
     *
     * Retour:
     * {Array de OpenLayers.Feature} Un tableau contenant des features openlayers ou un tableau vide.
     */
    getFeatures: function () {
        var result = [];
        for (var i = 0; i < this.OL_layers.length; i++) {
            var olLayer = this.OL_layers[i];
            if (olLayer.getSource().getFeatures) {
                var features = olLayer.getSource().getFeatures();
                result = result.concat(features);
            }
        }
        return result;
    },

    getFeatureById: function (objectId, att) {
        for (var i = 0; i < this.OL_layers.length; i++) {
            var olLayer = this.OL_layers[i];
            if (olLayer.getSource().getFeatures) {
                var featureWithId = olLayer.getSource().getFeatureById(objectId);
                if (featureWithId) {
                    return featureWithId;
                } else {
                    var features = olLayer.getSource().getFeatures();
                    for (var j = 0; j < features.length; j++) {
                        var feature = features[j];
                        if (!_.isNil(att)) {
                            if (feature.get(att) === objectId) {
                                 return feature;
                            }
                        } else if (feature.fid === objectId) {
                            return feature;
                        } else if (feature.getId() === objectId) {
                            return feature;
                        }
                    }
                }
            }
        }
        return null;
    },
    /**
     * Methode: removeSelectedFeatures
     * Supprime de la couche les features selectionnées.
     */
    removeSelectedFeatures: function (featureIdToDelete) {
        var olLayer, i, j, feature;
        if (this.isCompositeGeometry() && !_.isEmpty(this.getcompositeSelectionLayerSelectedFeatures(featureIdToDelete))) {
            var featuresSelected = this.getcompositeSelectionLayerSelectedFeatures(featureIdToDelete);

            for (i = 0; i < featuresSelected.length; i++) {
                var featureSelected = featuresSelected[i];
                olLayer = this.getFeatureOL_layers()[0];

                var parentFeature = featureSelected.get('parent');
                var subGeoms = Utils.getSubGeom(parentFeature.getGeometry());
                if (_.isNil(parentFeature.getId())) {
                    //si une seule géométrie et pas d'id alors on supprime
                    if (subGeoms.length === 1) {
                        olLayer.getSource().removeFeature(parentFeature);
                    } else {
                        var newGeom = Utils.computeMultiGeomFromChildren(parentFeature.getGeometry(), featureSelected.getGeometry());
                        parentFeature.setGeometry(newGeom);
                    }
                    this.compositeSelectionLayer.getSource().removeFeature(featureSelected);
                } else {
                    //mise à jour du parent
                    this.updateParentCompositeFeature(featureSelected);
                }
            }
            if (EditionManager.autoSave) {
                this.getSaveStrategy().save();
            }
        } else {
            for (i = 0; i < this.OL_layers.length; i++) {
                olLayer = this.OL_layers[i];
                if (!_.isNil(olLayer.getSource().getFeatures)) {
                    var featuresToRemove = [];
                    var source = olLayer.getSource();
                    var features = source.getFeatures();
                    for (j = 0; j < features.length; j++) {
                        feature = features[j];
                        if (feature.get('selected') === true || (!_.isNil(featureIdToDelete) && feature.getId() === featureIdToDelete)) {
                            if (_.isNil(feature.getId())) {
                                //cas des features ajoutées non sauvegardées
                                //et en cours de suppression
                                source.removeFeature(feature);
                            } else {
                                feature.set('state', FeatureState.DELETE);
                                feature.editionLayer = this;
                                var olStyles = Symbolizers.getOlStyle(this.symbolizers.delete);
                                var olStyle = olStyles[feature.getGeometry().getType()];
                                feature.setStyle(olStyle);

                                if (this.attributes &&
                                        this.attributes.attributeId &&
                                        this.attributes.attributeId.fieldName !== null) {
                                             feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
                                }

                                featuresToRemove.push(feature);
                            }
                        }
                    }

                    if (featuresToRemove.length > 0) {
                        if (EditionManager.autoSave) {
                            source.get('saveStrategy').save();
                        }
                    }
                }
            }
        }
    },
    /**
     * Methode: getSaveStrategy
     * Retourne la strategy de sauvegarde appliqué à ce layer ou null si non trouvé.
     *
     * Retourne {OpenLayers.Strategy.Save} ou null
     */
    getSaveStrategy: function () {
        for (var i = 0; i < this.getFeatureOL_layers().length; i++) {
            var olLayer = this.getFeatureOL_layers()[i];
            var source = olLayer.getSource();
            var saveStrategy = source.get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                return saveStrategy;
            }
        }
        return null;
    },
    unselectAll: function () {
        for (var i = 0; i < this.getFeatureOL_layers().length; i++) {
            var olLayer = this.getFeatureOL_layers()[i];
            var features = olLayer.getSource().getFeatures();
            for (var j = 0; j < features.length; j++) {
                var feature = features[j];
                if (feature.get('selected')) {
                      var style = Utils.getStyleByState(feature);
                      var olStyles = Symbolizers.getOlStyle(this.symbolizers[style]);
                      var olStyle = olStyles[feature.getGeometry().getType()];
                      feature.setStyle(olStyle);
                }
                feature.set('selected', false);
            }
        }
    },
    /**
     * Methode: isDirty
     * Retourne true s'il y a au moins une feature modifié, false sinon
     */
    isDirty: function () {
        for (var i = 0; i < this.getFeatureOL_layers().length; i++) {
            var olLayer = this.getFeatureOL_layers()[i];
            var features = olLayer.getSource().getFeatures();
            for (var j = 0; j < features.length; j++) {
                var feature = features[j];
                if (!_.isNil(feature.get('state'))) {
                    return true;
                }
            }
        }
        if (this.isCompositeGeometry() && this.isCompositeSelectionLayerDirty()) {
            return true;
        }
        return false;
    },
    isCompositeSelectionLayerDirty: function () {
        var olLayer = this.compositeSelectionLayer;
        var features = olLayer.getSource().getFeatures();
        for (var j = 0; j < features.length; j++) {
            var feature = features[j];
            if (!_.isNil(feature.get('state'))) {
                return true;
            }
        }
        return false;
    },
    /**
     * Methode: cancelChange
     * Annule les modifications faites. L'état de toutes les features passe à null.
     */
    cancelChange: function () {
        var olLayer, i, j, feature, features;
        for (i = 0; i < this.getFeatureOL_layers().length; i++) {
            olLayer = this.getFeatureOL_layers()[i];
            features = olLayer.getSource().getFeatures();

            for (j = 0; j < features.length; j++) {
                feature = features[j];
                feature.set('state', null);
            }
        }
        if (this.isCompositeGeometry()) {
            olLayer = this.compositeSelectionLayer;
            features = olLayer.getSource().getFeatures();
            for (j = 0; j < features.length; j++) {
                feature = features[j];
                feature.set('state', null);
            }
        }
    },
    /**
     * Methode: getFeaturesToSave
     * Retourne l'ensemble des features ayant l'état insert ou update ou delete
     */
    getFeaturesToSave: function () {
        var toSave = [];
        for (var i = 0; i < this.getFeatureOL_layers().length; i++) {
            var olLayer = this.getFeatureOL_layers()[i];
            var features = olLayer.getSource().getFeatures();
            for (var j = 0; j < features.length; j++) {
                var feature = features[j];
                var featureState = feature.get('state');
                if (featureState === FeatureState.INSERT ||
                        featureState === FeatureState.UPDATE ||
                        featureState === FeatureState.DELETE) {
                    toSave.push(feature);
                }
            }
        }
        return toSave;
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     *   title: <Intitulé>,
     * 	 type: <Type de couche>,
     * 	 layersDefinition: [{
     * 	   layerName: <Nom de la couche ou de l'objet OWS>
     * 	   serverUrl: <URL du serveur OWS>
     * 	 }],
     * 	 options: {
     * 	   format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 	   legend: <Adresses d'accès aux légendes>,
     * 	   metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 	   attribution: <Texte de copyright>,
     * 	   queryable: <Interrogation potentielle>,
     * 	   sheetable: <Affichage des propriétés sous forme de tableau>,
     * 	   maxScale: <Dénominateur de l'échelle maximale>,
     * 	   minScale: <Dénominateur de l'échelle minimale>,
     *	   maxEditionScale: <Dénominateur de l'échelle d'édition maximale>,
     * 	   id: <identifiant de la couche>,
     * 	   symbolizers: <Styles à appliquer>,
     * 	   attributes: <Configuration des attributs d'éditables et de l'attribut identifiant>,
     * 	   geometryType: <Type de géométrie>,
     * 	   snapping: <Configuration du magnétisme>,
     * 	   clone: <Configuration du clonage>,
     * 	   copy: <Configuration de la copie >,
     * 	   split: <Configuration de la scission par dessin>,
     * 	   divide: <Configuration de la division>,
     * 	   aggregate: <Configuration de l'agrégation>,
     * 	   unaggregate: <Configuration de la désagrégation>,
     * 	   buffer: <Configuration de l'extension (buffer normal)>,
     * 	   halo: <Configuration de l'extension (buffer halo) >,
     * 	   homothetic:  <Configuration de l'extentsion/réduction homothétique >,
     * 	   substract: <Configuration de la soustraction>,
     * 	   intersect: <Configuration de l'intersection>
     * 	 }
     * }
     * (end)
     */
    toJSON: function () {
        var featureOL_layers = this.getFeatureOL_layers();
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: featureOL_layers[0].get('layerName'),
                serverUrl: featureOL_layers[0].getSource().getUrl()
                        //serverVersion: featureOL_layers[0].protocol.version
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : "",
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : "",
            attribution: (this.attribution !== null) ? this.attribution : "",
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : "",
            minScale: (this.minScale !== null) ? this.minScale : "",
            maxEditionScale: (this.maxEditionScale !== null) ? this.maxEditionScale : "",
            minEditionScale: (this.minEditionScale !== null) ? this.minEditionScale : "",
            id: this.id,
            symbolizers: this.symbolizers,
            attributes: this.attributes,
            geometryType: this.geometryType,
            snapping: this.snapping,
            clone: this.clone,
            copy: this.copy,
            split: this.split,
            divide: this.divide,
            aggregate: this.aggregate,
            unaggregate: this.unaggregate,
            buffer: this.buffer,
            halo: this.halo,
            homothetic: this.homothetic,
            substract: this.substract,
            intersect: this.intersect
        };
        return json;
    },
    /**
     * Methode : addFeaturesToFeatureOLLayer
     *
     * A utiliser à la place de addfeatures de OpenLayers/layers/Vector.js lorsque l'on veut,
     * en sauvegarde automatique,
     * ajouter plusieurs features à la couche vector.
     */
    addFeaturesToFeatureOLLayer: function (features, options) {
        if (!_.isArray(features)) {
            features = [features];
        }

        var notify = !options || !options.silent;
        if (notify) {
            var event = {features: features};
            var ret = this.getFeatureOL_layers()[0].events.triggerEvent('beforefeaturesadded', event);
            if (ret === false) {
                return;
            }
            features = event.features;
        }

        // Track successfully added features for featuresadded event, since
        // beforefeatureadded can veto single features.
        var featuresAdded = [];
        for (var i = 0, len = features.length; i < len; i++) {
            if (i !== (features.length - 1)) {
                this.getFeatureOL_layers()[0].renderer.locked = true;
            } else {
                this.getFeatureOL_layers()[0].renderer.locked = false;
            }
            var feature = features[i];

            if (this.getFeatureOL_layers()[0].geometryType &&
                    !(feature.geometry instanceof this.getFeatureOL_layers()[0].geometryType)) {
                throw new TypeError('addFeatures: component should be an ' +
                        this.getFeatureOL_layers()[0].geometryType.prototype.CLASS_NAME);
            }

            //give feature reference to its layer
            feature.layer = this.getFeatureOL_layers()[0];

            if (!feature.style && this.getFeatureOL_layers()[0].style) {
                feature.style = _.extend({}, this.getFeatureOL_layers()[0].style);
            }

            if (notify) {
                if (this.getFeatureOL_layers()[0].events.triggerEvent('beforefeatureadded',
                        {feature: feature}) === false) {
                    continue;
                }
                this.getFeatureOL_layers()[0].preFeatureInsert(feature);
            }

            featuresAdded.push(feature);
            this.getFeatureOL_layers()[0].features.push(feature);
            this.getFeatureOL_layers()[0].drawFeature(feature);

        }

        if (notify) {
            this.getFeatureOL_layers()[0].events.triggerEvent('featuresadded', {features: featuresAdded});
        }
    },
    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde
     * d'un contexte de consultation.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour: {Object} Objet JSON.
     */
    serialize: function () {
        var json = {};
        json.itemType = 'Layer';
        json.title = this.title;
        json.type = this.type;
        json.options = {
            format: this.format,
            legend: this.legend,
            metadataURL: this.metadataURL,
            attribution: this.attribution,
            visible: this.visible,
            alwaysVisible: this.alwaysVisible,
            queryable: this.queryable,
            activeToQuery: this.activeToQuery,
            sheetable: this.sheetable,
            opacity: this.opacity,
            opacityMax: this.opacityMax,
            displayOrder: this.displayOrder,
            addedByUser: this.addedByUser,
            maxScale: this.maxScale,
            minScale: this.minScale,
            maxEditionScale: this.maxEditionScale,
            minEditionScale: this.minEditionScale,
            id: this.id,
            symbolizers: this.symbolizers,
            attributes: this.attributes,
            geometryType: this.geometryType,
            snapping: this.snapping,
            clone: this.clone,
            copy: this.copy,
            split: this.split,
            divide: this.divide,
            aggregate: this.aggregate,
            unaggregate: this.unaggregate,
            buffer: this.buffer,
            halo: this.halo,
            homothetic: this.homothetic,
            substract: this.substract,
            intersect: this.intersect,
            hide: this.hide
        };
        json.definition = [];
        for (var i = 0, len = this.resourceLayers.length; i < len; i++) {
            json.definition.push(this.resourceLayers[i].serialize());
        }
        return json;
    },
    CLASS_NAME: 'Descartes.Layer.EditionLayer'
});

module.exports = Class;

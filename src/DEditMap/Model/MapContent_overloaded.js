/* global Descartes */

var Utils = Descartes.Utils;

var Group = require('./Group_overloaded');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('./EditionLayerConstants');
var ClusterLayerConstants = Descartes.Layer.ClusterLayer;
var EditionLayer = require('./EditionLayer');
var log = require('loglevel');

var GeoJSON = Descartes.Layer.GeoJSON;
var KML = Descartes.Layer.KML;
var TMS = Descartes.Layer.TMS;
var WFS = Descartes.Layer.WFS;
var WMS = Descartes.Layer.WMS;
var WMSC = Descartes.Layer.WMSC;
var WMTS = Descartes.Layer.WMTS;
var OSM = Descartes.Layer.OSM;
var XYZ = Descartes.Layer.XYZ;
var VectorTile = Descartes.Layer.VectorTile;
var GenericVector = Descartes.Layer.GenericVector;
var EditionWFS = require('./EditionLayer/WFS');
var EditionKML = require('./EditionLayer/KML');
var EditionGeoJSON = require('./EditionLayer/GeoJSON');
var EditionGenericVector = require('./EditionLayer/GenericVector');
var EditionAnnotations = require('./EditionLayer/Annotations');
var ClusterWFS = Descartes.Layer.ClusterLayer.WFS;
var ClusterKML = Descartes.Layer.ClusterLayer.KML;
var ClusterGeoJSON = Descartes.Layer.ClusterLayer.GeoJSON;

var MapContent = Descartes.MapContent;

/**
 * Class: Descartes.MapContent
 * Objet "métier" correspondant au contenu de la carte en terme de groupes et couches.
 *
 * :
 * Les éléments constituant le contenu de la carte peuvent être des instances de <Descartes.Group> et/ou <Descartes.Layer>.
 *
 * Evénements déclenchés:
 * changed - La liste des éléments du contenu de la carte a changé.
 * itemPropertyChanged - Les propriétés des éléments du contenu de la carte ont changé (visibilités, opacités).
 * layerRemoved - Une couche a été supprimée.
 */
var Class = Utils.Class(MapContent, {

    EVENT_TYPES: ['changed', 'itemPropertyChanged', 'layerRemoved', 'layerUnderEdition', 'layerSnapping', 'layerAutoTracing', 'layerSupport'],

    /*
     * PRIVATE
     */
    _populate: function (items, baseItem) {
        for (var i = 0, len = items.length; i < len; i++) {
            var item = items[i];
            if (item && item.itemType === 'Group') {
                var groupItem = this.addItem(new Group(item.title, item.options), baseItem);
                this._populate(item.items, groupItem);
            } else if (item && item.itemType === 'Layer') {
                var LayerClass;
                switch (item.type) {
                    case LayerConstants.TYPE_WMS :
                        LayerClass = WMS;
                        break;
                    case LayerConstants.TYPE_WMSC :
                        LayerClass = WMSC;
                        break;
                    case LayerConstants.TYPE_TMS :
                        LayerClass = TMS;
                        break;
                    case LayerConstants.TYPE_WMTS :
                        LayerClass = WMTS;
                        break;
                    case LayerConstants.TYPE_WFS :
                        LayerClass = WFS;
                        break;
                    case LayerConstants.TYPE_KML :
                        LayerClass = KML;
                        break;
                    case LayerConstants.TYPE_GeoJSON :
                        LayerClass = GeoJSON;
                        break;
                    case LayerConstants.TYPE_GenericVector :
                        LayerClass = GenericVector;
                        break;
                    case LayerConstants.TYPE_OSM :
                        LayerClass = OSM;
                        break;
                    case LayerConstants.TYPE_XYZ :
                        LayerClass = XYZ;
                        break;
                    case LayerConstants.TYPE_VectorTile :
                        LayerClass = VectorTile;
                        break;
                    case LayerConstants.TYPE_GEOPORTAIL :
                        //ANNULER Descartes V5
                        log.warn('Couche du type Geoportail non pris en charge dans Descartes V5');
                        //LayerClass = LayerConstants.GeoPortail;
                        break;
                    case EditionLayerConstants.TYPE_WFS :
                        LayerClass = EditionWFS;
                        break;
                    case EditionLayerConstants.TYPE_KML :
                        LayerClass = EditionKML;
                        break;
                    case EditionLayerConstants.TYPE_GeoJSON :
                        LayerClass = EditionGeoJSON;
                        break;
                    case EditionLayerConstants.TYPE_GenericVector :
                        LayerClass = EditionGenericVector;
                        break;
                    case EditionLayerConstants.TYPE_Annotations :
                        LayerClass = EditionAnnotations;
                        break;
                    case ClusterLayerConstants.TYPE_WFS :
                        LayerClass = ClusterWFS;
                        break;
                    case ClusterLayerConstants.TYPE_KML :
                        LayerClass = ClusterKML;
                        break;
                    case ClusterLayerConstants.TYPE_GeoJSON :
                        LayerClass = ClusterGeoJSON;
                        break;
                }
                this.addItem(new LayerClass(item.title, item.definition, item.options), baseItem);
            }
        }
    },

    /**
     * Methode: refreshEditions
     * Rafraichit, le status underEdition des couches d'édition en fonction de la
     * couche passée en paramêtre et du mode d'édition choisit.
     * Envoie un événement pour signaler l'item passé en paramètre.
     *
     * Paramètres:
     * item - {<Descartes.Edtion.Layer>} Layer d'édition
     * unchanged - {Boolean} si true, indique que le rafraichissement n'est pas provoqué par une bascule utilisateur.
     */
    refreshEditions: function (item, unchanged) {
        if (this.refreshing !== true) {
            this.refreshing = true;
            if (!unchanged) {
                //bascule utilisateur
                item.toggleEdition();
            }

            var editionLayers = this.getEditionLayers();
            for (var i = 0; i < editionLayers.length; i++) {
                var editionLayer = editionLayers[i];
                if (editionLayer !== item && editionLayer.isUnderEdition()) {
                    editionLayer.disableEdition();
                }
            }

            if (unchanged) {
                this.events.triggerEvent('layerUnderEdition', item);
            }
            this.refreshing = false;
        }
    },
    /**
     * Methode: refreshSnapping
     * Envoie un événement pour signaler que l'item passé en paramètre à changer de statut pour le snapping
     */
    refreshSnapping: function (item) {
        this.events.triggerEvent('layerSnapping', item);
    },
    /**
     * Methode: refreshAutoTracing
     * Envoie un événement pour signaler que l'item passé en paramètre à changer de statut pour le tracing
     */
    refreshAutoTracing: function (item) {
        this.events.triggerEvent('layerAutoTracing', item);
    },
    /**
     * Methode: refreshSupport
     * Envoie un événement pour signaler que l'item passé en paramètre à changer de statut pour le support
     */
    refreshSupportLayers: function (item) {
        this.events.triggerEvent('layerSupport', item);
    },

    /**
     * Methode: getUnderEditionLayer
     * Retourne le layer en cours d'édition ou null si aucun layer n'est en cours d'édition.
     * Retour:
     * {<Descartes.Edtion.Layer>} ou null
     */
    getUnderEditionLayer: function () {
        var editionLayers = this.getEditionLayers();
        for (var i = 0; i < editionLayers.length; i++) {
            var editionLayer = editionLayers[i];
            if (editionLayer.isUnderEdition()) {
                return editionLayer;
            }
        }
        return null;
    },
    /**
     * Methode: getEditionLayers
     * Retourne tous les layers de type {<Descartes.Layer.EditionLayer>}
     *
     * Retour:
     * {Array (<Descartes.Edition.Layer>)}
     */
    getEditionLayers: function () {
        return this._getLayersByFilter(this.item, function (item) {
            if (item instanceof EditionLayer) {
                return item;
            }
            return null;
        });
    },

    CLASS_NAME: 'Descartes.MapContent'
});

module.exports = Class;

/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Strategy = require('../Core/Strategy');
var EventManager = Descartes.Utils.EventManager;
var FeatureState = require('../Model/FeatureState');

/**
 * Class: Descartes.Strategy.Save
 * Classe permettant de définir la stratégie de sauvegarde
 *
 * Hérite de:
 *  - <Descartes.Strategy>
 */
var Class = Utils.Class(Strategy, {

    /**
	 * Propriete: events
	 * {Descartes.Events} événements de l'instance.
	 */
    events: null,

    auto: false,

    timer: null,

    /**
	 * Propriete: layerDefinition
	 * {Object} ressource associée.
	 */
    layerDefinition: null,
    /**
     * Constructeur: Descartes.Strategy.Save
     * Constructeur d'instances.
     *
     * Paramètres:
     * options - {Object} Options pour l'instance
     */
    initialize: function (options) {
        Strategy.prototype.initialize.apply(this, [options]);
        if (options.layerDefinition) {
            this.layerDefinition = options.layerDefinition;
        }
        this.events = new EventManager();
    },

    activate: function () {
        var activated = Strategy.prototype.activate.call(this);
        if (activated) {
            if (this.auto) {
                if (typeof this.auto === 'number') {
                    this.timer = window.setInterval(this.save.bind(this),
                            this.auto * 1000
                            );
                } else {
                    this.layer.getSource().on({
                        'addfeature': this.triggerSave,
                        'changefeature': this.triggerSave,
                        scope: this
                    });
                }
            }
        }
        return activated;
    },

    deactivate: function () {
        var deactivated = Strategy.prototype.deactivate.call(this);
        if (deactivated) {
            if (this.auto) {
                if (typeof this.auto === 'number') {
                    window.clearInterval(this.timer);
                } else {
                    this.layer.getSource().un({
                        'addfeature': this.triggerSave,
                        'changefeature': this.triggerSave,
                        scope: this
                    });
                }
            }
        }
        return deactivated;
    },

    /**
     * Methode: triggerSave
     * Méthode de déclenchemennt de la sauvegarde.
     *
     * Paramètres:
	 * event - {Object} l'événement déclenchant.
     */
    triggerSave: function (event) {
        var feature = event.feature;
        var state = feature.get('state');
        if (state === FeatureState.INSERT ||
                state === FeatureState.UPDATE ||
                state === FeatureState.DELETE) {
            this.save([event.feature]);
        }
    },

    /**
     * Methode: save
     * Méthode permettant la sauvegarde.
     *
     * Paramètres:
	 * features - {Array} les objets à sauvegarder.
     */
    save: function (features) {
        if (!features) {
            features = this.layer.getSource().getFeatures();
        }

        if (!_.isArray(features)) {
            if (!_.isNil(features)) {
                features = [features];
            } else {
                features = [];
            }
        }

        var remote = this.layer.getSource().getProjection();
        if (_.isNil(remote)) {
            var format = this.layer.getSource().getFormat();
            if (!_.isNil(format)) {
            remote = format.defaultDataProjection;
            if (_.isNil(remote)) {
                var remoteCode = this.layerDefinition.internalProjection;
                if (!_.isNil(remoteCode)) {
                    remote = ol.proj.get(remoteCode);
                } else {
                    remote = this.layer.map.getView().getProjection();
                }
            } else if (_.isString(remote)) {
                remote = ol.proj.get(remote);
            }
            } else {
                remote = this.layer.map.getView().getProjection();
            }
        }

        var local = this.layer.map.getView().getProjection();
        if (!ol.proj.equivalent(local, remote)) {
            var len = features.length;
            var clones = new Array(len);
            var orig, clone;
            for (var i = 0; i < len; ++i) {
                orig = features[i];
                clone = orig.clone();
                clone.setId(orig.getId());
                clone.getGeometry().transform(local, remote);
                clones[i] = clone;
            }
            features = clones;
        }

        this.events.triggerEvent('start', {features: features});

        this.commit(features, {
            callback: this.onCommit,
            scope: this
        });
    },
    commit: function (features) {

        var inserts = [];
        var addObjects = [];

        var updates = [];
        var updatedObjects = [];

        var deletes = [];
        var removedObjects = [];


        _.each(features, function (feature) {
            var geometry = feature.getGeometry();

            //changement d'attribut pour la géometrie
            var geometryName = Utils.DEFAULT_FEATURE_GEOMETRY_NAME;
            if (!_.isEmpty(this.layerDefinition.featureGeometryName)) {
                geometryName = this.layerDefinition.featureGeometryName;
            }
            feature.setGeometryName(geometryName);
            feature.setGeometry(geometry);
            feature.unset('geometry');

            var featureId = feature.getId();
            var idAttribut = feature.idAttribut;
            var objectId = null;

            if (!_.isNil(idAttribut)) {
                objectId = idAttribut;
            } else {
                if (_.isNil(featureId)) {
                    objectId = 'dObjectId_' + Math.random().toString(36).slice(2);
                    feature.setId(objectId);
                } else {
                    objectId = featureId;
                }
            }

            //les outils indiquent si l'edition des attributs est active ou non.
            var editAttribut = feature.editAttribut;

            var state = feature.get('state');
            feature.unset('state');
            feature.unset('children');
            feature.unset('selected');
            feature.unset('initGeom');
            feature.unset('boundedBy');
            switch (state) {
                case FeatureState.INSERT:
                    inserts.push(feature);
                    var objectInsert = {
                        editionLayer: feature.editionLayer,
                        geometry: feature.getGeometry(),
                        objectId: objectId
                    };
                    if(editAttribut) {
                        var attributesInsert = feature.getProperties();
                        delete attributesInsert[geometryName];
                        delete attributesInsert.idAttribut;
                        objectInsert['attributs'] = attributesInsert;
                    }
                    addObjects.push(objectInsert);
                    break;
                case FeatureState.UPDATE:
                    updates.push(feature);
                    var objectUpdate = {
                            editionLayer: feature.editionLayer,
                            objectId: objectId,
                            geometry: feature.getGeometry()
                    };
                    if(editAttribut) {
                        var attributes = feature.getProperties();
                        delete attributes[geometryName];
                        delete attributes.idAttribut;
                        objectUpdate['attributs'] = attributes;
                    }
                    updatedObjects.push(objectUpdate);
                    break;
                case FeatureState.DELETE:
                    if (!_.isNil(featureId)) {
                        deletes.push(feature);
                        removedObjects.push({
                            editionLayer: feature.editionLayer,
                            objectId: objectId
                        });
                    }
                    break;
            }

        }.bind(this));


        var url;
        if (!_.isNil(this.layerDefinition.serverUrl)) {
            url = Utils.makeSameOrigin(this.layerDefinition.serverUrl, Descartes.PROXY_SERVER);
        }
        var descartesJson = {
            url: url,
            reqFeatures: features,
            fluxSimple: {
                addObjects: addObjects,
                removedObjects: removedObjects,
                updatedObjects: updatedObjects
            }
        };

        var srsName = this.layer.map.getView().getProjection().getCode();
        if (!_.isEmpty(this.layerDefinition.internalProjection)) {
            srsName = this.layerDefinition.internalProjection;
        }

        // il y a un problème avec la projection EPSG:4326
        // https://gis.stackexchange.com/questions/193178/inverted-lat-lon-on-drawn-polygons-in-openlayers-3
        // axisOrientation = neu au lieu de enu
        // (voir ol-debug.js ligne 42938
        // if (axisOrientation.substr(0, 2) === 'en') {
        if (srsName === 'EPSG:4326') {
            srsName = 'urn:ogc:def:crs:EPSG::4326';
        }

        var format = this.layer.getSource().getFormat();
        if (!_.isNil(format)) {
        if (format instanceof ol.format.WFS) {
            var node = format.writeTransaction(inserts, updates, deletes, {
                srsName: srsName,
                featureNS: this.layerDefinition.featureNameSpace,
                featureType: this.layerDefinition.layerName,
                version: this.layerDefinition.serverVersion
            });

            var wfstRequest = new XMLSerializer().serializeToString(node);

            descartesJson.fluxWfst = {
                content: wfstRequest,
                url: url,
                format: format
            };
        } else if (format instanceof ol.format.KML) {
            descartesJson.fluxKml = {
                content: {
                    addObjects: format.writeFeatures(inserts),
                    removedObjects: format.writeFeatures(deletes),
                    updatedObjects: format.writeFeatures(updates)
                },
                url: url,
                format: format
            };
        } else if (format instanceof ol.format.GeoJSON) {
            descartesJson.fluxGeoJSON = {
                content: {
                    addObjects: format.writeFeatures(inserts),
                    removedObjects: format.writeFeatures(deletes),
                    updatedObjects: format.writeFeatures(updates)
                },
                url: url,
                format: format
            };
        }
		}
        descartesJson.callback = this.createCallback(this.onCommit, descartesJson);

        Descartes.EditionManager.save(descartesJson);
    },

    createCallback: function (method, response, options) {
        return Utils.bindCallBack(function () {
            method.apply(this, [response, options]);
        }, this);
    },

    /**
     * Methode: onCommit
     * Méthode appelée après la sauvegarde.
     *
     * Paramètres:
     * response - {Object} la réponse suite à la sauvegarde.
     */
    onCommit: function (descartesJson) {
        if (descartesJson.priv.status === 200) {
            this.events.triggerEvent('success', descartesJson);
        } else {
            this.events.triggerEvent('fail', descartesJson);
        }
    },
    CLASS_NAME: 'Descartes.Strategy.Save'
});
module.exports = Class;

/* global MODE */

var _ = require('lodash');
var Strategy = require('../Core/Strategy');
var Save = require('./Save');



var namespace = {
	Save: Save
};

_.extend(Strategy, namespace);

module.exports = Strategy;

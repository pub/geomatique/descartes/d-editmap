/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var EditionLayerConstants = require('../../../Model/EditionLayerConstants');
var RemoveLayer = Descartes.Button.ContentTask.RemoveLayer;
var Messages = require('../../../Messages');

/**
 * Class: Descartes.Button.ContentTask.RemoveLayer
 * Classe "abstraite" définissant un bouton permettant la suppression d'une couche dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(RemoveLayer, {

    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés du groupe.
     */
    execute: function () {
        if (!_.isNil(this.datas) && this.datas.type === EditionLayerConstants.TYPE_Annotations) {
			alert(Messages.Descartes_ANNOTATIONSLAYER_REMOVE_IMPOSSIBLE);
        } else {
            RemoveLayer.prototype.execute.apply(this);
        }
	},


    CLASS_NAME: 'Descartes.Button.ContentTask.RemoveLayer'
});

module.exports = Class;

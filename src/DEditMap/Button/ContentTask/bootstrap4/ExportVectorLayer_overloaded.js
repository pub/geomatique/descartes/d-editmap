/* global MODE, Descartes */

var Utils = Descartes.Utils;

var ExportVectorLayer = Descartes.Button.ContentTask.ExportVectorLayer;

/**
 * Class: Descartes.Button.ContentTask.ExportVectorLayer
 * Classe définissant un bouton permettant l'export KML/GeoJSON d'une couche du contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(ExportVectorLayer, {

    checkLayerUnderEdition: function (event) {
        if (event && event.data[0] && event.data[0].underEdition) {
            this.enable();
            this._exportLayer = event.data[0];
        } else {
            this.disable();
        }
    },

    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('layerUnderEdition', this, this.checkLayerUnderEdition);
    },

    CLASS_NAME: 'Descartes.Button.ContentTask.ExportVectorLayer'
});

module.exports = Class;

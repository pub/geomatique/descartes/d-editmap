/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var RemoveGroup = Descartes.Button.ContentTask.RemoveGroup;
var Messages = require('../../Messages');

/**
 * Class: Descartes.Button.ContentTask.RemoveGroup
 * Classe définissant un bouton permettant la suppression d'un groupe dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(RemoveGroup, {

    execute: function () {
        if (!_.isNil(this.datas) && !_.isNil(this.datas.infosContent) && this.datas.infosContent.isPresentAnnotationsLayer) {
			alert(Messages.Descartes_ANNOTATIONSLAYER_GROUP_REMOVE_IMPOSSIBLE);
        } else {
            RemoveGroup.prototype.execute.apply(this);
        }
    },

    CLASS_NAME: 'Descartes.Button.ContentTask.RemoveGroup'
});

module.exports = Class;

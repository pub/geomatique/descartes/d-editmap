/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../Tool/Edition');
var EditionLayer = require('../Model/EditionLayer');

var ToolBarOpener = Descartes.Button.ToolBarOpener;

/**
 * Class: Descartes.Button.ToolBarOpener
 * Classe définissant un bouton permettant d'ouvrir un nouvelle barre d'outils, pouvant accueillir des boutons de type <Descartes.Button> et/ou <Descartes.Tool>.
 */
var Class = Utils.Class(ToolBarOpener, {

    /*
     * Private
     */
    _createToolBar: function () {
        var i, len;
        for (i = 0, len = this.descartesMap.toolBars.length; i < len; i++) {
            if (this.descartesMap.toolBars[i].toolBarName === this.toolBarName) {
                //this.tools = this.descartesMap.toolBars[i].controls;
                this.descartesMap.toolBars.splice(i, 1);
                break;
            }
        }

        if (!_.isNil(this.toolBarName)) {
            this.toolBarOptions.toolBarName = this.toolBarName;
        }
        this.toolBarOptions.title = this.title;

        this.toolBar = this.descartesMap.addNamedToolBar(this.innerDiv, this.tools, this.toolBarOptions);
        this.toolBarName = this.toolBar.toolBarName;
        this.toolBar.toolBarOpener = this;
        for (i = 0; i < this.toolBar.controls.length; i++) {
            var tool = this.toolBar.controls[i];
            if (tool instanceof Edition) {
                tool.setMapContent(this.descartesMap.mapContent, true);
                if (_.isFunction(tool.initAnnotationTool)) {
                   tool.initAnnotationTool(this.descartesMap);
                }
            }
        }
    },
    /**
     * Methode: openToolBarOnLayerEdition
     * Méthode appelée lorsqu'un layer d'édition change d'état en cours d'édition.
     *
     * Paramètres:
     * layerUnderEdition -  - {<Descartes.Layer.EditionLayer>} Layer d'édition
     */
    openToolBarOnLayerEdition: function (event) {
        var layerUnderEdition = event.data[0];
        if (layerUnderEdition instanceof EditionLayer &&
                layerUnderEdition.isUnderEdition() && !this.opening) {
            //la méthode openToolBar déclenche l'événement "layerUnderEdition"
            //ce qui déclenche à nouveau cette methode et donc une boucle infinie
            this.opening = true;
            this.openToolBar();
            this.opening = false;
        }
    },
    /**
     * Methode: openToolBar
     * ouverture de la barre d'outils.
     */
    openToolBar: function () {
        if (_.isNil(this.toolBar)) {
            this._createToolBar();
        }
        this.toolBar.visible = true;

        if (_.isNil(this.innerDiv)) {
            this.toolBar.alignTo(this.id);
        }

        var underEditionLayer = this.descartesMap.mapContent.getUnderEditionLayer();
        if (!_.isNil(underEditionLayer)) {
            for (var i = 0; i < this.toolBar.controls.length; i++) {
                var control = this.toolBar.controls[i];
                if (control.updateStateWithLayer) {
                    control.updateStateWithLayer({
                        data: [underEditionLayer]
                    });
                }
            }
        }

        if (Descartes.EditionManager.isGlobalEditonMode()) {
            if (this.activeFirstElement && this.toolBar.controls.length > 0 &&
                    this.toolBar.controls[0].CLASS_NAME.indexOf("Descartes.Tool") !== -1) {
                var controlToActivate = this.toolBar.controls[0];

                var activate = true;
                if (controlToActivate.CLASS_NAME.indexOf('Descartes.Tool.Edition') !== -1 &&
                        _.isNil(underEditionLayer)) {
                    //on active seulement l'outil d'édition s'il y a un layer en cours d'édition
                    activate = false;
                }

                if (activate) {
                    //activation de l'outil
                    controlToActivate.activate();
                    if (!_.isNil(controlToActivate.interaction)) {
                        //ajout de l'interaction dans la carte.
                        if (!this.descartesMap.isActiveInteraction(controlToActivate.interaction)) {
                            this.olMap.addInteraction(controlToActivate.interaction);
                        }
                    }
                }
            }
        } else if (this.toolBar.controls.length > 0) {
            //rafraichissement des états des boutons
            this.toolBar.controls[0].refreshEditionState();
        }
        this.enabledClass = 'active';
        this.updateButton();

        try {
            this.toolBar.events.register('closed', this, this.destroyToolBar);
        } catch (e) {
        }
        if (!_.isNil(this.initFunction)) {
            this.initFunction();
        }
    },

    CLASS_NAME: 'Descartes.Button.ToolBarOpener'
});

module.exports = Class;

/* global Descartes*/

var _ = require('lodash');

var Utils = Descartes.Utils;

/**
 * Class: Descartes.Strategy
 * Classe "abstraite" permettant de définir la stratégie de sauvegarde
 *
 * Classes dérivées:
 *  - <Descartes.Strategy.Save>
 */
var Class = Utils.Class({

    layer: null,

    options: null,

    active: null,

    autoActivate: true,

    autoDestroy: true,

    /**
     * Constructeur: Descartes.Strategy
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} options de l'instances
     */
    initialize: function (options) {
        _.extend(this, options);
        this.options = options;
        // set the active property here, so that user cannot override it
        this.active = false;
    },

    destroy: function () {
        this.deactivate();
        this.source = null;
        this.options = null;
    },

    setLayer: function (layer) {
        this.layer = layer;
    },

    activate: function () {
        if (!this.active) {
            this.active = true;
            return true;
        }
        return false;
    },

    deactivate: function () {
        if (this.active) {
            this.active = false;
            return true;
        }
        return false;
    },

    CLASS_NAME: 'Descartes.Strategy'
});

module.exports = Class;

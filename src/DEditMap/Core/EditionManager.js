/* global MODE */

var _ = require('lodash');
var ol = require('openlayers');

var Messages = require('../Messages');
var SaveStrategy = require('../Strategy/Save');
var MapConstants = Descartes.Map;
var Creation = require('../Tool/Edition/Creation');
/* global Descartes*/

var Modification = require('../Tool/Edition/Modification');
var Selection = require('../Tool/Edition/Selection');

var DrawCreation = require('../Tool/Edition/Creation/DrawCreation');
var AggregationCreation = require('../Tool/Edition/Creation/AggregationCreation');
var SplitCreation = require('../Tool/Edition/Creation/SplitCreation');
var DivideCreation = require('../Tool/Edition/Creation/DivideCreation');
var HomotheticCreation = require('../Tool/Edition/Creation/HomotheticCreation');

var SelectionSubstractModification = require('../Tool/Edition/Modification/SelectionSubstractModification');
var GlobalModification = require('../Tool/Edition/Modification/GlobalModification');
var VerticeModification = require('../Tool/Edition/Modification/VerticeModification');
var WaitingDialog = Descartes.UI.WaitingDialog;
var ConfirmDialog = Descartes.UI.ConfirmDialog;

var Utils = Descartes.Utils;
var Symbolizers = require('../Symbolizers');

/**
 * Class: Descartes.EditionManager
 * Classe permettant de gérer l'édition des objets géographique.
 *
 * Choix du mode d'édition:
 * - édition gloable pilotée par l'arbre des couches
 * - édition individuelle pilotée par les outils d'édition
 * - édition pilotée directement par l'application métier
 *
 * Choix de mode de sauvegarde:
 * - sauvegarde automatique
 * - sauvegarte manuelle
 *
 * Mise à disposition des mécanismes "atomiques":
 * - Demande de création de la géométrie d'un nouvel objet d'une couche, ou d'un objet existant sans géométrie depuis l'application métier.
 * - Sélection graphique pour modification d'un objet pr�alablement choisi dans l'application métier.
 * - Demande de suppression d'un objet préalablement choisi dans l'application métier.
 * - Demande de visualisation d'un objet préalablement choisi dans l'application métier.
 *
 */
var EditionManager = {
    /**
     * Propriete: autoSave
     * {Boolean} Indique si la sauvegarde doit être faite directement après une édition ou non.
     */
    autoSave: true,
    /**
     * Propriete: globalEditionMode
     * {Boolean} Indique si le mode d'édition est global ou individuel
     */
    globalEditionMode: true,

    /**
     * Propriete: onlyAppMode
     * {Boolean} Indique si seulement le mode d'édition piloté par d'application métier est activé
     */
    onlyAppMode: false,

    /**
     * Propriete: verifAppConformity
     * {Boolean} Indique si la vérifcation des règles "métier" de conformité est activé.
     */
    verifAppConformity: false,

    /**
     * Propriete: verifTopoConformity
     * {Objet} Configuration de la vérification de la conformité topologique.
     *
     * (start code)
     * verifTopoConformity: {
     *   enable: true | false,
     *   strict: true | false
     * }
     * (end)
     */
    verifTopoConformity: {
        enable: false,
        strict: false
    },

    /**
     * Propriete: waitingMsg
     * {Boolean} Indique si le message d'attente doit s'afficher
     */
    waitingMsg: true,

    /**
     * Propriete: waitingMaskClassName
     * {String} Classe CSS pour le masque inhibant la page pendant l'attente.
     */
    waitingMaskClassName: 'DescartesEditionSaveWaitingMask',

    /**
     * Propriete: waitingMessageClassName
     * {String} Classe CSS pour le message d'attente.
     */
    waitingMessageClassName: 'DescartesEditionSaveWaitingMessage',

    /**
     * Propriete: displaySaveMsg
     * {Boolean} Indique si le message de réussite ou non de la sauvegarde doit s'afficher
     */
    displaySaveMsg: true,
    /*
     * Private
     */
    createObjectId: null,
    /*
     * Private
     */
    createAttributes: null,
    /*
     * Private
     */
    selectFeature: null,
    /*
     * Private
     */
    saveStrategyLayer: null,

    /*
     * Private
     */
    map: null,

    /**
     * Methode: configure
     * Initialize l'objet Descartes.EditionManager avec les propriétés passées en paramètre.
     *
     * Paramètres:
     * options - {object} javascript contenant les options d'initialisation.
     */
    configure: function (options) {
        _.extend(this, options);

        if (this.onlyAppMode) {
            this.globalEditionMode = true;
            this.autoSave = true;
        }
    },
    /**
     * Methode: save
     * Handler par défaut appelé lorsqu'une sauvegarde est demandée.
     * C'est cette méthode qu'il faut surcharger pour appeler un service qui se chargera de la sauvegarde.
     */
    save: function (json) {
        json.priv = {status: 200};
        json.callback.call(json);
    },

    /*
     * Methode: sendInformations
     */
    sendInformations: function (json) {
        this.receiveInformations(json);
    },

    /**
     * Methode: receiveInformations
     * Méthode par défaut appelé lorsqu'une demande d'envoi d'information est demandée par l'application métier.
     * Cette méthode doit être surchargée pour traiter les informations dans l'application métier.
     */
    receiveInformations: function (json) {
    },

    /**
     * Methode: eraseInformations
     * Méthode appelé pour effacer les informations envoyées à l'application métier.
     * Cette méthode doit être surchargée par l'application métier.
     */
    eraseInformations: function () {
    },

    /**
     * Methode: conformityAppFct
     * Handler appelé lorsque la vérification de règles de conformité "metier" est demandée.
     * Règle par défaut: "Pas d'intersection entre les objets de la couche".
     * Cette méthode doit être surchargée pour vérifier les règles de conformité "metier".
     */
    conformityAppFct: function (json) {
        var jsonApp = {
            isConforme: true,
            message: ''
        };

        if (!jsonApp.isConforme) {
            jsonApp.message = Messages.Descartes_EDITION_NOT_CONFORM_APP;
        }

        return jsonApp;
    },

    /**
     * Methode: isGlobalEditionMode
     * Indique si le mode d'édition globale est actif ou non.
     *
     * Retourne true ou false.
     */
    isGlobalEditonMode: function () {
        return this.globalEditionMode;
    },

    /**
     * Methode: initSaveStrategy
     * Initialise et retourne la strategie de sauvegarde.
     *
     * Retour:
     * {OpenLayers.Strategy.Save} la stratégie de sauvegarde.
     */
    initSaveStrategy: function (layerDefinition) {
        var saveStrategy = new SaveStrategy({
            auto: this.autoSave,
            layerDefinition: layerDefinition
        });

        saveStrategy.events.register('start', this, function (event) {
            if (this.waitingMsg) {
                this.showWaitingMessage();
            }
        });

        saveStrategy.events.register('success', this, function (event) {
            var response = event.data[0];
            this.closeWaitingMessage();
            this.saveStrategyLayer = saveStrategy.layer;

            var message = 'La sauvegarde a été faite.';
            if (response.priv.message) {
                message = response.priv.message;
            }
            this.saveStrategyLayer.saveMessage = message;

            var refresh = function () {
                var saveSource = this.saveStrategyLayer.getSource();
                _.each(saveSource.getFeatures(), function (feature) {
                    feature.setStyle(null);
                });
                saveSource.clear();
            }.bind(this);

            var that = this;
            setTimeout(function () {
                that.closeWaitingMessage();
				if (that.displaySaveMsg && !_.isEmpty(that.saveStrategyLayer.saveMessage)) {
                var dialog = new ConfirmDialog({
						id: that.id + '_saveDialog',
                    title: '',
                    size: 'modal-sm',
						message: that.saveStrategyLayer.saveMessage
                });
                dialog.open();
            }
            }, 1000);
            refresh();
        });

        saveStrategy.events.register('fail', this, function (event) {
            var response = event.data[0];
            this.closeWaitingMessage();
            this.saveStrategyLayer = saveStrategy.layer;

            var message = 'Problème lors de la sauvegarde.';
            if (response.priv.message) {
                message = response.priv.message;
            }
            this.saveStrategyLayer.saveMessage = message;

            var refresh = function () {
                var saveSource = this.saveStrategyLayer.getSource();
                _.each(saveSource.getFeatures(), function (feature) {
                    feature.setStyle(null);
                });
                saveSource.clear();
            }.bind(this);

            var that = this;
            setTimeout(function () {
                that.closeWaitingMessage();
                if (that.displaySaveMsg && !_.isEmpty(that.saveStrategyLayer.saveMessage)) {
                var dialog = new ConfirmDialog({
                        id: that.id + '_saveDialog',
                    title: '',
                    size: 'modal-sm',
                        message: that.saveStrategyLayer.saveMessage
                });
                dialog.open();
            }
            }, 1000);
            refresh();
        });
        return saveStrategy;
    },

    /**
     * Methode: showWaitingMessage
     * Affiche un message d'attente.
     */
    showWaitingMessage: function () {
        this.closeWaitingMessage();
        this.waitingDialog = new WaitingDialog({
            message: 'Sauvegarde en cours'
        });
        this.waitingDialog.open();
    },
    /**
     * Methode: closeWaitingMessage
     * Supprime le message d'attente s'il existe et affiche une alerte en fonction du paramètre passé en paramètre.
     */
    closeWaitingMessage: function () {
        if (this.waitingDialog) {
            this.waitingDialog.close();
        }
    },
    /**
     * Methode: enableEditionLayer
     * Active une couche en édition.
     */
    enableEditionLayer: function (editionLayer) {

        this._disableAllEditionLayers();
        this._deactivateAllToolsInToolBars(editionLayer);

        if (editionLayer.isVisible()) {
            if (editionLayer.isEditable()) {

                //vérification que la couche n'est pas déjà en cours d'édition
                if (!editionLayer.isUnderEdition()) {

                    editionLayer.enableEdition();
                    this.map.mapContent.refreshEditions(editionLayer, true);
                    //rafraichissement du gestionnaire de contenu.
                    this.map.mapContent.refreshOnlyContentManager();

                }

            } else {
                alert('La couche "' + editionLayer.title + '" n\'est actuellement pas éditable.');
            }
        } else {
            alert('La couche "' + editionLayer.title + '" n\'est actuellement pas visible.');
        }

    },
    /**
     * Methode: disableEditionLayer
     * Déactive une couche en édition.
     */
    disableEditionLayer: function (editionLayer) {
        editionLayer.disableEdition();
        this.map.mapContent.refreshEditions(editionLayer, true);
        this.map.mapContent.refreshOnlyContentManager();
    },
    /**
     * Methode: createObject
     * Demande de création de la géométrie d'un nouvel objet d'une couche, ou d'un objet existant sans géométrie
     * La couche passe à l'état éditable
     * Les outils passent à l'état disponible
     * L'outil de création désiré est activé
     *
     * Paramètres:
     * (start code)
     * json - {
     *      editionLayer: layer // la couche d'édition. Obligatoire.
     *      objectId: "XXX", // identifiant de l'objet. Obligatoire
     *      activeTool: tool // identifiant outil de création à activer (Descartes.Map.EDITION_DRAW_CREATION ou autre) : facultatif
     *      (si pas d'outil de renseigné, alors par défaut on active Descartes.Map.EDITION_DRAW_CREATION si présent dans l'éditionToolBar)
     * }
     * (end)
     */
    createObject: function (json) {
        this._checkParameters(json);

        var editionLayer = json.editionLayer;

        if (this.onlyAppMode) {
            this._disableAllEditionLayers();
            this._deactivateAllToolsInToolBars(editionLayer);
        }

        if (editionLayer.isVisible()) {
            if (editionLayer.isEditable()) {

                //vérification que la couche n'est pas déjà en cours d'édition et qu'elle est éditable.
                if (!editionLayer.isUnderEdition() && editionLayer.isEditable()) {

                    //récupération de l'identifiant de l'objet à créer.
                    this.createObjectId = json.objectId;
                    this.createAttributes = json.attributs;

                    editionLayer.enableEdition();
                    this.map.mapContent.refreshEditions(editionLayer, true);
                    //rafraichissement du gestionnaire de contenu.
                    this.map.mapContent.refreshOnlyContentManager();

                }
                if (_.isNil(json.activeTool)) {
                    json.activeTool = MapConstants.EDITION_DRAW_CREATION;
                }

                //Activation de l'outil.
                this._activateTool(json, Creation, editionLayer);
            } else {
                alert('La couche "' + editionLayer.title + '" n\'est actuellement pas éditable.');
            }
        } else {
            alert('La couche "' + editionLayer.title + '" n\'est actuellement pas visible.');
        }

    },
    /**
     * Methode: selectForEdition
     * Sélection graphique pour modification d'un objet préalablement choisi dans l'application
     * La couche passe à l'état éditable.
     * Les outils passent à l'état disponible.
     * L'outil de modification désiré est activé.
     * L'objet graphique ayant l'id renseigné est sélectionné et centré sur la carte.
     *
     * Paramètres:
     * (start code)
     * json - {
     *          editionLayer: layer // la couche d'édition. Obligatoire.
     *          objectId:"XXX", // identifiant de l'objet. Obligatoire
     *          activeTool: // identifiant de l'outil de modification à activer.
     *          //Attention, vérifier que l'outil est de type ModificationTool.
     *          //Facultatif (si non présent, l'outil par défaut est Descartes.Map.EDITION_GLOBAL_MODIFICATION)
     *        }
     * (end)
     */
    selectForEdition: function (json) {
        this._checkParameters(json);

        if (this.onlyAppMode) {
            this._disableAllEditionLayers();
            this._deactivateAllToolsInToolBars(json.editionLayer);
        }

        if (_.isNil(json.activeTool)) {
            json.activeTool = MapConstants.EDITION_GLOBAL_MODIFICATION;
        }

        this._select(json);
    },

    /*
     * Méthode : _select
     */
    _select: function (json) {
        var editionLayer = json.editionLayer;

        if (editionLayer.isVisible()) {

            //vérification que la couche n'est pas déjà en cours d'édition et qu'elle est éditable.
            if (editionLayer.isEditable()) {
                editionLayer.enableEdition();
                this.map.mapContent.refreshEditions(editionLayer, true);
                //rafraichissement du gestionnaire de contenu.
                this.map.mapContent.refreshOnlyContentManager();
                //récupération de l'identifiant de l'objet à selectionner.
                var objectId = json.objectId;

                //si un attribut est spécifié par la MOE, on fait une recherche dessus.
                //sinon sur fid et id
                var att = null;
                if (!_.isNil(editionLayer.attributes) &&
                        !_.isNil(editionLayer.attributes.attributeId) &&
                        !_.isNil(editionLayer.attributes.attributeId.fieldName)) {
                    att = editionLayer.attributes.attributeId.fieldName;
                }

                var featureToSelect = editionLayer.getFeatureById(objectId, att);
                if (!_.isNil(featureToSelect)) {
                    featureToSelect.set('selected', true);
                    this.map.OL_map.getView().fit(featureToSelect.getGeometry(), {
                        callback: function () {
                            this._afterPositionning(json, featureToSelect);
                        }.bind(this)
                    });

                } else {
                    alert('Attention, il n\'y a pas d\'objet ayant l\'identifiant ' + objectId);
                }
            } else {
                alert('La couche "' + editionLayer.title + '" n\'est actuellement pas éditable.');
            }
        } else {
            alert('La couche "' + editionLayer.title + '" n\'est actuellement pas visible.');
        }
        return null;
    },

    _afterPositionning: function (json, featureToSelect) {
        var editionLayer = json.editionLayer;
        var interactions = this.map.OL_map.getInteractions();
        interactions.forEach(function (interaction) {
            if (interaction instanceof ol.interaction.Select) {
                interaction.getFeatures().forEach(function (feature) {
                    feature.set('selected', false);
                    var style = Utils.getStyleByState(feature);
                    var olStyles = Symbolizers.getOlStyle(editionLayer.symbolizers[style]);
                    var olStyle = olStyles[feature.getGeometry().getType()];
                    feature.setStyle(olStyle);
                });
                interaction.getFeatures().clear();
            }
        });

        if (_.isNil(this.selectFeature)) {
            this.selectFeature = new ol.interaction.Select({
                //layers: featureToSelect.layer,
                multi: false,
                layers: function (layer) {
                    return layer === featureToSelect.layer;
                }
            });
            this.selectFeature.on('select', function (e) {
                _.each(e.deselected, function (feature) {
                    feature.set('selected', false);

                    var style = Utils.getStyleByState(feature);
                    var olStyles = Symbolizers.getOlStyle(editionLayer.symbolizers[style]);
                    var olStyle = olStyles[feature.getGeometry().getType()];
                    feature.setStyle(olStyle);
                });
                _.each(e.selected, function (feature) {
                    feature.set('selected', true);

                    var olStyles = Symbolizers.getOlStyle(editionLayer.symbolizers['select']);
                    var olStyle = olStyles[feature.getGeometry().getType()];
                    feature.setStyle(olStyle);
                });

                this.setActive(false);
            }.bind(this.selectFeature));

            this.map.OL_map.addInteraction(this.selectFeature);
        } else {
            this.selectFeature.setActive(true);
        }
        if (_.isFunction(json.selectCallback)) {
            this.selectFeature.once('select', function (e) {
                var interaction = e.target;
                _.each(e.selected, function (feature) {
                    setTimeout(function () {
                        json.selectCallback(feature, interaction);
                    }, 500);
                });
            });
        }

        //activation de l'outil.
        var typeTool = Modification;

        if (json.activeTool === MapConstants.EDITION_SELECTION_SUBSTRACT_MODIFICATION) {
            typeTool = Selection;
            editionLayer.OL_layers[0].selectedFeatures = [];
            editionLayer.OL_layers[0].selectedFeatures.push(featureToSelect);
            this.selectFeature.setActive(false);
        } else if (json.activeTool === MapConstants.EDITION_VERTICE_MODIFICATION) {
            this.selectFeature.setActive(false);
        }

        if (json.activeTool === MapConstants.EDITION_AGGREGATION ||
                json.activeTool === MapConstants.EDITION_SPLIT ||
                json.activeTool === MapConstants.EDITION_DIVIDE) {
            typeTool = Creation;
            this.selectFeature.setActive(false);
        }
        this._activateTool(json, typeTool, editionLayer, featureToSelect);

        if ((json.activeTool !== MapConstants.EDITION_SELECTION_SUBSTRACT_MODIFICATION &&
                json.activeTool !== MapConstants.EDITION_VERTICE_MODIFICATION) ||
                json.activeTool === MapConstants.EDITION_AGGREGATION ||
                json.activeTool === MapConstants.EDITION_SPLIT ||
                json.activeTool === MapConstants.EDITION_DIVIDE) {
            this.selectFeature.getFeatures().clear();
            this.selectFeature.getFeatures().push(featureToSelect);
            this.selectFeature.dispatchEvent({
                type: 'select',
                selected: [featureToSelect],
                deselected: []
            });
        }

        return featureToSelect;
    },

    /**
     * Methode: selectForDeletion
     * Demande de suppression d'un objet préalablement choisi dans l'application.
     * La couche passe à l'état éditable.
     * Sélection de l'objet à supprimer.
     * Recadrage et une mise en évidence de l'objet candidat.
     * Une popup s'affiche pour demander la confirmation de la suppression.
     *
     * Paramètres:
     * (start code)
     * json - {
     *   		editionLayer: layer // la couche d'édition. Obligatoire.
     *  		objectId:"XXX", // identifiant de l'objet. Obligatoire
     * }
     * (end)
     */
    selectForDeletion: function (json) {
        var deletionCallback = function (featureToDelete, selectInteraction) {
            if (!_.isNil(featureToDelete)) {
                if (confirm('Voulez-vous supprimer l\'objet selectionné ?')) {
                    var editionLayer = json.editionLayer;
                    editionLayer.removeSelectedFeatures(featureToDelete.getId());
                    selectInteraction.getFeatures().clear();
                }
            }
        };
        this.select(json, deletionCallback);

    },
    /**
     * Methode: select
     * Demande de visualisation d'un objet préalablement choisi dans l'application.
     * La couche passe à l'état éditable.
     * Recadrage et une mise en évidence de l'objet candidat.
     *
     * Paramètres:
     * (start code)
     * json - {
     *   editionLayer: layer // la couche d'édition. Obligatoire.
     *   objectId: "XXX", // identifiant de l'objet. Obligatoire
     * }
     * (end)
     */
    select: function (json, selectCallback) {
        this._checkParameters(json);

        if (this.onlyAppMode) {
            this._disableAllEditionLayers();
        }
        this._deactivateAllToolsInToolBars(json.editionLayer);

        var newJson = {
            editionLayer: json.editionLayer,
            objectId: json.objectId
        };
        if (_.isFunction(selectCallback)) {
            newJson.selectCallback = selectCallback;
        }
        return this._select(newJson);
    },
    /*
     * Méthode privée _checkParameters
     * Vérifie que les paramètres obligatoires sont présent.
     *
     */
    _checkParameters: function (json) {
        if (_.isNil(json)) {
            throw new Error('Aucune propriété définie.');
        }

        var editionLayer = json.editionLayer;
        var objectId = json.objectId;
        if (_.isNil(editionLayer)) {
            throw new Error('La propriété editionLayer n\'est pas définie.');
        }
        if (_.isNil(objectId)) {
            throw new Error('La propriété objectId n\'est pas définie.');
        }

        if (_.isNil(this.map)) {
            throw new Error('La carte doit être renseignée.');
        }

        var found = false;
        var editionLayers = this.map.mapContent.getEditionLayers();
        for (var i = 0; i < editionLayers.length; i++) {
            var anEditionLayer = editionLayers[i];
            if (anEditionLayer === editionLayer) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new Error('Le layer ' + editionLayer + ' n\'existe pas.');
        }
    },
    /*
     * Méthode privée _activateTool
     * Active l'outil s'il existe et si la configuration du layer en cours d'édition le permet.
     */
    _activateTool: function (json, toolType, editionLayer, featureToSelect) {
        var tool = json.activeTool;
        if (!_.isNil(tool)) {
            var editionTool = this._findToolInToolBars(tool, editionLayer);
            if (_.isNil(editionTool)) {
                alert('Attention, aucun outil ' + tool + ' n\'a été trouvé.');
            } else if (editionTool instanceof toolType) {
                var activate = false;
                //il faut vérifier que la configuration de l'outil est compatible avec la couche demandée.
                if (this.globalEditionMode) {
                    if (editionTool.geometryType === null) {
                        activate = true;
                    } else if (editionTool.geometryType === editionLayer.geometryType) {
                        activate = true;
                    }
                } else {
                    //mode individuel
                    if (editionTool.editionLayer === null) {
                        if (editionTool.geometryType === null) {
                            activate = true;
                        } else if (editionTool.geometryType === editionLayer.geometryType) {
                            activate = true;
                        }
                    } else if (editionTool.editionLayer === editionLayer) {
                        activate = true;
                    }
                }
                if (activate) {
                    if (editionTool instanceof Creation) {
                        editionTool.createObjectId = this.createObjectId;
                        editionTool.createAttributes = this.createAttributes;
                    }

                    if (this.onlyAppMode) {
                        editionTool.setAvailable(true);
                    }

                    if (editionTool instanceof SelectionSubstractModification) {
                        editionTool.featureToModify = editionLayer.getSelectedFeatures()[0];
                        editionTool.updateStateWithLayer({data: [editionLayer]});
                    }

                    if (editionTool instanceof AggregationCreation ||
                            editionTool instanceof SplitCreation ||
                            editionTool instanceof DivideCreation) {
                        if (editionLayer.getSelectedFeatures().length === 1) {
                            // mode modification
                            editionTool.createObjectId = null;
                            editionTool.createAttributes = null;
                            editionTool.featureToModify = editionLayer.getSelectedFeatures()[0];
                            editionTool.updateStateWithLayer({data: [editionLayer]});
                        }
                    }

                    //On peut passer le centre d'homothétie depuis l'applications via 'optionsTool'
                    if (editionTool instanceof HomotheticCreation) {
                        editionTool.createObjectId = this.createObjectId;
                        editionTool.createAttributes = this.createAttributes;
                        if (json.optionsTool) {
                            editionTool.appCenter = json.optionsTool.homotheticCenter;
                        } else {
                            editionTool.appCenter = null;
                        }
                    }

                    editionTool.handleClick();

                    if (editionTool instanceof GlobalModification) {
                        editionTool.interaction.select(featureToSelect);
                    } else if (editionTool instanceof VerticeModification) {
                        var interaction = editionTool.interaction;
                        interaction.getFeatures().clear();
                        interaction.getFeatures().push(featureToSelect);
                        interaction.dispatchEvent({
                            type: 'select',
                            selected: [featureToSelect],
                            deselected: []
                        });
                    }

                } else {
                    alert(tool + ' ne peut être activé.');
                }
            } else {
                alert(tool + ' n\'est pas du type ' + toolType.prototype.CLASS_NAME);
            }
        }
    },
    /*
     * Méthode _findToolInToolBars
     *
     * Arguments:
     * tool - the tool to find
     */
    _findToolInToolBars: function (tool, editionLayer) {
        var toolClass = this.map.defaultTools[tool];

        for (var i = 0; i < this.map.toolBars.length; i++) {
            var toolBar = this.map.toolBars[i];
            var controls = toolBar.controls;
            for (var j = 0; j < controls.length; j++) {
                var aControl = controls[j];
                if (this.onlyAppMode) {
                    aControl.editionLayer = editionLayer;
                }
                if (aControl.CLASS_NAME === toolClass.prototype.CLASS_NAME &&
                        aControl.editionLayer === editionLayer) {
                    if (this.onlyAppMode && (aControl instanceof GlobalModification || aControl instanceof VerticeModification || aControl instanceof DrawCreation)) {
                        aControl._updateOLFeature(editionLayer.getFeatureOL_layers()[0]);
                    }

                    return aControl;
                }
            }
        }
        return null;
    },

    /*
     * Méthode _disableAllEditionLayers
     *
     */
    _disableAllEditionLayers: function () {
        var editionLayers = this.map.mapContent.getEditionLayers();
        for (var i = 0; i < editionLayers.length; i++) {
            editionLayers[i].disableEdition();
        }
        this.map.mapContent.refreshOnlyContentManager();
    },

    /*
     * Méthode _deactivateAllToolsInToolBars
     *
     */
    _deactivateAllToolsInToolBars: function () {
        if (!_.isNil(this.map)) {
            var others = this.map.OL_map.getControls().getArray();
            _.each(others, function (control) {
                if (control.deactivate) {
                    control.deactivate(true, false);
                }
            });
        }
    },
    toJSON: function () {
        return {
            autoSave: this.autoSave,
            globalEditionMode: this.globalEditionMode,
            onlyAppMode: this.onlyAppMode,
            verifAppConformity: this.verifAppConformity,
            verifTopoConformity: this.verifTopoConformity
        };
    },
    CLASS_NAME: 'Descartes.EditionManager'
};

module.exports = EditionManager;

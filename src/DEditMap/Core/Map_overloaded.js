﻿/* global MODE, Descartes */

var Utils = Descartes.Utils;
var _ = require('lodash');
var ol = require('openlayers');

var EditionManager = require('./EditionManager');
var MapConstants = Descartes.Map;
var MapContentManagerConstants = Descartes.Action.MapContentManager;
var MapContentManager = require('../Action/MapContentManager_overloaded');
var ToolBar = Descartes.ToolBar;

var EditionToolBar = require('../Toolbar/EditionToolBar');
var ToolBarConstants = require('../Toolbar/ToolBarConstants');
var CenterToCoordinates = Descartes.Button.CenterToCoordinates;
var ToolBarOpener = require('../Button/ToolBarOpener_overloaded');
var ExportPNG = Descartes.Button.ExportPNG;
var ExportPDF = Descartes.Button.ExportPDF;
var ShareLinkMap = Descartes.Button.ShareLinkMap;
var DisplayLayersTreeSimple = Descartes.Button.DisplayLayersTreeSimple;
var ExportVectorLayer = require('../Button/ContentTask/' + MODE + '/ExportVectorLayer_overloaded');

var CoordinatesInput = Descartes.Action.CoordinatesInput;
var ScaleSelector = Descartes.Action.ScaleSelector;
var ScaleChooser = Descartes.Action.ScaleChooser;
var SizeSelector = Descartes.Action.SizeSelector;
var PrinterParamsManager = Descartes.Action.PrinterParamsManager;
var AttributesEditor = require('../Action/AttributesEditor');

var GraphicScale = Descartes.Info.GraphicScale;
var MetricScale = Descartes.Info.MetricScale;
var MapDimensions = Descartes.Info.MapDimensions;
var LocalizedMousePosition = Descartes.Info.LocalizedMousePosition;
var Legend = Descartes.Info.Legend;
var Attribution = Descartes.Info.Attribution;

var EditionLayer = require('../Model/EditionLayer');
var LayerConstants = Descartes.Layer;

var CenterMap = Descartes.Tool.CenterMap;
var ZoomToInitialExtent = Descartes.Tool.ZoomToInitialExtent;
var ZoomToMaximalExtent = Descartes.Tool.ZoomToMaximalExtent;
var ZoomIn = Descartes.Tool.ZoomIn;
var ZoomOut = Descartes.Tool.ZoomOut;
var DragPan = Descartes.Tool.DragPan;
var GeolocationSimple = Descartes.Tool.Geolocation.GeolocationSimple;
var GeolocationTracking = Descartes.Tool.Geolocation.GeolocationTracking;
var NavigationHistory = Descartes.Tool.NavigationHistory;
var MeasureDistance = Descartes.Tool.Measure.MeasureDistance;
var MeasureArea = Descartes.Tool.Measure.MeasureArea;
var PointSelection = Descartes.Tool.Selection.PointSelection;
var PointRadiusSelection = Descartes.Tool.Selection.PointRadiusSelection;
var CircleSelection = Descartes.Tool.Selection.CircleSelection;
var PolygonSelection = Descartes.Tool.Selection.PolygonSelection;
var PolygonBufferHaloSelection = Descartes.Tool.Selection.PolygonBufferHaloSelection;
var LineBufferHaloSelection = Descartes.Tool.Selection.LineBufferHaloSelection;
var RectangleSelection = Descartes.Tool.Selection.RectangleSelection;

var EditionSelection = require('../Tool/Edition/' + MODE + '/Selection');
var CompositeSelection = require('../Tool/Edition/CompositeSelection');
var Save = require('../Tool/Edition/Save');
var DrawCreation = require('../Tool/Edition/Creation/DrawCreation');
var DrawAnnotation = require('../Tool/Edition/Annotation/DrawAnnotation');
var TextAnnotation = require('../Tool/Edition/Annotation/TextAnnotation');
var AddTextAnnotation = require('../Tool/Edition/Annotation/AddTextAnnotation');
var StyleAnnotation = require('../Tool/Edition/Annotation/StyleAnnotation');
var ArrowAnnotation = require('../Tool/Edition/Annotation/ArrowAnnotation');
var FreehandAnnotation = require('../Tool/Edition/Annotation/FreehandAnnotation');
var BufferHaloAnnotation = require('../Tool/Edition/Annotation/BufferHaloAnnotation');
var AideAnnotation = require('../Tool/Edition/Annotation/AideAnnotation');
var ImportAnnotation = require('../Tool/Edition/Annotation/ImportAnnotation');
var ExportAnnotation = require('../Tool/Edition/Annotation/ExportAnnotation');
var AttributeAnnotation = require('../Tool/Edition/Annotation/AttributeAnnotation');
var GeolocationSimpleAnnotation = require('../Tool/Edition/Annotation/GeolocationSimpleAnnotation');
var GeolocationTrackingAnnotation = require('../Tool/Edition/Annotation/GeolocationTrackingAnnotation');
var SnappingAnnotation = require('../Tool/Edition/Annotation/SnappingAnnotation');
var GlobalModificationAnnotation = require('../Tool/Edition/Annotation/GlobalModificationAnnotation');
var VerticeModificationAnnotation = require('../Tool/Edition/Annotation/VerticeModificationAnnotation');
var RubberAnnotation = require('../Tool/Edition/Annotation/RubberAnnotation');
var EraseAnnotation = require('../Tool/Edition/Annotation/EraseAnnotation');
var CopyCreation = require('../Tool/Edition/Creation/CopyCreation');
var CloneCreation = require('../Tool/Edition/Creation/CloneCreation');
var AggregationCreation = require('../Tool/Edition/Creation/AggregationCreation');
var UnAggregationCreation = require('../Tool/Edition/Creation/UnAggregationCreation');
var BufferNormalCreation = require('../Tool/Edition/Creation/BufferNormalCreation');
var BufferHaloCreation = require('../Tool/Edition/Creation/BufferHaloCreation');
var HomotheticCreation = require('../Tool/Edition/Creation/HomotheticCreation');
var DivideCreation = require('../Tool/Edition/Creation/DivideCreation');
var SplitCreation = require('../Tool/Edition/Creation/SplitCreation');

var GlobalModification = require('../Tool/Edition/Modification/GlobalModification');
var CompositeGlobalModification = require('../Tool/Edition/Modification/CompositeGlobalModification');
var TranslateModification = require('../Tool/Edition/Modification/TranslateModification');
var VerticeModification = require('../Tool/Edition/Modification/VerticeModification');
var MergeModification = require('../Tool/Edition/Modification/MergeModification');
var SelectionSubstractModification = require('../Tool/Edition/Modification/SelectionSubstractModification');

var Attribute = require('../Tool/Edition/Attribute/Attribute');
var SelectionAttribut = require('../Tool/Edition/Attribute/SelectionAttribute');

var RubberDeletion = require('../Tool/Edition/Deletion/RubberDeletion');
var SelectionDeletion = require('../Tool/Edition/Deletion/SelectionDeletion');
var CompositeRubberDeletion = require('../Tool/Edition/Deletion/CompositeRubberDeletion');

var IntersectInformation = require('../Tool/Edition/Information/' + MODE + '/IntersectInformation');

var ResultLayer = Descartes.Layer.ResultLayer;

var Map = Descartes.Map;

/**
 * Class: Descartes.Map
 * Classe abstraite *principale* de la librairie Descartes permettant d'associer une carte OpenLayers à un ensemble d'outils, de gestionnaires, d'informations.
 *
 * Classes dérivées:
 * - <Descartes.Map.ContinuousScalesMap>
 * - <Descartes.Map.DiscreteScalesMap>
 *
 * Description générale:
 * Pour la librairie Descartes, une carte est constituée :
 * - d'un *contenu*, défini par la classe <Descartes.MapContent>.
 * - d'une *carte OpenLayers*, alimentée par des couches OpenLayers fournies par le contenu.
 * - de *contrôles* permettant de piloter la carte selon les principes propres à OpenLayers.
 * - d'*actions* permettant de piloter la carte suite à des saisies effectuées hors de la carte. Elles sont définies par des classes dérivées de la classe <Descartes.Action>.
 * - de *zones informatives* automatiquement actualisées selon le contexte courant de la carte. Elles sont définies par des classes dérivées de la classe <Descartes.Info>.
 * - d'*outils* permettant de piloter la carte suite à des interactions sur celle-ci. Ils sont définis par des classes dérivées des classe <Descartes.Button> et <Descartes.Tool>.
 *
 * :
 * Le fonctionnement de la plupart des contrôles, actions, zones informatives et outils est relativement simple, tant pour l'utilisateur final que pour le développeur.
 *
 * :
 * Il est en conséquence aisé de les associer à la carte, grâce à des méthodes génériques :
 * - <addOpenLayersInteraction> et <addOpenLayersInteraction> pour les intéractions
 * - <addAction> et <addActions> pour les actions
 * - <addInfo> et <addInfos> pour les zones informatives
 * - <addToolInToolBar> et <addToolsInToolBar> pour les outils
 *
 * :
 * Des listes prédéfinies d'éléments disponibles pour ces méthodes facilitent leur utilisation.
 *
 * :
 * D'autres éléments, dont la manipulation est plus complexe, bénéficient de méthodes dédiées pour les associer à la carte :
 *
 * :
 * Barre d'outils - Instance de la classe <Descartes.ToolBar> ajoutée grâce à la méthode <addToolBar>
 * Gestionnaire de contenu - Instance de la classe <Descartes.Action.MapContentManager> ajouté grâce à la méthode <addContentManager>
 * Mini-carte de navigation - Instance de la classe <Descartes.Tool.MiniMap> ajoutée grâce à la méthode <addMiniMap>
 * "Rose des vents" de navigation - Instance de la classe <Descartes.Button.DirectionalPanPanel> ajoutée grâce à la méthode <addDirectionalPanPanel>
 * Gestionnaire d'info-bulles - Instance de la classe <Descartes.Info.ToolTip> ajouté grâce à la méthode <addToolTip>
 * Gestionnaire de requêtes attributaires - Instance de la classe <Descartes.ToolBar> ajouté grâce à la méthode <addRequestManager>
 * Gestionnaire de localisation rapide - Instance de la classe <Descartes.Action.Gazetteer> ajouté grâce à la méthode <addGazetteer>
 * Gestionnaire de localisation rapide administratif - Instance de la classe <Descartes.Action.DefaultGazetteer> ajouté grâce à la méthode <addDefaultGazetteer>
 * Gestionnaire de vues personnalisées - Instance de la classe <Descartes.Action.BookmarksManager> ajouté grâce à la méthode <addBookmarksManager>
 *
 * Contenu de la carte:
 * Il s'agit de couches éventuellement hiérarchisées en groupes, sous-groupes, sous-sous-groupes, etc.
 * - une couche est définie par la classe <Descartes.Layer>
 * - un groupe est défini par la classe <Descartes.Group>
 *
 * Intéraction OpenLayers disponibles (voir <OL_INTERACTIONS_NAME>) par défaut:
 * - ol.interaction.DragRotate pour le type <Descartes.Map.OL_DRAG_ROTATE>
 * - ol.interaction.DoubleClickZoom pour le type <Descartes.Map.OL_DOUBLE_CLICK_ZOOM>
 * - ol.interaction.DragPan pour le type <Descartes.Map.OL_DRAG_PAN>
 * - ol.interaction.PinchRotate pour le type <Descartes.Map.OL_PINCH_ROTATE>
 * - ol.interaction.PinchZoom pour le type <Descartes.Map.OL_PINCH_ZOOM>
 * - ol.interaction.KeyboardPan pour le type <Descartes.Map.OL_KEYBOARD_PAN>
 * - ol.interaction.KeyboardZoom pour le type <Descartes.Map.OL_KEYBOARD_ZOOM>
 * - ol.interaction.MouseWheelZoom pour le type <Descartes.Map.OL_MOUSE_WHEEL_ZOOM>
 * - ol.interaction.DragZoom pour le type <Descartes.Map.OL_DRAG_ZOOM>
 *
 * Actions disponibles (voir <ACTIONS_NAME>) par défaut:
 * - <Descartes.Action.AttributesEditor> pour le type <Descartes.Map.ATTRIBUTES_EDIT_ACTION>
 * - <Descartes.Action.CoordinatesInput> pour le type <Descartes.Map.COORDS_INPUT_ACTION>
 * - <Descartes.Action.ScaleChooser> pour le type <Descartes.Map.SCALE_CHOOSER_ACTION>
 * - <Descartes.Action.ScaleSelector> pour le type <Descartes.Map.SCALE_SELECTOR_ACTION>
 * - <Descartes.Action.SizeSelector> pour le type <Descartes.Map.SCALE_SELECTOR_ACTION>
 * - <Descartes.Action.PrinterParamsManager> pour le type <Descartes.Map.PRINTER_SETUP_ACTION>
 *
 * Zones informatives disponibles (voir <INFOS_NAME>) par défaut:
 * - <Descartes.Info.GraphicScale> pour le type <Descartes.Map.GRAPHIC_SCALE_INFO>
 * - <Descartes.Info.MetricScale> pour le type <Descartes.Map.METRIC_SCALE_INFO>
 * - <Descartes.Info.MapDimensions> pour le type <Descartes.Map.MAP_DIMENSIONS_INFO>
 * - <Descartes.Info.LocalizedMousePosition> pour le type <Descartes.Map.MOUSE_POSITION_INFO>
 * - <Descartes.Info.Legend> pour le type <Descartes.Map.LEGEND_INFO>
 * - <Descartes.Info.Attribution> pour le type <Descartes.Map.ATTRIBUTION_INFO>
 *
 * Outils disponibles (voir <TOOLS_NAME>) par défaut:
 * - <Descartes.Button.ZoomToInitialExtent> pour le type <Descartes.Map.MAXIMAL_EXTENT>
 * - <Descartes.Button.ZoomToMaximalExtent> pour le type <Descartes.Map.INITIAL_EXTENT>
 * - <Descartes.Tool.DragPan> pour le type <Descartes.Map.DRAG_PAN>
 * - <Descartes.Tool.GeolocationSimple> pour le type <Descartes.Map.GEOLOCATION_SIMPLE>
 * - <Descartes.Tool.GeolocationTracking> pour le type <Descartes.Map.GEOLOCATION_TRACKING>
 * - <Descartes.Tool.ZoomIn> pour le type <Descartes.Map.ZOOM_IN>
 * - <Descartes.Tool.ZoomOut> pour le type <Descartes.Map.ZOOM_OUT>
 * - <Descartes.Tool.NavigationHistory> pour le type <Descartes.Map.NAV_HISTORY>
 * - <Descartes.Tool.CenterMap> pour le type <Descartes.Map.CENTER_MAP>
 * - <Descartes.Button.CenterToCoordinates> pour le type <Descartes.Map.COORDS_CENTER>
 * - <Descartes.Tool.Measure.MeasureDistance> pour le type <Descartes.Map.DISTANCE_MEASURE>
 * - <Descartes.Tool.Measure.MeasureArea> pour le type <Descartes.Map.AREA_MEASURE>
 * - <Descartes.Button.ExportPDF> pour le type <Descartes.Map.PDF_EXPORT>
 * - <Descartes.Button.ExportPNG> pour le type <Descartes.Map.PNG_EXPORT>
 * - <Descartes.Button.ShareLinkMap> pour le type <Descartes.Map.SHARE_LINK_MAP>
 * - <Descartes.Button.DisplayLayersTreeSimple> pour le type <Descartes.Map.DISPLAY_LAYERSTREE_SIMPLE>
 * - <Descartes.Tool.Selection.PointSelection> pour le type <Descartes.Map.POINT_SELECTION>
 * - <Descartes.Tool.Selection.PointRadiusSelection> pour le type <Descartes.Map.POINT_RADIUS_SELECTION>
 * - <Descartes.Tool.Selection.CircleSelection> pour le type <Descartes.Map.CIRCLE_SELECTION>
 * - <Descartes.Tool.Selection.PolygonSelection> pour le type <Descartes.Map.POLYGON_SELECTION>
 * - <Descartes.Tool.Selection.PolygonBufferHaloSelection> pour le type <Descartes.Map.POLYGON_BUFFER_HALO_SELECTION>
 * - <Descartes.Tool.Selection.LineBufferHaloSelection> pour le type <Descartes.Map.LINE_BUFFER_HALO_SELECTION>
 * - <Descartes.Tool.Selection.RectangleSelection> pour le type <Descartes.Map.RECTANGLE_SELECTION>
 * - <Descartes.Button.ToolBarOpener> pour le type <Descartes.Map.TOOLBAR_OPENER>
 * - <Descartes.Tool.Edition.Save> pour le type <Descartes.Map.EDITION_SAVE>
 * - <Descartes.Tool.Edition.Selection> pour le type <Descartes.Map.EDITION_SELECTION>
 * - <Descartes.Tool.Edition.CompositeSelection> pour le type <Descartes.Map.EDITION_COMPOSITE_SELECTION>
 * - <Descartes.Tool.Edition.Attribute> pour le type <Descartes.Map.EDITION_ATTRIBUTE>
 * - <Descartes.Tool.Edition.SelectionAttribut> pour le type <Descartes.Map.EDITION_SELECTION_ATTRIBUT>
 * - <Descartes.Tool.Edition.Creation.DrawCreation> pour le type <Descartes.Map.EDITION_DRAW_CREATION>
 * - <Descartes.Tool.Edition.Creation.DrawAnnotation> pour le type <Descartes.Map.EDITION_DRAW_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.TextAnnotation> pour le type <Descartes.Map.EDITION_TEXT_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.ArrowAnnotation> pour le type <Descartes.Map.EDITION_ARROW_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.FreehandAnnotation> pour le type <Descartes.Map.EDITION_FREEHAND_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.BufferHaloAnnotation> pour le type <Descartes.Map.EDITION_BUFFER_HALO_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.GeolocationSimpleAnnotation> pour le type <Descartes.Map.EDITION_GEOLOCATION_SIMPLE_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.GeolocationTrackingAnnotation> pour le type <Descartes.Map.EDITION_GEOLOCATION_TRACKING_ANNOTATION>
 * - <Descartes.Tool.Edition.Creation.CopyCreation> pour le type <Descartes.Map.EDITION_COPY_CREATION>
 * - <Descartes.Tool.Edition.Creation.CloneCreation> pour le type <Descartes.Map.EDITION_CLONE_CREATION>
 * - <Descartes.Tool.Edition.Creation.AggregationCreation> pour le type <Descartes.Map.EDITION_AGGREGATION>
 * - <Descartes.Tool.Edition.Creation.UnAggregationCreation> pour le type <Descartes.Map.EDITION_UNAGGREGATION_CREATION>
 * - <Descartes.Tool.Edition.Creation.BufferHaloCreation> pour le type <Descartes.Map.EDITION_BUFFER_HALO_CREATION>
 * - <Descartes.Tool.Edition.Creation.BufferNormalCreation> pour le type <Descartes.Map.EDITION_BUFFER_NORMAL_CREATION>
 * - <Descartes.Tool.Edition.Creation.HomotheticCreation> pour le type <Descartes.Map.EDITION_HOMOTHETIC_CREATION>
 * - <Descartes.Tool.Edition.Creation.SplitCreation> pour le type <Descartes.Map.EDITION_SPLIT>
 * - <Descartes.Tool.Edition.Creation.DivideCreation> pour le type <Descartes.Map.EDITION_DIVIDE>
 * - <Descartes.Tool.Edition.Modification.TranslateModification> pour le type <Descartes.Map.EDITION_TRANSLATE_MODIFICATION>
 * - <Descartes.Tool.Edition.Modification.GlobalModification> pour le type <Descartes.Map.EDITION_GLOBAL_MODIFICATION>
 * - <Descartes.Tool.Edition.Modification.CompositeGlobalModification> pour le type <Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION>
 * - <Descartes.Tool.Edition.Modification.VerticeModification> pour le type <Descartes.Map.EDITION_VERTICE_MODIFICATION>
 * - <Descartes.Tool.Edition.Modification.MergeModification> pour le type <Descartes.Map.EDITION_MERGE_MODIFICATION>
 * - <Descartes.Tool.Edition.Modification.SelectionSubstractModification> pour le type <Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION>
 * - <Descartes.Tool.Edition.Modification.GlobalModificationAnnotation> pour le type <Descartes.Map.EDITION_GLOBAL_MODIFICATION_ANNOTATION>
 * - <Descartes.Tool.Edition.Modification.VerticeModificationAnnotation> pour le type <Descartes.Map.EDITION_VERTICE_MODIFICATION_ANNOTATION>
 * - <Descartes.Tool.Edition.Modification.AddTextAnnotation> pour le type <Descartes.Map.EDITION_ADD_TEXT_ANNOTATION>
 * - <Descartes.Tool.Edition.Modification.StyleAnnotation> pour le type <Descartes.Map.EDITION_STYLE_ANNOTATION>
 * - <Descartes.Tool.Edition.Deletion.RubberDeletion> pour le type <Descartes.Map.EDITION_RUBBER_DELETION>
 * - <Descartes.Tool.Edition.Deletion.CompositeRubberDeletion> pour le type <Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION>
 * - <Descartes.Tool.Edition.Deletion.SelectionDeletion> pour le type <Descartes.Map.EDITION_SELECTION_DELETION>
 * - <Descartes.Tool.Edition.Deletion.RubberAnnotation> pour le type <Descartes.Map.EDITION_RUBBER_ANNOTATION>
 * - <Descartes.Tool.Edition.Deletion.EraseAnnotation> pour le type <Descartes.Map.EDITION_ERASE_ANNOTATION>
 * - <Descartes.Tool.Edition.Information.IntersectInformation> pour le type <Descartes.Map.EDITION_INTERSECT_INFORMATION>
 * - <Descartes.Tool.Edition.ExportAnnotation> pour le type <Descartes.Map.EDITION_EXPORT_ANNOTATION>
 * - <Descartes.Tool.Edition.ImportAnnotation> pour le type <Descartes.Map.EDITION_IMPORT_ANNOTATION>
 * - <Descartes.Tool.Edition.AttributeAnnotation> pour le type <Descartes.Map.EDITION_ATTRIBUTE_ANNOTATION>
 * - <Descartes.Tool.Edition.SnappingAnnotation> pour le type <Descartes.Map.EDITION_SNAPPING_ANNOTATION>
 * - <Descartes.Tool.Edition.AideAnnotation> pour le type <Descartes.Map.EDITION_AIDE_ANNOTATION>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'moveend' de la classe ol.Map déclenche la méthode <refreshContentManager>.
 *  - l'événement 'changed' de la classe <Descartes.MapContent> déclenche la méthode <refresh>.
 *  - l'événement 'layerRemoved' de la classe <Descartes.MapContent> déclenche la méthode <refreshRequestManager>, si un gestionnaire de requêtes est associé à la carte.
 *  - l'événement 'layerRemoved' de la classe <Descartes.MapContent> déclenche la méthode <refreshToolTip>, si un gestionnaire d'info-bulle est associé à la carte.
 */
var Class = Utils.Class(Map, {

    /**
     * Constructeur: Descartes.Map
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la carte OpenLayers ou identifiant de cet élément.
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction de la classe:
     * initExtent - {Array} Emprise initiale de visualisation de la carte.
     * maxExtent - {Array} Emprise maximale de visualisation de la carte.
     * projection - {String} Code de la projection de la carte.
     * size - {Size} Taille initiale de la carte.
     * autoSize - {Boolean} Taille de la carte automatique par rapport au conteneur parent.
     * autoScreenSize - {Boolean} Taille de la carte prenant tout l'écran.
     */
    initialize: function (div, mapContent, options) {
        console.log("DEditMap - MAP");
        this.defaultInfos = {
            GraphicScale: GraphicScale,
            MetricScale: MetricScale,
            MapDimensions: MapDimensions,
            LocalizedMousePosition: LocalizedMousePosition,
            Legend: Legend,
            Attribution: Attribution
        };
        this.defaultActions = {
            AttributesEditor: AttributesEditor,
            CoordinatesInput: CoordinatesInput,
            ScaleChooser: ScaleChooser,
            ScaleSelector: ScaleSelector,
            SizeSelector: SizeSelector,
            PrinterParamsManager: PrinterParamsManager
        };
        this.defaultTools = {
            ZoomToInitialExtent: ZoomToInitialExtent,
            ZoomToMaximalExtent: ZoomToMaximalExtent,
            DragPan: DragPan,
            GeolocationSimple: GeolocationSimple,
            GeolocationTracking: GeolocationTracking,
            ZoomIn: ZoomIn,
            ZoomOut: ZoomOut,
            NavigationHistory: NavigationHistory,
            CenterMap: CenterMap,
            CenterToCoordinates: CenterToCoordinates,
            MeasureDistance: MeasureDistance,
            MeasureArea: MeasureArea,
            ExportPDF: ExportPDF,
            ExportPNG: ExportPNG,
            ShareLinkMap: ShareLinkMap,
            DisplayLayersTreeSimple: DisplayLayersTreeSimple,
            ExportVectorLayer: ExportVectorLayer,
            PointSelection: PointSelection,
            PointRadiusSelection: PointRadiusSelection,
            CircleSelection: CircleSelection,
            PolygonSelection: PolygonSelection,
            PolygonBufferHaloSelection: PolygonBufferHaloSelection,
            LineBufferHaloSelection: LineBufferHaloSelection,
            RectangleSelection: RectangleSelection,
            ToolBarOpener: ToolBarOpener,
            EditionSave: Save,
            EditionSelection: EditionSelection,
            EditionCompositeSelection: CompositeSelection,
            EditionAttribute: Attribute,
            EditionSelectionAttribut: SelectionAttribut,
            EditionDrawCreation: DrawCreation,
            EditionDrawAnnotation: DrawAnnotation,
            EditionTextAnnotation: TextAnnotation,
            EditionArrowAnnotation: ArrowAnnotation,
            EditionFreehandAnnotation: FreehandAnnotation,
            EditionBufferHaloAnnotation: BufferHaloAnnotation,
            EditionAddTextAnnotation: AddTextAnnotation,
            EditionStyleAnnotation: StyleAnnotation,
            EditionAideAnnotation: AideAnnotation,
            EditionImportAnnotation: ImportAnnotation,
            EditionExportAnnotation: ExportAnnotation,
            EditionAttributeAnnotation: AttributeAnnotation,
            EditionGlobalModificationAnnotation: GlobalModificationAnnotation,
            EditionVerticeModificationAnnotation: VerticeModificationAnnotation,
            EditionRubberAnnotation: RubberAnnotation,
            EditionEraseAnnotation: EraseAnnotation,
            EditionSnappingAnnotation: SnappingAnnotation,
            EditionGeolocationSimpleAnnotation: GeolocationSimpleAnnotation,
            EditionGeolocationTrackingAnnotation: GeolocationTrackingAnnotation,
            EditionCloneCreation: CloneCreation,
            EditionCopyCreation: CopyCreation,
            EditionAggregation: AggregationCreation,
            EditionUnAggregationCreation: UnAggregationCreation,
            EditionBufferHaloCreation: BufferHaloCreation,
            EditionBufferNormalCreation: BufferNormalCreation,
            EditionSplit: SplitCreation,
            EditionDivide: DivideCreation,
            EditionHomotheticCreation: HomotheticCreation,
            EditionGlobalModification: GlobalModification,
            EditionTranslateModification: TranslateModification,
            EditionCompositeGlobalModification: CompositeGlobalModification,
            EditionVerticeModification: VerticeModification,
            EditionMergeModification: MergeModification,
            EditionSelectionSubstractModification: SelectionSubstractModification,
            EditionRubberDeletion: RubberDeletion,
            EditionCompositeRubberDeletion: CompositeRubberDeletion,
            EditionSelectionDeletion: SelectionDeletion,
            EditionIntersectInformation: IntersectInformation
        };
        this.defaultOlInteractions = {
            DragRotate: ol.interaction.DragRotate,
            DoubleClickZoom: ol.interaction.DoubleClickZoom,
            DragPan: ol.interaction.DragPan,
            PinchRotate: ol.interaction.PinchRotate,
            PinchZoom: ol.interaction.PinchZoom,
            KeyboardPan: ol.interaction.KeyboardPan,
            KeyboardZoom: ol.interaction.KeyboardZoom,
            MouseWheelZoom: ol.interaction.MouseWheelZoom,
            DragZoom: ol.interaction.DragZoom,
            DragBox: ol.interaction.DragBox,
            DragRotateAndZoom: ol.interaction.DragRotateAndZoom
        };
        this.defaultOlControls = {
            Attribution: ol.control.Attribution,
            FullScreen: ol.control.FullScreen,
            MousePosition: ol.control.MousePosition,
            OverviewMap: ol.control.OverviewMap,
            Rotate: ol.control.Rotate,
            ScaleLine: ol.control.ScaleLine,
            Zoom: ol.control.Zoom,
            ZoomSlider: ol.control.ZoomSlider,
            ZoomToExtent: ol.control.ZoomToExtent
        };

        this.mapContent = mapContent;
        //initialisation de la map pour le manager d'édition
        EditionManager.map = this;
        if (!_.isNil(options)) {
            _.extend(this, options);
        }

        /*if (this.CLASS_NAME === 'Descartes.Map.ContinuousScalesMap') {
         this.maxExtent = Utils.extendBounds(this.maxExtent, this.size);
         }*/
        this.initExtent = (this.initExtent !== null) ? this.initExtent : this.maxExtent;
        this.createOlMap();
        this.OL_map.setTarget(div);
        this.mapContent.events.register('changed', this, this.refresh);
        this.OL_map.on('movestart', function (e) {
            var mapZoom = this.OL_map.getView().getZoomForResolution(e.frameState.viewState.resolution);
            this.OL_map.once('moveend', function (evt) {
                var currentZoom = this.OL_map.getView().getZoom();
                if (currentZoom !== mapZoom) {
                    this.refreshContentManager();
                }
            }.bind(this), this);
        }.bind(this), this);
    },
    _addToolInToolBar: function (toolBar, tool) {
        var tools = [];
        var toolInstance = null;
        var ToolClassName = null;
        if (typeof tool.type === 'string') {
            if (MapConstants.TOOLS_NAME.indexOf(tool.type) !== -1) {
                ToolClassName = this.defaultTools[tool.type];
            }
        } else if (typeof tool.type === 'function') {
            ToolClassName = tool.type;
        } else {
            toolInstance = tool;
            this.OL_map.removeControl(toolInstance);
            if (toolInstance.setTarget) {
                toolInstance.setTarget(toolBar.id);
            }
        }
        if (!_.isNil(ToolClassName)) {
            var args = [];
            var argsTools = null;
            if (tool.args instanceof Object && _.isNil(tool.args.CLASS_NAME)) {
                argsTools = _.extend({}, tool.args);
            } else {
                argsTools = tool.args;
            }
            if (!_.isNil(argsTools)) {
                if (!_.isArray(argsTools)) {
                    args = [argsTools];
                } else {
                    args = argsTools;
                }
            }

            args.push({
                target: document.getElementById(toolBar.id)
            });

            if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Tool.NavigationHistory') {
                var navigationHistoryToolPrevious = Object.create(ToolClassName.prototype);
                var navigationHistoryToolNext = Object.create(ToolClassName.prototype);

                var prevArguments = [{}, false];
                var nextArguments = [{}, true];

                if (args.length === 1) {
                    prevArguments[0] = args[0];
                    nextArguments[0] = args[0];
                } else if (args.length === 2) {
                    if (args[0].previousOptions) {
                        prevArguments[0] = args[0].previousOptions;
                    }
                    if (args[0].nextOptions) {
                        nextArguments[0] = args[0].nextOptions;
                    }
                    _.extend(prevArguments[0], args[1]);
                    _.extend(nextArguments[0], args[1]);
                }

                ToolClassName.apply(navigationHistoryToolPrevious, prevArguments);
                ToolClassName.apply(navigationHistoryToolNext, nextArguments);

                tools.push(navigationHistoryToolPrevious);
                tools.push(navigationHistoryToolNext);

                toolInstance = [navigationHistoryToolPrevious, navigationHistoryToolNext];
            } else if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Tool.ZoomToMaximalExtent') {
                toolInstance = Object.create(ToolClassName.prototype);
                args[0].maxExtent = this.maxExtent;
                ToolClassName.apply(toolInstance, args);
            } else if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Tool.ZoomToInitialExtent') {
                toolInstance = Object.create(ToolClassName.prototype);
                var bounds = args[0];
                args[0] = {};
                args[0].bounds = bounds;
                args[0].tooloptions = tool.options;
                ToolClassName.apply(toolInstance, args);
            } else if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Button.DisplayLayersTreeSimple') {
                toolInstance = Object.create(ToolClassName.prototype);
                ToolClassName.apply(toolInstance, args);
                this._toolsWithMapcontentManager.push(toolInstance);
            } else {
                toolInstance = Object.create(ToolClassName.prototype);
                ToolClassName.apply(toolInstance, args);
            }
        }
        if (!_.isNil(toolInstance)) {
            if (MapConstants._TOOLS_WITH_MAPCONTENT.indexOf(toolInstance.CLASS_NAME) !== -1) {
                if (!_.isNil(toolInstance.setMapContent)) {
                    toolInstance.setMapContent(this.mapContent);
                }
                tools.push(toolInstance);
            } else if (toolInstance.CLASS_NAME === 'Descartes.Button.ToolBarOpener') {
                toolInstance._initToolBarOpener(this);
                tools.push(toolInstance);
            } else {
                tools.push(toolInstance);
            }
            toolBar.addControls(tools);
            //toolBar.controls = toolBar.controls.concat(tools);
        }
        return toolInstance;
    },
    addContentManager: function (div, toolsType, contentManagerOptions) {

        var options = _.extend({}, contentManagerOptions);
        if ((!this.mapContent.editable || !this.mapContent.fctContextMenu) && options !== undefined) {
            if (options.contextMenuTools !== undefined && options.contextMenuTools !== null) {
                delete options.contextMenuTools;
            }
            if (options.contextMenuFct !== undefined && options.contextMenuFct !== null) {
                delete options.contextMenuFct;
            }
        }
        this.mapContentManager = new MapContentManager(div, this.mapContent, options);
        if (this.mapContent.fctContextMenu && this.mapContentManager.contextMenuTools && this.mapContentManager.contextMenuTools.ChooseWmsLayers) {
            this.mapContentManager.contextMenuTools.ChooseWmsLayers.olMap = this.OL_map;
        }
        if (this.mapContent.fctContextMenu && this.mapContentManager.contextMenuTools && this.mapContentManager.contextMenuTools.ExportVectorLayer) {
            this.mapContentManager.contextMenuTools.ExportVectorLayer.olMap = this.OL_map;
        }
        var self = this;
        if (this.mapContent.editable && toolsType !== undefined && toolsType !== null) {
            if (!(toolsType instanceof Array)) {
                toolsType = [toolsType];
            }
            var contentTools = [];
            for (var i = 0, len = toolsType.length; i < len; i++) {
                this.mapContentManager.addTool(toolsType[i]);
            }
            var managerToolBar = null;
            var toolBarOpener = (options !== undefined && options.openerTool);
            if (options !== undefined && options.toolBarDiv) {
                managerToolBar = new ToolBar(Utils.getDiv(options.toolBarDiv), this.OL_map, options.toolBarOptions);
                managerToolBar.toolBarName = MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME;
                self.toolBars.push(managerToolBar);
            } else if (options !== undefined && options.toolBarOptions !== undefined) {
                if (options.toolBarOptions.toolBar !== undefined) {
                    managerToolBar = options.toolBarOptions.toolBar;
                } else if (!toolBarOpener) {
                    managerToolBar = new ToolBar(Utils.getDiv(options.toolBarOptions.toolBarDiv), this.OL_map, options.toolBarOptions);
                    managerToolBar.toolBarName = MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME;
                    self.toolBars.push(managerToolBar);
                }
            }
            if (managerToolBar === null) {
                if (this.toolBars.length !== 0) {
                    managerToolBar = this.toolBars[0];
                } else if (this.mainToolBar !== null) {
                    managerToolBar = this.mainToolBar;
                } else {
                    throw new Error('Aucune barre d\'outils n\'est définie : les outils de gestion de contenu ne peuvent être ajoutés.');
                }
            }
            this.contentManagerToolBar = managerToolBar;
            if (this.mapContentManager.getTools().length !== 0) {
                if (toolBarOpener) {
                    if (options.toolBarOptions.toolBar !== undefined) {
                        delete options.toolBarOptions.toolBar;
                    }

                    var opts = {
                        displayClass: 'mapContentOpenerButton',
                        toolBarName: MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME,
                        toolBarOptions: options.toolBarOptions,
                        visible: false,
                        title: options.toolBarOptions.title,
                        tools: this.mapContentManager.getTools()/*,
                        initFunction: function () {
                            self.mapContentManager.activeInitialTools();
                        }*/
                    };
                    if (options.toolBarOptions.innerDiv) {
                        opts.innerDiv = options.toolBarOptions.innerDiv;
                    }
                    if (options.toolBarOptions.toolBarName) {
                        opts.toolBarName = options.toolBarOptions.toolBarName;
                    }
                    var openerButton = new ToolBarOpener(opts);
                    openerButton._initToolBarOpener(this);
                    managerToolBar.addControls([openerButton]);
                    managerToolBar.controls = managerToolBar.controls.concat(openerButton);
                } else {
                    managerToolBar.addControls(this.mapContentManager.getTools());
                    this.mapContentManager.activeInitialTools();
                    managerToolBar.controls = managerToolBar.controls.concat(this.mapContentManager.getTools());
                }
            }
        }

        for (var t = 0; t < this._toolsWithMapcontentManager.length; t++) {
            this._toolsWithMapcontentManager[t].setMapContentManager(this.mapContentManager);
        }
        ResultLayer.setMapContentManager(this.mapContentManager);

        return this.mapContentManager;
    },
    /**
     * Methode: addEditionToolBar
     * Ajoute une barre d'outils pour les outils d'édition uniquement.
     * Les autres outils ne seront pas ajoutés dans la barre.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils ou identifiant de cet élément.
     * tools - {Array(Object)} Objets JSON décrivant les outils d'édition de la barre (peut être null).
     * options - {Object} est un objet JSON embarquant diverses options offertes par la classe générant la barre d'outils.
     */
    addEditionToolBar: function (div, tools, options) {

        if (!_.isNil(tools)) {
            if (!_.isArray(tools)) {
                tools = [tools];
            }
            var isAnnotationToolBar = false;
            if (!_.isNil(options) && !_.isNil(options.isAnnotationToolBar)) {
                isAnnotationToolBar = options.isAnnotationToolBar;
            }
            //création de la barre d'outils
            var editionToolBar = null;
            var toolBarOpener = (!_.isNil(options) && options.openerTool);
            if (!_.isNil(options) && !_.isNil(options.toolBar)) {
                editionToolBar = options.toolBar;
            } else {
                // Utilisation d'une classe EditionToolBar
                editionToolBar = new EditionToolBar(div, this.OL_map, options);
                this.toolBars.push(editionToolBar); // ajout de cette barre à la liste des barres d'outils de la map pour pouvoir y accéder par la suite plus facilement
            }
            if (editionToolBar === null) {
                if (this.toolBars.length !== 0) {
                    editionToolBar = this.toolBars[0];
                } else if (this.mainToolBar !== null) {
                    editionToolBar = this.mainToolBar;
                } else {
                    throw new Error('Aucune barre d\'outils n\'est définie : les outils d\'édition de contenu ne peuvent être ajoutés.');
                }
            }
            this.editionToolBar = editionToolBar; //à voir si on ajoute une propriété à la map

            //création des instances de tools
            var editionTools = [];
            var saveEditionTool = null;
            var selectionEditionTool = null;
            var compositeSelectionEditionTool = null;
            for (var i = 0, len = tools.length; i < len; i++) {
                var ClassName;
                if (!_.isNil(tools[i].className)) {
                    ClassName = tools[i].className;
                } else {
                    ClassName = this.defaultTools[tools[i].type];
                }
                //ne pas prendre en compte les outils autres qu'édition.
                if (!_.isNil(ClassName)) {
                    var args = tools[i].args;
                    var target = document.getElementById(this.editionToolBar.id);
                    if (!_.isNil(options) && options.innerDiv) {
                        target = document.getElementById(options.innerDiv);
                    }

                    if (_.isNil(args)) {
                        args = {};
                    }
                    args.target = target;

                    var toolInstance = new ClassName(args);
                    if (toolInstance instanceof ToolBarOpener) {
                        toolInstance._initToolBarOpener(this);
                    } else {
                        toolInstance.setMapContent(this.mapContent);
                        if (isAnnotationToolBar && _.isFunction(toolInstance.initAnnotationTool)) {
                           toolInstance.initAnnotationTool(this);
                    }
                    }
                    editionTools.push(toolInstance);
                    if (toolInstance instanceof Save) {
                        saveEditionTool = toolInstance;
                    } else if (toolInstance.CLASS_NAME === 'Descartes.Tool.Edition.Selection') {
                        selectionEditionTool = toolInstance;
                    } else if (toolInstance.CLASS_NAME === 'Descartes.Tool.Edition.CompositeSelection') {
                        compositeSelectionEditionTool = toolInstance;
                    }
                }
            }

            // l'outil de sauvegarde doit être présent lorsque le mode auto est à false.
            // on affiche alors un message pour la MOE.
            if (!EditionManager.autoSave && saveEditionTool === null && !isAnnotationToolBar) {
                alert('Attention , l\'outil de sauvegarde doit être présent lorsque la sauvegarde des modifications n\'est pas automatique');
            }

            _.each(editionTools, function (editionTool) {
                if (_.isFunction(editionTool.setSelectionEditionTool)) {
                    editionTool.setSelectionEditionTool(selectionEditionTool);
                }
                if (_.isFunction(editionTool.setCompositeSelectionEditionTool)) {
                    editionTool.setCompositeSelectionEditionTool(compositeSelectionEditionTool);
                }
            });

            if (toolBarOpener) {
                if (!_.isNil(options.toolBar)) {
                    delete options.toolBar;
                }
                if (!_.isNil(options.toolBarName)) {
                    delete options.toolBarName; // pour ne pas que le MOE surcharge le nom "DescartesEditionToolBar" sinon on ne pourra plus la retrouver
                }

                var opts = {
                    displayClass: 'editionOpenerButton',
                    toolBarName: ToolBarConstants.EDITION_TEMPLATE_NAME + ToolBarConstants.ID,
                    toolBarOptions: options,
                    visible: false,
                    title: options.title,
                    tools: editionTools,
                    activeFirstElement: false//,
                    //initFunction: function () {
                    //    this.mapContentManager.activeInitialTools(); //A voir si on active un outil par défaut à l'ouverture de la barre d'outil
                    //}.bind(this)
                };
                if (!_.isNil(options) && options.innerDiv) {
                    opts.innerDiv = options.innerDiv;
                }

                var openerButton = new ToolBarOpener(opts);
                openerButton._initToolBarOpener(this);
                editionToolBar.addControls([openerButton]);
                //editionToolBar.controls = editionToolBar.controls.concat(openerButton);
                this.editionToolBar = this.getEditionToolBar();
                if (EditionManager.globalEditionMode) {
                    this.mapContent.events.register('layerUnderEdition', openerButton, openerButton.openToolBarOnLayerEdition);
                }

            } else {
                editionToolBar.addControls(editionTools);
                editionToolBar.controls = editionToolBar.controls.concat(editionTools);
            }

            if (isAnnotationToolBar) {
                this.annotationToolBar = editionToolBar;
            }
        }
        return this.editionToolBar;
    },
    /**
     * Methode: getEditionToolBar
     * Retourne la barre d'outil d'édition.
     */
    getEditionToolBar: function () {
        var editionToolBar = ToolBarConstants.EDITION_TEMPLATE_NAME;
        if (editionToolBar === null) {
            editionToolBar = this.editionToolBar;
        }
        return editionToolBar;
    },
    /**
     * Methode: addAnnotationToolBar
     * Ajoute une barre d'outils pour les outils d'annotation uniquement.
     * Les autres outils ne seront pas ajoutés dans la barre.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils ou identifiant de cet élément.
     * tools - {Array(Object)} Objets JSON décrivant les outils d'édition de la barre (peut être null).
     * options - {Object} est un objet JSON embarquant diverses options offertes par la classe générant la barre d'outils.
     */
    addAnnotationToolBar: function (div, tools, options) {
        if (_.isNil(options)) {
           options = {};
		}
        options.isAnnotationToolBar = true;
        return this.addEditionToolBar(div, tools, options);
    },
    /**
     * Methode: getEditionToolBar
     * Retourne la barre d'outil d'édition.
     */
    getAnnotationToolBar: function () {
        //this.annotationToolBar = this.getEditionToolBar();
        return this.annotationToolBar;
    },

    //copy for use MapConstants.ACTIONS_NAME with edition assistant
    addAction: function (action) {
        var ActionClassName = null;
        var actionInstance = null;
        if (typeof action.type === 'string') {
            if (MapConstants.ACTIONS_NAME.indexOf(action.type) !== -1) {
                ActionClassName = this.defaultActions[action.type];
            }
        } else if (typeof action.type === 'function') {
            ActionClassName = action.type;
        }
        if (ActionClassName !== null) {
            if (action.options !== null) {
                actionInstance = new ActionClassName(action.div, this.OL_map, action.options);
            } else {
                actionInstance = new ActionClassName(action.div, this.OL_map);
            }
            if (MapConstants._TOOLS_WITH_MAPCONTENT.indexOf(actionInstance.CLASS_NAME) !== -1) {
                if (actionInstance.setMapContent !== undefined) {
                    actionInstance.setMapContent(this.mapContent);
                }
            }
            if (actionInstance.CLASS_NAME === 'Descartes.Action.SizeSelector') {
                this.sizeSelector = actionInstance;
            }
        }
        return actionInstance;
    },

    /**
     * Methode: show
     * Affiche la carte.
     */
    show: function () {
        this.mapContent.refresh(this.mapContent.item);
        if (!this.autoScreenSize) {
            this.OL_map.getTargetElement().style.width = this.size[0].toString() + 'px';
            this.OL_map.getTargetElement().style.height = this.size[1].toString() + 'px';
            this.OL_map.updateSize();
        }
        if (this.autoSize || this.autoScreenSize) {
            this.OL_map.descartesAutoSize = true;
            this.autoSizeMap();
            var that = this;
            window.onresize = function () {
                that.autoSizeMap();
            };
        }

        this._addAllLayers();
        if (!this.OL_map.getView().getCenter()) {
            var mapType = this.OL_map.get('mapType');
            var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;
            this.OL_map.getView().fit(this.initExtent, {
                constrainResolution: constrainResolution,
                minResolution: this.minResolution,
                maxResolution: this.maxResolution
            });
        }
        this.mapContent.mapped = true;
        this._initializeLayersIconSnap();
        this._initializeLayersIconAutoTracing();
        this._initializeLayersIconSupport();
        this.refreshContentManager();
    },
    /*
     * Méthode privée _initializeLayersIconSnap
     * Initialise l'attribut displayIconSpan de chaque couche.
     */
    _initializeLayersIconSnap: function () {
        var snappingLayersSupport = [];
        var layers = this.mapContent.getLayers();
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            if (layer instanceof EditionLayer) {
                if (!_.isNil(layer.snapping) &&
                        !_.isNil(layer.snapping.snappingLayersIdentifier) &&
                        !_.isNil(layer.snapping.snappingLayersIdentifier)) {

                    var snappingLayersId = layer.snapping.snappingLayersIdentifier;
                    if (!(snappingLayersId instanceof Array)) {
                        snappingLayersId = [snappingLayersId];
                    }
                    for (var j = 0; j < snappingLayersId.length; j++) {
                        if (snappingLayersSupport.indexOf(snappingLayersId[j]) === -1) {
                            snappingLayersSupport.push(snappingLayersId[j]);
                        }
                    }
                }
            }
        }

        for (var k = 0; k < layers.length; k++) {
            var aLayer = layers[k];
            if (aLayer.snapping && aLayer.snapping.enable) {
                aLayer.displayIconSnap = true;
            } else {
                if (snappingLayersSupport.indexOf(aLayer.id) !== -1) {
                    aLayer.displayIconSnap = true;
                }
            }
        }
    },
    /*
     * Méthode privée _initializeLayersIconAutoTracing
     * Initialise l'attribut displayIconAutoTracing de chaque couche.
     */
    _initializeLayersIconAutoTracing: function () {
        var autotracingLayersSupport = [];
        var layers = this.mapContent.getLayers();
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            if (layer instanceof EditionLayer) {
                if (!_.isNil(layer.snapping) &&
                        layer.snapping.autotracing &&
                        !_.isNil(layer.snapping.snappingLayersIdentifier)) {

                    var snappingLayersId = layer.snapping.snappingLayersIdentifier;
                    if (!(snappingLayersId instanceof Array)) {
                        snappingLayersId = [snappingLayersId];
                    }
                    for (var j = 0; j < snappingLayersId.length; j++) {
                        if (autotracingLayersSupport.indexOf(snappingLayersId[j]) === -1) {
                             autotracingLayersSupport.push(snappingLayersId[j]);
                        }
                    }
                }
            }
        }

        for (var k = 0; k < layers.length; k++) {
            var aLayer = layers[k];
            if (aLayer.snapping && aLayer.snapping.enable && aLayer.snapping.autotracing && aLayer.geometryType !== LayerConstants.POINT_GEOMETRY && aLayer.geometryType !== LayerConstants.MULTI_POINT_GEOMETRY) {
                aLayer.displayIconAutoTracing = true;
            } else {
                if (autotracingLayersSupport.indexOf(aLayer.id) !== -1 && aLayer.geometryType !== LayerConstants.POINT_GEOMETRY && aLayer.geometryType !== LayerConstants.MULTI_POINT_GEOMETRY) {
                    aLayer.displayIconAutoTracing = true;
                }
            }
        }
    },
    /*
     * Méthode privée _initializeLayersIconSupport
     * Initialise l'attribut displayIconSupport de chaque couche.
     */
    _initializeLayersIconSupport: function () {
        var supportLayersSupport = [];
        var layers = this.mapContent.getLayers();
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            if (layer instanceof EditionLayer) {
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.split);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.copy);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.aggregate);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.unaggregate);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.buffer);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.halo);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.homothetic);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.divide);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.substract);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.clone);
                this._initLayerAllSupportLayerForLayer(layer, supportLayersSupport, layer.intersect);
            }
        }

        for (var k = 0; k < layers.length; k++) {
            var aLayer = layers[k];
            aLayer.displayIconSupport = this._checkAllSupportTools(aLayer, supportLayersSupport);
        }
    },
    _initLayerAllSupportLayerForLayer: function (layer, supportLayersSupport, toolConfig) {
        if (layer instanceof EditionLayer) {
            var supportLayersId;
            if (!_.isNil(toolConfig) && !_.isNil(toolConfig.supportLayersIdentifier)) {
                supportLayersId = toolConfig.supportLayersIdentifier;
                if (!_.isArray(supportLayersId)) {
                    supportLayersId = [supportLayersId];
                }
                for (var j = 0; j < supportLayersId.length; j++) {
                    if (supportLayersSupport.indexOf(supportLayersId[j]) === -1) {
                        supportLayersSupport.push(supportLayersId[j]);
                    }
                }
            } else if (!_.isNil(toolConfig) && !_.isNil(toolConfig.supportLayers)) {
                supportLayersId = toolConfig.supportLayers;
                if (!_.isArray(supportLayersId)) {
                    supportLayersId = [supportLayersId];
                }
                for (var k = 0; k < supportLayersId.length; k++) {
                    if (supportLayersSupport.indexOf(supportLayersId[k].id) === -1) {
                        supportLayersSupport.push(supportLayersId[k].id);
                    }
                }
            }
        }
    },
    /**
     * Méthode _checkAllSupportTools
     * Retourne true si au moins un outil utilise le layer passé en paramètre
     * en tant que couche support, false sinon.
     */
    _checkAllSupportTools: function (aLayer, supportLayersSupport) {
        return this._checkSupportTool(aLayer.split, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.copy, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.aggregate, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.unaggregate, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.buffer, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.halo, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.homothetic, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.divide, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.substract, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.clone, aLayer.id, supportLayersSupport) ||
                this._checkSupportTool(aLayer.intersect, aLayer.id, supportLayersSupport);
    },
    /**
     * Méthode _checkSupportTool
     * Retourn true si la configuration de l'outil passé en paramètre utilise le layer passé en paramètre
     */
    _checkSupportTool: function (toolConfig, layerId, supportLayersSupport) {
        if ((toolConfig && toolConfig.enable) || supportLayersSupport.indexOf(layerId) !== -1) {
            return true;
        }
        return false;
    },

    /**
     * Methode: refreshContentManager
     * Actualise la vue associée au gestionnaire de la carte après modifications de l'emprise courante.
     */
    refreshContentManager: function (redrawOnly) {
        if (this.mapContentManager !== null) {
            this.mapContentManager.redrawUI();
            if (!Descartes.EditionManager.isGlobalEditonMode() && !redrawOnly) {
                this.mapContent.refreshEditions(this.mapContent.getUnderEditionLayer(), true);
            }
        }
    },

    CLASS_NAME: 'Descartes.Map'
});
module.exports = Class;

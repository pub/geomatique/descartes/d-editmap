/* global Descartes */

var _ = require('lodash');
var jsts = require('jsts');
var FeatureState = require('../Model/FeatureState');
var LayerConstants = Descartes.Layer;

var Utils = Descartes.Utils;

/**
 * Class: Descartes.Utils
 * Classe offrant des méthodes statiques utilitaires cartographiques ou systèmes.
 */
var UtilsDEditMap = {

    _parser: new jsts.io.OL3Parser(),

    getStyleByState: function (feature) {
        var state = feature.get('state');
        var style;
        switch (state) {
            case FeatureState.DELETE:
                style = 'delete';
                break;
            case FeatureState.INSERT:
                style = 'create';
                break;
            case FeatureState.UPDATE:
                style = 'modify';
                break;
            default:
                style = 'default';
                break;
        }
        return style;
    },
    computeMultiGeomFromChildren: function (parentGeom, geomsToIgnore) {
        var result = null;

        if (_.isNil(geomsToIgnore)) {
            geomsToIgnore = [];
        } else if (!_.isArray(geomsToIgnore)) {
            geomsToIgnore = [geomsToIgnore];
        }

        var ignores = [];
        _.each(geomsToIgnore, function (geomToIgnore) {
            ignores.push(this._parser.read(geomToIgnore));
        }.bind(this));

        var subGeoms = this.getSubGeom(parentGeom);
        for (var i = 0; i < subGeoms.length; i++) {
            var subCeom = subGeoms[i];
            var jstsGeom = this._parser.read(subCeom);

            var ignoreSubGeom = false;
            for (var j = 0; j < ignores.length; j++) {
                var ignore = ignores[j];
                if (jstsGeom.equalsTopo(ignore)) {
                    ignoreSubGeom = true;
                }
            }

            if (!ignoreSubGeom) {
                if (result instanceof jsts.geom.GeometryCollection &&
                        !(jstsGeom instanceof jsts.geom.GeometryCollection)) {
                    //result.geometries.push(jstsGeom);
                    result = result.union(jstsGeom);
                } else if (parentGeom.getType() === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                    result = new jsts.geom.MultiPolygon([jstsGeom], jstsGeom.getFactory());
                } else if (parentGeom.getType() === LayerConstants.MULTI_LINE_GEOMETRY) {
                    result = new jsts.geom.MultiLineString([jstsGeom], jstsGeom.getFactory());
                } else if (parentGeom.getType() === LayerConstants.MULTI_POINT_GEOMETRY) {
                    result = new jsts.geom.MultiPoint([jstsGeom], jstsGeom.getFactory());
                }
            }
        }
        if (!_.isNil(result)) {
            result = this._parser.write(result);
        }
        return result;
    }
};

_.extend(Utils, UtilsDEditMap);

module.exports = Utils;

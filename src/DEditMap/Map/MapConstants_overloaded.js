var constants = {
    /**
     * Constante: ATTRIBUTES_EDIT_ACTION
     * Code de l'action pour la saisie des attributs des objets géographiques.
     */
    ATTRIBUTES_EDIT_ACTION: 'AttributesEditor',
    /**
     * Constante: EDITION_SAVE
     * Code de l'outil de sauvegarde.
     */
    EDITION_SAVE: 'EditionSave',
    /**
     * Constante: EDITION_SELECTION
     * Code de l'outil de selection pour l'edition.
     */
    EDITION_SELECTION: 'EditionSelection',
    /**
     * Constante: EDITION_COMPOSITE_SELECTION
     * Code de l'outil de selection d'une géometrie dans un objet pour l'edition.
     */
    EDITION_COMPOSITE_SELECTION: 'EditionCompositeSelection',
    /**
     * Constante: EDITION_ATTRIBUTE
     * Code de l'outil d'édition des attributs.
     */
    EDITION_ATTRIBUTE: 'EditionAttribute',
    /**
     * Constante: EDITION_SELECTION_ATTRIBUT
     * Code de l'outil d'édition des attributs de la sélection courante.
     */
    EDITION_SELECTION_ATTRIBUT: 'EditionSelectionAttribut',
    /**
     * Constante: EDITION_DRAW_CREATION
     * Code de l'outil d'édition de création par dessin.
     */
    EDITION_DRAW_CREATION: 'EditionDrawCreation',
    /**
     * Constante: EDITION_DRAW_ANNOTATION
     * Code de l'outil d'édition de création par dessin pour les annotations.
     */
    EDITION_DRAW_ANNOTATION: 'EditionDrawAnnotation',
    /**
     * Constante: EDITION_TEXT_ANNOTATION
     * Code de l'outil d'édition d'affichage d'un texte pour les annotations.
     */
    EDITION_TEXT_ANNOTATION: 'EditionTextAnnotation',
    /**
     * Constante: EDITION_ARROW_ANNOTATION
     * Code de l'outil d'édition de création d'une annotation de type flêche.
     */
    EDITION_ARROW_ANNOTATION: 'EditionArrowAnnotation',
    /**
     * Constante: EDITION_FREEHAND_ANNOTATION
     * Code de l'outil d'édition de création d'une annotation libre.
     */
    EDITION_FREEHAND_ANNOTATION: 'EditionFreehandAnnotation',
    /**
     * Constante: EDITION_BUFFER_ANNOTATION
     * Code de l'outil d'édition de création d'une annotation de type buffer halo.
     */
    EDITION_BUFFER_HALO_ANNOTATION: 'EditionBufferHaloAnnotation',
    /**
     * Constante: EDITION_ADD_TEXT_ANNOTATION
     * Code de l'outil d'édition d'ajout d'un texte à une annotations.
     */
    EDITION_ADD_TEXT_ANNOTATION: 'EditionAddTextAnnotation',
    /**
     * Constante: EDITION_STYLE_ANNOTATION
     * Code de l'outil d'édition de modification du style d'une annotation.
     */
    EDITION_STYLE_ANNOTATION: 'EditionStyleAnnotation',
    /**
     * Constante: EDITION_EXPORT_ANNOTATION
     * Code de l'outil d'édition d'exportes annotations.
     */
    EDITION_EXPORT_ANNOTATION: 'EditionExportAnnotation',
    /**
     * Constante: EDITION_IMPORT_ANNOTATION
     * Code de l'outil d'édition d'import d'annotations.
     */
    EDITION_IMPORT_ANNOTATION: 'EditionImportAnnotation',
    /**
     * Constante: EDITION_AIDE_ANNOTATION
     * Code de l'outil d'édition d'afficher l'aide à l'utilisation des outils d'annotations.
     */
    EDITION_AIDE_ANNOTATION: 'EditionAideAnnotation',
    /**
     * Constante: EDITION_ATTRIBUTE_ANNOTATION
     * Code de l'outil d'édition de saisie des attributs d'une annotations.
     */
    EDITION_ATTRIBUTE_ANNOTATION: 'EditionAttributeAnnotation',
    /**
     * Constante: EDITION_SNAPPINGANNOTATION
     * Code de l'outil d'édition de configuration du snapping.
     */
    EDITION_SNAPPING_ANNOTATION: 'EditionSnappingAnnotation',
    /**
     * Constante: EDITION_GEOLOCATION_SIMPLE_ANNOTATION
     * Code de l'outil d'édition de creation d'une annotation par geolocation simple.
     */
    EDITION_GEOLOCATION_SIMPLE_ANNOTATION: 'EditionGeolocationSimpleAnnotation',
    /**
     * Constante: EDITION_GEOLOCATION_TRACKING_ANNOTATION
     * Code de l'outil d'édition de creation des annotations par geolocation tracking.
     */
    EDITION_GEOLOCATION_TRACKING_ANNOTATION: 'EditionGeolocationTrackingAnnotation',
    /**
     * Constante: EDITION_GLOBAL_MODIFICATION_ANNOTATION
     * Code de l'outil d'édition de modification globale des annotations.
     */
    EDITION_GLOBAL_MODIFICATION_ANNOTATION: 'EditionGlobalModificationAnnotation',
    /**
     * Constante: EDITION_VERTICE_MODIFICATION_ANNOTATION
     * Code de l'outil d'édition de modification des vertex des annotations.
     */
    EDITION_VERTICE_MODIFICATION_ANNOTATION: 'EditionVerticeModificationAnnotation',
    /**
     * Constante: EDITION_RUBBER_ANNOTATION
     * Code de l'outil d'édition de suppression des annotations.
     */
    EDITION_RUBBER_ANNOTATION: 'EditionRubberAnnotation',
    /**
     * Constante: EDITION_ERASE_ANNOTATION
     * Code de l'outil d'édition de suppression de toutes les annotations.
     */
    EDITION_ERASE_ANNOTATION: 'EditionEraseAnnotation',
    /**
     * Constante : EDITION_COPY_CREATION
     * Code de l'outil d'édition de création par copie.
     */
    EDITION_COPY_CREATION: 'EditionCopyCreation',
    /**
     * Constante: EDITION_CLONE_CREATION
     * Code de l'outil d'édition de création par clonage.
     */
    EDITION_CLONE_CREATION: 'EditionCloneCreation',
    /**
     * Constante: EDITION_UNAGGREGATION_CREATION
     * Code de l'outil d'édition de création par desaggregation.
     */
    EDITION_UNAGGREGATION_CREATION: 'EditionUnAggregationCreation',
    /**
     * Constante: EDITION_AGGREGATION
     * Code de l'outil d'édition de création par agrégation.
     */
    EDITION_AGGREGATION: 'EditionAggregation',
    /**
     * Constante: EDITION_BUFFER_HALO_CREATION
     * Code de l'outil d'édition de création par extension/reduction avec un buffer simple (halo).
     */
    EDITION_BUFFER_HALO_CREATION: 'EditionBufferHaloCreation',
    /**
     * Constante: EDITION_BUFFER_NORMAL_CREATION
     * Code de l'outil d'édition de création par extension/reduction avec un buffer normal.
     */
    EDITION_BUFFER_NORMAL_CREATION: 'EditionBufferNormalCreation',
    /**
     * Constante: EDITION_SPLIT
     * Code de l'outil d'édition de création/modification par scission.
     */
    EDITION_SPLIT: 'EditionSplit',
    /**
     * Constante: EDITION_DIVIDE
     * Code de l'outil d'édition de création/modification par division
     */
    EDITION_DIVIDE: 'EditionDivide',
    /**
     * Constante : EDITION_HOMOTHETIC_CREATION
     * Code de l'outil d'édition de création par homothétie
     */
    EDITION_HOMOTHETIC_CREATION: 'EditionHomotheticCreation',
    /**
     * Constante: EDITION_TRANSLATE_MODIFICATION
     * Code de l'outil d'édition de modification par translation.
     */
    EDITION_TRANSLATE_MODIFICATION: 'EditionTranslateModification',
    /**
     * Constante: EDITION_GLOBAL_MODIFICATION
     * Code de l'outil d'édition de modification par rotation, translation et/ou homothétie.
     */
    EDITION_GLOBAL_MODIFICATION: 'EditionGlobalModification',
    /**
     * Constante: EDITION_COMPOSITE_GLOBAL_MODIFICATION
     * Code de l'outil d'édition de modification d'une géométrie d'un objet par rotation, translation et/ou homothétie.
     */
    EDITION_COMPOSITE_GLOBAL_MODIFICATION: 'EditionCompositeGlobalModification',
    /**
     * Constante: EDITION_VERTICE_MODIFICATION
     * Code de l'outil d'édition de modification de sommet.
     */
    EDITION_VERTICE_MODIFICATION: 'EditionVerticeModification',
    /**
     * Constante : EDITION_MERGE_MODIFICATION
     * Code de l'outil d'édition de modification par fusion.
     */
    EDITION_MERGE_MODIFICATION: 'EditionMergeModification',
    /**
     * Constante : EDITION_SELECTION_SUBSTRACT_MODIFICATION
     * Code de l'outil d'édition de modification par soustraction.
     */
    EDITION_SELECTION_SUBSTRACT_MODIFICATION: 'EditionSelectionSubstractModification',
    /**
     * Constante: EDITION_RUBBER_DELETION
     * Code de l'outil d'édition de suppression par la gomme.
     */
    EDITION_RUBBER_DELETION: 'EditionRubberDeletion',
    /**
     * Constante: EDITION_COMPOSITE_RUBBER_DELETION
     * Code de l'outil d'édition de suppression d'une géometrie dans un objet composite par la gomme.
     */
    EDITION_COMPOSITE_RUBBER_DELETION: 'EditionCompositeRubberDeletion',
    /**
     * Constante: EDITION_SELECTION_DELETION
     * Code de l'outil d'édition de suppression d'une selection.
     */
    EDITION_SELECTION_DELETION: 'EditionSelectionDeletion',
    /**
     * Constante: EDITION_INTERSECT_INFORMATION
     * Code de l'outil d'affichage d'informations sur l'intersection d'objets de la couche d'édition.
     */
    EDITION_INTERSECT_INFORMATION: 'EditionIntersectInformation',

    MAP_TYPES: {
        CONTINUOUS: 'Continuous',
        DISCRETE: 'Discrete'
    }
};

module.exports = constants;

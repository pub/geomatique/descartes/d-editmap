/* global Descartes */

var _ = require('lodash');

var Map_overloaded = require('../Core/Map_overloaded');
var ContinuousScalesMap = require('./ContinuousScalesMap_overloaded');
var DiscreteScalesMap = require('./DiscreteScalesMap_overloaded');
var mapConstants = require('./MapConstants_overloaded');

var namespace = {
    ContinuousScalesMap: ContinuousScalesMap,
    DiscreteScalesMap: DiscreteScalesMap
};

_.extend(namespace, mapConstants);

_.extend(Map_overloaded, namespace);

module.exports = Map_overloaded;

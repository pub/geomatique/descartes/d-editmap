/* global Descartes*/
var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Map = require('../Core/Map_overloaded');
var MapConstants = require('./MapConstants_overloaded');
var Projection = Descartes.Projection;

/**
 * Class: Descartes.Map.DiscreteScalesMap
 * Classe proposant une carte avec une navigation limitée à des *échelles discrètes*.
 *
 * Hérite de:
 * - <Descartes.Map>
 */
var Class = Utils.Class(Map, {
    /**
     * Propriete: resolutions
     * {Array(Float)} Résolutions disponibles.
     */
    resolutions: null,
    /**
     * Constructeur: Descartes.Map.DiscreteScalesMap
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la carte OpenLayers ou identifiant de cet élement.
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction de la classe:
     * resolutions - {Array(Float)} Résolutions disponibles.
     */
    initialize: function (div, mapContent, options) {
        Map.prototype.initialize.apply(this, [div, mapContent, options]);
    },
    /**
     * Methode: createOlMap
     * Construit la carte OpenLayers associée.
     */
    createOlMap: function () {

        if (_.isString(this.projection)) {
            this.projection = new Projection(this.projection);
        }

        if (this.projection.getUnits() !== 'm') {
            this.units = this.projection.getUnits();
        }

        if (!_.isNil(this.minScale)) {
            this.maxResolution = Utils.getResolutionForScale(this.minScale, this.projection.getUnits());
        }
        if (!_.isNil(this.maxScale)) {
            this.minResolution = Utils.getResolutionForScale(this.maxScale, this.projection.getUnits());
        }

        this.OL_map = new ol.Map({
            interactions: [],
            controls: [],
            view: new ol.View({
                projection: this.projection,
                extent: this.maxExtent,
                resolutions: this.resolutions,
                maxResolution: this.maxResolution,
                minResolution: this.minResolution,
                rotation: this.rotation
            })
        });
        //Utile pour l'outil de zoom in
        this.OL_map.set('mapType', MapConstants.MAP_TYPES.DISCRETE);
    },
    CLASS_NAME: 'Descartes.Map.DiscreteScalesMap'
});

module.exports = Class;

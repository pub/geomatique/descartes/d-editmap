/* global Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;
var UI = Descartes.UI;

var abstractAttributesEditorTemplate = require('./templates/AbstractAttributesEditor.ejs');

/**
 * Class: Descartes.UI.AbstractAttributesEditor
 * Classe "abstraite" proposant la saisie des attributs d'un objet géographique.
 *
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 * Classes dérivées:
 *  - <Descartes.UI.AttributesEditorDialog>
 *  - <Descartes.UI.AttributesEditorInPlace>
 */
var Class = Utils.Class(UI, {
    /**
     * Propriete: errors
     * {Array(String)} Liste des erreurs rencontrés lors des contrôles de surface.
     */
    errors: null,

    /**
     * Propriete: form
     * {DOMElement} Elément DOM correspondant au formulaire de saisie.
     */
    form: null,

    /**
     * Propriete: size
     * {Integer} Taille des zones de saisie (10 par défaut).
     */
    size: 10,
    /**
     * Constructeur: Descartes.UI.AbstractAttributesEditor
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * coordinatesModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.CoordinatesInput>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     */
    initialize: function (div, model, options) {
        UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: populateForm
     * Construit les zones de saisie du formulaire.
     */
    populateForm: function () {
        var inputTextOptions = this.displayClasses;
        if (!_.isNil(this.size)) {
            inputTextOptions = _.extend({size: this.size}, this.displayClasses);
        }

        var inviteLayerLabel = this.getMessage('INVITE_LAYER') + ' "' + this.model.layerTitle + '" :';
        var emptyAttributesLabel = this.getMessage('EMPTY_ATTRIBUTES') + ' : ' + this.model.layerTitle;
        var emptySelectionLabel = this.getMessage('EMPTY_SELECTION');

        var formContent = abstractAttributesEditorTemplate({
            model: this.model,
            inviteLayerLabel: inviteLayerLabel,
            emptyAttributesLabel: emptyAttributesLabel,
            emptySelectionLabel: emptySelectionLabel,
            inputTextOptions: inputTextOptions
        });

        return formContent;
    },

    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function () {
        this.errors = [];

        if (this.model.attributesEditable && this.model.attributesEditable.length > 0) {
            for (var i = 0; i < this.model.attributesEditable.length; i++) {
                var element = this.model.attributesEditable[i];
                element.value = this.result[element.fieldName];
            }
        }
        return (this.errors.length === 0);
    },

    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = this.getMessage('ERRORS_LIST');
            for (var i = 0, len = this.errors.length; i < len; i++) {
                errorsMessage += '\n\t- ' + this.errors[i];
            }
            alert(errorsMessage);
        }
    },

    CLASS_NAME: 'Descartes.UI.AbstractAttributesEditor'
});

module.exports = Class;

/* global Descartes */

require('../css/LayersTree_overloaded.css');

var _ = require('lodash');
var jQuery = require('jquery');
var log = require('loglevel');

var Utils = Descartes.Utils;

var EditionLayer = require('../../Model/EditionLayer');
var EditionManager = require('../../Core/EditionManager');

var treeItemTemplate = require('./templates/TreeItem.ejs');
var treeEditionItemTemplate = require('./templates/TreeEditionItem.ejs');


var LayersTree = Descartes.UI.LayersTree;

/**
 * Class: Descartes.UI.LayersTree
 * Classe affichant une arborescence du contenu de la carte et offrant des interactions sur celui-ci.
 *
 * Hérite de:
 * - <Descartes.UI>
 *
 * Evénements déclenchés:
 * layerSelected - Une couche a été sélectionnée.
 * groupSelected - Un groupe a été sélectionné.
 * nodeUnselect - Une couche ou une couche a été désélectionné.
 * rootSelected - La racine a été sélectionnée.
 */
var Class = Utils.Class(LayersTree, {

    defaultDisplayClasses: {
        tree: 'Descartes-jstree',
        treeRoot: 'fa fa-globe',
        loadErrorClassName: 'fa fa-exclamation-triangle',
        groupOpened: 'DescartesLayersTreeFolderOpened',
        groupClosed: 'DescartesLayersTreeFolderClosed',
        groupLayersVisibilityOn: 'DescartesLayersTreeGroupVisible',
        groupLayersVisibilityOff: 'DescartesLayersTreeGroupHidden',
        groupLayersVisibilityMixed: 'DescartesLayersTreeGroupMixed',
        itemSelectedText: 'DescartesLayersTreeTextSelected',
        itemText: 'DescartesLayersTreeText',
        itemTextUnderEdition: 'DescartesLayersTreeTextUnderEdition',
        layerVisibilityOn: 'fa fa-eye',
        layerVisibilityOff: 'fa fa-eye-slash',
        layerVisibilityOnForbidden: 'fa fa-eye DescartesLayersTreeIconColorForbidden',
        layerVisibilityNeedZoomOut: 'fa fa-minus DescartesLayersTreeBt4LayerZoomOut',
        layerVisibilityNeedZoomIn: 'fa fa-plus DescartesLayersTreeBt4LayerZoomIn',
        layerInfoEnabled: 'fa fa-info-circle DescartesLayersTreeIconColor',
        layerInfoDisabled: 'fa fa-ban DescartesLayersTreeIconColor',
        layerInfoForbidden: 'fa fa-ban DescartesLayersTreeIconColorForbidden',
        layerSheetActive: 'fa fa-th-list DescartesLayersTreeIconColor',
        layerSheetInactive: 'fa fa-th-list DescartesLayersTreeIconColorForbidden',
        layerMoreInfos: 'fa fa-chevron-down',
        layerMoreInfoLegend: 'fa fa-image',
        blankBlocQuote: 'DescartesLayersTreeBlockQuote',
        layerOpacitySliderTrack: 'DescartesLayersTreeLayerSliderTrack',
        layerOpacitySliderHandle: 'DescartesLayersTreeLayerSliderHandle',
        layerLienMetadata: 'DescartesLayersTreeIconColor fa fa-question-circle',
        layerNoLienMetadata: 'DescartesLayersTreeIconColorForbidden fa fa-question-circle',
        layerLoading: 'DescartesLayersTreeLayerLoading',
        layerEditableOn: 'fa fa-edit DescartesLayersTreeIconColorUnderEdition',
        layerEditableOff: 'fa fa-edit DescartesLayersTreeIconColor',
        layerEditableIndividualOff: 'fa fa-edit DescartesLayersTreeIconColor',
        layerEditableIndividualOn: 'fa fa-edit DescartesLayersTreeIconColorUnderEdition',
        layerEditableUnavailable: 'fa fa-edit DescartesLayersTreeIconColorForbidden',
        layerEditableNeedZoomOut: 'fa fa-edit DescartesLayersTreeLayerEditableZoomOut',
        layerEditableNeedZoomIn: 'fa fa-edit DescartesLayersTreeLayerEditableZoomIn',
        layerEditableNot: 'DescartesLayersTreeLayerNotEditable',
        layerEditableSnapOn: 'fa fa-magnet DescartesLayersTreeLayerSnapOn',
        layerEditableSnapOff: 'fa fa-magnet DescartesLayersTreeLayerSnapOff',
        layerEditableSnapUnavailable: 'fa fa-magnet DescartesLayersTreeIconColorForbidden',
        layerEditableAutoTracingOn: 'fa fa-flash DescartesLayersTreeLayerAutoTracingOn',
        layerEditableAutoTracingOff: 'fa fa-flash DescartesLayersTreeLayerAutoTracingOff',
        layerEditableAutoTracingUnavailable: 'fa fa-flash DescartesLayersTreeIconColorForbidden',
        layerSupportOn: 'DescartesLayersTreeLayerSupportOn',
        layerSupportOff: 'DescartesLayersTreeLayerSupportOff',
        layerSupportUnavailable: 'DescartesLayersTreeLayerSupportUnavailable'
    },

    /**
     * Methode: draw
     * Construit et affiche l'arborescence de gestion du contenu de la carte.
     */
    draw: function () {
        var self = this;
        var divSelector = '#' + self.div.id;

        this.tree = jQuery.jstree.reference(divSelector);
        if (this.tree) {
            log.debug('Draw refresh tree');
            if (!_.isNil(this.model.getUnderEditionLayer()) && !this.model.getUnderEditionLayer().isEditable()) {
                //si une couche est en cours d'édition mais qu'elle devient plus éditable (changement de niveau de zoom par example)
                var layerUnderEdition = this.model.getUnderEditionLayer();
                this.toggleLayerEditionState(layerUnderEdition);
            } else {
                this.tree.refresh(true, true);
            }

        } else {
            log.debug('Draw First Time tree');

            var plugins = [];

            if (this.model.fctVisibility) {
                plugins.push('checkbox');
            }

            plugins.push('dnd');

            var jstreeCMFct = function () {};
            if (this.model.editable && this.model.fctContextMenu) {
                plugins.push('contextmenu');
                jstreeCMFct = this.jstreeContextMenuItemsFct;
                if (this.jstreeContextMenuItemsCustomFct) {
                     jstreeCMFct = this.jstreeContextMenuItemsCustomFct;
                }
            }

            this.treeConfig = {
                'default': {
                    dnd: {
                        is_draggable: this.model.editable
                    }
                },
                core: {
                    check_callback: this.model.editable,
                    themes: {
                        name: this.jstreeTheme,
                        icons: false,
                        dots: true
                    },
                    data: function (obj, callback) {
                        var json = self._getStructure(self.model.item);
                        self.timestamp = new Date().getTime();
                        json.data = self.timestamp;
                        callback.call(this, json);
                    },
                    animation: 0,
                    worker: false,
                    html_titles: true
                },
                checkbox: {
                    three_state: true,
                    tie_selection: false,
                    whole_node: false
                },
                dnd: {
                    copy: false
                },
                contextmenu: {
                    items: jstreeCMFct.bind(this),
                    select_node: true,
                    show_at_node: true
                },
                plugins: plugins
            };

            var treeDiv = jQuery(divSelector);
            if (!treeDiv.hasClass(this.defaultDisplayClasses.tree)) {
                treeDiv.addClass(this.defaultDisplayClasses.tree);
            }


            treeDiv.jstree(this.treeConfig);
            this.tree = jQuery.jstree.reference(divSelector);

            treeDiv.on('loaded.jstree', function (e, data) {
                log.debug('loaded', arguments);
            });
            treeDiv.on('redraw.jstree', function (e, data) {
                //suppression des checkbox
                jQuery('li[nocheckbox="true"]').find('i.jstree-checkbox:first').hide();
                var dataTimestamp = data.instance._model.data.DescartesUILayersTree.data;
                if (self.timestamp === dataTimestamp) {
                    self._afterRender();
                }
            });
            treeDiv.on('open_node.jstree', function (e, data) {
                log.debug('open', arguments);
                self.openCloseGroup(data, true);
            });
            treeDiv.on('close_node.jstree', function (e, data) {
                log.debug('close', arguments);
                self.openCloseGroup(data, false);
            });
            treeDiv.on("check_node.jstree", function (e, data) {
                self.toggleLayerVisibilityByCheckBoxChanged(data, true);
            });
            treeDiv.on("uncheck_node.jstree", function (e, data) {
                self.toggleLayerVisibilityByCheckBoxChanged(data, false);
            });
            treeDiv.on('move_node.jstree', function (e, data) {
                log.debug('move');
                self.onDropItem(e, data);
            });
            treeDiv.on("select_node.jstree", function (e, data) {
                if (self.model.editable) {
                    self.selectItem();
                }
            });
            jQuery(document).on('dnd_start.vakata', function (event) {
                log.debug('drag');
                event.preventDefault();
            });
            this.tree = jQuery.jstree.reference('#' + self.div.id);

            //Prise en compte de l'icone de chargement pour les couches ajoutées en cours d'utilisation de l'application.
            if (this.model.displayIconLoading) {
                this.registerToLayerLoading(this.model.item);
            } else {
                //besoin de rafraichir pour ne pas afficher les checkbox de l'arbre
                this.tree.refresh(true, true);
            }

            jQuery(this.div).on('hidden.bs.collapse', function (e) {
                var pos = self.savItemsOpen.indexOf(e.target.id);
                if (pos >= 0) {
                    self.savItemsOpen.splice(pos, 1);
                }
            });
            jQuery(this.div).on('shown.bs.collapse', function (e) {
                if (self.moreInfosUiParams.fctDisplayLegend && self.model.fctDisplayLegend) {
                    _.each(jQuery('.legend-' + e.target.id), function (image) {
                        jQuery(image).attr('src', jQuery(image).attr('async-src'));
                    });
                }
                var pos = self.savItemsOpen.indexOf(e.target.id);
                if (pos === -1) {
                    self.savItemsOpen.push(e.target.id);
                }
            });
        }
    },

    /**
     * Méthode _afterRender
     * Méthode appelée une fois le rendu de l'ihm effectué
     */
    _afterRender: function () {
        log.debug('afterRender call');

        this._setToolTip(false);

        if (this.model.fctOpacity) {
            this._addToolsSlider();
        }

        //ajout handler de changement de visibilité
        var classOnSelector = this.displayClasses.layerVisibilityOn.replace(/ /g, '.'); //si plusieurs classes css
        var classOffSelector = this.displayClasses.layerVisibilityOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerVisibilityByDescartesIconChanged.bind(this));

        //ajout handler de changement de requête d'information
        classOnSelector = this.displayClasses.layerInfoEnabled.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerInfoDisabled.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerQueryInfo.bind(this));

        //ajout handler d'affichage des informations d'une couche
        classOnSelector = this.displayClasses.layerSheetActive.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector).click(this.showDatasLayer.bind(this));

        //ajout handler du changement d'état d'édition
        classOnSelector = this.displayClasses.layerEditableOn.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerEditableOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerEditionStateByClick.bind(this));

        //ajout handler du changement d'état de snapping
        classOnSelector = this.displayClasses.layerEditableSnapOn.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerEditableSnapOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerSnapping.bind(this));

        //ajout handler du changement d'état de tracing
        classOnSelector = this.displayClasses.layerEditableAutoTracingOn.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerEditableAutoTracingOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerAutoTracing.bind(this));

        //ajout handler du changement d'état de support
        classOnSelector = this.displayClasses.layerSupportOn.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerSupportOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerSupport.bind(this));

        //ajout du handler d'ouverture du lien metadata.
        classOnSelector = this.displayClasses.layerLienMetadata.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector).click(function (event) {
            var href = event.target.href;
            window.open(href);
        });
    },

    _getStructure: function (node) {
        var id = this.CLASS_NAME.replace(/\./g, '') + node.index.toString();
        var sliderId = id + 'SliderA';
        var visibleClassName = this.displayClasses.layerVisibilityOff;
        var visibleTitle = '';
        var needZoomClassName = null;

        var state = {
            opened: node.opened
        };
        if (node.hide) {
            state.hidden = true;
        }
        var nocheckbox = !this.model.fctVisibility;

        if (node.index === '') {
            nocheckbox = true;
        }
        if (node.CLASS_NAME.indexOf('Descartes.Layer') === 0) {
            state.checked = node.visible;
            nocheckbox = true;
            if (!node.acceptMaxScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomOut;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (!node.acceptMinScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomIn;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (node.visible) {
                visibleClassName = this.displayClasses.layerVisibilityOn;
            }
            if (node.CLASS_NAME.indexOf('Descartes.Layer.EditionLayer.GenericVector') === 0) {
                node.loading = false;
            }

            //récupération du tooltip à afficher
            if (node.isInScalesRange()) {
                visibleTitle = (node.visible ? this.getMessage('HIDE_LAYER') : this.getMessage('SHOW_LAYER'));
            } else if (node.minScale !== null && node.maxScale !== null) {
                visibleTitle = this.getMessage('MIN_SCALE') +
                        Utils.readableScale(node.minScale) +
                        this.getMessage('MAX_SCALE') +
                        Utils.readableScale(node.maxScale);
            } else if (node.minScale != null) {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.minScale);
            } else {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.maxScale);
            }
        }

        var queryableInfo = this.getQueryableInfo(node);
        var metadataInfo = this.getMetadataItem(node);
        var editableInfo = this.getEditableInfo(node);

        var itemTextClassName = this.displayClasses.itemText;
        if (node instanceof EditionLayer && node.isUnderEdition()) {
            itemTextClassName += ' ' + this.displayClasses.itemTextUnderEdition;
        }

        var snapClassName = '';
        if (editableInfo.snap) {
            snapClassName = editableInfo.snap.className;
        }

        var autotracingClassName = '';
        if (editableInfo.autotracing) {
            autotracingClassName = editableInfo.autotracing.className;
        }

        var supportClassName = '';
        if (editableInfo.support) {
            supportClassName = editableInfo.support.className;
        }

        var text = treeItemTemplate({
            id: sliderId,
            node: node,
            displayIconLoading: this.model.displayIconLoading,
            editionTemplate: treeEditionItemTemplate,
            rootTitle: this.nodeRootTitle,
            rootClassName: this.displayClasses.treeRoot,
            loadErrorClassName: this.displayClasses.loadErrorClassName,
            visibleClassName: visibleClassName,
            visibleTitle: visibleTitle,
            needZoomClassName: needZoomClassName,
            itemTextClassName: itemTextClassName,
            fctVisibility: this.model.fctVisibility,
            fctOpacity: this.model.fctOpacity,
            fctQueryable: this.model.fctQueryable,
            fctDisplayLegend: this.model.fctDisplayLegend,
            displayMoreInfos: this.model.displayMoreInfos,
            displayMoreInfosParams: this.moreInfosUiParams,
            displayMoreInfosTitle: this.getMessage("MORE_INFOS"),
            displayMoreInfosClassName: this.displayClasses.layerMoreInfos,
            displayMoreInfoLegendTitle: this.getMessage("MORE_INFO_LEGEND"),
            displayMoreInfoLegendClassName: this.displayClasses.layerMoreInfoLegend,
            queryableClassName: queryableInfo.className,
            queryableTitle: queryableInfo.title,
            sheetableClassName: queryableInfo.sheetableClassName,
            sheetableTitle: queryableInfo.sheetableTitle,
            fctMetadataLink: this.model.fctMetadataLink,
            metadataHref: metadataInfo.href,
            metadataClassName: metadataInfo.className,
            metadataTitle: metadataInfo.title,
            noMetadataClassName: this.displayClasses.layerNoLienMetadata,
            noMetadataTitle: this.getMessage('NO_LIEN_METADATA'),
            snapClassName: snapClassName,
            autotracingClassName: autotracingClassName,
            supportClassName: supportClassName,
            editableInfo: editableInfo
        });

        var json = {
            id: id,
            text: text,
            icon: '',
            state: state,

            li_attr: {
                'nocheckbox': nocheckbox
            }
        };
        if (node.items) {
            json.children = [];
            for (var i = 0; i < node.items.length; i++) {
                var item = node.items[i];
                var child = this._getStructure(item);
                json.children.push(child);
            }
        }
        return json;
    },

    /**
     * Méthode getEditableInfo
     * Retourne les informations à afficher lorsque
     */
    getEditableInfo: function (layer) {
        var id = this.CLASS_NAME.replace(/\./g, "") + layer.index.toString();
        id += 'Editable';

        var result = {
            id: id
        };

        if (layer instanceof EditionLayer) {
            if (layer.isEditable()) {
                if (layer.isUnderEdition()) {
                    if (EditionManager.isGlobalEditonMode() && !EditionManager.onlyAppMode) {
                        result.title = this.getMessage('STOP_EDIT_LAYER');
                        result.className = this.displayClasses.layerEditableOn;
                    } else {
                        result.title = this.getMessage('UNDER_EDITION_LAYER');
                        result.className = this.displayClasses.layerEditableIndividualOn;
                    }
                } else {
                    if (EditionManager.isGlobalEditonMode() && !EditionManager.onlyAppMode) {
                        result.title = this.getMessage('START_EDIT_LAYER');
                        result.className = this.displayClasses.layerEditableOff;
                    } else {
                        result.title = this.getMessage('NOT_UNDER_EDITION_LAYER');
                        result.className = this.displayClasses.layerEditableIndividualOff;
                    }
                }
            } else if (!layer.isVisible()) {
                result.title = this.getMessage('UNAVAILABLE_EDIT_LAYER');
                result.className = this.displayClasses.layerEditableUnavailable;
            } else if (!layer.acceptEditionMaxScaleRange()) {
                result.title = this.getMessage('ZOOM_OUT_EDIT_LAYER');
                result.className = this.displayClasses.layerEditableUnavailable;
                result.needZoomClassName = this.displayClasses.layerVisibilityNeedZoomOut;
            } else if (!layer.acceptEditionMinScaleRange()) {
                result.title = this.getMessage('ZOOM_IN_EDIT_LAYER');
                result.className = this.displayClasses.layerEditableUnavailable;
                result.needZoomClassName = this.displayClasses.layerVisibilityNeedZoomIn;
            } else {
                result.title = this.getMessage('INACTIVE_EDIT_LAYER');
                result.className = this.displayClasses.layerEditableInactive;
            }
        } else {
            result.className = this.displayClasses.layerEditableNot;
        }

        if (layer.displayIconSnap) {
            result.snap = {};
            result.snap.className = this.displayClasses.layerEditableSnapUnavailable;
            if (layer.isVisible()) {
                if (layer.snappingEnable === true) {
                    result.snap.className = this.displayClasses.layerEditableSnapOn;
                } else if (layer.snappingEnable === false) {
                    result.snap.className = this.displayClasses.layerEditableSnapOff;
                }
            }
        }
        if (layer.displayIconAutoTracing) {
            result.autotracing = {};
            result.autotracing.className = this.displayClasses.layerEditableAutoTracingUnavailable;
            if (layer.isVisible()) {
                if (layer.autotracingEnable === true) {
                    result.autotracing.className = this.displayClasses.layerEditableAutoTracingOn;
                } else if (layer.autotracingEnable === false) {
                    result.autotracing.className = this.displayClasses.layerEditableAutoTracingOff;
                }
            }
        }
        if (layer.displayIconSupport) {
            result.support = {};
            result.support.className = this.displayClasses.layerSupportUnavailable;
            if (layer.isVisible()) {
                if (layer.supportEnable === true) {
                    result.support.className = this.displayClasses.layerSupportOn;
                } else if (layer.supportEnable === false) {
                    result.support.className = this.displayClasses.layerSupportOff;
                }
            }
        }

        return result;
    },

    /**
     * Methode: toggleLayerVisibilityByCheckBoxChanged
     * Inverse la visibilité des groupes et des couches lors du changement d'état d'une checkbox.
     *
     * Paramêtres:
     * data - {Object} contenant les informations de l'événements
     * checked - Egale à true si l'événement correspond à une case cochée. Décochée sinon.
     */
    toggleLayerVisibilityByCheckBoxChanged: function (data, checked) {
        if (!this.changingVisibilityState) {
            this.changingVisibilityState = true;

            var id = data.node.id.substring(this.CLASS_NAME.replace(/\./g, "").length);
            var groupOrLayer = this.model.getItemByIndex(id);
            log.debug(groupOrLayer);
            groupOrLayer.setVisibility(checked);

            var underEditionLayer = this.model.getUnderEditionLayer();

            if (groupOrLayer.CLASS_NAME === 'Descartes.Group') {
                _.each(groupOrLayer.items, function (descartesLayer) {

                    descartesLayer.setVisibility(checked);
                    this.model.refreshVisibilities(descartesLayer, true);
                    this.model.refreshSupportLayers(descartesLayer);
                    this.model.refreshSnapping(descartesLayer);
                    this.model.refreshAutoTracing(descartesLayer);
                    if (!_.isNil(underEditionLayer) && descartesLayer === underEditionLayer) {
                        this.toggleLayerEditionState(underEditionLayer);
                    } else if (!Descartes.EditionManager.isGlobalEditonMode()) {
                        this.model.refreshEditions(null, true);
                    }
                }.bind(this));
            }

            this.tree.refresh(true, true);
            this.changingVisibilityState = false;
            this.events.triggerEvent('treeUserEvent');
        }
    },
    /**
     * Methode: toggleLayerVisibilityByDescartesIconChanged
     * Inverse la visibilité des groupes et des couches lors du changement de la visibilite d'une couche.
     *
     * Paramètres:
     * current - {HTMLElement} L'élément DOM concerné.
     */
    toggleLayerVisibilityByDescartesIconChanged: function (event) {
        var visibilityId = jQuery(event.target).closest('a').attr('id');
        var nodeDescartesIndex = visibilityId.substring(this.CLASS_NAME.replace(/\./g, "").length, visibilityId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        if (descartesLayer.isInScalesRange && descartesLayer.isInScalesRange()) {
            this.model.refreshVisibilities(descartesLayer);
            this.model.refreshSupportLayers(descartesLayer);
            this.model.refreshSnapping(descartesLayer);
            this.model.refreshAutoTracing(descartesLayer);
            this.tree.refresh(true, true);
            this.events.triggerEvent('treeUserEvent');
        }
        var underEditionLayer = this.model.getUnderEditionLayer();
        if (!_.isNil(underEditionLayer) && descartesLayer === underEditionLayer) {
            this.toggleLayerEditionState(underEditionLayer);
        } else if (!Descartes.EditionManager.isGlobalEditonMode()) {
            this.model.refreshEditions(null, true);
        }
    },

    /**
     * Methode: toggleLayerEditionStateByClick
     * Bascule le status d'édition de la couche provenant du noeud passé en paramètre
     * Redessine l'arbre.
     *
     * Paramêtres:
     * event- evênement associé au clic
     */
    toggleLayerEditionStateByClick: function (event) {
        var editionId = jQuery(event.target).closest('[id]').attr('id');
        var nodeDescartesIndex = editionId.substring(this.CLASS_NAME.replace(/\./g, '').length, editionId.indexOf('_'));

        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);
        if (descartesLayer.isEditable() && EditionManager.isGlobalEditonMode() && !EditionManager.onlyAppMode) {
            this.toggleLayerEditionState(descartesLayer);
        }
    },
    toggleLayerEditionState: function (descartesLayer) {
        this.model.refreshEditions(descartesLayer);
        this.tree.refresh(true, true);
        this.model.refreshEditions(descartesLayer, true);
    },
    /**
     * Methode: toggleLayerSnapping
     * Active ou désactive le magnétisme.
     *
     */
    toggleLayerSnapping: function (event) {
        var editionId = event.target.parentNode.id;
        var nodeDescartesIndex = editionId.substring(this.CLASS_NAME.replace(/\./g, '').length, editionId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        descartesLayer.snappingEnable = !descartesLayer.snappingEnable;
        if (descartesLayer.snappingEnable) {
            event.target.className = this.displayClasses.layerEditableSnapOn;
        } else {
            event.target.className = this.displayClasses.layerEditableSnapOff;
        }
        this.model.refreshSnapping(descartesLayer);
        if (descartesLayer.displayIconAutoTracing) {
            event.target = event.target.nextElementSibling;
            this.toggleLayerAutoTracing(event);
        }
    },
    /**
     * Methode: toggleLayerAutoTracing
     * Active ou désactive le tracé automatique.
     *
     */
    toggleLayerAutoTracing: function (event) {
        var editionId = event.target.parentNode.id;
        var nodeDescartesIndex = editionId.substring(this.CLASS_NAME.replace(/\./g, '').length, editionId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        descartesLayer.autotracingEnable = !descartesLayer.autotracingEnable;
        if (descartesLayer.snappingEnable && descartesLayer.autotracingEnable) {
            event.target.className = this.displayClasses.layerEditableAutoTracingOn;
        } else {
            event.target.className = this.displayClasses.layerEditableAutoTracingOff;
        }
        this.model.refreshAutoTracing(descartesLayer);
    },
    /**
     * Methode: toggleLayerSupport
     * Active ou désactive la couche support.
     *
     */
    toggleLayerSupport: function (event) {
        var editionId = event.target.parentNode.id;
        var nodeDescartesIndex = editionId.substring(this.CLASS_NAME.replace(/\./g, '').length, editionId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);
        descartesLayer.supportEnable = !descartesLayer.supportEnable;
        if (descartesLayer.supportEnable) {
            event.target.className = this.displayClasses.layerSupportOn;
        } else {
            event.target.className = this.displayClasses.layerSupportOff;
        }
        this.model.refreshSupportLayers(descartesLayer);
    },

    /**
     * Methode: jstreeContextMenuItemsFct
     * Retourne les items du menu contextuel.
     */
    jstreeContextMenuItemsFct: function (itemTree) {
        var self = this;
        var position = itemTree.id.substring(self.CLASS_NAME.replace(/\./g, '').length);
        var descartesItem = null;
        if (position !== '') {
            descartesItem = self.model.getItemByIndex(position);
        }
        var contextMenuItems = {};
        if (self.jstreeContextMenuItems) {
            if (descartesItem === null) {
                contextMenuItems = _.clone(self.jstreeContextMenuItems.itemsRoot);
            } else if (descartesItem.CLASS_NAME === "Descartes.Group") {
                contextMenuItems.addGroupItem = _.clone(self.jstreeContextMenuItems.itemsGroup.addGroupItem);
                contextMenuItems.updateGroupItem = _.clone(self.jstreeContextMenuItems.itemsGroup.updateGroupItem);
                contextMenuItems.deleteGroupItem = _.clone(self.jstreeContextMenuItems.itemsGroup.deleteGroupItem);
                contextMenuItems.addLayerItem = _.clone(self.jstreeContextMenuItems.itemsGroup.addLayerItem);
                contextMenuItems.addLayerWMSItem = _.clone(self.jstreeContextMenuItems.itemsGroup.addLayerWMSItem);
                if (!self.model.editInitialItems && !descartesItem.addedByUser) {
                    if (contextMenuItems.deleteGroupItem) {
                        contextMenuItems.deleteGroupItem._disabled = true;
                    }
                    if (contextMenuItems.updateGroupItem) {
                        contextMenuItems.updateGroupItem._disabled = true;
                    }
                }
            } else {
                contextMenuItems.updateLayerItem = _.clone(self.jstreeContextMenuItems.itemsLayer.updateLayerItem);
                contextMenuItems.deleteLayerItem = _.clone(self.jstreeContextMenuItems.itemsLayer.deleteLayerItem);
                contextMenuItems.exportVectorLayerItem = _.clone(self.jstreeContextMenuItems.itemsLayer.exportVectorLayerItem);
                if ((!self.model.editInitialItems && !descartesItem.addedByUser)) {
                    if (contextMenuItems.updateLayerItem) {
                        contextMenuItems.updateLayerItem._disabled = true;
                    }
                    if (contextMenuItems.deleteLayerItem) {
                        contextMenuItems.deleteLayerItem._disabled = true;
                    }
                    if (contextMenuItems.exportVectorLayerItem) {
                        contextMenuItems.exportVectorLayerItem._disabled = true;
                    }
                }
                if (!(descartesItem instanceof Descartes.Layer.EditionLayer || descartesItem instanceof Descartes.Layer.Vector)) {
                    if (contextMenuItems.exportVectorLayerItem) {
                        contextMenuItems.exportVectorLayerItem._disabled = true;
                    }
                }
            }
        }
        return contextMenuItems;
    },

    CLASS_NAME: 'Descartes.UI.LayersTree'
});

module.exports = Class;

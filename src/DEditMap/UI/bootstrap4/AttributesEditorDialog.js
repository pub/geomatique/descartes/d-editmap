/* global Descartes */

var Utils = Descartes.Utils;

var AbstractAttributesEditor = require('./AbstractAttributesEditor');
var ModalFormDialog = Descartes.UI.ModalFormDialog;

/**
 * Class: Descartes.UI.AttributesEditorDialog
 * Classe proposant, sous forme de boite de dialogue, la saisie des attributs d'un objet géographique.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractAttributesEditor>
 *
 * Evénements déclenchés:
 * uiSaveObject - La suavegarde des attributs.
 * uiCancel - l'annulation de la saisie des attributs.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(AbstractAttributesEditor, {

    EVENT_TYPES: ['uiSaveObject', 'uiCancel'],
    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par défaut des éléments HTML du panneau de légendes.
     *
     * globalClassName - ("DescartesModalDialogAttributesEditorLabelAndValue" par défaut)
     * labelClassName - ("DescartesModalDialogAttributesEditorLabel" par défaut)
     * inputClassName - ("DescartesModalDialogInput" par défaut)
     * textClassName - ("DescartesModalDialogText" par défaut)
     * selectClassName - ("DescartesModalDialogSelect" par défaut)
     * fieldSetClassName - ("DescartesModalDialogFieldSet" par défaut)
     * legendClassName - ("DescartesModalDialogLegend" par défaut)
     */
    defaultDisplayClasses: {
        globalClassName: 'DescartesModalDialogAttributesEditorLabelAndValue',
        labelClassName: 'DescartesModalDialogAttributesEditorLabel',
        inputClassName: 'DescartesModalDialogInput',
        textClassName: 'DescartesModalDialogText',
        selectClassName: 'DescartesModalDialogSelect',
        fieldSetClassName: 'DescartesModalDialogFieldSet',
        legendClassName: 'DescartesModalDialogLegend'
    },
    /**
     * Constructeur: Descartes.UI.AttributesEditorDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - null.
     * model - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.AttributesEditor>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des éléments HTML de la zone d'affichage des mesures.
     */
    initialize: function (div, model, options) {
        AbstractAttributesEditor.prototype.initialize.apply(this, [div, model, options]);
    },

    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des attributs.
     */
    draw: function () {
        var content = this.populateForm();

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_MESSAGE'),
            //formClass: 'form-horizontal',
            //sendLabel: this.getMessage('SEND_BUTTON'),
            sendLabel: this.getMessage('OK_BUTTON'),
            //size: 'modal-lg',
            content: content
        });

        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);

        dialog.open(done, cancel);
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'createObjet', ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.result = result;
        if (this.validateDatas()) {
            this.events.triggerEvent('uiSaveObject');
        } else {
            this.showErrors();
        }
    },
    cancel: function () {
        this.events.triggerEvent('uiCancel');
    },

    CLASS_NAME: 'Descartes.UI.AttributesEditorDialog'
});

module.exports = Class;

/* global Descartes */

require('../css/LayersTree_overloaded.css');

var Utils = Descartes.Utils;

var treeItemTemplate = require('./templates/TreeItemSimple.ejs');
var treeEditionItemTemplate = require('./templates/TreeEditionItem.ejs');

var LayersTreeSimple = Descartes.UI.LayersTreeSimple;

/**
 * Class: Descartes.UI.LayersTreeSimple
 * Classe affichant une arborescence du contenu de la carte et offrant des interactions sur celui-ci.
 *
 * Hérite de:
 * - <Descartes.UI>
 *
 * Evénements déclenchés:
 * layerSelected - Une couche a été sélectionnée.
 * groupSelected - Un groupe a été sélectionné.
 * nodeUnselect - Une couche ou une couche a été désélectionné.
 * rootSelected - La racine a été sélectionnée.
 */
var Class = Utils.Class(LayersTreeSimple, {

    _getStructureLayer: function (node) {
        var id = this.CLASS_NAME.replace(/\./g, '') + node.index.toString();
        var sliderId = id + 'SliderB';
        var visibleClassName = this.displayClasses.layerVisibilityOff;
        var visibleTitle = '';
        var needZoomClassName = null;

        var state = {
            opened: node.opened,
            disabled: true
        };
        if (node.hide) {
            state.hidden = true;
        }

        if (node.CLASS_NAME.indexOf('Descartes.Layer') === 0) {
            state.checked = node.visible;

            if (!node.acceptMaxScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomOut;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (!node.acceptMinScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomIn;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (node.visible) {
               visibleClassName = this.displayClasses.layerVisibilityOn;
            }
            if (node.CLASS_NAME.indexOf('Descartes.Layer.EditionLayer.GenericVector') === 0) {
                node.loading = false;
            }

            //récupération du tooltip à afficher
            if (node.isInScalesRange()) {
                visibleTitle = (node.visible ? this.getMessage('HIDE_LAYER') : this.getMessage('SHOW_LAYER'));
            } else if (node.minScale !== null && node.maxScale !== null) {
                visibleTitle = this.getMessage('MIN_SCALE') +
                        Utils.readableScale(node.minScale) +
                        this.getMessage('MAX_SCALE') +
                        Utils.readableScale(node.maxScale);
            } else if (node.minScale != null) {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.minScale);
            } else {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.maxScale);
            }
        }

        var itemTextClassName = this.displayClasses.itemText;

        var queryableInfo = this.getQueryableInfo(node);

        var text = treeItemTemplate({
            id: sliderId,
            node: node,
            displayIconLoading: this.model.displayIconLoading,
            editionTemplate: treeEditionItemTemplate,
            visibleClassName: visibleClassName,
            visibleTitle: visibleTitle,
            needZoomClassName: needZoomClassName,
            itemTextClassName: itemTextClassName,
            fctVisibility: this.model.fctVisibility,
            fctOpacity: this.model.fctOpacity,
            fctQueryable: this.model.fctQueryable,
            fctDisplayLegend: this.model.fctDisplayLegend,
            queryableClassName: queryableInfo.className,
            queryableTitle: queryableInfo.title
        });

        var json = {
            id: id,
            text: text,
            icon: '',
            state: state
        };

        return json;
    },

    CLASS_NAME: 'Descartes.UI.LayersTreeSimple'
});

module.exports = Class;

/* global Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var UI = Descartes.UI;
var ModalFormDialog = Descartes.UI.ModalFormDialog;
var HomotheticInputDialog = require('./templates/HomotheticInputDialog.ejs');

/**
 * Class: Descartes.UI.HomotheticEditorDialog
 * Classe proposant, sous forme de boite de dialogue, la saisie d'éléments
 * pour la transformation par extension/réduction homothétique.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 * Ecouteurs mis en place:
 * uiSave - La suavegarde des options.
 * uiCancel - l'annulation de la saisie des options.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(UI, {

    EVENT_TYPES: ['uiSave', 'uiCancel'],
    /**
     * Constructeur: HomotheticEditorDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * coordinatesModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.HomotheticEditor>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     */
    initialize: function (div, model, options) {
        UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: draw
     * Ouvre la boite de dialogue modale pour la saisie des infos.
     */
    draw: function () {

        var content = HomotheticInputDialog({
            id: this.id,
            label: this.getMessage('BUFFER_LABEL'),
            bufferValue: this.value,
            coordLabel: this.getMessage('CENTER_MESSAGE'),
            xLabel: this.getMessage('X_LABEL'),
            yLabel: this.getMessage('Y_LABEL'),
            coord: this.coord,
            clickLabel: this.getMessage('CLICK_MESSAGE'),
            click: this.click
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_MESSAGE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            content: content
        });
        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);
        dialog.open(done, cancel);
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'createObjet',
     * ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.result = result;
        if (this.validateDatas(result)) {
            this.events.triggerEvent('uiSave');
        } else {
            this.showErrors();
        }
    },
    cancel: function () {
        this.events.triggerEvent('uiCancel');
    },

    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function (result) {
        this.errors = [];


        this.model.buffer = Number(result.buffer.replace(',', '.'));

        if (this.coord) {
            this.model.center = [
                Number(result.x.replace(',', '.')),
                Number(result.y.replace(',', '.'))
            ];

        }

        return (this.errors.length === 0);
    },
    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = '';
            _.each(this.errors, function (error) {
                errorsMessage += error + '\n';
            });
            alert(errorsMessage);
        }
    },

    CLASS_NAME: 'Descartes.UI.HomotheticEditorDialog'
});

module.exports = Class;

/* global Descartes */

var Utils = Descartes.Utils;
var UI = Descartes.UI;

var ModalFormDialog = Descartes.UI.ModalFormDialog;

/**
 * Class: Descartes.UI.MapPreviewDialog
 * Classe permettant la visualisation d'objets géographiques dans une carte sous forme de boite de dialogue.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(UI, {

    EVENT_TYPES: ['uiSave', 'uiCancel'],

    /**
     * Constructeur: Descartes.UI.MapPreviewDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * MapPreview - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.MapPreview>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     */
    initialize: function (div, model, options) {
        UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },
    draw: function () {
        var mapId = this.id + '_map';
        var content = '<div id="' + mapId + '"></div>';

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title || this.getMessage('TITLE_MESSAGE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            content: content
        });

        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);

        dialog.open(done, cancel);
        dialog.dialog.on('shown.bs.modal', function () {
            this.model.loadMap(mapId);
        }.bind(this));

    },
    /**
     * Methode: done
     */
    done: function (e) {
        if (this.model.validateDatas()) {
            this.events.triggerEvent('uiSave', this.arguments);
            return true;
        }
        return false;
    },
    cancel: function () {
        this.events.triggerEvent('uiCancel', this.arguments);
    },

    CLASS_NAME: 'Descartes.UI.MapPreviewDialog'
});

module.exports = Class;

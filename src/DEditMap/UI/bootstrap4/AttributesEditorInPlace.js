/* global Descartes */
var $ = require('jquery');

var Utils = Descartes.Utils;

var AbstractAttributesEditor = require('./AbstractAttributesEditor');
var AbstractInPlace = Descartes.UI.AbstractInPlace;

/**
 * Class: Descartes.UI.AttributesEditorInPlace
 * Classe proposant la saisie des attributs d'un objet géographique.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractAttributesEditor>
 *  - <Descartes.UI.AbstractInPlace>
 *
 *
 * Evénements déclenchés:
 * uiCreateObject - Création d'une géométrie.
 */
var Class = Utils.Class(AbstractInPlace, AbstractAttributesEditor, {

    EVENT_TYPES: ['uiCreateObject'],
    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par défaut des éléments HTML du panneau de légendes.
     *
     * textClassName - ("DescartesUIText" par défaut)
     * labelClassName - ("DescartesUILabel" par défaut)
     * inputClassName - ("DescartesUIInput" par défaut)
     * titleClassName - ("DescartesUITitle" par défaut)
     * buttonClassName - ("DescartesUIButton" par défaut)
     */
    defaultDisplayClasses: {
        textClassName: 'DescartesUIText',
        labelClassName: 'DescartesUILabel',
        inputClassName: 'DescartesUIInput',
        titleClassName: 'DescartesUITitle',
        buttonClassName: 'DescartesUIButton'
    },
    /**
     * Propriete: label
     * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     */
    label: true,
    /**
     * Constructeur: Descartes.UI.AttributesEditorInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * coordinatesModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.AttributesEditor>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des éléments HTML de la zone d'affichage des mesures.
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     */
    initialize: function (div, model, options) {
        // S'il l'utilisateur a definit le layer on recupere les attributs a editer
        if (options.editionLayer && options.editionLayer.attributesEditable) {
            options.attributesEditable = options.editionLayer.attributesEditable;
        }
        AbstractInPlace.prototype.initialize.apply(this, [div, model, options]);
        AbstractAttributesEditor.prototype.initialize.apply(this, [div, model, options]);
    },

    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des attributs.
     */
    draw: function () {
        var that = this;

        var content = '<form id="' + this.id + '" class="form-horizontal">';
        content += this.populateForm();
        content += '<button type="submit" class="btn ' + Descartes.UIBootstrap4Options.btnCssSubmit + '">';
        content += this.getMessage('BUTTON_MESSAGE');
        content += '</button>';
        content += '</form>';

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            that.result = Utils.serializeFormArrayToJson($(this).serializeArray());
            that.done();
        });
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'createObjet', ou affiche les messages d'erreur rencontrés.
     */
    done: function () {
        if (this.validateDatas()) {
            this.events.triggerEvent('uiCreateObject');
        } else {
            this.showErrors();
        }
    },

    CLASS_NAME: 'Descartes.UI.AttributesEditorInPlace'
});

module.exports = Class;

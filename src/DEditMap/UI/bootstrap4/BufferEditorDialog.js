/* global Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var UI = Descartes.UI;
var ModalFormDialog = Descartes.UI.ModalFormDialog;
var BufferInputDialog = require('./templates/BufferInputDialog.ejs');

/**
 * Class: Descartes.UI.BufferEditorDialog
 * Classe proposant, sous forme de boite de dialogue, la saisie d'éléments pour la transformation par extension (buffer).
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Ecouteurs mis en place:
 * uiSave - La sauvegarde des options.
 * uiCancel - l'annulation de la saisie des options.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(UI, {

    EVENT_TYPES: ['uiSave', 'uiCancel'],
    /**
     * Constructeur: BufferEditorDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * coordinatesModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.CoordinatesInput>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     * value - {String} Contenu initial de la zone de saisie (vide par défaut).
     */
    initialize: function (div, model, options) {
        UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des attributs.
     */
    draw: function () {

        var content = BufferInputDialog({
            id: this.id,
            bufferLabel: this.getMessage('BUFFER_LABEL'),
            nbPointsLabel: this.getMessage('NB_POINTS_LABEL'),
            isHalo: this.isHalo,
            bufferValue: this.value
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_MESSAGE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            content: content
        });
        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);
        dialog.open(done, cancel);
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'createObjet',
     * ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.result = result;
        if (this.validateDatas(result)) {
            this.events.triggerEvent('uiSave');
        } else {
            this.showErrors();
        }
    },
    cancel: function () {
        this.events.triggerEvent('uiCancel');
    },

    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function (result) {
        this.errors = [];

        this.model.buffer = Number(result.buffer);

        if (result.nbPoints) {
            this.model.nbPoints = Number(result.nbPoints);
        }

        if (!_.isNumber(this.model.buffer)) {
            this.errors.push(this.getMessage('FORMAT_BUFFER_ERROR'));
        }
        if (this.isHalo && !_.isNil(this.model.nbPoints) &&
                this.model.nbPoints !== '' &&
                !_.isNumber(this.model.nbPoints)) {
            this.errors.push(this.getMessage('FORMAT_NBPOINTS_ERROR'));
        }

        return (this.errors.length === 0);
    },
    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = '';
            _.each(this.errors, function (error) {
                errorsMessage += error + '\n';
            });
            alert(errorsMessage);
        }
    },

    CLASS_NAME: 'Descartes.UI.BufferEditorDialog'
});

module.exports = Class;

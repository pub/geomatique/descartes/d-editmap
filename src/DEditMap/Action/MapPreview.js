/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');
var $ = require('jquery');

var Utils = Descartes.Utils;

var Action = Descartes.Action;
var EventManager = Descartes.Utils.EventManager;

var template = require('../UI/' + MODE + '/templates/showAttributesOverlay.ejs');
var MapPreviewDialog = require('../UI/' + MODE + '/MapPreviewDialog');

/**
 * Class: Descartes.Action.MapPreview
 * Classe permettant la visualisation et la selection d'objet dans une carte
 *
 * Hérite de:
 * - <Descartes.Action>
 */
var Class = Utils.Class(Action, {
    EVENT_TYPES: ['save', 'cancel'],
    featureSelectStyle: null,
    featureStyle: null,

    /**
     * Constructeur: Descartes.Action.MapPreview
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * OL_map - {OpenLayers.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * Options propres à la vue associée.
     * Voir <Descartes.UI.AttributesEditorInPlace> | <Descartes.UI.AttributesEditorDialog> pour la vue par défaut.
     */
    initialize: function (div, olMap, options) {
        Action.prototype.initialize.call(this, div, olMap);

        this.model.selectedFeatures = [];

        //Creation d'une carte
        var layers = [];
        this.features;
        var selectedFeatures = [];
        this.selectionActive = true;

        if (options && options.features) {
            this.features = options.features;
            if (options.doNotSelect) {
                selectedFeatures = options.features;
                this.selectionActive = false;
            }
        }

        if (options && options.featureSelectStyle) {
            this.featureSelectStyle = options.featureSelectStyle;
        }

        if (options && options.featureStyle) {
            this.featureStyle = options.featureStyle;
        }

        var tempLayer = new ol.layer.Vector({
            title: 'tempLayer',
            style: options.featureStyle,
            selectedFeatures: selectedFeatures,
            source: new ol.source.Vector({
                features: this.features
            })
        });
        layers.push(tempLayer);

        if (options && options.layers) {
            layers.push(options.layers);
        }

        this.multiple = true;
        if (!_.isNil(options) && !_.isNil(options.multiple)) {
            this.multiple = options.multiple;
        }
        this.model.validateDatas = this.validateDatas.bind(this);
        this.model.loadMap = function (target) {
            var initView = olMap.getView();
            this.previewMap = new ol.Map({
                layers: layers,
                controls: ol.control.defaults({
                    attribution: false,
                    rotate: false,
                    zoom: true
                }),
                target: target,
                view: new ol.View({
                    projection: initView.getProjection(),
                    center: initView.getCenter(),
                    zoom: initView.getZoom()
                })
            });
            var extent = tempLayer.getSource().getExtent();
            this.previewMap.getView().fit(extent, this.previewMap.getSize());

            var toggleCondition;
            if (this.multiple) {
                toggleCondition = function (event) {
                    if (event.type === 'singleclick') {
                        var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                            return feature;
                        }, {
                            layerFilter: function (aLayer) {
                                return aLayer === tempLayer;
                            }
                        });
                        return !_.isNil(features);
                    }
                    return false;
                };
            }

            this.selectInteraction = new ol.interaction.Select({
                multi: this.multiple,
                style: function (feature, resolution) {
                    var featureStyleFunction = feature.getStyleFunction();
                    if (featureStyleFunction) {
                        return featureStyleFunction.call(feature, resolution);
                    } else {
                        return options.featureSelectStyle;
                    }
                },
                toggleCondition: toggleCondition
            });
            this.previewMap.on('pointermove', function (evt) {
                var hit = this.previewMap.hasFeatureAtPixel(evt.pixel, {
                    layerFilter: function (aLayer) {
                        return aLayer === tempLayer;
                    }
                });
                this.previewMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
            }.bind(this), this);

            if (options.showAttributes) {
                this.selectInteraction.on('select', this.toggleFeatureSelected.bind(this), this);
            }
            if (this.selectionActive === true) {
                this.previewMap.addInteraction(this.selectInteraction);
            }
        }.bind(this);

        this.events = new EventManager();

        if (_.isNil(this.renderer)) {
            this.renderer = new MapPreviewDialog(div, this.model, options);
        }

        this.renderer.events.register('uiSave', this, this.onSave);
        this.renderer.events.register('uiCancel', this, this.onCancel);
        this.renderer.draw();
    },
    toggleFeatureSelected: function (event) {
        _.each(event.deselected, function (feature) {
            this.unhighlight(feature);
            feature.setStyle(this.featureStyle);
        }.bind(this));
        _.each(event.selected, function (feature) {
            this.highlight(feature);
            feature.setStyle(this.featureSelectStyle);
        }.bind(this));
    },
    highlight: function (feature) {
        var element = this.createHighlight(feature);

        var extent = feature.getGeometry().getExtent();
        var position = ol.extent.getTopRight(extent);

        this._showInfoOverlay = new ol.Overlay({
            element: element,
            autoPan: true
        });
        this.previewMap.addOverlay(this._showInfoOverlay);
        this._showInfoOverlay.setPosition(position);

        this.afterHighlight();
    },
    unhighlight: function () {
        this.previewMap.removeOverlay(this._showInfoOverlay);
    },
    createHighlight: function (feature) {
        this.highlightId = Utils.createUniqueID();

        var attributes = _.clone(feature.getProperties());
        delete attributes[feature.getGeometryName()];
        delete attributes.selected;
        delete attributes.boundedBy;
        delete attributes.state;

        var html = template({
            id: this.highlightId,
            noAttributeMsg: this.getMessage('PREVIEW_MAP_NO_ATTRIBUTES'),
            attributes: attributes
        });

        var div = document.createElement('div');

        div.innerHTML = html.trim();
        return div.firstChild;
    },
    afterHighlight: function () {
        $('#' + this.highlightId + '_close').click(function () {
            this.unhighlight();
        }.bind(this));
    },
    validateDatas: function () {
        if (this.selectionActive) {
            var nb = this.selectInteraction.getFeatures().getLength();
            if (nb === 0) {
                alert(this.getMessage('WARNING_NO_GEOMETRY'));
                return false;
            }
            if (!this.multiple && nb > 1) {
                alert(this.getMessage('WARNING_ONLY_ONE_GEOMETRY'));
                return false;
            }
        }
        return true;
    },
    /**
     * Methode: onSaveObject
     * Appelé lorque l'utilisateur sauvegarde.
     */
    onSave: function () {
        var features;
        if (this.selectionActive) {
            if (this.selectInteraction.getFeatures().getLength() === 0) {
                alert(this.getMessage('WARNING_NO_GEOMETRY'));
                return false;
            }
            features = this.selectInteraction.getFeatures();
        } else {
            features = this.features;
        }
        return this.events.triggerEvent('save', features);
    },
    /**
     * Methode: onCancel
     * Appelé lorsque l'utilisateur annule.
     */
    onCancel: function () {
        this.events.triggerEvent('cancel', []);
    },
    CLASS_NAME: 'Descartes.Action.MapPreview'
});
module.exports = Class;

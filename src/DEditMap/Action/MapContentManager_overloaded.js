/* global MODE, Descartes*/

var _ = require('lodash');
var Utils = Descartes.Utils;
var I18N = Descartes.I18N;
var ContentManagerConstants = Descartes.Action.MapContentManager;
var MapContentManager = Descartes.Action.MapContentManager;
var Group = require('../Model/Group_overloaded');

var AddGroup = Descartes.Button.ContentTask.AddGroup;
var AlterGroup = Descartes.Button.ContentTask.AlterGroup;
var RemoveGroup = require('../Button/ContentTask/RemoveGroup_overloaded');
var AddLayer = Descartes.Button.ContentTask.AddLayer;
var AlterLayer = Descartes.Button.ContentTask.AlterLayer;
var RemoveLayer = require('../Button/ContentTask/' + MODE + '/RemoveLayer_overloaded');
var ChooseWmsLayers = Descartes.Button.ContentTask.ChooseWmsLayers;
var ExportVectorLayer = require('../Button/ContentTask/' + MODE + '/ExportVectorLayer_overloaded');

var LayersTree = require('../UI/' + MODE + '/LayersTree_overloaded');
var LayersTreeSimple = require('../UI/' + MODE + '/LayersTreeSimple_overloaded');

var Class = Utils.Class(MapContentManager, {

    /**
     * Constructeur: Descartes.Action.MapContentManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'arborescence ou identifiant de cet élement.
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * uiOptions - {Object} Objet JSON stockant les options de la vue générant l'arborescence (voir <Descartes.UI.LayersTree.Descartes.UI.LayersTree>).
     */
    initialize: function (div, mapContent, options) {
        I18N.prototype.initialize.call(this, [arguments]);
        this.availableTools = {
            AddGroup: AddGroup,
            AddLayer: AddLayer,
            RemoveGroup: RemoveGroup,
            RemoveLayer: RemoveLayer,
            AlterGroup: AlterGroup,
            AlterLayer: AlterLayer,
            ChooseWmsLayers: ChooseWmsLayers,
            ExportVectorLayer: ExportVectorLayer
        };
        this.mapContent = mapContent;

        var contextMenuTools = null;
        var uiOptions = {};
        if (!_.isNil(options)) {
            _.extend(uiOptions, options.uiOptions);
            contextMenuTools = uiOptions.contextMenuTools;
            delete uiOptions.contextMenuTools;
            delete options.uiOptions;
        }
        _.extend(this, options);

        if (this.onlySimpleVue) {
            this.uiLayersTree = new LayersTreeSimple(Utils.getDiv(div), this.mapContent, uiOptions);
        } else {
            this.uiLayersTree = new LayersTree(Utils.getDiv(div), this.mapContent, uiOptions);

            if (contextMenuTools) {
                 this.uiLayersTree.setJstreeContextMenuItems(this.configContextMenuItems(contextMenuTools));
            }

            if (this.mapContent.editable) {
                this.uiLayersTree.events.register('layerSelected', this, this.layerSelected);
                this.uiLayersTree.events.register('groupSelected', this, this.groupSelected);
                this.uiLayersTree.events.register('nodeUnselect', this, this.nodeUnselect);
                this.uiLayersTree.events.register('rootSelected', this, this.rootSelected);
            }
        }

    },

    /**
     * Methode: addTool
     * Ajoute un outil au gestionnaire.
     *
     * Paramètres:
     * toolDef - {Object} Objet JSON décrivant l'outil.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,            // parmi les valeurs de Descartes.Action.MapContentManager.CONTENT_TOOLS_NAME
     * 		className: <classe personnalisée>,  // classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		options: <options>                  // options propres à l'outil
     * }
     * (end)
     */
    addTool: function (toolDef) {
        if (ContentManagerConstants.CONTENT_TOOLS_NAME.indexOf(toolDef.type) !== -1) {
            var ClassName;
            if (!_.isNil(toolDef.className)) {
                ClassName = toolDef.className;
            } else {
                ClassName = this.availableTools[toolDef.type];
            }
            var tool = new ClassName(toolDef.options);
            this.tools[toolDef.type] = tool;
            if (tool instanceof Descartes.Button.ContentTask.ExportVectorLayer) {
                this.mapContent.events.register('layerUnderEdition', tool, tool.checkLayerUnderEdition);
            }
        }
    },

    /**
     * Methode: layerSelected
     * Active les outils correspondant au contexte où une couche est sélectionnée dans l'arborescence.
     */
    layerSelected: function () {
        this._desactiveTools();
        var layer = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        if (this.mapContent.editInitialItems || layer.addedByUser) {
            this._activeTool(ContentManagerConstants.REMOVE_LAYER_TOOL, this.removeLayer, layer.toJSON());
            this._activeTool(ContentManagerConstants.ALTER_LAYER_TOOL, this.alterLayer, layer.toJSON());
            if (layer instanceof Descartes.Layer.Vector || layer instanceof Descartes.Layer.EditionLayer) {
                this._activeTool(ContentManagerConstants.EXPORT_VECTORLAYER_TOOL, null, layer);
            }
        }
    },

    /**
     * Methode: createGroup
     * Ajoute un groupe au contenu de la carte et actualise l'arborescence.
     */
    createGroup: function () {
        var title = this.tools.AddGroup.datas.title;
        var opened = this.tools.AddGroup.datas.opened;
        var parentGroup = this._getMapContentItem(this.uiLayersTree.selectedItem);

        var childsParentGroup = parentGroup.items;
        if (childsParentGroup.length > 0) {
            this.mapContent.insertItemBefore(
                    new Group(title, {
                        opened: opened,
                        addedByUser: true
                    }),
                    this.mapContent.getItemByIndex(childsParentGroup[0].index));
        } else {
            this.mapContent.addItem(new Group(title, {
                opened: opened,
                addedByUser: true
            }), parentGroup);
        }

        this.tools.AddGroup.datas = {};
        this.redrawUI();
    },

    CLASS_NAME: 'Descartes.Action.MapContentManager'
});

module.exports = Class;

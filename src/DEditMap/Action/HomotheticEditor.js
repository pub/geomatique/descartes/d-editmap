/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;
var EventManager = Descartes.Utils.EventManager;
var Action = Descartes.Action;

var HomotheticEditorDialog = require('../UI/' + MODE + '/HomotheticEditorDialog');

/**
 * Class: Descartes.Action.HomotheticEditor
 * Classe permettant l'affichage du formulaire pour la transformation homothetie
 *
 * Hérite de:
 * - <Descartes.Action>
 */
var Class = Utils.Class(Action, {
    /**
     * Constructeur: Descartes.Action.HomotheticEditor
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * OL_map - {OpenLayers.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * Options propres à la vue associée.
     */
    initialize: function (div, olMap, options) {
        if (!_.isNil(options)) {
            _.extend(this, options);
        }

        Action.prototype.initialize.call(this, div, olMap);
        this.model.buffer = null;

        this.events = new EventManager();
        if (_.isNil(this.renderer)) {
            this.renderer = new HomotheticEditorDialog(div, this.model, options);
        }
        this.renderer.events.register('uiSave', this, this.onSave);
        this.renderer.events.register('uiCancel', this, this.onCancel);
        this.renderer.draw();
    },

    /**
     * Methode: onSave
     * Appelé lorque l'utilisateur sauvegarde.
     */
    onSave: function () {
        return this.events.triggerEvent('save', this.model);
    },
    /**
     * Methode: onCancel
     * Appelé lorsque l'utilisateur annule.
     */
    onCancel: function () {
        this.events.triggerEvent('cancel', []);
    },
    CLASS_NAME: 'Descartes.Action.HomotheticEditor'
});
module.exports = Class;

/* global MODE, Descartes */

var _ = require('lodash');
var Utils = Descartes.Utils;

var MapConstants = Descartes.Map;

var BookmarksManager = Descartes.Action.BookmarksManager;

var Class = Utils.Class(BookmarksManager, {

    /**
     * Methode: refreshVue
     * Actualise la carte avec le contexte correspondant à la vue demandée.
     *
     * Paramètres:
     * response - {String} Réponse AJAX contenant le contexte sous forme d'objet JSON.
     */
    refreshVue: function (response) {
        if (response.charAt(0) !== '{') {
            response = response.substring(response.indexOf('{'));
        }
        var context = JSON.parse(response);
        var content = context.items;
        this.map.mapContentManager.populate(content);
        this.map._initializeLayersIconSnap();
        this.map._initializeLayersIconAutoTracing();
        this.map._initializeLayersIconSupport();

        var bbox = context.bbox;
        var mapType = this.map.OL_map.get('mapType');
        var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;

        this.map.OL_map.getView().fit([bbox.xMin, bbox.yMin, bbox.xMax, bbox.yMax], {
            constrainResolution: constrainResolution,
            minResolution: this.map.OL_map.getView().getMinResolution(),
            maxResolution: this.map.OL_map.getView().getMaxResolution()
        });
        var mapSize = context.size;
        if (!_.isNil(this.map.sizeSelector)) {
            this.map.sizeSelector.selectSize(mapSize.w, mapSize.h);
        }
        if (this.map.selectToolTip && this.map.selectToolTip.selectToolTipLayers && this.map.selectToolTip.selectToolTipLayers.length !== 0) {
           this.map.selectToolTip.selectToolTipLayers.forEach(function (selectToolTipLayer) {
              if (selectToolTipLayer.layer.id === Descartes.AnnotationsLayer.id) {
                  selectToolTipLayer.layer.OL_layers[0] = Descartes.AnnotationsLayer.OL_layers[0];
              }
           }, this);
        }
        this.map.OL_map.removeInteraction(this.map.selectToolTip.selectInteraction);
        this.map.OL_map.addInteraction(this.map.selectToolTip.selectInteraction);
        this.map.selectToolTip.selectInteraction.setActive(true);
    },

    CLASS_NAME: 'Descartes.Action.BookmarksManager'
});

module.exports = Class;

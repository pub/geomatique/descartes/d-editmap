/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var Action = Descartes.Action;

var AttributesEditorInPlace = require('../UI/' + MODE + '/AttributesEditorInPlace');

var EventManager = Descartes.Utils.EventManager;

var MapConstants = Descartes.Map;
var FeatureState = require('../Model/FeatureState');

/**
 * Class: Descartes.Action.AttributesEditor
 * Classe permettant la saisie des attributs des objets géographiques.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *   editionLayer: <la couche d'édition>,
 *   attributesEditable: <la liste des attributs éditables>
 *   feature: <objet manipulé>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'uiSaveObject' de la classe définissant la vue MVC associée (<Descartes.UI.AttributesEditorInPlace> par défaut) déclenche la méthode <onSaveObject>.
 *  - l'événement 'uiCreateObject' de la classe définissant la vue MVC associée (<Descartes.UI.AttributesEditorInPlace> par défaut) déclenche la méthode <onCreateObject>.
 *  - l'événement 'uiCancel' de la classe définissant la vue MVC associée (<Descartes.UI.AttributesEditorInPlace> par défaut) déclenche la méthode <onCancel>.
 */
var Class = Utils.Class(Action, {

    EVENT_TYPES: ['saveObject', 'cancel'],
    /**
     * Constructeur: Descartes.Action.AttributesEditor
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * OL_map - {OpenLayers.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * Options propres à la vue associée.
     * Voir <Descartes.UI.AttributesEditorInPlace> | <Descartes.UI.AttributesEditorDialog> pour la vue par défaut.
     */
    initialize: function (div, olMap, options) {
        Action.prototype.initialize.apply(this, arguments);

        if (options) {
            if (options.editionLayer) {
                this.model.editionLayer = options.editionLayer;
                if (options.editionLayer.attributes) {
                    this.model.attributesEditable = options.editionLayer.attributes.attributesEditable;
                }

                this.model.layerTitle = options.editionLayer.title;
                delete options.editionLayer;
            }

            if (options.feature) {
                this.model.feature = options.feature;
                delete options.feature;
            }

            if (options.objectId) {
                this.model.objectId = options.objectId;
                delete options.objectId;
            } else {
                this.model.objectId = 'descartesId_' + Math.random().toString(36).slice(2);
            }

            if (options.activeTool) {
                this.model.activeTool = options.activeTool;
                delete options.activeTool;
            }
        }

        this.events = new EventManager();

        if (_.isNil(this.renderer)) {
            this.renderer = new AttributesEditorInPlace(div, this.model, options);
        }

        this.renderer.events.register('uiSaveObject', this, this.onSaveObject);
        this.renderer.events.register('uiCreateObject', this, this.onCreateObject);
        this.renderer.events.register('uiCancel', this, this.onCancel);

        this.renderer.draw(this.model);
    },

    /**
     * Methode onCreateObject
     * Listener appelé lorsque l'utilisateur veut créer un objet géographique.
     */
    onCreateObject: function () {
        var json = {
            editionLayer: this.model.editionLayer,
            objectId: this.model.objectId,
            attributs: this.model.attributesEditable,
            activeTool: MapConstants.EDITION_DRAW_CREATION
        };
        Descartes.EditionManager.createObject(json);
    },
    /**
     * Methode: onSaveObject
     * Appelé lorque l'utilisateur souhaite enregistrer les attributs.
     */
    onSaveObject: function () {
        this.model.feature.editAttribut = true;
        this.model.feature.attributesEditable = [];
        if (this.model.attributesEditable) {
            if (this.model.feature.modified !== null &&
                    this.model.feature.state === FeatureState.UPDATE &&
                    (Object.getOwnPropertyNames(this.model.feature.attributes).length > 0)) {
                this.model.feature.modified.attributes = _.extend({}, this.model.feature.attributes);
            }

            for (var i = 0; i < this.model.attributesEditable.length; i++) {
                var attribut = this.model.attributesEditable[i];
                this.model.feature.set(attribut.fieldName, attribut.value);
            }
        }
        this.events.triggerEvent('saveObject', this.model.feature);
    },
    /**
     * Methode: onCancel
     * Appelé lorsque l'utilisateur annule l'enregistrement des attributs.
     */
    onCancel: function () {
        this.events.triggerEvent('cancel', this.model.feature);
    },
    CLASS_NAME: 'Descartes.Action.AttributesEditor'
});
module.exports = Class;

/* global MODE */
var _ = require('lodash');

var CreationNamespace = require('./Edition/CreationNamespace');
var ModificationNamespace = require('./Edition/ModificationNamespace');
var DeletionNamespace = require('./Edition/DeletionNamespace');
var InformationNamespace = require('./Edition/InformationNamespace');
var EditionConstants = require('./EditionConstants');
var AnnotationConstants = require('./Edition/Annotation/AnnotationConstants');
var Selection = require('./Edition/Selection');
var CompositeSelection = require('./Edition/CompositeSelection');
var SelectionAttribut = require('./Edition/Attribute/SelectionAttribute');
var Attribute = require('./Edition/Attribute/Attribute');
var Save = require('./Edition/Save');
var ExportAnnotation = require('./Edition/Annotation/ExportAnnotation');
var ImportAnnotation = require('./Edition/Annotation/ImportAnnotation');
var AideAnnotation = require('./Edition/Annotation/AideAnnotation');
var AttributeAnnotation = require('./Edition/Annotation/AttributeAnnotation');
var SnappingAnnotation = require('./Edition/Annotation/SnappingAnnotation');

var namespace = {
	Creation: CreationNamespace,
	Modification: ModificationNamespace,
	Deletion: DeletionNamespace,
	Selection: Selection,
	Information: InformationNamespace,
	CompositeSelection: CompositeSelection,
	SelectionAttribut: SelectionAttribut,
	Attribute: Attribute,
	Save: Save,
	ExportAnnotation: ExportAnnotation,
	ImportAnnotation: ImportAnnotation,
	AideAnnotation: AideAnnotation,
	AttributeAnnotation: AttributeAnnotation,
    SnappingAnnotation: SnappingAnnotation
};

var Edition = require('./Edition');
_.extend(Edition, EditionConstants);
_.extend(Edition, AnnotationConstants);
_.extend(Edition, namespace);

module.exports = Edition;

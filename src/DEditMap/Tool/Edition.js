/* global Descartes, MODE */
var _ = require('lodash');
var jsts = require('jsts');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Tool = Descartes.Tool;
var Messages = require('../Messages');
var EditionConstants = require('./EditionConstants');

var WMS = Descartes.Layer.WMS;
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../Model/EditionLayerConstants');

var Symbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Tool.Edition
 * Classe "abstraite" pour les outils d'édition.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {
    /**
     * Propriete: editionLayer
     * {<Descartes.Layer.EditionLayer>} Layer d'édition associé à l'outil.
     */
    editionLayer: null,
    /**
     * Propriete: geometryType
     * {<Descartes.Layer.POLYGON_GEOMETRY> | <Descartes.Layer.LINE_GEOMETRY> | <Descartes.Layer.POINT_GEOMETRY>} Type de géométrie.
     */
    geometryType: null,
    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} le contenu de la carte.
     */
    mapContent: null,
    /**
     * Propriete: available
     * {Boolean}, indique si l'outil est disponible ou non.
     */
    available: false,
    /**
     * Propriete: snapping
     * {Boolean} Indique si l'outil utilise ou non la fonction de magnétisme.
     */
    snapping: false,
    /**
     * Propriete: autotracing
     * {Boolean} Indique si l'outil utilise ou non la fonction de magnétisme avec tracé automatique.
     */
    autotracing: false,
    /**
     * Propriété: editAttribut
     * {boolean} activation ou non de la demande d'édition des attributs lors de la creation de nouvelles géométries.
     */
    editAttribut: false,
    /*
     * Propriete privée : _snappingInteraction
     * Outil de snapping.
     */
    _snappingInteraction: null,
    /**
     * Propriete privée : _supportLayers
     * Liste les layers de support
     */
    _supportLayers: [],

    _supportLayersTopoConformity: [],
    /**
     * ToolConfiguration
     */
    _toolConfig: null,
    /*
     * Propriété : _OLToolInitialize
     * {Boolean}, indique si l'outil openlayer a été initialisé.
     * Utile pour le mode piloté par la carte.
     */
    _OLToolInitialize: false,

    _parser: new jsts.io.OL3Parser(),
    /**
     * Constructeur: Descartes.Tool.Edition
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * editAttribut - {boolean} activation ou non de la demande d'édition des attributs lors de la creation de nouvelles géométries.
     * geometryType - {<Descartes.Layer.POLYGON_GEOMETRY> | <Descartes.Layer.LINE_GEOMETRY> | <Descartes.Layer.POINT_GEOMETRY>} Type de géométrie.
     * snapping - {boolean} activation ou non du magnétisme.
     * autotracing - {boolean} activation ou non du tracé automatique.
     */
    initialize: function (options) {
        this.setAvailable(false);
        Tool.prototype.initialize.apply(this, arguments);
        if (options) {
            _.extend(this, options);
        }
        this.aide = Descartes.Messages.Descartes_EDITION_NOT_AIDE;
        if (this.getMessage('AIDE')) {
            this.aide = this.getMessage('AIDE');
        }
    },
    /**
     * Methode: isAvailable
     * Retourne true si l'outil est disponible, false sinon
     */
    isAvailable: function () {
        return this.available;
    },
    /**
     * Methode: activate
     * Rend l'outil actif parmi l'ensemble des outils de type <Descartes.Tool> présents la barre d'outils à laquelle il appartient.
     * L'outil est activé seulement s'il est disponible.
     */
    activate: function () {
        var active = this.isActive();

        if (this.isAvailable() && !active) {

            this._hideVectorLayerAdresse();

            Tool.prototype.activate.apply(this, arguments);

            this.addLayerSupportListener(this, this._updateSupportsLayers);

            this._activateSupportLayers();

            if (!Descartes.EditionManager.isGlobalEditonMode()) {
                this.editionLayer.enableEdition();
                this.refreshEditionState();
                if (!this._activateSnapping()) {
                    this.mapContent.refreshOnlyContentManager();
                }
                this.editionLayer.enableEdition();
            } else {
                this._activateSnapping();
            }

            if (Descartes.EditionManager.onlyAppMode && active) {
                this._updateOLFeature(this.editionLayer.getFeatureOL_layers()[0]);
            }

            if (this.editionLayer.verifTopoConformity &&
                    this.editionLayer.verifTopoConformity.supportLayersIdentifier &&
                    this.editionLayer.verifTopoConformity.supportLayersIdentifier.length > 0) {
                this._activateSupportLayersForTopoConformity();
            }

            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(this.aide);
            }
        }
    },
    /**
     * Methode: deactivate
     * Rend l'outil inactif parmi l'ensemble des outils de type <Descartes.Tool> présents la barre d'outils à laquelle il appartient.
     * L'outil est désactivé seulement s'il est disponible.
     */
    deactivate: function (disableEdition, refreshLayerTree) {
        if (this.isAvailable() && this.isActive()) {
            if (Descartes.EditionManager.onlyAppMode) {
                this._deactivateSnapping(true);
                this._deactivateSupportLayers(true);

                if (!_.isNil(this.active)) {
                    this.active = false;
                }
                this.setAvailable(false);

                this.removeLayerSupportListener(this, this._updateSupportsLayers);

                if (this._supportLayersTopoConformity.length > 0) {
                    this._deactivateSupportLayersForTopoConformity();
                }

                Tool.prototype.deactivate.apply(this, arguments);

                if (this.editionLayer) {
                    this.editionLayer.disableEdition();
                    this.mapContent.refreshOnlyContentManager();
                }
            } else if (this.editionLayer && !this.underActivation) {

                var disableEditionAndRefreshTree = !Descartes.EditionManager.isGlobalEditonMode() &&
                        (disableEdition === true || disableEdition === undefined);

                this._enableDirectionalPanPanel();
                this._deactivateSnapping(!disableEditionAndRefreshTree);
                this._deactivateSupportLayers(!disableEditionAndRefreshTree);

                this.removeLayerSupportListener(this, this._updateSupportsLayers);

                if (this._supportLayersTopoConformity.length > 0) {
                    this._deactivateSupportLayersForTopoConformity();
                }

                Tool.prototype.deactivate.apply(this, arguments);

                if (disableEditionAndRefreshTree) {
                    this.editionLayer.disableEdition();
                    if (refreshLayerTree !== false) {
                        this.mapContent.refreshOnlyContentManager();
                    }
                }
            }

            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
            }
        }
    },
    /**
     * Methode: setMapContent
     * Permet de mettre à jour le contenu cartographique.
     *
     * Paramètres:
     * mapContent -  {<Descartes.MapContent>} le contenu cartographique.
     */
    setMapContent: function (mapContent, doNotRefreshEditionState) {
        this.mapContent = mapContent;
        this.addLayerUnderEdtionListener(this, this.updateStateWithLayer);
        if (this.snapping) {
            this.addLayerSnappingListener(this, this._updateSnappingTarget);
            if (this.autotracing) {
                this.addLayerAutoTracingListener(this, this._updateAutoTracingTarget);
            }
        }

        if (!doNotRefreshEditionState) {
            this.refreshEditionState();
        }

        if (Descartes.EditionManager.onlyAppMode) {
            this.removeLayerUnderEditionListener(this, this.updateStateWithLayer);
        }
    },
    /**
     * Methode: refreshEditionState
     * Permet le rafraichissement des états des couches d'édition dans la carte.
     */
    refreshEditionState: function () {
        var underEditionLayer = this.mapContent.getUnderEditionLayer();

        if (_.isNil(underEditionLayer)) {
            //au démarrage, s'il n'y a pas de layer en cours d'édition alors
            //il faut initialiser les boutons avec un layer par default.
            underEditionLayer = this.mapContent.getEditionLayers()[0];
        }
        this.mapContent.refreshEditions(underEditionLayer, true);
    },
    /**
     * Methode: setAvailable
     * Permet de mettre à jour l'attribut isAvailable ainsi que la classe CSS du bouton.
     *
     * Paramètres:
     * available  - {boolean} indiquant le nouveau statut de disponibilié
     */
    setAvailable: function (available) {
        this.available = available;
        this.displayClass = this.CLASS_NAME.replace(/\./g, '');
        if (!this.isAvailable()) {
            this.displayClass += 'Unavailable';
        }
        var itemActivityClass = 'ItemInactive';
        if (this.active) {
            itemActivityClass = 'ItemActive';
        }
        if (this.panel_div) {
            this.panel_div.className = this.displayClass + itemActivityClass + ' olButton';
            if (this.drawingType) {
                this.panel_div.className += " " + this.drawingType;
            }
        }
    },
    /**
     * Methode: setToolTip
     * Met à jour le tooltip de l'outil avec le texte passé en paramètre.
     *
     * Paramètres:
     * text - {String} le texte à afficher dans le tooltip.
     */
    setToolTip: function (text) {
        if (this.panel_div) {
            this.panel_div.title = text;
        }
    },
    /**
     * Methode: refresh
     * Cette méthode est appelée lorsque l'événement moveend est déclenché par la carte.
     */
    refresh: function () {
        if (this.editionLayer) {
            this.setAvailable(this.editionLayer.isEditable());
            this.updateElement();
        }
    },
    /*
     * Méthode: updateStateWithLayer
     * Rend l'outil disponible si la couche est différente que la couche courante, sinon l'outil est indisponible.
     *
     * Paramètres:
     * editionLayer - {<Descartes.Layer.EditionLayer>} Couche d'édition en cours d'édition.
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];

        if (this.isActive()) {
            if (!this.underActivation) {
                this.deactivate(this.editionLayer !== editionLayer);
            }
        }

        if (Descartes.EditionManager.isGlobalEditonMode()) {
            var EditionLayer = require('../Model/EditionLayer');
            if (editionLayer instanceof EditionLayer) {
                //il y a une undereditionLayer
                if (this.editionLayer !== editionLayer) {
                    //clic sur un autre layer
                    this.editionLayer = editionLayer;
                    this._updateOLFeature(this.editionLayer.getFeatureOL_layers()[0]);
                    if (this.geometryType) {
                        this.setAvailable((this.geometryType === this.editionLayer.geometryType || this.editionLayer.type === EditionLayerConstants.TYPE_GenericVector || this.editionLayer.type === EditionLayerConstants.TYPE_Annotations) && this.editionLayer.isUnderEdition());
                    } else {
                        this.setAvailable(this.editionLayer.isUnderEdition());
                    }
                } else if (editionLayer !== this.mapContent.getUnderEditionLayer()) {
                    this.setAvailable(false);
                    //en mode globale, l'outil n'est pas lié à un layer.
                    this.editionLayer = null;
                } else if (this.editionLayer.isUnderEdition() && !this.isAvailable()) {
                    if (this.geometryType) {
                        this.setAvailable(this.geometryType === this.editionLayer.geometryType || this.editionLayer.type === EditionLayerConstants.TYPE_GenericVector || this.editionLayer.type === EditionLayerConstants.TYPE_Annotations);
                    } else {
                        this.setAvailable(true);
                    }
                }
            } else {
                //cas pas de undereditionLayer
                this.editionLayer = null;
                this.setAvailable(false);
            }
            this.updateElement();
        } else {
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            var olLayer = this.editionLayer.getFeatureOL_layers()[0];
            this._updateOLFeature(olLayer);
            this.setAvailable(this.editionLayer.isEditable());
            setTimeout(function () {
                this.updateElement();
            }.bind(this), 100);
        }
    },
    /*
     * Méthode _updateOLFeature
     * Permet de mettre à jour le fonctionnement de l'outil en fonction du layer associé à l'outil.
     */
    _updateOLFeature: function (olLayer) {
        if (this.mapContent) {
            //pour le mode édition globale, il faut mettre à jour l'outil avec le bon layer.
            //pour le mode piloté par la carte, il faut mettre à jour l'outil une seule fois.
            if (Descartes.EditionManager.isGlobalEditonMode() ||
                    (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize)) {
                this.initializeOLFeature(olLayer);
            }
        }
    },
    /*
     * Méthode initializeOLFeature
     * Méthode abstraite d'initialisation de l'outil.
     * A implémenter par l'outil héritant de la classe.
     */
    initializeOLFeature: function () {
    },
    /*
     * Méthode addLayerUnderEdtionListener
     * Ajoute un listener qui écoute le changement de couche en cours d'édition.
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    addLayerUnderEdtionListener: function (context, listener) {
        this.mapContent.events.register('layerUnderEdition', context, listener);
    },
    /*
     * Méthode removeLayerUnderEditionListener
     * Supprime un listener de l'écoute du changement de couche en cours d'édition.
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    removeLayerUnderEditionListener: function (context, listener) {
        this.mapContent.events.unregister('layerUnderEdition', context, listener);
    },
    /**
     * Méthode addLayerSupportListener
     * Ajoute un listener qui écoute le changement de statut des couches de supports d'une couche
     *
     * Arguments :
     * context : l'objet représentatnt this dans le code du lsitener
     * listener : la fonction a exécuter.
     */
    addLayerSupportListener: function (context, listener) {
        this.mapContent.events.register('layerSupport', context, listener);
    },
    /*
     * Méthode removeLayerSupportListener
     * Supprime un listener de l'écoute du changement de statut des couches de supports d'une couche
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    removeLayerSupportListener: function (context, listener) {
        this.mapContent.events.unregister('layerSupport', context, listener);
    },
    /*
     * Méthode : addLayerSnappingListener
     * Ajoute un listener qui écoute le changement de snapping d'une couche
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    addLayerSnappingListener: function (context, listener) {
        this.mapContent.events.register('layerSnapping', context, listener);
    },
    /*
     * Méthode removeLayerSnappingListener
     * Supprime un listener de l'écoute du changement de snapping d'une couche
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    removeLayerSnappingListener: function (context, listener) {
        this.mapContent.events.unregister('layerSnapping', context, listener);
    },
    /*
     * Méthode : addLayerAutoTracingListener
     * Ajoute un listener qui écoute le changement de snapping d'une couche
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    addLayerAutoTracingListener: function (context, listener) {
        this.mapContent.events.register('layerAutoTracing', context, listener);
    },
    /*
     * Méthode removeLayerAutoTracingListener
     * Supprime un listener de l'écoute du changement de snapping d'une couche
     *
     * Arguments :
     * context - l'objet représentant this dans le code du listener.
     * listener - la function a exécuter.
     */
    removeLayerAutoTracingListener: function (context, listener) {
        this.mapContent.events.unregister('layerAutoTracing', context, listener);
    },
    /*
     * Methode privé: _getOLHandlerFromGeometryType
     * Retourne {<Descartes.Layer.POINT_GEOMETRY>} ou {<Descartes.Layer.LINE_GEOMETRY>} ou {<Descartes.Layer.POLYGON_GEOMETRY>}
     *
     * Paramètres:
     * geometryType - {<String>} un type de géométrie (point, line ou polygon).
     */
    _getOLHandlerFromGeometryType: function (geometryType) {
        var result = {};
        switch (geometryType) {
            case LayerConstants.POINT_GEOMETRY:
                result = {
                    type: 'Point',
                    multi: false
                };
                break;
            case LayerConstants.LINE_GEOMETRY:
                result = {
                    type: 'LineString',
                    multi: false
                };
                break;
            case LayerConstants.POLYGON_GEOMETRY:
                result = {
                    type: 'Polygon',
                    multi: false
                };
                break;
            case LayerConstants.MULTI_POINT_GEOMETRY:
                result = {
                    type: 'MultiPoint',
                    multi: true
                };
                break;
            case LayerConstants.MULTI_LINE_GEOMETRY:
                result = {
                    type: 'MultiLineString',
                    multi: true
                };
                break;
            case LayerConstants.MULTI_POLYGON_GEOMETRY:
                result = {
                    type: 'MultiPolygon',
                    multi: true
                };
                break;
        }
        return result;
    },
    /**
     * Méthode privée : _activateSupportLayers
     * Active l'outil de snapping si nécessaire.
     * Dans le cas ou la couche d'édition à changer l'outil bascule sur la nouvelle couche.
     */
    _activateSupportLayers: function () {
        if (this._toolConfig && this.editionLayer) {
            this.editionLayer.supportEnable = this._toolConfig.enable;

            if (!this._toolConfig.supportLayersIdentifier && this._toolConfig.supportLayers) {
                this._toolConfig.supportLayersIdentifier = [];
                for (var i = 0; i < this._toolConfig.supportLayers.length; i++) {
                    this._toolConfig.supportLayersIdentifier.push(this._toolConfig.supportLayers[i].id);
                }
            }

            this._supportLayers = this._getOlSupportsLayers(this._toolConfig.supportLayersIdentifier, true);

            this.mapContent.refreshOnlyContentManager();
            return true;
        }
        return false;
    },
    /**
     * Méthode privée : _updateSupportsLayers
     * Met à jour l'ensemble des couches servant de support
     */
    _updateSupportsLayers: function () {
        if (this._toolConfig && this.editionLayer) {
            this._supportLayers = this._getOlSupportsLayers(this._toolConfig.supportLayersIdentifier, false);
        }
    },
    /*
     * Méthode privée : _deactivateSupportLayers
     * Desactive les layers de supports
     */
    _deactivateSupportLayers: function (refreshTree) {
        if (this.editionLayer || Descartes.EditionManager.onlyAppMode) {
            if (this.editionLayer) {
                delete this.editionLayer.supportEnable;
            }
            var descartesLayers = this.mapContent.getLayers();

            var i;
            for (i = 0; i < this._supportLayers.length; i++) {
                if (this._supportLayers[i].alternative) {
                    this._supportLayers[i].setVisible(false);
                    this.olMap.removeLayer(this._supportLayers[i]);
                }
            }

            this._supportLayers = [];

            for (i = 0; i < descartesLayers.length; i++) {
                delete descartesLayers[i].supportEnable;
            }
        }
        if (refreshTree) {
            this.mapContent.refreshOnlyContentManager();
        }
    },
    /**
     * Méthode privée _getOlSupportsLayers
     * Retourne les layers servants de supports.
     */
    _getOlSupportsLayers: function (supportLayersIdentifier, toggleSupportStateOnLayer) {
        if (!_.isNil(supportLayersIdentifier) && !_.isArray(supportLayersIdentifier)) {
            supportLayersIdentifier = [supportLayersIdentifier];
        }
        var result = [];

        if (this.editionLayer.supportEnable) {
            var eLayer = this.editionLayer.getFeatureOL_layers()[0];
            result.push(eLayer);
        }

        if (supportLayersIdentifier) {
            for (var i = 0; i < supportLayersIdentifier.length; i++) {
                var descartesLayer = this.mapContent.getLayerById(supportLayersIdentifier[i]);
                if (!_.isNil(descartesLayer)) {
                    if ((descartesLayer instanceof Descartes.Layer.EditionLayer &&
                            descartesLayer.isEditable()) ||
                            descartesLayer.isVisible()) {
                        if (toggleSupportStateOnLayer) {
                            descartesLayer.supportEnable = !descartesLayer.supportEnable;
                        }

                        var olLayer = null;

                        var olLayers = descartesLayer.getFeatureOL_layers(true);
                        if (olLayers.length > 0) {
                            olLayer = olLayers[0];
                        }

                        var dStyle = null;
                        if (!_.isNil(descartesLayer.symbolizers)) {
                            if (descartesLayer.symbolizers['default']) {
                                dStyle = descartesLayer.symbolizers['default'][descartesLayer.geometryType];
                            } else {
                                //cas WFS
                                dStyle = descartesLayer.symbolizers[descartesLayer.geometryType];
                            }
                        }

                        if (descartesLayer.supportEnable && olLayer !== null) {
                            olLayer.dStyle = dStyle;
                            if (descartesLayer instanceof Descartes.Layer.WMS) {
                                olLayer.alternative = true;
                            }
                            olLayer.setVisible(true);
                            if (_.isNil(olLayer.map)) {
                                this.olMap.addLayer(olLayer);
                                olLayer.charged = false;
                                olLayer.map = this.olMap;
                            }

                            result.push(olLayer);

                        } else if (olLayer !== null && olLayer.alternative) {
                            olLayer.setVisible(false);
                        }
                    }
                }
            }
        }

        return result;
    },
    /*
     * Méthode privé : _activateSnapping
     * Active l'outil de snapping si nécessaire.
     * Dans le cas ou la couche d'édition à changer l'outil bascule sur la nouvelle couche.
     */
    _activateSnapping: function () {
        if (this.snapping && this.editionLayer) {
            if (this.editionLayer.snapping.enable) {
            this.editionLayer.snappingEnable = this.editionLayer.snapping.enable;
            }
            if (this.autotracing && this.editionLayer.snappingEnable && this.editionLayer.snapping.autotracing) {
                this.editionLayer.autotracingEnable = this.editionLayer.snapping.autotracing;
            }
            var targets = this._getSnappingTargets(true);
            if (targets.length > 0) {

                for (var i = 0; i < targets.length; i++) {
                    var target = targets[i];
                    target.layer.getSource().on('featureloadend', this._updateSnappingTarget.bind(this), this);
                    if (target.alternative) {
                        this.olMap.addLayer(target.layer);
                        target.layer.setVisible(true);
                    }
                }

                var snappingFeatures = new ol.Collection();
                var that = this;
                _.each(targets, function (target) {
                    var layer = target.layer;
                    if (that.autotracing && that.editionLayer.snapping && that.editionLayer.snapping.autotracing) {
                        target.layer.autotracingEnable = true;
                    }
                    var features = layer.getSource().getFeatures();
                    snappingFeatures.extend(features);
                });

                //on garde les targets initiales afin que tous les layers alternatifs
                //soit supprimés de la carte même si l'utilisateur a activé ou non la snapping
                //depuis l'arbre des couches.
                this._snappingInitialTargets = targets.slice();

                this._snappingInteraction = new ol.interaction.Snap({
                    features: snappingFeatures
                });
                this.olMap.addInteraction(this._snappingInteraction);

                this.mapContent.refreshOnlyContentManager();
                return true;
            }
        }
        return false;
    },
    /**
     * Méthode privée _getSnappingLayer
     * Retourne le layer servant au snapping.
     * Peux etre surchargée
     */
    _getSnappingLayer: function () {
        return this.editionLayer.getFeatureOL_layers()[0];
    },
    /*
     * Méthode updateSnappingTarget
     * Si le snapping est actif sur cet outil et que le snapping est actif sur une couche,
     * alors mets à jour les cibles surlesquelles s'applique le snapping.
     */
    _updateSnappingTarget: function () {
        if (this.snapping && this._snappingInteraction !== null) {

            /*var existingSnappingFeatures = this._snappingInteraction.getFeatures_().getArray();
             _.each(existingSnappingFeatures, function (exitingFeature) {
             this._snappingInteraction.removeFeature(exitingFeature);
             }.bind(this));*/

            this._snappingInteraction.rBush_.forEach(function (nodes) {
                this._snappingInteraction.rBush_.remove(nodes);
            }, this);

            var targets = this._getSnappingTargets(false);

            var snappingFeatures = [];
            _.each(targets, function (target) {
                var layer = target.layer;
                var features = layer.getSource().getFeatures();
                snappingFeatures = snappingFeatures.concat(features);
            });

            _.each(snappingFeatures, function (feature) {
                this._snappingInteraction.addFeature(feature);
            }.bind(this));
        }
    },
    /*
     * Méthode updateAutoTracingTarget
     * Si le snapping est actif sur cet outil et que le snapping est actif sur une couche,
     * alors mets à jour les cibles surlesquelles s'applique le snapping.
     */
    _updateAutoTracingTarget: function (target) {
        if (this.snapping && this._snappingInteraction !== null && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
             _.each(target.data[0].OL_layers, function (layer) {
                 layer.autotracingEnable = !layer.autotracingEnable;
             });
        }
    },
    /*
     * Méthode privée _getSnappingTargets
     * Retourne la liste des targets sur lesquelles le snapping va s'appuyer.
     *
     * Argument :
     * toggleSnappingStateOnLayer - indique s'il faut changer l'état du snapping sur le layer ou non.
     */
    _getSnappingTargets: function (toggleSnappingStateOnLayer) {
        var layersIds = this.editionLayer.snapping.snappingLayersIdentifier;

        if (!_.isNil(layersIds) && !_.isArray(layersIds)) {
            layersIds = [layersIds];
        }

        var targets = [];

        var tolerance;
        if (this.editionLayer.snappingEnable) {
            // ajout du layer en cours d'édition
            tolerance = this.editionLayer.snapping.tolerance || EditionConstants.SNAPPING_DEFAULT_TOLERANCE;

            targets.push({
                layer: this.editionLayer.getFeatureOL_layers()[0],
                tolerance: tolerance
            });
        }

        if (_.isNil(layersIds)) {
            return targets;
        }

        var EditionLayer = require('../Model/EditionLayer');
        for (var i = 0; i < layersIds.length; i++) {
            var descarteLayer = this.mapContent.getLayerById(layersIds[i]);
            if (!_.isNil(descarteLayer)) {
                var isActiveTarget = false;
                if (descarteLayer instanceof EditionLayer && descarteLayer.isEditable()) {
                    isActiveTarget = true;
                } else if (descarteLayer.isVisible()) {
                    isActiveTarget = true;
                }

                var olLayer = null;

                if (isActiveTarget) {
                    if (toggleSnappingStateOnLayer) {
                        descarteLayer.snappingEnable = !descarteLayer.snappingEnable;
                        if (this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                            descarteLayer.autotracingEnable = this.editionLayer.snapping.autotracing;
                        }
                    }

                    var olLayers = descarteLayer.getFeatureOL_layers(true);
                    if (olLayers.length > 0) {
                        olLayer = olLayers[0];
                    }

                    if (descarteLayer.snappingEnable) {
                        if (olLayer !== null) {
                            tolerance = EditionConstants.SNAPPING_DEFAULT_TOLERANCE;
                            if (descarteLayer.snapping !== undefined && descarteLayer.snapping.tolerance) {
                                tolerance = descarteLayer.snapping.tolerance;
                            }

                            var target = {
                                layer: olLayer,
                                tolerance: tolerance
                            };
                            if (descarteLayer instanceof Descartes.Layer.WMS) {
                                target.alternative = true;
                                olLayer.alternative = true;
                                olLayer.setVisible(true);
                            }
                            targets.push(target);
                        } else {
                            alert(Messages.Descartes_EDITION_NOT_SNAPPING_LAYER_PART_ONE +
                                    descarteLayer.id +
                                    Messages.Descartes_EDITION_NOT_SNAPPING_LAYER_PART_TWO);
                        }
                    } else if (olLayer !== null && olLayer.alternative) {
                        olLayer.setVisible(false);
                    }
                }
            }
        }

        return targets;
    },
    /*
     * Méthode privée : _deactivateSnapping
     * Desactive l'outil de snapping.
     */
    _deactivateSnapping: function (refreshTree) {
        if (this.snapping) {

            var i;
            if (this._snappingInteraction !== null &&
                    this._snappingInteraction.getActive() &&
                    this.editionLayer) {
                if (this._snappingInitialTargets) {
                    for (i = 0; i < this._snappingInitialTargets.length; i++) {
                        var target = this._snappingInitialTargets[i];
                        target.layer.getSource().un('featureloadend', this._updateSnappingTarget.bind(this), this);
                        if (target.alternative) {
                            target.layer.setVisible(false);
                            this.olMap.removeLayer(target.layer);
                        }
                    }

                    this._snappingInteraction.setActive(false);
                }
                this.olMap.removeInteraction(this._snappingInteraction);
                this._snappingInteraction = null;
            }

            var layers = this.mapContent.getLayers();
            for (i = 0; i < layers.length; i++) {
                delete layers[i].snappingEnable;
                if (this.autotracing) {
                    delete layers[i].autotracingEnable;
                }
            }
            if (refreshTree) {
                this.mapContent.refreshOnlyContentManager();
            }
        }
    },
    /*
     * Méthode: _enableDirectionalPanPanel
     * Active la rose des vents si elle existe.
     */
    _enableDirectionalPanPanel: function () {
        var directionalPanel = this.getDirectionalPanPanel();
        if (directionalPanel !== null) {
            directionalPanel.activate();
        }
    },
    /*
     * Méthode _disableDirectionalPanPanel
     * Desactive la roxe des vents si elle existe.
     */
    _disableDirectionalPanPanel: function () {
        var directionalPanel = this.getDirectionalPanPanel();
        if (directionalPanel !== null) {
            directionalPanel.deactivate();
        }
    },
    /*
     * Méthode: getDirectionalPanPanel
     * Retourne la "rose des vents" de navigation <Descartes.Button.DirectionalPanPanel> si elle existe,
     * sinon retourne null.
     */
    getDirectionalPanPanel: function () {
        var controls = this.getMap().getControls().getArray();
        for (var i = 0; i < controls.length; i++) {
            if (controls[i].CLASS_NAME === 'Descartes.Button.DirectionalPanPanel') {
                return controls[i];
            }
        }
        return null;
    },
    /*
     * Méthode _hideVectorLayerAdresse
     * Supprimer l'attribut z-index de la couche de localisation si elle existe (pour permettre l'édition des features)
     */
    _hideVectorLayerAdresse: function () {
        //TODO (peut etre inutile)
        /*if (!_.isNil(Descartes.Layer.LocalisationAdresseLayer !== undefined) {
         for (var i = 0, len = this.map.layers.length; i < len; i++) {
         if (this.map.layers[i].name === Descartes.Layer.LocalisationAdresseLayer.NAME) {
         this.map.layers[i].div.style['z-index'] = '';
         break;
         }
         }
         }*/
    },
    /**
     * Methode: isConforme
     * Renvoie true si la géométrie passée en paramètre est conforme,
     * c'est à dire simple et, dans le cas de (multi-)polygones, valide.
     */
    isConforme: function (geometry) {
        var jstsGeometry;
        try {
            jstsGeometry = this._parser.read(geometry);
        } catch (e) {
            return false;
        }
        if (jstsGeometry instanceof jsts.geom.GeometryCollection && !(jstsGeometry instanceof jsts.geom.MultiLineString) &&
                !(jstsGeometry instanceof jsts.geom.MultiPoint) && !(jstsGeometry instanceof jsts.geom.MultiPolygon)) {
            var firstGeom = jstsGeometry._geometries[0];
            if (firstGeom instanceof jsts.geom.LineString) {
                jstsGeometry = new jsts.geom.MultiLineString(jstsGeometry._geometries);
            } else if (firstGeom instanceof jsts.geom.Point) {
                jstsGeometry = new jsts.geom.MultiPoint(jstsGeometry._geometries);
            } else if (firstGeom instanceof jsts.geom.Polygon) {
                jstsGeometry = new jsts.geom.MultiPolygon(jstsGeometry._geometries);
            }
        }

        if (!(jstsGeometry.isSimple() && jstsGeometry.isValid())) {
            return false;
        }
        return true;
    },
    _activateSupportLayersForTopoConformity: function () {
        if (this.editionLayer.verifTopoConformity && this.editionLayer.verifTopoConformity.supportLayersIdentifier) {
            var supportLayersIdentifier = this.editionLayer.verifTopoConformity.supportLayersIdentifier;
            if (_.isNil(supportLayersIdentifier)) {
                return;
            }
            if (!_.isArray(supportLayersIdentifier)) {
                supportLayersIdentifier = [supportLayersIdentifier];
            }

            var EditionLayer = require('../Model/EditionLayer');

            for (var i = 0; i < supportLayersIdentifier.length; i++) {
                var descartesLayer = this.mapContent.getLayerById(supportLayersIdentifier[i]);

                if (_.isNil(descartesLayer)) {
                    continue;
                }

                if ((descartesLayer instanceof EditionLayer && descartesLayer.isEditable()) ||
                        descartesLayer.isVisible()) {

                    var olLayer = null;

                    var olLayers = descartesLayer.getFeatureOL_layers(true);
                    if (olLayers.length > 0) {
                        olLayer = olLayers[0];
                    }

                    if (olLayer !== null) {
                        if (descartesLayer instanceof Descartes.Layer.WMS) {
                            olLayer.alternative = true;
                        }

                        if (_.isNil(olLayer.map)) {
                            olLayer.setVisible(true);
                            this.olMap.addLayer(olLayer);
                            olLayer.setOpacity(0);
                            olLayer.map = this.olMap;
                        }

                        this._supportLayersTopoConformity.push(olLayer);
                    }
                }
            }
        }
    },
    _deactivateSupportLayersForTopoConformity: function () {
        if ((this._supportLayersTopoConformity.length > 0 && this.editionLayer) ||
                Descartes.EditionManager.onlyAppMode) {
            for (var i = 0; i < this._supportLayersTopoConformity.length; i++) {
                if (this._supportLayersTopoConformity[i].alternative) {
                    this._supportLayersTopoConformity[i].setVisible(false);
                }
            }
            this._supportLayersTopoConformity = [];
        }
    },
    /**
     * Methode: isActiveVerifTopoConformity
     * Renvoie true si la vérification des règles de conformité topolgique doit être faite.
     */
    isActiveVerifTopoConformity: function () {
        return ((Descartes.EditionManager.verifTopoConformity && Descartes.EditionManager.verifTopoConformity.enable) ||
                (this.editionLayer.verifTopoConformity && this.editionLayer.verifTopoConformity.enable) ||
                (this.editionLayer.verifTopoConformity && this.editionLayer.verifTopoConformity.supportLayersIdentifier.length > 0));
    },

    isVerifTopoConformityStrict: function () {
        return ((Descartes.EditionManager.verifTopoConformity && Descartes.EditionManager.verifTopoConformity.strict) ||
                (this.editionLayer.verifTopoConformity && this.editionLayer.verifTopoConformity.strict));
    },
    /**
     * Methode: isConformeTopology
     * Renvoie true si la geometry passée en paramètre est conforme aux règles de topologie.
     */
    isConformeTopology: function (geometry, presentInLayer, geometryToIgnore) {
        var allGeometry = [];
        var allSupportGeometry = [];
        var geometryIntersected = null;

        var jstsGeom = this._parser.read(geometry);

        var strictIntersection = false;
        if (this.isVerifTopoConformityStrict()) {
            strictIntersection = true;
        }

        var isConforme = true;

        var i, j, a, aJstsGeom, aGeometry, intersectJstsGeom, intersectGeom;

        //vérification par rapport aux geometries de la couche d'edition
        if ((Descartes.EditionManager.verifTopoConformity && Descartes.EditionManager.verifTopoConformity.enable) ||
                (this.editionLayer.verifTopoConformity && this.editionLayer.verifTopoConformity.enable)) {

            //récupération de toutes les géométries de la couche d'édition
            allGeometry = this._recupAllGeometryInLayer(this.editionLayer);
            if (presentInLayer) {
                //suppression de la géométrie courante
                allGeometry = this._deleteGeometryIfPresent(geometry, allGeometry);
                if (geometryToIgnore && _.isArray(geometryToIgnore)) {
                    //cas particulier Merge
                    for (a = 0; a < geometryToIgnore.length; a++) {
                         allGeometry = this._deleteGeometryIfPresent(geometryToIgnore[a], allGeometry);
                    }
                } else if (geometryToIgnore) {
                    allGeometry = this._deleteGeometryIfPresent(geometryToIgnore, allGeometry);
                }
            }

            //vérifier si intersection avec les objets de la couche d'édition
            for (i = 0; i < allGeometry.length; i++) {
                aJstsGeom = this._parser.read(allGeometry[i]);

                if (jstsGeom.intersects(aJstsGeom)) {
                    if (strictIntersection) {
                        isConforme = false;
                        break;
                    } else {
                        aGeometry = geometry;
                        if ((aGeometry instanceof ol.geom.Polygon ||
                                aGeometry instanceof ol.geom.MultiPolygon) && allGeometry[i] instanceof ol.geom.Polygon) {
                            //pour les polygones, on autorise les intersections de type point ou ligne
                            intersectJstsGeom = jstsGeom.intersection(aJstsGeom);
                            intersectGeom = this._parser.write(intersectJstsGeom);
                            if (intersectGeom instanceof ol.geom.Polygon ||
                                    intersectGeom instanceof ol.geom.MultiPolygon) {
                                isConforme = false;
                                break;
                            }
                        } else {
                            isConforme = false;
                            break;
                        }
                    }
                }
            }

            if (!isConforme) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_TOPO);
                return false;
            }
        }

        //récupération des geometries des couches supports
        if (this.editionLayer.verifTopoConformity &&
                this.editionLayer.verifTopoConformity.supportLayersIdentifier &&
                this._supportLayersTopoConformity &&
                this._supportLayersTopoConformity.length > 0) {
            for (i = 0; i < this._supportLayersTopoConformity.length; i++) {
                var olLayer = this._supportLayersTopoConformity[i];
                var features = olLayer.getSource().getFeatures();
                for (j = 0; j < features.length; j++) {
                    var aFeature = features[j];
                    var aGeom = aFeature.getGeometry().clone();
                    aGeom.layertitle = olLayer.get('title');
                    allSupportGeometry.push(aGeom);
                }
            }

            //vérifier si intersection avec les objets des couches supports
            for (i = 0; i < allSupportGeometry.length; i++) {
                aJstsGeom = this._parser.read(allSupportGeometry[i]);
                if (jstsGeom.intersects(aJstsGeom)) {
                    if (strictIntersection) {
                        isConforme = false;
                        geometryIntersected = allSupportGeometry[i];
                        break;
                    } else {
                        aGeometry = geometry;
                        if ((aGeometry instanceof ol.geom.Polygon ||
                                aGeometry instanceof ol.geom.MultiPolygon) &&
                                (allSupportGeometry[i] instanceof ol.geom.Polygon ||
                                        allSupportGeometry[i] instanceof ol.geom.MultiPolygon)) {
                            //pour les polygones, on autorise les intersections de type point ou ligne
                            intersectJstsGeom = jstsGeom.intersection(aJstsGeom);
                            intersectGeom = this._parser.write(intersectJstsGeom);
                            if (intersectGeom instanceof ol.geom.Polygon ||
                                    intersectGeom instanceof ol.geom.MultiPolygon) {
                                isConforme = false;
                                geometryIntersected = allSupportGeometry[i];
                                break;
                            } else if (intersectGeom instanceof ol.geom.GeometryCollection) {
                                for (j = 0; j < intersectGeom.components.length; j++) {
                                    if (intersectGeom.components[j] instanceof ol.geom.Polygon ||
                                            intersectGeom.components[j] instanceof ol.geom.MultiPolygon) {
                                        isConforme = false;
                                        geometryIntersected = allSupportGeometry[i];
                                        break;
                                    }
                                }
                            }
                        } else {
                            isConforme = false;
                            geometryIntersected = allSupportGeometry[i];
                            break;
                        }
                    }
                }
            }

            if (!isConforme) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_TOPO_SUPPORT + "'" + geometryIntersected.layertitle + "'");
                return false;
            }
        }
        return isConforme;
    },
    /**
     * Methode: isConformeTopologyFeatureToModify
     * Renvoie true si la géométrie passée en paramètre est conforme aux règles métier.
     */
    isConformeTopologyFeatureToModify: function (geometry) {
        var isConforme = true;
        if (!this.isConformeTopology(geometry, true, this.featureToModify.getGeometry())) {
            isConforme = false;
        }
        return isConforme;
    },
    /**
     * Methode: isActiveVerifAppConformity
     * Renvoie true si la vérification des règles de l'application métier doit être faite.
     */
    isActiveVerifAppConformity: function () {
        return Descartes.EditionManager.verifAppConformity || this.editionLayer.verifAppConformity;
    },
    /**
     * Methode: isConformeRulesApp
     * Renvoie true si la feature passée en paramètre est conforme aux règles métier.
     */
    isConformeRulesApp: function (feature, presentInLayer) {
        var geometry = feature.getGeometry();
        var allGeometry = [];

        //récupération de toutes les géométries de la couche
        allGeometry = this._recupAllGeometryInLayer(this.editionLayer);

        if (presentInLayer) {
            //suppression de la géométrie courante
            allGeometry = this._deleteGeometryIfPresent(geometry, allGeometry);
        }

        var json = {
            feature: feature,
            allSignificantGeometryInLayer: allGeometry,
            editionLayer: this.editionLayer
        };
        var jsonApp = Descartes.EditionManager.conformityAppFct(json);

        if (jsonApp && _.isBoolean(jsonApp.isConforme)) {
            if (!jsonApp.isConforme) {
                alert(jsonApp.message);
            }
            return jsonApp.isConforme;
        }
        return true;
    },
    /**
     * Methode: isConformeRulesAppFeatureToModify
     * Renvoie true si la géom�trie passée en paramètre est conforme aux règles métier.
     */
    isConformeRulesAppFeatureToModify: function (feature, presentInLayer) {
        var geometry = feature.getGeometry();
        var isConforme = true;
        var index = this.editionLayer.getFeatureOL_layers()[0].features.indexOf(this.featureToModify);
        if (index > -1) {
            this.editionLayer.getFeatureOL_layers()[0].features.splice(index, 1);
        }
        if (!this.isConformeRulesApp(geometry)) {
            isConforme = false;
        }
        this.editionLayer.getFeatureOL_layers()[0].features.push(this.featureToModify);
        return isConforme;
    },
    _recupAllGeometryInLayer: function (layer) {
        var allGeometry = [];
        var features = layer.getFeatures();
        for (var i = 0; i < features.length; i++) {
            var feature = features[i];
            if (!feature.isTemporaireFeature) {
                var geometry = feature.getGeometry();
                if (geometry instanceof ol.geom.MultiPolygon ||
                        geometry instanceof ol.geom.MultiPoint /*&& !(geometry instanceof ol.geom.Curve))*/ ||
                        geometry instanceof ol.geom.MultiLineString) {
                    var subGeoms = Utils.getSubGeom(geometry);
                    for (var j = 0; j < subGeoms.length; j++) {
                        allGeometry.push(subGeoms[j]);
                    }
                } else if (!_.isNil(geometry)) {
                    allGeometry.push(geometry);
                }
            }
        }
        return allGeometry;
    },
    _deleteGeometryIfPresent: function (geometry, allGeometry) {
        var jstsGeom = this._parser.read(geometry);

        var i, index, aJstsGeom;
        if (geometry instanceof ol.geom.MultiPolygon ||
                geometry instanceof ol.geom.MultiPoint ||
                geometry instanceof ol.geom.MultiLineString) {
            var subGeoms = Utils.getSubGeom(geometry);
            for (i = 0; i < subGeoms.length; i++) {
                var subGeom = subGeoms[i];
                index = allGeometry.indexOf(subGeom);
                if (index > -1) {
                    allGeometry.splice(index, 1);
                } else {
                    var cJstsGeom = this._parser.read(subGeom);
                    for (var j = 0; j < allGeometry.length; j++) {
                        aJstsGeom = this._parser.read(allGeometry[j]);
                        if (cJstsGeom.equalsTopo(aJstsGeom)) {
                            allGeometry.splice(j, 1);
                            break;
                        }
                    }
                }
            }

        } else {
            index = allGeometry.indexOf(geometry);
            if (index > -1) {
                allGeometry.splice(index, 1);
            } else {
                for (i = 0; i < allGeometry.length; i++) {
                    aJstsGeom = this._parser.read(allGeometry[i]);
                    if (jstsGeom.equalsTopo(aJstsGeom)) {
                        allGeometry.splice(i, 1);
                        break;
                    }
                }
            }

        }
        return allGeometry;
    },
    removeStyle: function (feature, toSave) {
        var hasParentStyleUrl = (feature.parentFeature && feature.parentFeature.data.styleUrl);
        if (feature.style && (feature.data.styleUrl || hasParentStyleUrl)) {
            feature.kmlStyle = feature.style;

            if (feature.parentFeature) {
                feature.parentFeature.kmlStyle = feature.parentFeature.style;
            }

            if (!toSave) {
                feature.style = null;
            }
        } else {
            feature.style = null;
        }
    },
    setFeatureStyle: function (feature, type) {
        if (_.isNil(type)) {
            type = 'default';
        }
        if (feature.get('selected')) {
            type = 'select';
        }
        var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers[type]);
        var olStyle = olStyles[feature.getGeometry().getType()];
        feature.setStyle(olStyle);
    },
    drawDefaultStyle: function (feature) {
        if (feature.kmlStyle) {
            feature.style = feature.kmlStyle;
        } else if (feature.parentFeature && feature.parentFeature.kmlStyle) {
            feature.style = feature.parentFeature.kmlStyle;

        }
        this.layer.drawFeature(feature, 'default');
    },
    CLASS_NAME: 'Descartes.Tool.Edition'
});

module.exports = Class;

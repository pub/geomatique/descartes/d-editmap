/* global MODE */
var _ = require('lodash');

var AbstractSplit = require('./Creation/AbstractSplit');
var AggregationCreation = require('./Creation/AggregationCreation');
var BufferCreation = require('./Creation/BufferCreation');
var BufferHaloCreation = require('./Creation/BufferHaloCreation');
var BufferNormalCreation = require('./Creation/BufferNormalCreation');
var CloneCreation = require('./Creation/CloneCreation');
var CopyCreation = require('./Creation/CopyCreation');
var DivideCreation = require('./Creation/DivideCreation');
var DrawCreation = require('./Creation/DrawCreation');
var DrawAnnotation = require('./Annotation/DrawAnnotation');
var TextAnnotation = require('./Annotation/TextAnnotation');
var ArrowAnnotation = require('./Annotation/ArrowAnnotation');
var FreehandAnnotation = require('./Annotation/FreehandAnnotation');
var BufferHaloAnnotation = require('./Annotation/BufferHaloAnnotation');
var GeolocationSimpleAnnotation = require('./Annotation/GeolocationSimpleAnnotation');
var GeolocationTrackingAnnotation = require('./Annotation/GeolocationTrackingAnnotation');
var HomotheticCreation = require('./Creation/HomotheticCreation');
var SplitCreation = require('./Creation/SplitCreation');
var UnAggregationCreation = require('./Creation/UnAggregationCreation');

var namespace = {
	AbstractSplit: AbstractSplit,
	AggregationCreation: AggregationCreation,
	BufferCreation: BufferCreation,
	BufferHaloCreation: BufferHaloCreation,
	BufferNormalCreation: BufferNormalCreation,
	CloneCreation: CloneCreation,
	CopyCreation: CopyCreation,
	DivideCreation: DivideCreation,
	DrawCreation: DrawCreation,
	DrawAnnotation: DrawAnnotation,
	TextAnnotation: TextAnnotation,
	ArrowAnnotation: ArrowAnnotation,
	FreehandAnnotation: FreehandAnnotation,
	BufferHaloAnnotation: BufferHaloAnnotation,
	GeolocationSimpleAnnotation: GeolocationSimpleAnnotation,
	GeolocationTrackingAnnotation: GeolocationTrackingAnnotation,
	HomotheticCreation: HomotheticCreation,
	SplitCreation: SplitCreation,
	UnAggregationCreation: UnAggregationCreation
};

var Creation = require('./Creation');

_.extend(Creation, namespace);

module.exports = Creation;

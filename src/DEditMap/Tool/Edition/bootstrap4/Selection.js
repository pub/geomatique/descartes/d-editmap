/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');
var $ = require('jquery');

var Utils = Descartes.Utils;
var Selection = require('../Selection');

var template = require('./templates/showInfoOverlay.ejs');

var Class = Utils.Class(Selection, {

    highlightId: null,

    createHighlight: function (feature) {
        this.highlightId = Utils.createUniqueID();

        var attributes = _.clone(feature.getProperties());
        delete attributes[feature.getGeometryName()];
        delete attributes.selected;
        delete attributes.boundedBy;
        delete attributes.state;

        var geom = feature.getGeometry();

        var geomInfo = {};
        if (this.showInfos.geometricCalculs) {
            geomInfo.perimeter = this.getPerimeter(geom);
            if (geom instanceof ol.geom.Polygon || geom instanceof ol.geom.MultiPolygon) {
                geomInfo.area = this.getArea(geom);
            }
        }

        var html = template({
            id: this.highlightId,
            title: this.getMessage('SHOW_INFO_TITLE'),
            identifierLabel: this.getMessage('SHOW_INFO_IDENTIFIER'),
            areaLabel: this.getMessage('SHOW_INFO_AREA'),
            perimeterLabel: this.getMessage('SHOW_INFO_PERIMETER'),
            showInfos: this.showInfos,
            featureId: feature.getId(),
            attributes: attributes,
            noAttributesLabel: this.getMessage('NO_ATTRIBUTES'),
            geomInfo: geomInfo
        });

        var div = document.createElement('div');

        div.innerHTML = html.trim();
        return div.firstChild;
    },
    afterHighlight: function () {
        $('#' + this.highlightId + '_close').click(function (e) {
            var id = e.currentTarget.id;
            id = id.substring(0, id.indexOf('_close'));
            this.unhighlight(id);
        }.bind(this));
    },
    unhighlight: function (id) {
        if (_.isNil(id)) {
            id = this.highlightId;
        }
        Selection.prototype.unhighlight.apply(this, [id]);
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Selection'
});

module.exports = Class;

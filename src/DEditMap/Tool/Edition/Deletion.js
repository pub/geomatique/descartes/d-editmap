
/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../' + MODE + '/Edition');

require('./css/modification.css');

/**
 * Class: Descartes.Tool.Edition.Deletion
 * Classe "abstraite" pour les outils d'édition de suppression
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /*
     * Propriete: defaultDeletionOptions
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires pour le fonctionnement de l'outil.
     */
    defaultDeletionOptions: {
        clickout: false,
        hover: false,
        toggle: false,
        multiple: false,
        toggleKey: 'ctrlKey',
        multipleKey: 'shiftKey'
    },
    /**
     * Propriete: deletionOptions
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires pour le fonctionnement de l'outil.
     */
    deletionOptions: {},

    interaction: null,

    /**
     * Constructeur: Descartes.Tool.Edition.Deletion
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * deletionOptions - {Object} Objet JSON stockant les options d'utilisation de l'outil.
     */
    initialize: function (options) {
        this.deletionOptions = _.extend({}, this.defaultDeletionOptions);
        if (!_.isNil(options)) {
            _.extend(this.deletionOptions, options.deletionOptions);
            delete options.deletionOptions;
        }

        Edition.prototype.initialize.apply(this, arguments);
    },

    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer {<ol.interaction.Select>}
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.interaction = new ol.interaction.Select({
            layers: [olLayer],
            hitTolerance: 5,
            toggleCondition: ol.events.condition.singleClick,
            multi: this.deletionOptions.multiple
        });
        this.interaction.set('id', 'DeletionSelection_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            Edition.prototype.activate.apply(this, arguments);
            if (!_.isNil(this.olLayer) && !_.isNil(this.olLayer.map)) {
                var map = this.olLayer.map;
                map.on('pointermove', this.changeCursorPointerOnFeature, this);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            if (!_.isNil(this.olLayer) && !_.isNil(this.olLayer.map)) {
                var map = this.olLayer.map;
                map.un('pointermove', this.changeCursorPointerOnFeature, this);
            }
            Edition.prototype.deactivate.apply(this, arguments);
        }
    },
    changeCursorPointerOnFeature: function (evt) {
        var map = this.olLayer.map;
        var hit = map.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.olLayer ||
                        aLayer === this.editionLayer.compositeSelectionLayer;
            }.bind(this)
        });
        map.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },

    // méthode à surcharger
    onSelect: function () {
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Deletion'
});

module.exports = Class;

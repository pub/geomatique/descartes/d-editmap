/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');
var log = require('loglevel');

var Utils = Descartes.Utils;
var Edition = require('../' + MODE + '/Edition');
var AttributesEditor = require('../../Action/AttributesEditor');
var AttributesEditorDialog = require('../../UI/' + MODE + '/AttributesEditorDialog');

require('./css/selection.css');

/**
 * Class: Descartes.Tool.Edition.Selection
 * Outil de sélection pour l'édition.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /**
     * Propriete: showInfos
     * {Object} Permet d'activer ou non l'affichage d'informations (identifiant, attributs, calculs géométriques)
     * sur l'objet sélectionné.
     *
     * attributes - false
     * geometricCalculs - false
     * identifier - false
     *
     */
    showInfos: {
        attributes: false,
        geometricCalculs: false,
        identifier: false
    },
    multiple: false,
    /**
     * Propriete: calculArround
     * {Boolean}  - pour arrondir les caculs géométriques.
     */
    calculArround: true,

    /**
     * Propriete: sendAppResult
     * Pour envoyer les informations à l'applcation métier.
     */
    sendAppResults: false,

    hitTolerance: 5,
    interaction: null,

    /**
     * Constructeur: Descartes.Tool.Edition.Selection
     * Constructeur d'instances
     */
    initialize: function () {
        this.interaction = new ol.interaction.Select({
            toggleCondition: ol.events.condition.singleClick,
            hitTolerance: this.hitTolerance,
            layers: function (layer) {
                if (!_.isNil(this.editionLayer)) {
                    for (var i = 0; i < this.editionLayer.OL_layers.length; i++) {
                        if (this.editionLayer.OL_layers[i] === layer) {
                            return true;
                        }
                    }
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'SelectionInteraction');
        this.interaction.on('select', this.toggleFeatureSelected.bind(this));
        this.interaction.setActive(false);
        Edition.prototype.initialize.apply(this, arguments);
    },
    toggleFeatureSelected: function (event) {
        if (!this.multiple) {
            event.deselected = this.editionLayer.getSelectedFeatures();
        }

        _.each(event.deselected, function (feature) {
            feature.set('selected', false);
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
            if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
                if (!this.sendAppResults) {
                    this.unhighlight();
                }
            }
            if (!this.multiple) {
                this.interaction.getFeatures().remove(feature);
            }
        }.bind(this));

        _.each(event.selected, function (feature) {
            feature.set('selected', true);
            this.setFeatureStyle(feature, 'select');
            if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
                if (this.sendAppResults) {
                    this.sendInformations(feature);
                } else {
                    this.highlight(feature);
                }
            }
        }.bind(this));
    },
    highlight: function (feature) {
        var element = this.createHighlight(feature);

        var extent = feature.getGeometry().getExtent();
        var position = ol.extent.getTopRight(extent);

        var overlay = new ol.Overlay({
            id: element.id,
            element: element,
            autoPan: true
        });
        this.olMap.addOverlay(overlay);
        overlay.setPosition(position);

        this.afterHighlight();
    },
    unhighlight: function (id) {
        if (!_.isNil(id)) {
            var overlay = this.olMap.getOverlayById(id);
            if (!_.isNil(overlay)) {
                this.olMap.removeOverlay(overlay);
            }
        }
    },
    sendInformations: function (feature) {
        var geometry = feature.getGeometry();
        var objectSelected = {
            geometry: geometry
        };

        if (this.showInfos.attributes) {
            var attributes = _.clone(feature.getProperties());
            delete attributes[feature.getGeometryName()];
            delete attributes.selected;
            delete attributes.boundedBy;
            delete attributes.state;
            objectSelected.attributs = attributes;
        }

        if (this.showInfos.identifier) {
            objectSelected.objectId = feature.getId();
        }

        if (this.showInfos.geometricCalculs &&
                !(geometry instanceof ol.geom.Point || geometry instanceof ol.geom.MultiPoint)) {
            var area = this.getArea(geometry);
            if (!_.isNil(area)) {
                objectSelected.area = area;
            }
            var perimeter = this.getPerimeter(geometry);
            objectSelected.perimeter = perimeter;
        }

        var informations = {
            type: 'descartes_outil_selection',
            featureSelected: feature,
            fluxSimple: {
                objectSelected: objectSelected
            }
        };
        var EditionManager = require('../../Core/EditionManager');
        EditionManager.sendInformations(informations);
    },
    /**
     * Méthode permettant de créer le contenu de la popup d'information.
     * Doit être implémenté par les classes filles pour chaque mode de Descartes.
     *
     * @param {type} feature
     * @returns un élement dom.
     */
    createHighlight: function (feature) {
    },
    afterHighlight: function () {
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Edition.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.underActivation = false;
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Edition.prototype.deactivate.apply(this, arguments);
            if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
                if (!this.sendAppResults) {
                    this.unhighlight();
                }
            }
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];

        var EditionLayer = require('../../Model/EditionLayer');
        if (editionLayer instanceof EditionLayer) {
            var featureLayer = editionLayer.getFeatureOL_layers()[0];
            featureLayer.getSource().on('featureloadend', this.unselectAll, this);
            if (!_.isNil(this.editionLayer) && this.editionLayer !== editionLayer) {
                featureLayer = this.editionLayer.getFeatureOL_layers()[0];
                featureLayer.getSource().un('featureloadend', this.unselectAll, this);
            }
        }
        Edition.prototype.updateStateWithLayer.apply(this, arguments);

        if (Descartes.EditionManager.globalEditionMode) {
            if (_.isNil(this.editionLayer)) {
                //il faut tout de même la couche pour remettre les styles.
                this.editionLayer = editionLayer;
                this.unselectAll();
                this.editionLayer = null;
            } else {
                this.unselectAll();
            }
        } else {
            if (this.editionLayer !== null &&
                    editionLayer instanceof Descartes.Layer.EditionLayer &&
                    (this.editionLayer !== editionLayer || !this.editionLayer.isEditable())) {
                this.unselectAll();
            }
        }
    },
    /**
     * Unselect all features
     * @returns {undefined}
     */
    unselectAll: function () {
        if (!_.isNil(this.editionLayer)) {
            this.editionLayer.unselectAll();
        }
        if (!_.isNil(this.interaction) && this.interaction.getFeatures().getLength() > 0) {
            this.interaction.getFeatures().forEach(function (feature) {
                feature.set('selected', false);
                var style = Utils.getStyleByState(feature);
                this.setFeatureStyle(feature, style);
            }.bind(this));
            this.interaction.getFeatures().clear();
        }
        if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
            if (!this.sendAppResults) {
                this.unhighlight();
            }
        }
    },
    /*
     * Méthode: registerToSelection
     * Abonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    registerToSelection: function (olLayer) {
        var source = olLayer.getSource();
        var features = source.getFeatures();

        _.each(features, function (feature) {
            feature.on('change:selected', this.featureSelectionChanged, this);
        }.bind(this));

        source.on('addfeature', this.registerToAddFeature, this);
        source.on('removefeature', this.registerToRemoveFeature, this);
    },

    registerToAddFeature: function (e) {
        e.feature.on('change:selected', this.featureSelectionChanged, this);
    },

    registerToRemoveFeature: function (e) {
        e.feature.un('change:selected', this.featureSelectionChanged, this);
    },

    /*
     * Méthode: unregisterToSelection
     * Désabonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    unregisterToSelection: function (olLayer) {
        var source = olLayer.getSource();
        source.un('addfeature', this.registerToAddFeature, this);
        source.un('removefeature', this.registerToRemoveFeature, this);
        var features = source.getFeatures();
        _.each(features, function (feature) {
            feature.un('change:selected', this.featureSelectionChanged, this);
        }.bind(this));
    },
    /*
     * Méthode: featureSelectionChanged
     * Listener agissant sur l'état de l'outil lorsqu'un changement de selection
     * est fait dans le layer d'édition courant.
     */
    featureSelectionChanged: function (event) {
        if (this.editionLayer !== null) {
            if (!_.isNil(event) && !_.isNil(event.target)) {
                var feature = event.target;
                var selected = feature.get('selected');
                //si déclenché sur une selection alors il y a au moins une feature
                //selectionnée
                if (this.isAvailable() !== selected) {
                    this.setAvailable(selected);
                    this.updateElement();
                }
            } else {
                var featureSelected = this.editionLayer.getSelectedFeatures();
                var atLeastOneSelected = featureSelected.length !== 0;
                log.debug('Feature Selection changed ! Nb feature selected = %s', featureSelected.length);

                if (!atLeastOneSelected && this.editionLayer.isCompositeGeometry()) {
                    var compositefeatureSelected = this.editionLayer.getcompositeSelectionLayerSelectedFeatures().length !== 0;
                    if (compositefeatureSelected) {
                        this.setAvailable(true);
                    } else {
                        this.setAvailable(false);
                    }
                } else {
                    this.setAvailable(atLeastOneSelected);
                }
                this.updateElement();
            }
        }
    },
    getArea: function (geom) {
        var unit = this.getMessage('TITLE_AIRE_UNITE');
        var projection = this.olMap.getView().getProjection();
        var area = 0;
        if (geom instanceof ol.geom.Polygon) {
            area = ol.Sphere.getArea(geom, {
                projection: projection
            });
        } else if (geom instanceof ol.geom.MultiPolygon) {
            var polygons = geom.getPolygons();
            _.each(polygons, function (polygon) {
                area += ol.Sphere.getArea(polygon, {
                    projection: projection
                });
            });
        }

        var fullUnit;
        if (area > 10000) {
            fullUnit = 'km' + unit;
            area = Math.round(area / 1000000 * 100) / 100;
        } else {
            fullUnit = 'm' + unit;
            area = Math.round(area * 100) / 100;
        }

        if (this.calculArround) {
            area = area.toFixed(3);
        }

        return {
            measure: area,
            unit: fullUnit
        };
    },
    getPerimeter: function (geom) {
        var length = 0;
        var projection = this.olMap.getView().getProjection();
        if (geom instanceof ol.geom.Polygon) {
            length = Utils.getPolygonLength(geom, projection);
        } else if (geom instanceof ol.geom.MultiPolygon) {
            var polygons = geom.getPolygons();
            _.each(polygons, function (polygon) {
                length += Utils.getPolygonLength(polygon, projection);
            });
        } else if (geom instanceof ol.geom.LineString) {
            length = ol.Sphere.getLength(geom, {
                projection: projection
            });
        } else if (geom instanceof ol.geom.MultiLineString) {
            var lines = geom.getLineStrings();
            _.each(lines, function (line) {
                length += ol.Sphere.getLength(line, {
                    projection: projection
                });
            });
        } else {
            return null;
        }

        var unit;
        if (length > 100) {
            unit = 'km';
            length = Math.round(length / 1000 * 100) / 100;
        } else {
            unit = 'm';
            length = Math.round(length * 100) / 100;
        }

        if (this.calculArround) {
            length = length.toFixed(3);
        }

        return {
            measure: length,
            unit: unit
        };
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature, save, cancel) {
        feature.editAttribut = true;
        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.map, viewOptions);
        if (save) {
            action.events.register('saveObject', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
    },
    changeCursorPointerOnFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.editionLayer.getFeatureOL_layers()[0];
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Selection'
});

module.exports = Class;

/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var GlobalModification = require('../Modification/GlobalModification');
var Modification = require('../Modification');
var FeatureState = require('../../../Model/FeatureState');

require('../css/globalModificationAnnotation.css');

/**
 * Class: Descartes.Tool.Edition.Modification.GolobalModificationAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Modification.GlobalModification>
 */
var Class = Utils.Class(GlobalModification, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        GlobalModification.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            GlobalModification.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.interaction.layers_[0] = this.editionLayer.OL_layers[0];
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(false);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            this.deActivating = true;
            if (Descartes.EditionManager.autoSave) {
                //this.checkForModificationToSave();
            }
            Modification.prototype.deactivate.apply(this, arguments);
            this.deActivating = false;


            //changement de style a la selection
            if (!_.isNil(this.featureToModify)) {
                //var style = Utils.getStyleByState(this.featureToModify);
                //this.setFeatureStyle(this.featureToModify, style);
//alert("deactivate this.featureToModify._origStyle" + this.featureToModify._origStyle.getFill().getColor());
                if (this.featureToModify.get("dAnnotationType")) {
                   this.featureToModify.setStyle(this.featureToModify._origStyle);
                   if (this.featureToModify.get("dAnnotationType") === "arrowAnnotation" || this.featureToModify.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                      this.editionLayer.refreshFeatureStyle(this.featureToModify);
                   }
                }
//alert("deactivate this.featureToModify" + this.featureToModify.getStyle().getFill().getColor());
                this.saveFeature(this.featureToModify);
            }
            if (!_.isNil(this.feature)) {
                //var style = Utils.getStyleByState(this.featureToModify);
                //this.setFeatureStyle(this.featureToModify, style);
//alert("deactivate this.featureToModify._origStyle" + this.featureToModify._origStyle.getFill().getColor());
                if (this.feature.get("dAnnotationType")) {
                   this.feature.setStyle(this.feature._origStyle);
                   if (this.feature.get("dAnnotationType") === "arrowAnnotation" || this.feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                      this.editionLayer.refreshFeatureStyle(this.feature);
                   }
                }
//alert("deactivate this.featureToModify" + this.featureToModify.getStyle().getFill().getColor());
                this.saveFeature(this.feature);
            }
            this.featureToModify = null;
            this.feature = null;
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(true);
              }
            });
            this.editionLayer.refresh();

            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    onModification: function (e) {
        if (!this.feature) {
            this.feature = e.feature.clone();
            this.feature.setId(e.feature.getId());
            this.feature._origStyle = e.feature._origStyle;
        } else {
            this.feature.setGeometry(e.feature.getGeometry());
        }
        if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(this.feature.getGeometry(), true)) {
            this.feature.setGeometry(this.initialGeom);
            return;
        }

        if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(this.feature, true)) {
            this.feature.setGeometry(this.initialGeom);
            return;
        }

        if (!_.isNil(this.feature.getId())) {
            this.feature.set('state', FeatureState.UPDATE);
        }

    },
    saveModification: function (e) {
        if (!this.underActivation && !this.deActivating) {
            //changement de style a la selection
            if (!_.isNil(this.featureToModify)) {
//alert("saveModification this.featureToModify" + this.featureToModify.getStyle().getFill().getColor());
                var style = Utils.getStyleByState(this.featureToModify);
                this.setFeatureStyle(this.featureToModify, style);
                if (this.featureToModify.get("dAnnotationType")) {
                   this.featureToModify.setStyle(this.featureToModify._origStyle);
                   if (this.featureToModify.get("dAnnotationType") === "arrowAnnotation" || this.featureToModify.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                      this.editionLayer.refreshFeatureStyle(this.featureToModify);
                   }
                }
            }

            if (!_.isNil(e.feature)) {
//alert("saveModification e.feature" + e.feature.getStyle().getFill().getColor());
                if (e.feature.getGeometry() instanceof ol.geom.Point) {
                   this.interaction.set("rotate", false);
                } else {
                   this.interaction.set("rotate", true);
                }
                if (e.feature.get("dAnnotationType") && !e.feature._origStyle) {
                   e.feature._origStyle = e.feature.getStyle();
//alert("saveModification e.feature._origStyle" + e.feature._origStyle.getFill().getColor());
                }
                this.featureToModify = e.feature;
                this.setFeatureStyle(this.featureToModify, 'select');
            }
            var feature = e.feature;

            if (_.isNil(feature) && !_.isNil(this.feature) ||
                    (!_.isNil(feature) && !_.isNil(this.feature) && feature !== this.feature)) {
//alert("saveModification this.feature" + this.feature.getStyle().getFill().getColor());
                if (this.editAttribut) {
                    this._showAttributesDialog(this.feature, this.saveFeatureAfterDialog.bind(this), this.cancel.bind(this));
                } else {
                    this.saveFeature(this.feature);
                }
            }
        }
    },
    saveFeature: function (feature) {
        //var style = Utils.getStyleByState(feature);
        //this.setFeatureStyle(feature, style);
        //feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                     feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }
        /*if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }*/
        feature.unset('state');
        feature.unset('selected');
//console.log("saveFeature feature" + feature.getStyle().getFill().getColor());
        if (feature.get("dAnnotationType")) {
//console.log("saveFeature feature._origStyle" + feature._origStyle.getFill().getColor());
           feature.setStyle(feature._origStyle);
           if (feature.get("dAnnotationType") === "arrowAnnotation" || feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
              this.editionLayer.refreshFeatureStyle(feature);
           }
        } else {
           feature.setStyle(null);
//alert("saveFeature null");
        }
//console.log("saveFeature feature" + feature.getStyle().getFill().getColor());
        for (var i = 0; i < this.editionLayer.annotationsFeatures.length; i++) {
           if (this.editionLayer.annotationsFeatures[i].getId() === feature.getId()) {
               this.editionLayer.annotationsFeatures[i].setGeometry(feature.getGeometry());
               this.editionLayer.annotationsFeatures[i].setStyle(feature._origStyle);
               delete this.editionLayer.annotationsFeatures[i]._origStyle;
               if (this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAnnotation" || this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAndTexteAnnotation") {
                  this.editionLayer.refreshFeatureStyle(this.editionLayer.annotationsFeatures[i]);
               }
               break;
           }
        }
        /*if (feature.getGeometry() instanceof ol.geom.Point) {
              this.interaction.set("rotate", true);
        }*/
        this.featureToModify = null;
        this.feature = null;
        this.editionLayer.refresh();
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.GlobalModificationAnnotation'
});
module.exports = Class;

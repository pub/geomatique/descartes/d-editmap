/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var $ = require('jquery');

var Utils = Descartes.Utils;
var BufferHaloCreation = require('../Creation/BufferHaloCreation');
var Creation = require('../Creation');
var FeatureState = require('../../../Model/FeatureState');

var ModalFormDialog = Descartes.UI.ModalFormDialog;
var template = require('./templates/BufferHaloAnnotations.ejs');

require('../css/bufferHaloAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Creation.BufferHaloAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation.BufferHaloCreation>
 */
var Class = Utils.Class(BufferHaloCreation, {

    aggregateResult: false,
    deleteOrig: false,
    confirm: false,

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        BufferHaloCreation.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        olLayer.getSource().on('addfeature', this.refresh, this);
        olLayer.getSource().on('removefeature', this.refresh, this);

    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            BufferHaloCreation.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        } else {
            if (Descartes.AnnotationButtonAide) {
                var aide = this.getMessage('TEXTE_WARNING') + this.aide;
                Descartes.AnnotationButtonAide.html(aide);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            BufferHaloCreation.prototype.deactivate.apply(this, arguments);
            this.unselectAll();
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode global
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer[this.configKey];
            } else {
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer[this.configKey];
        }

        //contrôle de la configuration
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        }/* else if (Descartes.EditionManager.isGlobalEditonMode() &&
                editionLayer.geometryType !== LayerConstants.POLYGON_GEOMETRY &&
                editionLayer.geometryType !== LayerConstants.MULTI_POLYGON_GEOMETRY) {
            this.setAvailable(false);
            this.updateElement();
        } else if (!_.isNil(this.editionLayer) &&
                this.editionLayer.geometryType !== LayerConstants.POLYGON_GEOMETRY &&
                this.editionLayer.geometryType !== LayerConstants.MULTI_POLYGON_GEOMETRY) {
            this.setAvailable(false);
            this.updateElement();
        }*/

        this.refresh();
    },
    /**
     * Methode: refresh
     * Met à jour l'état de l'outil et l'info-bulle en fonction du nombre d'éléments à sauvegarder.
     */
    refresh: function () {
        if (this.editionLayer.getAnnotationsFeatures().length === 0 || this.editionLayer.OL_layers[0].getSource().getFeatures().length === 0) {
            this.setAvailable(false);
            this.updateElement();
        } else {
            this.setAvailable(true);
            this.updateElement();
        }
    },
    _showBufferDialog: function () {
        var content = template({
            id: this.id,
            bufferLabel: this.getMessage('BUFFER_LABEL'),
            checkboxAggregateLabel: this.getMessage('CHECKBOX_AGGREGATE_LABEL'),
            checkboxDeleteOrigLabel: this.getMessage('CHECKBOX_DELETE_ORIG_LABEL'),
            checked: false
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_DIALOG'),
            content: content
        });
        dialog.open(this.sendDatas.bind(this), this.cancel.bind(this));
    },
    sendDatas: function (result) {
         this.bufferOptions = result;
         this.aggregateResult = $("#DescartesAggregateBufferCheckBox")[0].checked;
         this.deleteOrig = $("#DescartesBufferDeleteOrigCheckBox")[0].checked;
    },
    cancel: function () {
       this.deactivate();
	},
    /**
     * Méthode _filterFeatureForTempLayer
     * Retourne l'ensemble des features eligibles à la couche temporaire.
     *
     */
    _filterFeatureForTempLayer: function (feature, featureFilter) {
        //application du filtre sur une feature
        var result = [];
        if (_.isFunction(featureFilter)) {
            if (!featureFilter.apply(this, [feature])) {
                return result;
            }
        }

        if (feature.state !== FeatureState.DELETE) {
            if (this._explodeMultiGeometryInTempLayer === true && this.isMultipleGeometry(feature)) {
                var subGeoms = Utils.getSubGeom(feature.getGeometry());

                for (var k = 0; k < subGeoms.length; k++) {
                    var subGeom = subGeoms[k];
                    var clonedGeom = subGeom.clone();
                    var polygonFeature = new ol.Feature(feature.getProperties());

                    polygonFeature.setGeometry(clonedGeom);
                    this.setFeatureStyle(polygonFeature, 'support');
                    result.push(polygonFeature);
                }
            } else {
                var copy = feature.clone();
                copy.setId(feature.getId());
                this.setFeatureStyle(copy, 'support');
                result.push(copy);
            }
        }
        return result;
    },
    /** Méthode clickoutFeature
     * Appelée lorsque l'utilisateur clic en dehors de toute géométrie
     */
    clickoutFeature: function (features) {
        var that = this;
        if (this.bufferOptions) {
            this._bufferingSelection(features);
            if (this.deleteOrig) {
                _.each(features, function (feature) {
                    var newAnnotationsFeatures = [];
                    var annotationsFeatures = that.editionLayer.getAnnotationsFeatures();
                    for (var i = 0; i < annotationsFeatures.length; i++) {
                       if (annotationsFeatures[i].getId() !== feature.getId()) {
                           newAnnotationsFeatures.push(annotationsFeatures[i]);
                       }
                    }
                    that.editionLayer.setAnnotationsFeatures(newAnnotationsFeatures);
                    that.editionLayer.refresh();
                });
            }
        }
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveObjects: function (features) {
        /*var objectId = this.createObjectId;
        if (!_.isNil(objectId)) {
            _.each(features, function (feature) {
                feature.set('idAttribut', objectId);
            });
        }*/

        this.createObjectId = null;
        this.createAttributes = null;

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        _.each(features, function (feature) {
            /*if (feature.get('state') === FeatureState.INSERT) {
                olLayer.getSource().addFeature(feature);
            }
            feature.editionLayer = this.editionLayer;
            this.setFeatureStyle(feature, 'create');*/
            var objectId = 'dObjectId_' + Math.random().toString(36).slice(2);
            feature.setId(objectId);
            feature.unset('state');
            feature.unset('selected');
            if (!feature.get("dAnnotationType")) {
                feature.set("dAnnotationType", "drawAnnotation");
            }
            this.setFeatureStyle(feature, "default");
            this.editionLayer.getAnnotationsFeatures().push(feature);
        }.bind(this));

        this.editionLayer.refresh();

        /*if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            saveStrategy.save();
        }*/

        this.deactivate();
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.BufferHaloAnnotation'
});
module.exports = Class;

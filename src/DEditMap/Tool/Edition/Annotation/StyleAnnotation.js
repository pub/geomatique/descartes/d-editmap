/* global MODE, Descartes */

var $ = require('jquery');
var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');
var Symbolizers = require('../../../Symbolizers');

var ModalFormDialog = Descartes.UI.ModalFormDialog;
var template = require('./templates/StyleAnnotations.ejs');

require('../css/styleAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Modification.StyleAnnotation
 * Outil de création par dessin.
 *
 * Hérite de
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    hitTolerance: 5,
    interaction: null,
    multiple: false,
    defaultColor: '#454543',
    colorFill: null,
    colorStroke: null,
    defaultOpacity: 0.4,
    opacityFill: null,
    defaultWidth: 2,
    widthStroke: null,
    configStyles: {
        fillColor: this.defaultColor,
        fillOpacity: this.defaultOpacity,
        strokeColor: this.defaultColor,
        strokeWidth: this.defaultWidth,
        imageFillColor: this.defaultColor,
        imageFillOpacity: this.defaultOpacity,
        imageStrokeColor: this.defaultColor,
        imageStrokeWidth: this.defaultWidth,
        textFillColor: this.defaultColor,
        textScale: this.defaultWidth
    },

    initialize: function () {
        this.interaction = new ol.interaction.Select({
            toggleCondition: ol.events.condition.singleClick,
            hitTolerance: this.hitTolerance,
            layers: function (layer) {
                if (!_.isNil(this.editionLayer)) {
                    for (var i = 0; i < this.editionLayer.OL_layers.length; i++) {
                        if (this.editionLayer.OL_layers[i] === layer) {
                            return true;
                        }
                    }
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'SelectionInteraction');
        this.interaction.on('select', this.toggleFeatureSelected.bind(this));
        this.interaction.setActive(false);
        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

        this.colorFill = this.defaultColor;
        this.colorStroke = this.defaultColor;
        this.opacityFill = this.defaultOpacity;
        this.widthStroke = this.defaultWidth;
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.openStyleDialog();
            this.underActivation = false;
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Edition.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    toggleFeatureSelected: function (event) {

        if (!this.multiple) {
            event.deselected = this.editionLayer.getSelectedFeatures();
        }

        _.each(event.deselected, function (feature) {
            //feature.set('selected', false);
           // var style = Utils.getStyleByState(feature);
            //this.setFeatureStyle(feature, style);
            if (feature.get("dAnnotationType")) {
               feature.setStyle(this._origStyle);
               if (feature.get("dAnnotationType") === "arrowAnnotation" || feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                  this.editionLayer.refreshFeatureStyle(feature);
               }
            }

            if (!this.multiple) {
                this.interaction.getFeatures().remove(feature);
            }
        }.bind(this));

        _.each(event.selected, function (feature) {
            //feature.set('selected', true);
            this._origStyle = feature.getStyle();
            //this.setFeatureStyle(feature, 'select');
            this.changeColor(feature);
        }.bind(this));
    },
    openStyleDialog: function () {

        var content = template({
          colorFill: this.colorFill,
          colorStroke: this.colorStroke,
          opacityFill: this.opacityFill,
          widthStroke: this.widthStroke
        });
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_DIALOG'),
            content: content
        });
        dialog.open(this.sendDatas.bind(this));
        var that = this;
        var colorStrokeStyleAnnotation = document.getElementById('colorStrokeStyleAnnotation');
        /*colorStrokeStyleAnnotation.addEventListener('input', function (e) {
            console.log(this.value);
        });*/
        colorStrokeStyleAnnotation.addEventListener('change', function (e) {
            that.colorStroke = this.value;
        });
        var widthStrokeStyleAnnotation = document.getElementById('widthStrokeStyleAnnotation');
        widthStrokeStyleAnnotation.addEventListener('change', function (e) {
            that.widthStroke = parseFloat(this.value);
        });
        var colorFillStyleAnnotation = document.getElementById('colorFillStyleAnnotation');
        colorFillStyleAnnotation.addEventListener('change', function (e) {
            that.colorFill = this.value;
        });
        var opacityFillStyleAnnotation = document.getElementById('opacityFillStyleAnnotation');
        opacityFillStyleAnnotation.addEventListener('change', function (e) {
            that.opacityFill = this.value;
        });
    },
    sendDatas: function () {
        this.configStyles.fillColor = Symbolizers.colorOpacity(this.colorFill, this.opacityFill);
        this.configStyles.strokeColor = this.colorStroke;
        this.configStyles.strokeWidth = this.widthStroke;
        this.configStyles.imageFillColor = Symbolizers.colorOpacity(this.colorFill, this.opacityFill);
        this.configStyles.imageStrokeColor = this.colorStroke;
        this.configStyles.imageStrokeWidth = this.widthStroke;
        this.configStyles.textFillColor = this.colorStroke;
        this.configStyles.textScale = this.widthStroke;

        this.editionLayer.symbolizers["default"]["Point"].fillColor = this.colorFill;
        this.editionLayer.symbolizers["default"]["Point"].fillOpacity = this.opacityFill;
        this.editionLayer.symbolizers["default"]["Point"].strokeColor = this.colorStroke;
        this.editionLayer.symbolizers["default"]["Point"].strokeWidth = this.widthStroke;
        this.editionLayer.symbolizers["default"]["Line"].fillColor = this.colorFill;
        this.editionLayer.symbolizers["default"]["Line"].fillOpacity = this.opacityFill;
        this.editionLayer.symbolizers["default"]["Line"].strokeColor = this.colorStroke;
        this.editionLayer.symbolizers["default"]["Line"].strokeWidth = this.widthStroke;
        this.editionLayer.symbolizers["default"]["Polygon"].fillColor = this.colorFill;
        this.editionLayer.symbolizers["default"]["Polygon"].fillOpacity = this.opacityFill;
        this.editionLayer.symbolizers["default"]["Polygon"].strokeColor = this.colorStroke;
        this.editionLayer.symbolizers["default"]["Polygon"].strokeWidth = this.widthStroke;

        var applyStyleAllAnnotations = $("#DescartesStyleAllAnnotationsCheckBox")[0].checked;
        if (this.editionLayer.OL_layers[0].getSource().getFeatures().length === 0) {
            this.deactivate();
        } else if (applyStyleAllAnnotations) {
            this.editionLayer.OL_layers[0].getSource().getFeatures().forEach(function (feature) {
                 this.changeColor(feature);
            }, this);
            this.deactivate();
        }
    },

	changeColor: function (feature) {

        var style = feature.getStyle();
        var styleFeature = null;
        var typeStyle = typeof style;
        if (style && typeStyle !== "function") {
            if (style instanceof Array) {
                styleFeature = style[0];
            } else {
                styleFeature = feature.getStyle();
            }
        } else {
             var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['default']);
             if (feature.getGeometry()) {
                 styleFeature = olStyles[feature.getGeometry().getType()];
             }
		}
        if (styleFeature && styleFeature.getFill && styleFeature.getFill()) {
            styleFeature.getFill().setColor(this.configStyles.fillColor);
        }
        if (styleFeature && styleFeature.getStroke && styleFeature.getStroke()) {
            styleFeature.getStroke().setColor(this.configStyles.strokeColor);
            styleFeature.getStroke().setWidth(this.configStyles.strokeWidth);
        }
        if (styleFeature && styleFeature.getImage && styleFeature.getImage() && styleFeature.getImage().getFill && styleFeature.getImage().getFill()) {
            styleFeature.getImage().getFill().setColor(this.configStyles.imageFillColor);
        }
        if (styleFeature && styleFeature.getImage && styleFeature.getImage() && styleFeature.getImage().getStroke && styleFeature.getImage().getStroke()) {
            styleFeature.getImage().getStroke().setColor(this.configStyles.imageStrokeColor);
            styleFeature.getImage().getStroke().setWidth(this.configStyles.strokeWidth);
        }
        if (styleFeature && styleFeature.getText && styleFeature.getText() && styleFeature.getText().getFill && styleFeature.getText().getFill()) {
            styleFeature.getText().getFill().setColor(this.configStyles.textFillColor);
            styleFeature.getText().setScale(this.configStyles.strokeWidth);
		}
        if (style instanceof Array) {
            style[0] = styleFeature;
        } else {
            style = styleFeature;
        }
        feature.setStyle(styleFeature);
		for (var i = 0; i < this.editionLayer.annotationsFeatures.length; i++) {
           if (this.editionLayer.annotationsFeatures[i].getId() === feature.getId()) {
               this.editionLayer.annotationsFeatures[i].setStyle(style);
               if (this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAnnotation" || this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAndTexteAnnotation") {
                   this.editionLayer.refreshFeatureStyle(this.editionLayer.annotationsFeatures[i]);
               }
               break;
           }
        }
        this.editionLayer.refresh();
        this.interaction.getFeatures().remove(feature);

	},

	changeCursorPointerOnFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.editionLayer.getFeatureOL_layers()[0];
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.StyleAnnotation'
});
module.exports = Class;

/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');

require('../css/exportAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.ExportAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.active = true;
            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.exporterAnnotations();
            this.deactivate();
            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(this.aide);
            }
		}
    },

    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
           this.active = false;
           Edition.prototype.deactivate.apply(this, arguments);
        }
    },
    exporterAnnotations: function () {
        var that = this;
        var dialogListExportFormat = new Descartes.Button.ContentTask.ExportVectorLayer();
        dialogListExportFormat.title = this.getMessage("DIALOG_TITLE_EXPORT_FORMAT");
        dialogListExportFormat.close = this.close;
        if (dialogListExportFormat.listExportFormat[0].id !== "dcarto") {
           dialogListExportFormat.listExportFormat.unshift({
              id: "dcarto", label: 'JSON (format spécifique - export des géométries et des styles)', olClass: null, fileName: "annotations_epsg_4326.dcarto"
           });
        }
        for (var i = 0, len = dialogListExportFormat.listExportFormat.length; i < len; i++) {
             if (dialogListExportFormat.listExportFormat[i].id === "kml") {
                 dialogListExportFormat.listExportFormat[i].label = "KML (export des géométries et des styles si possible)";
             }
             if (dialogListExportFormat.listExportFormat[i].id === "geojson") {
                 dialogListExportFormat.listExportFormat[i].label = "GeoJson (export des géométries seulement)";
             }
        }

        dialogListExportFormat.runExport = function (form) {

            var selectedFormat = form.format;
            var Classname = null;
            var filename = "exportDescartes.txt";
            for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
                if (selectedFormat === this.listExportFormat[i].id) {
                    Classname = this.listExportFormat[i].olClass;
                    filename = this.listExportFormat[i].fileName;
                    break;
                }
            }
            if (this.listExportFormat[0].id === "dcarto") {
                this.listExportFormat.shift();
            }
            var flux = "";
            var features = null;
            if (Classname) {
              var optionsFormat = {};
              //if (selectedFormat === "kml") {
                  optionsFormat.featureProjection = that.olMap.getView().getProjection().getCode();
                  optionsFormat.defaultDataProjection = 'EPSG:4326'; //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
              //}
              var format = new Classname();
              features = that.editionLayer.OL_layers[0].getSource().getFeatures();
              features.forEach(function (feature) {
                feature.unset('state');
                feature.unset('selected');
              });
              filename = "annotations_epsg_4326." + selectedFormat;
              flux = format.writeFeatures(features, optionsFormat);

            } else {
              features = that.editionLayer.OL_layers[0].getSource().getFeatures();
              var featurescloned = [];
              features.forEach(function (feature) {
                   var featurecloned = feature.clone();
                   featurecloned.setId(feature.getId());
                   featurescloned.push(featurecloned);
              });
              featurescloned.forEach(function (feature) {
                 var geometry = feature.getGeometry().clone();
                 geometry.transform(that.olMap.getView().getProjection().getCode(), 'EPSG:4326');
                 feature.setGeometry(geometry);
              });
              var content = that.editionLayer.serializeAnnotations(featurescloned);
              flux = JSON.stringify(content);
            }
              Utils.download(filename, flux);
              that.close();
        };
        dialogListExportFormat.execute();

    },
    close: function () {
		if (Descartes.AnnotationButtonAide) {
            Descartes.AnnotationButtonAide.html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
        }
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.ExportAnnotation'
});
module.exports = Class;

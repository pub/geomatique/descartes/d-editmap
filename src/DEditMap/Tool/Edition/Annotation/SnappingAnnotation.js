/* global MODE, Descartes */

var $ = require('jquery');
var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');
var LayerConstants = Descartes.Layer;
var Symbolizers = require('../../../Symbolizers');

var ModalFormDialog = Descartes.UI.ModalFormDialog;
var template = require('./templates/SnappingAnnotations.ejs');

require('../css/snappingAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.SnappingAnnotation
 * Outil de création par dessin.
 *
 * Hérite de
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    snappingLayersIdentifier: [],

    initialize: function () {

        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

        this._nbautotracing = 0;

    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {

            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.active = true;
            this.openSnappingDialog();

            Descartes._activeClickToolTip = false;

            this.deactivate();

            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(this.aide);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {

            Edition.prototype.deactivate.apply(this, arguments);
            this.active = false;
            this.editionLayer.disableEdition();
            Descartes._activeClickToolTip = true;
        }
    },

    openSnappingDialog: function () {

        if (this.editionLayer.snapping && this.editionLayer.snapping.snappingLayersIdentifier && this.editionLayer.snapping.snappingLayersIdentifier.length > 0) {
            this.snappingLayersIdentifier = this.editionLayer.snapping.snappingLayersIdentifier;
        }

        var layers = this.dmap.mapContent.getLayers();
        this.listLayers = [];
        var count = 0;
        layers.forEach(function (layer) {
           if (layer.resourceLayers[0].getFeatureServerParams() !== null) {
               this.listLayers.push(layer);
               var index = this.snappingLayersIdentifier.indexOf(layer.id);
               if (index !== -1 && layer.geometryType !== LayerConstants.POINT_GEOMETRY && layer.geometryType !== LayerConstants.MULTI_POINT_GEOMETRY) {
                   count++;
               }
           }
        }, this);
        this._nbautotracing = count;

        if (this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
            this.autotracing = this.editionLayer.snapping.autotracing;
        }

        var content = template({
           layers: this.listLayers,
           snappingLayersIdentifier: this.snappingLayersIdentifier,
           autotracing: this.autotracing
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_DIALOG'),
            content: content
        });
        dialog.open(this.sendDatas.bind(this), this.close.bind(this));

        if (this._nbautotracing === 0) {
            $("#DescartesAutotracingCheckBox")[0].disabled = true;
        }
        var that = this;
        for (var i = 0; i < this.listLayers.length; i++) {
           if (this.listLayers[i].geometryType !== LayerConstants.POINT_GEOMETRY && this.listLayers[i].geometryType !== LayerConstants.MULTI_POINT_GEOMETRY) {
              $("#DescartesSnappingLayerCheckBox_" + i).click(function () {
                if($(this).is(":checked")) {
                   that._nbautotracing++;
                   $("#DescartesAutotracingCheckBox")[0].disabled = false;
                } else {
                   that._nbautotracing--;
                   if (that._nbautotracing === 0) {
                      $("#DescartesAutotracingCheckBox")[0].disabled = true;
                      $("#DescartesAutotracingCheckBox")[0].checked = false;
                   }
                }
              });
           }
        }
    },
    sendDatas: function () {
       for (var i = 0; i < this.listLayers.length; i++) {
           delete this.listLayers[i].displayIconSnap;
           delete this.listLayers[i].displayIconAutoTracing;
           var checkbox = $("#DescartesSnappingLayerCheckBox_" + i)[0];
           var index = this.snappingLayersIdentifier.indexOf(checkbox.value);
           if (checkbox.checked) {
               if (index === -1) {
                    this.snappingLayersIdentifier.push(checkbox.value);
               }
           } else {
               if (index !== -1) {
                   this.snappingLayersIdentifier.splice(index, 1);
               }
           }
       }
       this.autotracing = $("#DescartesAutotracingCheckBox")[0].checked;
       if (this.snappingLayersIdentifier.length > 0) {
            this.editionLayer.snapping = {
                snappingLayersIdentifier: this.snappingLayersIdentifier,
                tolerance: 15,
                enable: false,
                autotracing: this.autotracing
            };
       } else {
            delete this.editionLayer.snapping;
       }
       this.dmap._initializeLayersIconSnap();
       this.dmap._initializeLayersIconAutoTracing();
       //this.dmap.refresh();
       this.dmap.mapContentManager.redrawUI();
       this.deactivate();
       this.close();
    },
    close: function () {
        if (Descartes.AnnotationButtonAide) {
            Descartes.AnnotationButtonAide.html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
        }
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.SnappingAnnotation'
});
module.exports = Class;

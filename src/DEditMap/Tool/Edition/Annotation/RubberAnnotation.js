/* global MODE, Descartes */
var _ = require('lodash');

var Utils = Descartes.Utils;
var RubberDeletion = require('../Deletion/RubberDeletion');
var FeatureState = require('../../../Model/FeatureState');
var EditionManager = require('../../../Core/EditionManager');

require('../css/rubberAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Deletion.RubberAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Deletion.RubberDeletion>
 */
var Class = Utils.Class(RubberDeletion, {

    /**
     * Constructeur: Descartes.Tool.Edition.Deletion.RubberDeletionAnnotation
     */
    initialize: function () {
        RubberDeletion.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            //pour rechargement via context
            this.interaction.un('select', this.onSelect.bind(this));
            this.olMap.removeInteraction(this.interaction);
            this.interaction = null;
            this._OLToolInitialize = false;
            this.editionLayer = Descartes.AnnotationsLayer;
            //this.olLayer = this.editionLayer.OL_layers[0];
            //this.olLayer.getSource().on('removefeature', this.refresh, this);
            RubberDeletion.prototype.activate.apply(this, arguments);
            this.olMap.addInteraction(this.interaction);
            this.interaction.setActive(true);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            RubberDeletion.prototype.deactivate.apply(this, arguments);
            this.interaction.setActive(false);
            this.interaction.un('select', this.onSelect.bind(this));
            this.olMap.removeInteraction(this.interaction);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    onSelect: function (event) {
        _.each(event.deselected, function (feature) {
            feature.set('state', feature.get('oldstate'));
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
        }.bind(this));
        _.each(event.selected, function (feature) {
            if (_.isNil(feature.getId())) {
                this.olLayer.getSource().removeFeature(feature);
            } else {
                feature.set('oldstate', feature.get('state'));
                feature.set('state', FeatureState.DELETE);
                feature.editionLayer = this.editionLayer;
                if (this.editionLayer &&
                        this.editionLayer.attributes &&
                        this.editionLayer.attributes.attributeId &&
                        this.editionLayer.attributes.attributeId.fieldName !== null) {
                             feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
                }
                this.setFeatureStyle(feature, 'delete');

                feature.unset('state');
                feature.unset('selected');
                //feature.setStyle(null);
                this.olLayer.getSource().removeFeature(feature);

                var newAnnotationsFeatures = [];
                var annotationsFeatures = this.editionLayer.getAnnotationsFeatures();
                for (var i = 0; i < annotationsFeatures.length; i++) {
                   if (annotationsFeatures[i].getId() !== feature.getId()) {
                       newAnnotationsFeatures.push(annotationsFeatures[i]);
                   }
                }
                this.editionLayer.setAnnotationsFeatures(newAnnotationsFeatures);
                this.editionLayer.refresh();

            }
        }.bind(this));

        if (EditionManager.autoSave) {
            //this.olLayer.getSource().get('saveStrategy').save();
        }
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Deletion.RubberAnnotation'
});
module.exports = Class;

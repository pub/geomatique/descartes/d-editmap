/* global MODE, Descartes */

var $ = require('jquery');
var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');
var Tool = Descartes.Tool;
var FormDialog = Descartes.UI.FormDialog;
var template = require('./templates/AideAnnotations.ejs');

require('../css/aideAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.ImportAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        Tool.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
        this.formDialog = null;
    },

    /**
     * Methode: activate
     * Active le bouton et désactive les autres boutons d'interaction de la barre d'outils.
     */
    activate: function () {

        this.active = true;
        /*if (this.isActive()) {
            return false;
        }*/

        //this.events.triggerEvent('activate', this);

        this.updateElement();

        //Descartes._activeClickToolTip = false;

        if (_.isNil(this.formDialog)) {
            this.createModalDialog();
            Descartes.AnnotationButtonAide = $('#DescartesAideAnnotations');
        }

        var toolbar = this.dmap.getAnnotationToolBar();
        if (this.formDialog && toolbar) {
           var texte = this.getAnnotationButtonAide(toolbar);
           if (texte === "") {
              $('#DescartesAideAnnotations').html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
           } else {
              $('#DescartesAideAnnotations').html(texte);
           }
        }

        return true;
    },
    /**
     * Methode: deactivate
     * Désactive le bouton
     */
    deactivate: function () {
        if (arguments.length === 0) {
           this.active = false;
           this.updateElement();
           //Descartes._activeClickToolTip = true;
           if (this.formDialog) {
               this.formDialog.close();
                this.formDialog = null;
                Descartes.AnnotationButtonAide = null;
           }
        }
        /*var toolbar = this.dmap.getAnnotationToolBar();
        if (this.formDialog && toolbar) {
           var texte = this.getAnnotationButtonAide(toolbar);
           if (texte === "") {
              $('#DescartesAideAnnotations').html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
           } else {
              $('#DescartesAideAnnotations').html(texte);
           }
        }*/
        return true;
    },
    close: function () {
        this.formDialog = null;
        this.deactivate();
    },
    createModalDialog: function () {
        var close = this.close.bind(this);
        //var content = "<div id=\"DescartesAideAnnotations\" name=\"DescartesAideAnnotations\" class=\"DescartesDialogContent\"></div>";
        var content = template();
        var formDialogOptions = {
                id: this.id + '_dialog',
                title: this.getMessage('TITLE_DIALOG'),
                size: 'modal-lg',
                sendLabel: 'Fermer',
                content: content
            };
        this.formDialog = new FormDialog(formDialogOptions);

        this.formDialog.open(close, close);
        var formSelector = '#' + this.formDialog.id + '_formDialog';

        var formDialogDiv = $(formSelector);

        var optionsResize = {
            minHeight: 400,
            minWidth: 400,
            alsoResize: formSelector + ' .DescartesDialogContent'
        };

        formDialogDiv.parent().resizable(optionsResize);
    },
    getAnnotationButtonAide: function (toolbar) {
        var texte = "";
		for (var i = 0; i < toolbar.controls.length; i++) {
             var control = toolbar.controls[i];
             if (control instanceof Edition && control.CLASS_NAME !== "Descartes.Tool.Edition.AideAnnotation" && control.isActive()) {
                 texte = control.aide;
                 break;
             }
             if (control.CLASS_NAME === "Descartes.Button.ToolBarOpener") {
                texte = this.getAnnotationButtonAide(control.toolBar);
             }
		}

        return texte;
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.AideAnnotation'
});
module.exports = Class;

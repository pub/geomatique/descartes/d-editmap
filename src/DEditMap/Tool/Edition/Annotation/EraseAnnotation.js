/* global MODE, Descartes */

var Utils = Descartes.Utils;
var Edition = require('../../Edition');
var ConfirmDialog = Descartes.UI.ConfirmDialog;

require('../css/eraseAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Deletion.EraseAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
    },

    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.active = true;
            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.afficherConfirmation();
            this.deactivate();
            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(this.aide);
            }
		}
    },

    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
           this.active = false;
           Edition.prototype.deactivate.apply(this, arguments);
        }
    },

	afficherConfirmation: function () {
		var that = this;
		var dialog = new ConfirmDialog({
            id: this.id + '_removeDialog',
            title: this.getMessage('DIALOG_TITLE'),
            message: this.getMessage('DIALOG_MSG'),
            type: 'remove'
        });
        dialog.open(function (result) {
            if (result) {
                 that.removeAllFeatures();
            } else {
                if (Descartes.AnnotationButtonAide) {
                     Descartes.AnnotationButtonAide.html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
                }
            }
        });
	},
	removeAllFeatures: function () {
        this.editionLayer.setAnnotationsFeatures([]);
        this.editionLayer.refresh();
	},
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Deletion.EraseAnnotation'
});
module.exports = Class;

/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var VerticeModification = require('../Modification/VerticeModification');
var Modification = require('../Modification');
var Symbolizers = require('../../../Symbolizers');

require('../css/verticeModificationAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Modification.VerticeModificationAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Modification.VerticeModification>
 */
var Class = Utils.Class(VerticeModification, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        VerticeModification.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            //pour rechargement via context
            this.olMap.removeInteraction(this.interaction);
            this.olMap.removeInteraction(this.modifyInteraction);
            this.olMap.removeInteraction(this._snappingInteraction);
            this.interaction = null;
            this.modifyInteraction = null;
            this._snappingInteraction = null;
            this._OLToolInitialize = false;
            this.editionLayer = Descartes.AnnotationsLayer;
            VerticeModification.prototype.activate.apply(this, arguments);
            this.olMap.addInteraction(this.interaction);
            this.olMap.addInteraction(this.modifyInteraction);
            if (this._snappingInteraction) {
            this.olMap.addInteraction(this._snappingInteraction);
            }
            this.interaction.setActive(true);
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(false);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            if (!_.isNil(this.olLayer) && !_.isNil(this.olLayer.map)) {
                var map = this.olLayer.map;
                map.un('pointermove', this.changeCursorPointerOnFeature, this);
            }
            if (Descartes.EditionManager.autoSave) {
                //this.checkForModificationToSave();
            }
            if (!_.isNil(this.featureToModify)) {
                var style = Utils.getStyleByState(this.featureToModify);
                this.setFeatureStyle(this.featureToModify, style);
                if (this.featureToModify.get("dAnnotationType")) {
                   this.featureToModify.setStyle(this.featureToModify._origStyle);
                   if (this.featureToModify.get("dAnnotationType") === "arrowAnnotation" || this.featureToModify.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                      this.editionLayer.refreshFeatureStyle(this.featureToModify);
                   }
                }
            }
            Modification.prototype.deactivate.apply(this, arguments);
            this.interaction.getFeatures().clear();
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(true);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }

        }
    },
    updateOLFeature: function (olLayer) {
        this.olLayer = olLayer;

        if (!_.isNil(this.olMap)) {
            if (!_.isNil(this.interaction)) {
                this.olMap.removeInteraction(this.interaction);
            }
            if (!_.isNil(this.modifyInteraction)) {
                this.olMap.removeInteraction(this.modifyInteraction);
            }
        }

        var insertVertexCondition = ol.events.condition.always;
        if (this.createVertices === false) {
            insertVertexCondition = ol.events.condition.never;
        }

        this.interaction = new ol.interaction.Select({
            layers: [olLayer],
            toggleCondition: function (event) {
                if (event.type === 'singleclick') {
                    var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this.olLayer;
                        }.bind(this),
                        hitTolerance: this.hitTolerance
                    });
                    if (!_.isNil(features)) {
                        return features === this.featureToModify;
                    }
                    return false;
                }
                return false;
            }.bind(this)
        });
        this.interaction.on('select', this.onSelection.bind(this));

        var style;
        if (this.createVertices === true) {
           style = new ol.style.Style({
                image: new ol.style.RegularShape({
                    radius: 5,
                    radius2: 0,
                    points: 4,
                    angle: 0,
                    stroke: new ol.style.Stroke({
                        color: 'black',
                        width: 3
                      }),
                    fill: new ol.style.Fill({
                      color: 'black'
                    })
                  })
            });
        } else {
            style = new ol.style.Style();
        }

        this.modifyInteraction = new ol.interaction.Modify({
            insertVertexCondition: insertVertexCondition,
            style: style,
            deleteCondition: this.deleteCondition,
            features: this.interaction.getFeatures()
        });
        this.modifyInteraction.set('id', 'VerticeModification_' + Utils.createUniqueID());
        this.modifyInteraction.on('modifystart', this.onModificationStart, this);
        this.modifyInteraction.on('modifyend', this.onModificationEnd, this);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    onSelection: function (event) {
        _.each(event.deselected, function (feature) {
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
            if (feature.get("dAnnotationType")) {
                   feature.setStyle(feature._origStyle);
                   if (feature.get("dAnnotationType") === "arrowAnnotation" || feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                      this.editionLayer.refreshFeatureStyle(feature);
                   }
            }
        }.bind(this));

        if (event.deselected.length > 0) {
            if (event.deselected[0] === this.featureToModify) {
                var sameGeom = true;
                if (!_.isNil(this.initialGeom)) {
                    var initGeom = this._parser.read(this.initialGeom);
                    var newGeom = this._parser.read(this.featureToModify.getGeometry());
                    sameGeom = initGeom.equalsTopo(newGeom);
                }

                if (!sameGeom) {
                    if (this.editAttribut) {
                        this._showAttributesDialog(this.featureToModify, this.saveFeatureAfterDialog.bind(this), this.cancel.bind(this));
                    } else {
                        this.saveFeature(this.featureToModify);
                    }
                    //this.interaction.getFeatures().clear();
                    this.interaction.getFeatures().remove(this.featureToModify);
                } else {
                    this.interaction.getFeatures().remove(this.featureToModify);
                }
            }
        }

        _.each(event.selected, function (feature) {
            this.featureToModify = feature;
            this.featureToModify._origStyle = feature.getStyle();
            this.setFeatureStyle(feature, 'select');
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['select']);
            var olStyle = olStyles[feature.getGeometry().getType()];
            var styles = [
                olStyle,
                new ol.style.Style({
                  image: new ol.style.RegularShape({
                    radius: 5,
                    radius2: 0,
                    points: 4,
                    angle: 0,
                    stroke: new ol.style.Stroke({
                        color: 'black',
                        width: 3
                      }),
                    fill: new ol.style.Fill({
                      color: 'black'
                    })
                  }),
                  geometry: function (feature) {
                    // return the coordinates of the first ring of the polygon
                    var coordinates, i, len;
                    var allCoordinates = [];
                    var geom = feature.getGeometry();
                    if (feature.getGeometry() instanceof ol.geom.Point || feature.getGeometry() instanceof ol.geom.MultiPoint) {
                        return null;
                    } else if (feature.getGeometry() instanceof ol.geom.LineString) {
                        coordinates = feature.getGeometry().getCoordinates();
                        geom = new ol.geom.MultiPoint(coordinates);
                    } else if (feature.getGeometry() instanceof ol.geom.Polygon) {
                        coordinates = feature.getGeometry().getCoordinates()[0];
                        geom = new ol.geom.MultiPoint(coordinates);
                    } else if (feature.getGeometry() instanceof ol.geom.MultiLineString) {
                        coordinates = feature.getGeometry().getCoordinates();
                        for (i = 0, len = coordinates.length; i < len; i++) {
                            allCoordinates = allCoordinates.concat(coordinates[i]);
                        }
                        geom = new ol.geom.MultiPoint(allCoordinates);
                    } else if (feature.getGeometry() instanceof ol.geom.MultiPolygon) {
                        coordinates = feature.getGeometry().getCoordinates();
                        for (i = 0, len = coordinates.length; i < len; i++) {
                            allCoordinates = allCoordinates.concat(coordinates[i][0]);
                        }
                        geom = new ol.geom.MultiPoint(allCoordinates);
                    }
                    return geom;
                  }
                })
              ];
            feature.setStyle(styles);
        }.bind(this));

    },
    saveFeature: function (feature) {
        feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                     feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }
        /*if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }*/
        feature.unset('state');
        feature.unset('selected');
        if (feature.get("dAnnotationType")) {
           feature.setStyle(feature._origStyle);
           if (feature.get("dAnnotationType") === "arrowAnnotation" || feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
              this.editionLayer.refreshFeatureStyle(feature);
           }
        }
        for (var i = 0; i < this.editionLayer.annotationsFeatures.length; i++) {
           if (this.editionLayer.annotationsFeatures[i].getId() === feature.getId()) {
               this.editionLayer.annotationsFeatures[i].setGeometry(feature.getGeometry());
               this.editionLayer.annotationsFeatures[i].setStyle(feature._origStyle);
               delete this.editionLayer.annotationsFeatures[i]._origStyle;
               if (this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAnnotation" || this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAndTexteAnnotation") {
                  this.editionLayer.refreshFeatureStyle(this.editionLayer.annotationsFeatures[i]);
               }
               break;
           }
        }
        this.featureToModify = null;
        this.initialState = null;
        this.initialGeom = null;
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.VerticeModificationAnnotation'
});
module.exports = Class;

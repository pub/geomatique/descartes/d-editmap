/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var DrawCreation = require('../Creation/DrawCreation');

var Symbolizers = require('../../../Symbolizers');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../../../Model/EditionLayerConstants');
var AnnotationConstants = require('./AnnotationConstants');

require('../css/drawAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Creation.DrawAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation.DrawCreation>
 */
var Class = Utils.Class(DrawCreation, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        DrawCreation.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
        this.title = this.formatButtonTitle(this.geometryType);
        this.aide = this.formatButtonAide(this.geometryType);
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.editionLayer = Descartes.AnnotationsLayer;
            DrawCreation.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            if (this.snapping && _.isNil(this._snappingInteraction)) {
                 this._activateSnapping();
            }
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
              this.interaction.autotracingAppendCoordinates = function (coordinateExtension) {
                var mode = this.mode_;
                let coordinates = [];
                if (mode === ol.interaction.Draw.Mode_.LINE_STRING) {
                  coordinates = /** @type {LineCoordType} */ this.sketchCoords_;
                } else if (mode === ol.interaction.Draw.Mode_.POLYGON) {
                  coordinates = this.sketchCoords_ && this.sketchCoords_.length ? /** @type {PolyCoordType} */ (this.sketchCoords_)[0] : [];
                }

                // Remove last coordinate from sketch drawing (this coordinate follows cursor position)
                const ending = coordinates.pop();

                // Append coordinate list
                for (let i = 0; i < coordinateExtension.length; i++) {
                  this.autotracingAddToDrawing_(coordinateExtension[i]);
                }

                // Duplicate last coordinate for sketch drawing
                this.autotracingAddToDrawing_(ending);
              };
              this.interaction.autotracingAddToDrawing_ = function (coordinate) {
                  var geometry = /** @type {ol.geom.SimpleGeometry} */ (this.sketchFeature_.getGeometry());
                  var done;
                  var coordinates;
                  if (this.mode_ === ol.interaction.Draw.Mode_.LINE_STRING) {
                    this.finishCoordinate_ = coordinate.slice();
                    coordinates = this.sketchCoords_;
                    if (coordinates.length >= this.maxPoints_) {
                        if (this.freehand_) {
                           coordinates.pop();
                        } else {
                           done = true;
                        }
                    }
                    coordinates.push(coordinate.slice());
                    this.geometryFunction_(coordinates, geometry);
                   } else if (this.mode_ === ol.interaction.Draw.Mode_.POLYGON) {
                    coordinates = this.sketchCoords_[0];
                    if (coordinates.length >= this.maxPoints_) {
                      if (this.freehand_) {
                          coordinates.pop();
                      } else {
                          done = true;
                      }
                    }
                    coordinates.push(coordinate.slice());
                    if (done) {
                       this.finishCoordinate_ = coordinates[0];
                    }
                    this.geometryFunction_(this.sketchCoords_, geometry);
                  }
                  this.updateSketchFeatures_();
                  if (done) {
                     this.finishDrawing();
                  }
               };
            }
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(false);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(this.aide);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            DrawCreation.prototype.deactivate.apply(this, arguments);
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(true);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    /*
     * Methode privé: _getOLHandlerFromGeometryType
     * Retourne {<Descartes.Layer.POINT_GEOMETRY>} ou {<Descartes.Layer.LINE_GEOMETRY>} ou {<Descartes.Layer.POLYGON_GEOMETRY>}
     *
     * Paramètres:
     * geometryType - {<String>} un type de géométrie (point, line ou polygon).
     */
    _getOLHandlerFromGeometryType: function (geometryType) {
        var result = {};
        switch (geometryType) {
            case LayerConstants.POINT_GEOMETRY:
                result = {
                    type: 'Point',
                    multi: false
                };
                break;
            case LayerConstants.LINE_GEOMETRY:
                result = {
                    type: 'LineString',
                    multi: false
                };
                break;
            case LayerConstants.POLYGON_GEOMETRY:
                result = {
                    type: 'Polygon',
                    multi: false
                };
                break;
            case LayerConstants.MULTI_POINT_GEOMETRY:
                result = {
                    type: 'MultiPoint',
                    multi: true
                };
                break;
            case LayerConstants.MULTI_LINE_GEOMETRY:
                result = {
                    type: 'MultiLineString',
                    multi: true
                };
                break;
            case LayerConstants.MULTI_POLYGON_GEOMETRY:
                result = {
                    type: 'MultiPolygon',
                    multi: true
                };
                break;
            case AnnotationConstants.ANNOTATION_RECTANGLE_GEOMETRY:
                result = {
                    type: 'Circle',
                    multi: false
                };
                break;
            case AnnotationConstants.ANNOTATION_CIRCLE_GEOMETRY:
                result = {
                    type: 'Circle',
                    multi: false
                };
                break;
        }
        return result;
    },

    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer {<OpenLayers.Control.DrawFeature.prototype>}
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;
        var olHandler;
        if (this.editionLayer.type === EditionLayerConstants.TYPE_GenericVector || this.editionLayer.type === EditionLayerConstants.TYPE_Annotations) {
           if (this.geometryType) {
                olHandler = this._getOLHandlerFromGeometryType(this.geometryType);
           } else {
                olHandler = this._getOLHandlerFromGeometryType(LayerConstants.POLYGON_GEOMETRY);
           }
        } else {
            olHandler = this._getOLHandlerFromGeometryType(this.editionLayer.geometryType);
        }
        var temporaryTypes = Symbolizers.getOlStyle(this.editionLayer.symbolizers['temporary']);

        if (!_.isNil(this.olMap)) {
            if (!_.isNil(this.interaction)) {
                this.olMap.removeInteraction(this.interaction);
            }
            if (!_.isNil(this.holeInteraction)) {
                this.olMap.removeInteraction(this.holeInteraction);
            }
        }

        var geomType = LayerConstants.POLYGON_GEOMETRY;
        if (this.editionLayer.type === EditionLayerConstants.TYPE_Annotations) {
            if (this.geometryType && (this.geometryTyp === LayerConstants.POINT_GEOMETRY || this.geometryTyp === LayerConstants.LINE_GEOMETRY)) {
                geomType = this.geometryType;
            }
        }
        if (_.endsWith(geomType, 'Line')) {
            geomType += 'String';
        }
        var style = temporaryTypes[geomType];
        var optsDrawInteraction = {
            source: olLayer.getSource(),
            //geometryName: 'geom', //TO FIX
            type: olHandler.type,
            style: style
        };
        if (this.geometryType === AnnotationConstants.ANNOTATION_CIRCLE_GEOMETRY) {
            optsDrawInteraction.geometryFunction = ol.interaction.Draw.createRegularPolygon(100);
        }
        if (this.geometryType === AnnotationConstants.ANNOTATION_RECTANGLE_GEOMETRY) {
            optsDrawInteraction.geometryFunction = ol.interaction.Draw.createBox();
        }
        this.interaction = new ol.interaction.Draw(optsDrawInteraction);
        this.interaction.set('id', 'DrawCreation_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawstart', function (e) {
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this._drawing = true;
            }
        }.bind(this));
        this.interaction.on('drawend', function (e) {
            var feature = e.feature;
            if (this.drawFeature(feature) === false) {
               /* setTimeout(function () {
                    olLayer.getSource().removeFeature(feature);
                }, 100);*/
            }
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this._drawing = false;
                this.autotracingFeature = null;
                this.autotracingPreviewLine.getGeometry().setCoordinates([]);
                this.autotracingStartPoint = null;
            }
        }.bind(this));

        this.interaction.on('finishdrawing', function (e) {
            if (Descartes.EditionManager.autoSave && _.isNil(this.compositeFeature) && !this.editAttribut) {
                this.interaction.source_.clear();
            }
        }.bind(this));

        if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
            this.interaction.autotracingAppendCoordinates = function (coordinateExtension) {
                var mode = this.mode_;
                let coordinates = [];
                if (mode === ol.interaction.Draw.Mode_.LINE_STRING) {
                  coordinates = /** @type {LineCoordType} */ this.sketchCoords_;
                } else if (mode === ol.interaction.Draw.Mode_.POLYGON) {
                  coordinates = this.sketchCoords_ && this.sketchCoords_.length ? /** @type {PolyCoordType} */ (this.sketchCoords_)[0] : [];
                }

                // Remove last coordinate from sketch drawing (this coordinate follows cursor position)
                const ending = coordinates.pop();

                // Append coordinate list
                for (let i = 0; i < coordinateExtension.length; i++) {
                  this.autotracingAddToDrawing_(coordinateExtension[i]);
                }

                // Duplicate last coordinate for sketch drawing
                this.autotracingAddToDrawing_(ending);
            };
            this.interaction.autotracingAddToDrawing_ = function (coordinate) {
                  var geometry = /** @type {ol.geom.SimpleGeometry} */ (this.sketchFeature_.getGeometry());
                  var done;
                  var coordinates;
                  if (this.mode_ === ol.interaction.Draw.Mode_.LINE_STRING) {
                    this.finishCoordinate_ = coordinate.slice();
                    coordinates = this.sketchCoords_;
                    if (coordinates.length >= this.maxPoints_) {
                        if (this.freehand_) {
                           coordinates.pop();
                        } else {
                           done = true;
                        }
                    }
                    coordinates.push(coordinate.slice());
                    this.geometryFunction_(coordinates, geometry);
                   } else if (this.mode_ === ol.interaction.Draw.Mode_.POLYGON) {
                    coordinates = this.sketchCoords_[0];
                    if (coordinates.length >= this.maxPoints_) {
                      if (this.freehand_) {
                          coordinates.pop();
                      } else {
                          done = true;
                      }
                    }
                    coordinates.push(coordinate.slice());
                    if (done) {
                       this.finishCoordinate_ = coordinates[0];
                    }
                    this.geometryFunction_(this.sketchCoords_, geometry);
                  }
                  this.updateSketchFeatures_();
                  if (done) {
                     this.finishDrawing();
                  }
               };
        }

        this.holeInteraction = new ol.interaction.DrawHole({
            layers: [olLayer],
            style: new ol.style.Style({
                image: new ol.style.RegularShape({
                    fill: new ol.style.Fill({
                        color: 'red'
                    }),
                    points: 4,
                    radius1: 10,
                    radius2: 1
                }),
                fill: ol.style.Fill({
                    color: 'red'
                }),
                stroke: new ol.style.Stroke({
                    color: 'red'
                })
            })
        });
        this.holeInteraction.set('id', 'DrawHoleCreation_' + Utils.createUniqueID());
        this.holeInteraction.setActive(false);
        //pb avec l'interaction hole
        this.holeInteraction.un('drawend', this.holeInteraction._finishDrawing, this.holeInteraction);
        this.holeInteraction.on('drawstart', function (e) {
            this.holedFeature = e.target.getPolygon();
        }.bind(this));
        this.holeInteraction.on('drawend', this.onHoleDrawn.bind(this));


        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     * Methode privé: onHoleDrawn
     * permet de savoir quand l'utilisateur a dessiné un trou et de sauver la modification
     */
    onHoleDrawn: function (e) {
        var feature = e.feature;
        var geom = feature.getGeometry();

        var jstsGeom = this._parser.read(geom);
        var jstsHoledGeom = this._parser.read(this.holedFeature.getGeometry());
        var jstsResult = jstsHoledGeom.symDifference(jstsGeom);
        if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY && jstsResult instanceof jsts.geom.Polygon) {
            jstsResult = new jsts.geom.MultiPolygon([jstsResult], jstsResult.getFactory());
        }
        var result = this._parser.write(jstsResult);
        this.holedFeature.setGeometry(result);


        this.holedFeature.set('state', FeatureState.UPDATE);
        this.holedFeature.editionLayer = this.editionLayer;
        //this.setFeatureStyle(this.holedFeature, 'modify');
        /*if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(this.holedFeature);
            }
        }*/
        this.toggleHoleMode();
    },
    /**
     * Methode: saveObject
     * Enregistrement de l'objet créé.
     *
     * Paramètres:
     * feature - l'objet à sauvegarder.
     */
    saveObject: function (feature) {
        this.createObjectId = null;
        this.createAttributes = null;
        feature.editionLayer = this.editionLayer;
        feature.modified = false;
        var style = Utils.getStyleByState(feature);
        this.setFeatureStyle(feature, style);
        //mode sauvegarde manuel
        /*if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }*/
        var objectId = 'dObjectId_' + Math.random().toString(36).slice(2);
        feature.setId(objectId);
        feature.unset('state');
        feature.unset('selected');
        if (!feature.get("dAnnotationType")) {
            feature.set("dAnnotationType", "drawAnnotation");
        }
        this.setFeatureStyle(feature, "default");
        this.editionLayer.getAnnotationsFeatures().push(feature);
        this.editionLayer.refresh();
        if (!_.isNil(this.selectionEditionTool)) {
            this.selectionEditionTool.unselectAll();
        }
        return true;
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    formatButtonTitle: function (geometryType) {
        var title = this.getMessage("TITLE");
        if (title === "Créer une annotation") {
            switch (geometryType) {
                case LayerConstants.POINT_GEOMETRY:
                    title += this.getMessage("TITLE_POINT");
                    break;
                case LayerConstants.LINE_GEOMETRY:
                    title += this.getMessage("TITLE_LINE");
                    break;
                case LayerConstants.POLYGON_GEOMETRY:
                    title += this.getMessage("TITLE_POLYGON");
                    break;
                case AnnotationConstants.ANNOTATION_RECTANGLE_GEOMETRY:
                    title += this.getMessage("TITLE_RECTANGLE");
                    break;
                case AnnotationConstants.ANNOTATION_CIRCLE_GEOMETRY:
                    title += this.getMessage("TITLE_CIRCLE");
                    break;
            }
        }
        return title;
    },
    formatButtonAide: function (geometryType) {
        var aide = this.getMessage("AIDE");
        if (!aide) {
            aide = Descartes.Messages.Descartes_EDITION_NOT_AIDE;
        } else if (aide === "Créer une annotation") {
            switch (geometryType) {
                case LayerConstants.POINT_GEOMETRY:
                    aide += this.getMessage("AIDE_POINT");
                    break;
                case LayerConstants.LINE_GEOMETRY:
                    aide += this.getMessage("AIDE_LINE");
                    break;
                case LayerConstants.POLYGON_GEOMETRY:
                    aide += this.getMessage("AIDE_POLYGON");
                    break;
                case AnnotationConstants.ANNOTATION_RECTANGLE_GEOMETRY:
                    aide += this.getMessage("AIDE_RECTANGLE");
                    break;
                case AnnotationConstants.ANNOTATION_CIRCLE_GEOMETRY:
                    aide += this.getMessage("AIDE_CIRCLE");
                    break;
            }
        }
        return aide;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.DrawAnnotation'
});
module.exports = Class;

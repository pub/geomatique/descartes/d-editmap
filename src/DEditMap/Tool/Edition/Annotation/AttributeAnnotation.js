/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;
var Attribute = require('../Attribute/Attribute');
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var AttributesEditor = require('../../../Action/AttributesEditor');

var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');

require('../css/attributeAnnotation.css');

/**
 * Class: Descartes.Tool.Edition.AttributeAnnotation
 * Outil de saisie des attributs.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Attribute>
 */
var Class = Utils.Class(Attribute, {

    /**
     * Constructeur: Descartes.Tool.Edition.AttributeAnnotation
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * toolOptions - {Object} Objet JSON stockant les options d'utilisation de l'outil.
     */
    initialize: function (options) {
        Attribute.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            //pour rechargement via context
            this.olMap.removeInteraction(this.interaction);
            this.interaction = null;
            this._OLToolInitialize = false;
            this.editionLayer = Descartes.AnnotationsLayer;
            Attribute.prototype.activate.apply(this, arguments);
            this.olMap.addInteraction(this.interaction);
            this.interaction.setActive(true);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Attribute.prototype.deactivate.apply(this, arguments);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     * Affichage de la boite de dialogue pour la saisie des attributs.
     *
     * Paramètres:
     * event - l'événement de sélection l'objet sélectionné.
     */
    onSelect: function (event) {
        var features = event.selected;
        if (features.length > 0) {
            var feature = features[0];
            feature._origStyle = feature.getStyle();
            if (!this.isConforme(feature.getGeometry())) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                this.resetNonConformModif(feature);
                return false;
            }

            if (this.isActiveVerifTopoConformity() &&
                    !this.isConformeTopology(feature.getGeometry(), true)) {
                this.resetNonConformModif(feature);
                return false;
            }

            if (this.isActiveVerifAppConformity() &&
                    !this.isConformeRulesApp(feature, true)) {
                this.resetNonConformModif(feature);
                return false;
            }

            this.setFeatureStyle(feature, 'select');

            var featureState = feature.get('state');
            if (featureState !== FeatureState.DELETE) {

                // Creation de la vue
                var viewOptions = {
                    view: AttributesEditorDialog,
                    feature: feature,
                    editionLayer: this.editionLayer
                };

                // Creation de l'action qui va ouvrir la vue
                var action = new AttributesEditor(null, this.map, viewOptions);
                action.events.register('saveObject', this, this.saveObject);
                action.events.register('cancel', this, this.cancel);
                return true;
            }
        }
        return false;
    },
    /**
     * Methode: saveObject
     * Enregistrement des attributs saisis.
     *
     * Listener appelé lorsque l'utilisateur clic sur OK.
     * Fait les modifications sur l'objet et sauvegarde les modification en sauvegarde automatique.
     * Sinon change l'aspect de l'objet
     *
     * Paramètres:
     * event - Evénement Descartes contenant l'objet à sauvegarder.
     */
    saveObject: function (event) {
        var feature = event.data[0];
        /*var featureState = feature.get('state');
        if (featureState !== FeatureState.INSERT) {
            feature.set('state', FeatureState.UPDATE);
        }*/

        feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                     feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];

        olLayer.getSource().dispatchEvent('changefeature', {
            feature: feature,
            modified: true
        });
        //Sauvegarde en mode manuel.
        /*if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            } else {
                log.warn('Strategy de sauvegarde non affectée. Sauvegarde impossible');
            }
        } else {
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été modifiée
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.create);
            var olStyle = olStyles[feature.getGeometry().getType()];
            feature.setStyle(olStyle);
        }*/

        feature.unset('state');
        feature.unset('selected');
        for (var i = 0; i < this.editionLayer.annotationsFeatures.length; i++) {
           if (this.editionLayer.annotationsFeatures[i].getId() === feature.getId()) {
               var properties = _.clone(feature.getProperties());
               this.editionLayer.annotationsFeatures[i].setProperties(properties);
               this.editionLayer.annotationsFeatures[i].setStyle(feature._origStyle);
               delete this.editionLayer.annotationsFeatures[i]._origStyle;
               if (this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAnnotation" || this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAndTexteAnnotation") {
                  this.editionLayer.refreshFeatureStyle(this.editionLayer.annotationsFeatures[i]);
               }
               break;
           }
        }

        this.unselectAll();
    },
    /**
     * Methode: cancel
     * Annulation des attributs saisies.
     *
     */
    cancel: function (event) {
        this.unselectAll();
        var feature = event.data[0];
        for (var i = 0; i < this.editionLayer.annotationsFeatures.length; i++) {
           if (this.editionLayer.annotationsFeatures[i].getId() === feature.getId()) {
               this.editionLayer.annotationsFeatures[i].setStyle(feature._origStyle);
               delete this.editionLayer.annotationsFeatures[i]._origStyle;
               if (this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAnnotation" || this.editionLayer.annotationsFeatures[i].get("dAnnotationType") === "arrowAndTexteAnnotation") {
                  this.editionLayer.refreshFeatureStyle(this.editionLayer.annotationsFeatures[i]);
               }
               break;
           }
        }
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        this.editionLayer.unselectAll();
        /*this.interaction.getFeatures().forEach(function (feature) {
            var style = feature._origStyle;
            this.setFeatureStyle(feature, style);
        }, this);*/
        this.interaction.getFeatures().clear();
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.AttributeAnnotation'
});

module.exports = Class;

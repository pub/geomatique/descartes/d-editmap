/* global MODE, Descartes */

var $ = require('jquery');
var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');
var Symbolizers = require('../../../Symbolizers');

var ModalFormDialog = Descartes.UI.ModalFormDialog;

require('../css/geolocationTrackingAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Creation.GeolocationTrackingAnnotation
 * Outil de création par dessin.
 *
 * Hérite de
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    initialize: function () {

        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

    },

    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;

            this.active = true;

            this.geolocation = new ol.Geolocation({
              trackingOptions: {
                  enableHighAccuracy: true
              },
              projection: this.olMap.getView().getProjection()
            });

            this.geolocation.setTracking(true);

            var that = this;
            this.geolocation.on('change:position', function () {
                var coordinates = that.geolocation.getPosition();
                var positionFeature = new ol.Feature();
                positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
                var objectId = 'dObjectId_' + Math.random().toString(36).slice(2);
                positionFeature.setId(objectId);
                positionFeature.set("dAnnotationType", "drawAnnotation");
                var precision = Utils.getProjectionPrecision("EPSG:4326-DMS");
                var lonlat = ol.proj.transform(coordinates, that.olMap.getView().getProjection(), "EPSG:4326");
                var displayCoordinatesText = Utils.coordinateToStringHDMS(lonlat, precision, " - ");
                positionFeature.set("nom", "Ma position");
                positionFeature.set("description", displayCoordinatesText + " (Générer automatiquement)");
                that.setFeatureStyle(positionFeature, "default");
                that.editionLayer.getAnnotationsFeatures().push(positionFeature);
                that.editionLayer.refresh();
                that.olMap.getView().setCenter(positionFeature.getGeometry().getCoordinates());

            });

            Descartes._activeClickToolTip = false;

        }
        return true;
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Edition.prototype.deactivate.apply(this, arguments);
            this.active = false;
            this.updateElement();

            this.geolocation.setTracking(false);

            Descartes._activeClickToolTip = true;

        }
        return true;
    },

    CLASS_NAME: 'Descartes.Tool.Edition.Creation.GeolocationTrackingAnnotation'
});
module.exports = Class;

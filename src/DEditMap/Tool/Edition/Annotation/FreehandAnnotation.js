/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Symbolizers = require('../../../Symbolizers');
var DrawAnnotation = require('./DrawAnnotation');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../../../Model/EditionLayerConstants');
var AnnotationConstants = require('./AnnotationConstants');

require('../css/freehandAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Creation.FreehandAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation.DrawCreation>
 */
var Class = Utils.Class(DrawAnnotation, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        arguments[0].geometryType = Descartes.Layer.LINE_GEOMETRY;
        DrawAnnotation.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            DrawAnnotation.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(false);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            DrawAnnotation.prototype.deactivate.apply(this, arguments);
            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(true);
              }
            });
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer {<OpenLayers.Control.DrawFeature.prototype>}
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;
        var olHandler;
        if (this.editionLayer.type === EditionLayerConstants.TYPE_GenericVector || this.editionLayer.type === EditionLayerConstants.TYPE_Annotations) {
           if (this.geometryType) {
                olHandler = this._getOLHandlerFromGeometryType(this.geometryType);
           } else {
                olHandler = this._getOLHandlerFromGeometryType(LayerConstants.POLYGON_GEOMETRY);
           }
        } else {
            olHandler = this._getOLHandlerFromGeometryType(this.editionLayer.geometryType);
        }
        var temporaryTypes = Symbolizers.getOlStyle(this.editionLayer.symbolizers['temporary']);

        if (!_.isNil(this.olMap)) {
            if (!_.isNil(this.interaction)) {
                this.olMap.removeInteraction(this.interaction);
            }
            if (!_.isNil(this.holeInteraction)) {
                this.olMap.removeInteraction(this.holeInteraction);
            }
        }

        var geomType = LayerConstants.POLYGON_GEOMETRY;
        if (this.editionLayer.type === EditionLayerConstants.TYPE_Annotations) {
            if (this.geometryType && (this.geometryTyp === LayerConstants.POINT_GEOMETRY || this.geometryTyp === LayerConstants.LINE_GEOMETRY)) {
                geomType = this.geometryType;
            }
        }
        if (_.endsWith(geomType, 'Line')) {
            geomType += 'String';
        }
        var style = temporaryTypes[geomType];
        var optsDrawInteraction = {
            source: olLayer.getSource(),
            //geometryName: 'geom', //TO FIX
            type: olHandler.type,
            style: style,
            freehand: true
        };
        if (this.geometryType === AnnotationConstants.ANNOTATION_CIRCLE_GEOMETRY) {
            optsDrawInteraction.geometryFunction = ol.interaction.Draw.createRegularPolygon(100);
        }
        if (this.geometryType === AnnotationConstants.ANNOTATION_RECTANGLE_GEOMETRY) {
            optsDrawInteraction.geometryFunction = ol.interaction.Draw.createBox();
        }
        this.interaction = new ol.interaction.Draw(optsDrawInteraction);
        this.interaction.set('id', 'DrawCreation_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawstart', function (e) {
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this._drawing = true;
            }
        }.bind(this));
        this.interaction.on('drawend', function (e) {
            var feature = e.feature;
            if (this.drawFeature(feature) === false) {
                setTimeout(function () {
                    olLayer.getSource().removeFeature(feature);
                }, 100);
            }
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this._drawing = false;
                this.autotracingFeature = null;
                this.autotracingPreviewLine.getGeometry().setCoordinates([]);
                this.autotracingStartPoint = null;
            }
        }.bind(this));

        this.interaction.on('finishdrawing', function (e) {
            if (Descartes.EditionManager.autoSave && _.isNil(this.compositeFeature) && !this.editAttribut) {
                this.interaction.source_.clear();
            }
        }.bind(this));

        if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
            this.interaction.autotracingAppendCoordinates = function (coordinateExtension) {
                var mode = this.mode_;
                let coordinates = [];
                if (mode === ol.interaction.Draw.Mode_.LINE_STRING) {
                  coordinates = /** @type {LineCoordType} */ this.sketchCoords_;
                } else if (mode === ol.interaction.Draw.Mode_.POLYGON) {
                  coordinates = this.sketchCoords_ && this.sketchCoords_.length ? /** @type {PolyCoordType} */ (this.sketchCoords_)[0] : [];
                }

                // Remove last coordinate from sketch drawing (this coordinate follows cursor position)
                const ending = coordinates.pop();

                // Append coordinate list
                for (let i = 0; i < coordinateExtension.length; i++) {
                  this.autotracingAddToDrawing_(coordinateExtension[i]);
                }

                // Duplicate last coordinate for sketch drawing
                this.autotracingAddToDrawing_(ending);
            };
            this.interaction.autotracingAddToDrawing_ = function (coordinate) {
                  var geometry = /** @type {ol.geom.SimpleGeometry} */ (this.sketchFeature_.getGeometry());
                  var done;
                  var coordinates;
                  if (this.mode_ === ol.interaction.Draw.Mode_.LINE_STRING) {
                    this.finishCoordinate_ = coordinate.slice();
                    coordinates = this.sketchCoords_;
                    if (coordinates.length >= this.maxPoints_) {
                        if (this.freehand_) {
                           coordinates.pop();
                        } else {
                           done = true;
                        }
                    }
                    coordinates.push(coordinate.slice());
                    this.geometryFunction_(coordinates, geometry);
                   } else if (this.mode_ === ol.interaction.Draw.Mode_.POLYGON) {
                    coordinates = this.sketchCoords_[0];
                    if (coordinates.length >= this.maxPoints_) {
                      if (this.freehand_) {
                          coordinates.pop();
                      } else {
                          done = true;
                      }
                    }
                    coordinates.push(coordinate.slice());
                    if (done) {
                       this.finishCoordinate_ = coordinates[0];
                    }
                    this.geometryFunction_(this.sketchCoords_, geometry);
                  }
                  this.updateSketchFeatures_();
                  if (done) {
                     this.finishDrawing();
                  }
               };
        }

        this.holeInteraction = new ol.interaction.DrawHole({
            layers: [olLayer],
            style: new ol.style.Style({
                image: new ol.style.RegularShape({
                    fill: new ol.style.Fill({
                        color: 'red'
                    }),
                    points: 4,
                    radius1: 10,
                    radius2: 1
                }),
                fill: ol.style.Fill({
                    color: 'red'
                }),
                stroke: new ol.style.Stroke({
                    color: 'red'
                })
            })
        });
        this.holeInteraction.set('id', 'DrawHoleCreation_' + Utils.createUniqueID());
        this.holeInteraction.setActive(false);
        //pb avec l'interaction hole
        this.holeInteraction.un('drawend', this.holeInteraction._finishDrawing, this.holeInteraction);
        this.holeInteraction.on('drawstart', function (e) {
            this.holedFeature = e.target.getPolygon();
        }.bind(this));
        this.holeInteraction.on('drawend', this.onHoleDrawn.bind(this));


        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    isConforme: function (geometry) {
        return true;
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.FreehandAnnotation'
});
module.exports = Class;

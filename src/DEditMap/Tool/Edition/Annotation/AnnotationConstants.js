
/**
 * Constantes utilisées pour les outils d'édition.
 */
module.exports = {
    /**
     * Constante: ANNOTATION_RECTANGLE_GEOMETRY
     * Géométrie surfacique.
     */
    ANNOTATION_RECTANGLE_GEOMETRY: 'Rectangle',
    /**
     * Constante: ANNOTATION_CIRCLE_GEOMETRY
     * Géométrie surfacique.
     */
    ANNOTATION_CIRCLE_GEOMETRY: 'Circle'
};

/* global MODE, Descartes */

var $ = require('jquery');
var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');

var ModalFormDialog = Descartes.UI.ModalFormDialog;
var template = require('./templates/ImportAnnotations.ejs');

require('../css/importAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.ImportAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {
	listImportFormat: [
       {
          id: "dcarto", label: 'JSON (format spécifique)', olClass: null
       },
       {
          id: "kml", label: 'KML', olClass: ol.format.KML
       },
       {
          id: "geojson", label: 'GeoJson', olClass: ol.format.GeoJSON
       },
       {
          id: "gpx", label: 'GPX', olClass: ol.format.GPX
       }
    ],
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.active = true;
            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.openImportDialog();
            this.deactivate();
            if (Descartes.AnnotationButtonAide) {
                Descartes.AnnotationButtonAide.html(this.aide);
            }
		}
    },

    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
           this.active = false;
           Edition.prototype.deactivate.apply(this, arguments);
        }
    },
    openImportDialog: function () {

        var content = template({
            labelText: this.getMessage('FORMAT'),
            listImportFormat: this.listImportFormat
        });
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_DIALOG'),
            content: content
        });
        dialog.open($.proxy(this.importerAnnotations, this), $.proxy(this.close, this));

       var that = this;
       $('#fileImportAnnotation')[0].accept = "." + this.listImportFormat[0].id;
       $('#importformat').change(function () {
       $('#fileImportAnnotation')[0].accept = "." + this.value;
       });
       $('#fileImportAnnotation').change(function () {
          that.selectedFile = this.files[0];
       });
    },

    importerAnnotations: function (form) {
        var keepAnnotations = $("#DescartesKeepAnnotationsCheckBox")[0].checked;
        var selectedFormat = form.importformat;
        var Classname = null;
        var reader = new FileReader();
        var that = this;
        reader.addEventListener("loadend", () => {
          for (var i = 0, len = that.listImportFormat.length; i < len; i++) {
             if (selectedFormat === that.listImportFormat[i].id) {
                Classname = that.listImportFormat[i].olClass;
                break;
             }
          }
          var features;
          var data = reader.result;
          if (Classname) {
               var optionsFormat = {};
               if (selectedFormat === "kml") {
                   optionsFormat = {
                       extractStyles: true,
                       showPointNames: true
                       //defaultStyle: [],
                       //defaultDataProjection: 'EPSG:4326' //flux kml toujours en EPSG:4326
                   };
               }
               try {
                 features = new Classname(optionsFormat).readFeatures(data, {
                   dataProjection: 'EPSG:4326',
                   featureProjection: that.olMap.getView().getProjection().getCode()
                 });
                 for (var j = 0, jlen = features.length; j < jlen; j++) {
                     if (_.isNil(features[j].get("dAnnotationType"))) {
                         features[j].set("dAnnotationType", "importAnnotation");
                     }
                 }
                 if (keepAnnotations) {
                     this.editionLayer.addAnnotationsFeatures(features);
                 } else {
                     this.editionLayer.setAnnotationsFeatures(features);
                 }
              } catch (e) {
                 alert("ERREUR LORS DE L'IMPORT " + e);
              }
          } else {
              try {
                 var annotationsSerialized = JSON.parse(data);
                 annotationsSerialized.forEach(function (annot) {
                     var geom;
                     if (annot.geometry && annot.geometry.type === "Point") {
                         geom = new ol.geom.Point(annot.geometry.coordinates);
                     } else if (annot.geometry && annot.geometry.type === "LineString") {
                         geom = new ol.geom.LineString(annot.geometry.coordinates);
                     } else if (annot.geometry && annot.geometry.type === "Polygon") {
                         geom = new ol.geom.Polygon(annot.geometry.coordinates);
                     }
                     geom.transform('EPSG:4326', that.olMap.getView().getProjection().getCode());
                     annot.geometry.coordinates = geom.getCoordinates();
                 });
                 this.editionLayer.setAnnotationsFeaturesSerialized(annotationsSerialized, keepAnnotations);
              } catch (e) {
                 alert("ERREUR LORS DE L'IMPORT " + e);
              }
          }

          this.editionLayer.refresh();
        });
        reader.addEventListener("error", () => {
           alert("ERREUR: impossible de lire le fichier");
        });
        if (this.selectedFile) {
            reader.readAsText(this.selectedFile);
        } else {
            alert("ERREUR: aucun fichier sélectionné");
        }

        this.close();

    },
    close: function () {
        if (Descartes.AnnotationButtonAide) {
            Descartes.AnnotationButtonAide.html(Descartes.Messages.Descartes_Messages_Tool_Edition_AideAnnotation.TEXTE_DIALOG);
        }
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.ImportAnnotation'
});
module.exports = Class;

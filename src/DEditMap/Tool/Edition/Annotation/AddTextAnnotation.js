/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../../Edition');


require('../css/addTextAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Modification.AddTextAnnotation
 * Outil de création par dessin.
 *
 * Hérite de
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    hitTolerance: 5,
    interaction: null,
    multiple: false,

    initialize: function () {
        this.interaction = new ol.interaction.Select({
            toggleCondition: ol.events.condition.singleClick,
            hitTolerance: this.hitTolerance,
            layers: function (layer) {
                if (!_.isNil(this.editionLayer)) {
                    for (var i = 0; i < this.editionLayer.OL_layers.length; i++) {
                        if (this.editionLayer.OL_layers[i] === layer) {
                            return true;
                        }
                    }
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'SelectionInteraction');
        this.interaction.on('select', this.toggleFeatureSelected.bind(this));
        this.interaction.setActive(false);
        Edition.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;
    },
    toggleFeatureSelected: function (event) {

        if (!this.multiple) {
            event.deselected = this.editionLayer.getSelectedFeatures();
        }

        _.each(event.deselected, function (feature) {
            //feature.set('selected', false);
           // var style = Utils.getStyleByState(feature);
            //this.setFeatureStyle(feature, style);
            if (feature.get("dAnnotationType")) {
               feature.setStyle(this._origStyle);
               if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                  this.editionLayer.refreshFeatureStyle(feature);
               }
            }

            if (!this.multiple) {
                this.interaction.getFeatures().remove(feature);
            }
        }.bind(this));

        _.each(event.selected, function (feature) {
            //feature.set('selected', true);
            this._origStyle = feature.getStyle();
            //this.setFeatureStyle(feature, 'select');
            this.afficherPrompt(feature);
        }.bind(this));
    },

    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Edition.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.underActivation = false;
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Edition.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },

	afficherPrompt: function (feature) {
        var initLabel = "";
        if (feature.getStyle() instanceof Function) {
           alert(this.getMessage("FEATURE_STYLE_ERROR"));
           return;
        }
        if (feature.get("labelTextAnnotion")) {
           initLabel = feature.get("labelTextAnnotion");
        }
		var label = prompt(this.getMessage("PROMPT_MESSAGE"), initLabel);
		this.callbackPrompt(this, feature, label);
	},

	callbackPrompt: function (instance, feature, label) {

        var style;
		if (label !== null && label !== "") {
			if (feature.get("dAnnotationType") === "arrowAnnotation") {
				feature.set("dAnnotationType", "arrowAndTexteAnnotation");
			} else {
				feature.set("dAnnotationType", "drawAndTexteAnnotation");
                feature.dAnnotationType = "drawAndTexteAnnotation";
			}
			feature.set("labelTextAnnotion", label);

            //this.setFeatureStyle(feature, "default");
            if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
               this.editionLayer.refreshFeatureStyle(feature);
            }
            style = feature.getStyle();
            if (_.isNil(style)) {
                this.setFeatureStyle(feature, "default");
                style = feature.getStyle();
            }
            if (!(style instanceof ol.style.Style) && !(style instanceof Array)) {
                style = new ol.style.Style();
            }
            var textStyle = this.editionLayer.getTextStyle(feature).getText();
            if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation" || style instanceof Array) {
                style[0].setText(textStyle);
            } else{
                style.setText(textStyle);
            }

            feature.setStyle(style);
            this.interaction.getFeatures().remove(feature);
		} else {
            if (label === "" && feature.get("labelTextAnnotion")) {
               if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
					feature.set("dAnnotationType", "arrowAnnotation");
               } else {
					feature.set("dAnnotationType", "drawAnnotation");
                    feature.dAnnotationType = "drawAnnotation";
               }
               feature.unset("labelTextAnnotion");
               if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation") {
                    this.editionLayer.refreshFeatureStyle(feature);
               }
               style = feature.getStyle();
               if (_.isNil(style)) {
                   this.setFeatureStyle(feature, "default");
                   style = feature.getStyle();
               }
               if (!(style instanceof ol.style.Style) && !(style instanceof Array)) {
                    style = new ol.style.Style();
               }
               if (feature.get("dAnnotationType") === "arrowAndTexteAnnotation" || style instanceof Array) {
                    style[0].setText(null);
               } else{
                    style.setText(null);
               }
               feature.setStyle(style);
               this.interaction.getFeatures().remove(feature);
            }
			//feature.feature.destroy();
		}

		//instance.dLayer.OL_layer.redraw();
	},
	changeCursorPointerOnFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.editionLayer.getFeatureOL_layers()[0];
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.AddTextAnnotation'
});
module.exports = Class;

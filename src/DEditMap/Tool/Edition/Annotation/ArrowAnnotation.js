/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Symbolizers = require('../../../Symbolizers');
var DrawAnnotation = require('./DrawAnnotation');
var Creation = require('../Creation');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../../../Model/EditionLayerConstants');
var AnnotationConstants = require('./AnnotationConstants');

require('../css/arrowAnnotation.css');


/**
 * Class: Descartes.Tool.Edition.Creation.ArrowAnnotation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation.DrawCreation>
 */
var Class = Utils.Class(DrawAnnotation, {

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        arguments[0].geometryType = Descartes.Layer.LINE_GEOMETRY;
        DrawAnnotation.prototype.initialize.apply(this, arguments);
        this.editionLayer = Descartes.AnnotationsLayer;

    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            DrawAnnotation.prototype.activate.apply(this, arguments);
            this.editionLayer = Descartes.AnnotationsLayer;
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(false);
            }
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            DrawAnnotation.prototype.deactivate.apply(this, arguments);
            if (this.dmap && this.dmap.selectToolTip && this.dmap.selectToolTip.selectInteraction) {
               this.dmap.selectToolTip.selectInteraction.setActive(true);
            }
        }
    },
    setFeatureStyle: function (feature, type) {
        var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['default']);
        var olStyle = olStyles[feature.getGeometry().getType()];
        feature.setStyle(olStyle);
        feature.dAnnotationType = "arrowAnnotation";
        feature.set("dAnnotationType", "arrowAnnotation");
        this.editionLayer.refreshFeatureStyle(feature);
    },
    initAnnotationTool: function (dmap) {
        this.dmap = dmap;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.ArrowAnnotation'
});
module.exports = Class;

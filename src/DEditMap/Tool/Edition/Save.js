/* global MODE, Descartes */
var Utils = Descartes.Utils;
var Edition = require('../' + MODE + '/Edition');
var EditionLayer = require('../../Model/EditionLayer');

require('./css/save.css');

/**
 * Class: Descartes.Tool.Edition.Save
 * Outil de sauvegarde.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /**
     * c'est outil n'est pas actif ou non (bouton enfoncé ou pas)
     * Son état est lié uniquement à l'état disponible (available)
     */
    active: false,

    /*
     * Méthode: updateStateWithLayer
     * Met à jour l'état de l'outil en fonction des changements de couche en cours d'édition.
     * S'abonne au changement de selection du layer en cours d'édition et se désabonne si nécessaire.
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (editionLayer instanceof EditionLayer) {
            if (this.editionLayer !== editionLayer) {
                if (this.editionLayer !== null) {
                    this.unregisterToEdition(this.editionLayer);

                    if (this.editionLayer.isDirty() && !Descartes.EditionManager.isGlobalEditonMode()) {
                        var confirm = function () {
                            var featuresToSave = this.editionLayer.getFeaturesToSave();
                            this.editionLayer.getSaveStrategy().save(featuresToSave);
                            this.editionLayer.cancelChange();
                        }.bind(this);

                        var lose = function () {
                            this.editionLayer.cancelChange();
                            this.editionLayer.refresh();
                        }.bind(this);

                        this.editionLayer.confirmOrLoseModification(confirm, lose);
                    }
                }
                this.registerToEdition(editionLayer);
            }
            //Cet outil n'est pas dépendant d'une couche en particulier.
            //il faut donc forcé à mettre à jour la couche même en mode individuel.
            this.editionLayer = editionLayer;
            this.refresh();
        } else if (this.editionLayer !== null && Descartes.EditionManager.isGlobalEditonMode()) {
            this.unregisterToEdition(this.editionLayer);
            this.editionLayer = null;
        }
    },
    /*
     * Méthode: registerToEdition
     * Abonne l'outil au changement  dans la couche passée en paramètre.
     *
     * Paramètres:
     * editionLayer : Couche editionLayer
     */
    registerToEdition: function (editionLayer) {
        if (!Descartes.EditionManager.autoSave) {
            var olLayer = editionLayer.getFeatureOL_layers()[0];

            olLayer.getSource().on('addfeature', this.refresh, this);
            olLayer.getSource().on('removefeature', this.refresh, this);
            olLayer.getSource().on('changefeature', this.refresh, this);
            editionLayer.getSaveStrategy().events.register('success', this, this.refresh);
        }
    },
    /*
     * Méthode: unregisterToEdition
     * Désabonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    unregisterToEdition: function (editionLayer) {
        if (!Descartes.EditionManager.autoSave) {
            var olLayer = editionLayer.getFeatureOL_layers()[0];

            olLayer.getSource().un('addfeature', this.refresh, this);
            olLayer.getSource().un('removefeature', this.refresh, this);
            olLayer.getSource().un('changefeature', this.refresh, this);
            editionLayer.getSaveStrategy().events.unregister('success', this, this.refresh);
        }
    },
    /**
     * Methode: refresh
     * Met à jour l'état de l'outil et l'info-bulle en fonction du nombre d'éléments à sauvegarder.
     */
    refresh: function () {
        var defaultTitle = this.getMessage('TITLE');
        if (this.editionLayer) {
            var nbFeaturesToSave = this.editionLayer.getFeaturesToSave().length;
            var available = !Descartes.EditionManager.autoSave &&
                    this.editionLayer.isDirty() &&
                    nbFeaturesToSave > 0;
            this.setAvailable(available);
            this.title = nbFeaturesToSave + ' élements à sauvegarder';
            this.setToolTip();
            this.updateElement();
        } else if (this.title !== defaultTitle) {
            this.title = defaultTitle;
            this.updateElement();
        }
    },
    /**
     * Methode: activate
     * Active l'outil lorsque l'utilisateur clic sur le bouton.
     */
    activate: function () {
        if (this.isAvailable()) {
            var featuresToSave = this.editionLayer.getFeaturesToSave();
            this.editionLayer.getSaveStrategy().save(featuresToSave);
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Save'
});

module.exports = Class;

/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../' + MODE + '/Edition');
var Messages = require('../../Messages');
var FeatureState = require('../../Model/FeatureState');

var AttributesEditor = require('../../Action/AttributesEditor');
var MapPreview = require('../../Action/MapPreview');
var AttributesEditorDialog = require('../../UI/' + MODE + '/AttributesEditorDialog');
var MapPreviewDialog = require('../../UI/' + MODE + '/MapPreviewDialog');
var Symbolizers = require('../../Symbolizers');

require('./css/creation.css');

/**
 * Class: Descartes.Tool.Edition.Creation
 * Classe "abstraite" pour les outils d'édition de création
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {
    /**
     * Propriete privée
     * Layer temporaire
     */
    _tempLayer: null,
    /**
     * Propriete privée _explodeMultiGeometryInTempLayer
     * Indique si les géométries multiple doivent être transformées en géométrie simple
     * dans le layer temporaire
     */
    _explodeMultiGeometryInTempLayer: false,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation
     * Constructeur d'instances
     */
    initialize: function () {
        Edition.prototype.initialize.apply(this, arguments);
    },
    changeCursorPointerOnTempFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    /**
     * Méthode privée _activateSupportLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et ajoute l'initialisation du layer temporaire.
     */
    _activateSupportLayers: function () {
        Edition.prototype._activateSupportLayers.apply(this, arguments);
        this._initializeSupportTempLayer(arguments);
    },
    /**
     * Méthode privée _deactivateSupportLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et supprime le layer temporaire
     */
    _deactivateSupportLayers: function () {
        Edition.prototype._deactivateSupportLayers.apply(this, arguments);
        if (this._tempLayer !== null) {
            this.olMap.removeLayer(this._tempLayer);
            this._tempLayer = null;
        }
    },
    /**
     * Méthode privée _updateSupportsLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et ajoute l'initialisation du layer temporaire.
     */
    _updateSupportsLayers: function () {
        if (this.isAvailable() && this.isActive()) {
            Edition.prototype._updateSupportsLayers.apply(this, arguments);
            this._initializeSupportTempLayer();
        }
    },
    /**
     * Méthode privée _initializeSupportTempLayer
     * Initialize et ajoute la couche temporaire en fonction des couches supports
     * sélectionnés et du filtre sur les features facultatif.
     *
     * ATTENTION si modification : peut être surchargée ailleurs (BufferCreation par ex)
     */
    _initializeSupportTempLayer: function () {
        if (this.olMap && this.isActive()) {
            var that = this;
            var charged = true;
            //récupération de toutes les features
            var features = [];
            for (var i = 0; i < this._supportLayers.length; i++) {
                var supportLayer = this._supportLayers[i];
                var featureToAddForSupportLayer = [];

                // Si une couche a été chargé après que la couche temporaire ait été créée
                // il faut ajouter les features à la fin du chargement.
                if (this.mapContent.getLayerByOLLayer(supportLayer) !== this.editionLayer) {
                    if (!_.isNil(supportLayer.charged) && supportLayer.charged === false) {
                        charged = false;
                    }

                    supportLayer.getSource().on('featureloadend', function (event) {
                        if (this._tempLayer) {
                            var newFeatures = event.target.getFeatures();
                            if (!_.isNil(newFeatures)) {
                                var result = [];
                                for (var j = 0; j < newFeatures.length; j++) {
                                    var aFeature = newFeatures[j];

                                    var featuresToAdd = that._filterFeatureForTempLayer(aFeature, this.featureFilter);
                                    if (this.addAttributesToSupportFeatures) {
                                        this.addAttributesToSupportFeatures(featuresToAdd, supportLayer);
                                    }
                                    result = result.concat(featuresToAdd);
                                }
                                var descartesLayer;
                                if (result.length === 0) {
                                    //griser le bouton de support de l'arbre des couches
                                    descartesLayer = this.mapContent.getLayerByOLLayer(supportLayer);
                                    descartesLayer.supportEnable = false;
                                    this.mapContent.refreshOnlyContentManager();
                                } else {
                                    descartesLayer = this.mapContent.getLayerByOLLayer(supportLayer);
                                    descartesLayer.supportEnable = true;
                                    this.mapContent.refreshOnlyContentManager();
                                    this._tempLayer.getSource().addFeatures(result);
                                }
                                supportLayer.charged = true;
                            }
                        }
                    }.bind(this), this);

                }

                var supportFeatures = supportLayer.getSource().getFeatures();
                for (var j = 0; j < supportFeatures.length; j++) {
                    var feature = supportFeatures[j];
                    var featuresToAdd = this._filterFeatureForTempLayer(feature, this.featureFilter);
                    if (this.addAttributesToSupportFeatures) {
                        this.addAttributesToSupportFeatures(featuresToAdd, supportLayer);
                    }
                    featureToAddForSupportLayer = featureToAddForSupportLayer.concat(featuresToAdd);
                }
                if (featureToAddForSupportLayer.length === 0) {
                    //griser le bouton de support de l'arbre des couches.
                    var descartesLayer = this.mapContent.getLayerByOLLayer(supportLayer);
                    if (charged) {
                        descartesLayer.supportEnable = false;
                    }
                    this.mapContent.refreshOnlyContentManager();
                }
                features = features.concat(featureToAddForSupportLayer);
            }

            //creation d'une couche temporaire
            if (_.isNil(this._tempLayer)) {
                this._tempLayer = new ol.layer.Vector({
                    title: 'creationTemp',
                    source: new ol.source.Vector()
                });
                this.olMap.addLayer(this._tempLayer);

            } else {
                this._tempLayer.getSource().clear();
            }
            if (charged && features.length === 0) {
                alert(Messages.Descartes_EDITION_NOT_AVAILABLE_GEOMETRY);
                this.deactivate();
            } else {
                this._tempLayer.getSource().addFeatures(features);
            }
        }
    },
    /**
     * Méthode _filterFeatureForTempLayer
     * Retourne l'ensemble des features eligibles à la couche temporaire.
     *
     */
    _filterFeatureForTempLayer: function (feature, featureFilter) {
        //application du filtre sur une feature
        var result = [];
        if (_.isFunction(featureFilter)) {
            if (!featureFilter.apply(this, [feature])) {
                return result;
            }
        }

        if (feature.state !== FeatureState.DELETE) {
            if (this._explodeMultiGeometryInTempLayer === true && this.isMultipleGeometry(feature)) {
                var subGeoms = Utils.getSubGeom(feature.getGeometry());

                for (var k = 0; k < subGeoms.length; k++) {
                    var subGeom = subGeoms[k];
                    var clonedGeom = subGeom.clone();
                    var polygonFeature = new ol.Feature(feature.getProperties());

                    polygonFeature.setGeometry(clonedGeom);
                    this.setFeatureStyle(polygonFeature, 'support');
                    result.push(polygonFeature);
                }
            } else {
                var copy = feature.clone();
                this.setFeatureStyle(copy, 'support');
                result.push(copy);
            }
        }
        return result;
    },
    /**
     * Private
     */
    isMultipleGeometry: function (feature) {
        var geom = feature.getGeometry();
        return (geom instanceof ol.geom.MultiPolygon ||
                (geom instanceof ol.geom.MultiPoint) || /*&&
                 !(feature.geometry instanceof OpenLayers.Geometry.Curve))*/
                geom instanceof ol.geom.MultiLineString);
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature, save, cancel) {
        feature.editAttribut = true;
        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.map, viewOptions);
        if (save) {
            action.events.register('saveObject', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
    },
    /**
     * Private
     */
    _showMapPreviewDialog: function (features, save, cancel, title, doNotSelect, multiple) {

        var wmsSource = null;
        var layers = this.olMap.getLayers();
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            var source = layer.getSource();
            if (layer instanceof ol.source.ImageWMS) {
                wmsSource = source;
                break;
            }
        }

        /*var wmsLayer = new ol.layer.Image({
         source: wmsSource
         });*/
        var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['default']);
        var featureStyle = olStyles[features[0].getGeometry().getType()];

        var olSelectStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['select']);
        var featureSelectStyle = olSelectStyles[features[0].getGeometry().getType()];

        var viewOptions = {
            view: MapPreviewDialog,
            //layers: [wmsLayer], //pas de couche de fond pour le moment
            features: features,
            featureStyle: featureStyle,
            featureSelectStyle: featureSelectStyle,
            title: title,
            doNotSelect: doNotSelect,
            multiple: multiple
        };

        var action = new MapPreview(null, this.olMap, viewOptions);
        if (save) {
            action.events.register('save', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
        return action;
    },
    /**
     * Private
     */
    registerToSelection: function (editionLayer) {
        if (this.editionLayer !== editionLayer || !Descartes.EditionManager.isGlobalEditonMode()) {
            if (!_.isNil(this.editionLayer)) {
                this.unregisterToOLSelection(this.editionLayer.getFeatureOL_layers()[0]);
            }
            if (editionLayer.isUnderEdition()) {
                this.registerToOLSelection(editionLayer.getFeatureOL_layers()[0]);
                if (editionLayer.isCompositeGeometry()) {
                    this.registerToOLSelection(editionLayer.compositeSelectionLayer);
                    this.registerToOLSelection(editionLayer.compositeVectorLayer);

                }
            }
        }
    },

    /**
     * Private
     */
    unregisterToSelection: function () {
        if (!_.isNil(this.editionLayer)) {
            this.unregisterToOLSelection(this.editionLayer.getFeatureOL_layers()[0]);
            if (this.editionLayer.isCompositeGeometry()) {
                this.unregisterToOLSelection(this.editionLayer.compositeSelectionLayer);
                this.unregisterToOLSelection(this.editionLayer.compositeVectorLayer);
            }
        }
    },
    /**
     * Methode privée: registerToSelection
     * Abonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    registerToOLSelection: function (olLayer) {
        var source = olLayer.getSource();
        var features = source.getFeatures();

        _.each(features, function (feature) {
            feature.on('change:selected', this.featureSelectionChanged, this);
        }.bind(this));

        source.on('addfeature', this.registerToAddFeature, this);
        source.on('removefeature', this.registerToRemoveFeature, this);
    },
    registerToAddFeature: function (e) {
        e.feature.on('change:selected', this.featureSelectionChanged, this);
    },

    registerToRemoveFeature: function (e) {
        e.feature.un('change:selected', this.featureSelectionChanged, this);
    },
    /**
     * Methode privée: unregisterToSelection
     * Désabonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    unregisterToOLSelection: function (olLayer) {
        var source = olLayer.getSource();
        source.un('addfeature', this.registerToAddFeature, this);
        source.un('removefeature', this.registerToRemoveFeature, this);
        var features = source.getFeatures();
        _.each(features, function (feature) {
            feature.un('change:selected', this.featureSelectionChanged, this);
        }.bind(this));
    },
    /*
     * Méthode: featureSelectionChanged
     * A implémenter par les classes filles
     */
    featureSelectionChanged: function () {
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation'
});

module.exports = Class;

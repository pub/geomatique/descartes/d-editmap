/* global MODE, Descartes */
var ol = require('openlayers');
var _ = require('lodash');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Information = require('../Information');
var EditionManager = require('../../../Core/EditionManager');
var TabbedDataGrid = Descartes.UI.TabbedDataGrids;
var MapConstants = Descartes.Map;

require('../css/intersectInformation.css');

/**
 * Class: Descartes.Tool.Edition.Information.IntersectInformation
 * Outil pour afficher des informations d'intersection.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Information>
 */
var Class = Utils.Class(Information, {

    /**
     * Propriete: showInfos
     * {Object} Permet d'activer ou non l'affichage d'informations
     * (identifiant, attributs, calculs géométriques) sur l'objet sélectionné.
     *
     * attributes - false
     * geometricCalculs - false
     * identifier - false
     */
    showInfos: {
        attributes: false,
        geometricCalculs: false,
        identifier: false
    },

    /**
     * Propriete: simpleResultsView
     * {Boolean} Permet d'afficher les résultats de façon simplifiée.
     */
    simpleResultsView: true,

    /**
     *  Propriete: sendAppResult
     * {Boolean} Permet d'envoyer les résultats à l'application métier.
     */
    sendAppResults: false,

    /*
     * Private
     */
    _sizeEltPopup: 23,

    popupInfosWidth: 200,

    /*
     * Private
     */
    _overlay: null,

    /*
     * _parser Jsts parser
     */
    _parser: null,

    /**
     * Propriete: defaultResultUiParams
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires
     * à la génération de l'affichage des résultats de l'intersection.
     *
     * withReturn - false
     * withCsvExport - false
     * withResultLayerExport - false
     * withUIExports - false
     * withAvancedView - false
     * withFilterColumns - false
     * withListResultLayer - false
     * withPagination - false
     * withTabs - false
     * calculArround - true
     */
    defaultResultUiParams: {
        withReturn: false,
        withCsvExport: false,
        withResultLayerExport: false,
        withUIExports: false,
        withAvancedView: false,
        withFilterColumns: false,
        withListResultLayer: false,
        withPagination: false,
        withTabs: false,
        calculArround: true
    },

    /**
     * Propriete: resultUiParams
     * {Object} Objet JSON stockant les propriétés nécessaires à la génération
     * de l'affichage des résultats de la sélection.
     *
     * Construit à partir de defaultResultUiParams, puis surchargé en présence d'options complémentaires.
     *
     * div - {DOMElement|String} : Elément DOM de la page accueillant le résultat de l'intersection.
     * withReturn - {Boolean} : Indique si le résultat doit proposer une localisation rapide sur les objets retournés.
     * withCsvExport - {Boolean} : Indique si le résultat doit proposer une exportation CSV (côté serveur) pour les objets retournés.
     * withResultLayerExport - {Boolean} : Indique si le résultat doit proposer une exportation des objets retournés avec les geométries.
     * withUIExports - {Boolean} : Indique si le résultat doit proposer les exports (côté client) pour les objets retournés.
     * withAvancedView - {Boolean} : Indique si le résultat doit être affiché dans une vue (UI) avancée.7
     * withFilterColumns - {Boolean} : Indicateur pour la possibilité de filtrer les colonnes.
     * withListResultLayer - {Boolean} : Indicateur pour la possibilité d'affichage de la liste des couches'.
     * withPagination - {Boolean} : Indicateur pour la possibilité d'afficher la pagination'.
     * withTabs - {Boolean} : Indicateur pour la possibilité d'afficher les onglets'.
     */
    resultUiParams: null,

    /**
     * Constructeur: Descartes.Tool.Edition.Information.IntersectInformation
     * Constructeur d'instances
     */
    initialize: function (options) {
        Information.prototype.initialize.apply(this, arguments);
        this.resultUiParams = _.extend({}, this.defaultResultUiParams);
        if (!_.isNil(options) && !_.isNil(options.resultUiParams)) {
            _.extend(this.resultUiParams, options.resultUiParams);
            delete options.resultUiParams;
        }
        if (this.resultUiParams.div) {
            this.simpleResultsView = false;
        }
        _.extend(this, options);

        this._parser = new jsts.io.OL3Parser();
    },
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (EditionManager.isGlobalEditonMode()) {
            //mode globale
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer.intersect;
                this.registerToSelection(editionLayer);
            } else {
                this.unregisterToSelection();
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.intersect;
            this.registerToSelection(this.editionLayer);
        }

        Information.prototype.updateStateWithLayer.apply(this, arguments);
        //controle de la configuration
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'interaction d'openlayers
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisée par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;

        if (!_.isNil(this.interaction) && !_.isNil(this.olMap)) {
            this.olMap.removeInteraction(this.interaction);
        }

        this.interaction = new ol.interaction.Select({
            multi: false,
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this)
        });
        this.interaction.set('id', 'IntersetInformation_' + Utils.createUniqueID());
        this.interaction.on('select', this.toggleFeatureSelected.bind(this));
        this.interaction.setActive(false);


        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    toggleFeatureSelected: function (event) {
        if (event.selected.length === 1) {
            var selectedFeature = event.selected[0];
            if (this.selectedFeature === selectedFeature) {
                this.unselectAll();
            } else {
                //if (!this.sendAppResults) {
                    this.unselectAll();
                //}
                this.selectedFeature = selectedFeature;

                _.each(event.selected, function (feature) {
                    this.setFeatureStyle(feature, 'select');
                    if (this.sendAppResults) {
                        this.sendInformations(feature);
                    } else {
                        if (!this.simpleResultsView) {
                            this.tabbedGrid(feature);
                            if (this.showInfos.attributes ||
                                    this.showInfos.identifier ||
                                    this.showInfos.geometricCalculs) {
                                this.overlay(feature);
                            }
                        } else if (this.showInfos) {
                            this.overlay(feature);
                        }
                    }
                }.bind(this));
            }
        }
    },

    /*
     * Méthode: featureSelectionChanged
     * Listener agissant sur l'état de l'outil lorsqu'un changement
     * de selection est fait dans le layer d'édition courant.
     */
    featureSelectionChanged: function () {
        if (this.editionLayer !== null && this._tempLayer === null && !_.isNil(this._toolConfig)) {
            if (this.editionLayer.getSelectedFeatures().length === 1) {
                this.featureToIntersect = this.editionLayer.getSelectedFeatures()[0];
            } else {
                this.featureToIntersect = null;
            }
        } else if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
        }
    },
    overlay: function (feature) {
        var element = this.createOverlay(feature);

        var extent = feature.getGeometry().getExtent();
        var position = ol.extent.getTopRight(extent);

        this._overlay = new ol.Overlay({
            element: element,
            autoPan: true
        });

        this.olMap.addOverlay(this._overlay);

        this._overlay.setPosition(position);
        this.afterHighlight();
    },
    tabbedGrid: function (feature) {
        var info = this.getIntersectedFeaturesInfo(feature);
        var nbIntersection = 0;

        var noAttributesMsg = this.getMessage('NO_ATTRIBUTES');
        var model = [];
        _.each(info.layers, function (layer) {
            nbIntersection += layer.layerDatas.length;
            var modelLayerDatas = {
                layerTitle: layer.layerTitle,
                layerDatas: [],
                layerMsgError: ""
            };
            _.each(layer.layerDatas, function (layerData) {
                var data = {
                    identifiant: layerData.objectId
                };
                var strAttributes = noAttributesMsg;
                if (layerData.attributes) {
                    var str = '';
                    for (var attribute in layerData.attributes) {
                        if (layerData.attributes[attribute] && layerData.attributes[attribute].value) {
                            str += attribute + ': ' + layerData.attributes[attribute].value + ', ';
                        } else {
                            str += attribute + ': ' + layerData.attributes[attribute] + ', ';
                        }
                    }
                    if (str.lastIndexOf(',') !== -1) {
                        str = str.substr(0, str.lastIndexOf(','));
                    }
                    if (str.length > 0) {
                        strAttributes = str;
                    }
                }
                data.attributes = strAttributes;

                if (layerData.geometry) {
                    data['Type de géométrie'] = this.getTypeGeometry(layerData.geometry);
                    if (this.resultUiParams.withReturn === true) {
                        data['bounds'] = layerData.geometry.getExtent();
                    }
                }
                if (layerData.geometryIntersection) {
                    var intersection = layerData.geometryIntersection;
                    data['Type d\'intersection'] = this.getTypeGeometry(intersection);
                    var nb = 1;
                    var area = null;
                    if (intersection instanceof ol.geom.MultiPolygon) {
                        nb = intersection.getPolygons().length;
                        area = this.getArea(intersection);
                    } else if (intersection instanceof ol.geom.MultiPoint) {
                        nb = intersection.getPoints().length;
                    } else if (intersection instanceof ol.geom.MultiLineString) {
                        nb = intersection.getLineStrings().length;
                    } else if (intersection instanceof ol.geom.Polygon) {
                        area = this.getArea(intersection);
                    }
                    data['Nombre d\'intersection'] = nb + '';
                    if (!_.isNil(area)) {
                        data['Surface d\'intersection'] = area.measure + ' ' + area.unit;
                    }
                }

                modelLayerDatas.layerDatas.push(data);
            }.bind(this));
            model.push(modelLayerDatas);
        }.bind(this));

        var intersectMsg = this.getMessage('INTERSECT_PREFIX');
        intersectMsg += nbIntersection;
        intersectMsg += this.getMessage('INTERSECT_SUFFIX');

        var options = {
            withCsvExport: this.resultUiParams.withCsvExport,
            withResultLayerExport: this.resultUiParams.withResultLayerExport,
            withUIExports: this.resultUiParams.withUIExports,
            withAvancedView: this.resultUiParams.withAvancedView,
            withFilterColumns: this.resultUiParams.withFilterColumns,
            withListResultLayer: this.resultUiParams.withListResultLayer,
            withPagination: this.resultUiParams.withPagination,
            withTabs: this.resultUiParams.withTabs,
            dialogTitle: intersectMsg,
            messageError: 'Aucune intersection'
        };

        var renderer = new TabbedDataGrid(this.resultUiParams.div, model, options);
        renderer.events.register('gotoFeatureBounds', this, this.gotoFeatureBounds);
        renderer.events.register('close', this, function (event) {
            this.unselectAll();
        });
        renderer.draw(model);
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Information.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.underActivation = false;
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            this.unselectAll();
            Information.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
        }
    },
    unselectAll: function () {
        if (!_.isNil(this._overlay)) {
            this.olMap.removeOverlay(this._overlay);
        }
        var features = this._tempLayer.getSource().getFeatures();
        _.each(features, function (feature) {
            this.setFeatureStyle(feature, 'support');
        }.bind(this));
        this.selectedFeature = null;
        this.interaction.getFeatures().clear();
    },
    sendInformations: function (feature) {
        var geometry = feature.getGeometry();
        var objectSelected = {
            geometry: geometry
        };

        if (this.showInfos.attributes) {
            objectSelected.attributs = this.getFeatureAttributes(feature);
        }

        if (this.showInfos.identifier) {
            objectSelected.objectId = feature.getId();
        }

        if (this.showInfos.geometricCalculs &&
                !(geometry instanceof ol.geom.Point || geometry instanceof ol.geom.MultiPoint)) {
            var area = this.getArea(geometry);
            if (!_.isNil(area)) {
                objectSelected.area = area;
            }
            var perimeter = this.getPerimeter(geometry);
            objectSelected.perimeter = perimeter;
        }

        var intersectInfo = this.getIntersectedFeaturesInfo(feature);

        var informations = {
            type: 'descartes_outil_selection',
            featureSelected: feature,
            featuresIntersected: intersectInfo.layers,
            fluxSimple: {
                objectSelected: objectSelected,
                objectsIntersected: intersectInfo.layers
            }
        };
        var EditionManager = require('../../../Core/EditionManager');
        EditionManager.sendInformations(informations);
    },
    getArea: function (geom) {
        var unit = this.getMessage('TITLE_AIRE_UNITE');
        var projection = this.olMap.getView().getProjection();
        var area = 0;
        if (geom instanceof ol.geom.Polygon) {
            area = ol.Sphere.getArea(geom, {
                projection: projection
            });
        } else if (geom instanceof ol.geom.MultiPolygon) {
            var polygons = geom.getPolygons();
            _.each(polygons, function (polygon) {
                area += ol.Sphere.getArea(polygon, {
                    projection: projection
                });
            });
        }

        var fullUnit;
        if (area > 10000) {
            fullUnit = 'km' + unit;
            area = Math.round(area / 1000000 * 100) / 100;
        } else {
            fullUnit = 'm' + unit;
            area = Math.round(area * 100) / 100;
        }

        if (this.calculArround) {
            area = area.toFixed(3);
        }

        return {
            measure: area,
            unit: fullUnit
        };
    },
    getPerimeter: function (geom) {
        var length = 0;
        var projection = this.olMap.getView().getProjection();
        if (geom instanceof ol.geom.Polygon) {
            length = Utils.getPolygonLength(geom, projection);
        } else if (geom instanceof ol.geom.MultiPolygon) {
            var polygons = geom.getPolygons();
            _.each(polygons, function (polygon) {
                length += Utils.getPolygonLength(polygon, projection);
            });
        } else if (geom instanceof ol.geom.LineString) {
            length = ol.Sphere.getLength(geom, {
                projection: projection
            });
        } else if (geom instanceof ol.geom.MultiLineString) {
            var lines = geom.getLineStrings();
            _.each(lines, function (line) {
                length += ol.Sphere.getLength(line, {
                    projection: projection
                });
            });
        } else {
            return null;
        }

        var unit;
        if (length > 100) {
            unit = 'km';
            length = Math.round(length / 1000 * 100) / 100;
        } else {
            unit = 'm';
            length = Math.round(length * 100) / 100;
        }

        if (this.calculArround) {
            length = length.toFixed(3);
        }

        return {
            measure: length,
            unit: unit
        };
    },
    getIntersectedFeaturesInfo: function (feature) {
        var jstsSourceGeom = this._parser.read(feature.getGeometry());

        var result = {
            layers: []
        };
        for (var i = 0; i < this._supportLayers.length; i++) {
            var supportLayer = this._supportLayers[i];

            var layerIntersections = {
                layerTitle: supportLayer.get('title'),
                layerDatas: []
            };

            var sourceGeom = this._parser.read(feature.getGeometry());

            var features = supportLayer.getSource().getFeatures();
            for (var j = 0; j < features.length; j++) {
                var aFeature = features[j];

                var aJstsGeom = this._parser.read(aFeature.getGeometry());

                if (aJstsGeom.equalsTopo(sourceGeom)) {
                    continue;
                }

                if (!jstsSourceGeom.intersects(aJstsGeom)) {
                    continue;
                }

                var tempFeatures = this._tempLayer.getSource().getFeatures();
                _.each(tempFeatures, function (tempFeature) {
                    var tempGeom = this._parser.read(tempFeature.getGeometry());
                    if (aJstsGeom.equalsTopo(tempGeom)) {
                        this.setFeatureStyle(tempFeature, 'intersect');
                    }
                }.bind(this));

                var jstsIntersection = jstsSourceGeom.intersection(aJstsGeom);
                var intersectionGeom = this._parser.write(jstsIntersection);

                var layerData = {
                    objectId: aFeature.getId(),
                    geometry: aFeature.getGeometry(),
                    attributes: this.getFeatureAttributes(aFeature),
                    geometryIntersection: intersectionGeom,
                    layer: supportLayer
                };
                if (!(intersectionGeom.getType().indexOf('Point') > -1)) {
                    var infos = this.getGeometricInfos(intersectionGeom);
                    delete infos.strMeasure;
                    delete infos.strPerimetre;
                    delete infos.order;
                    layerData.geometricCalculs = infos;
                }
                layerIntersections.layerDatas.push(layerData);
            }
            if (layerIntersections.layerDatas.length > 0) {
                result.layers.push(layerIntersections);
            }
        }
        return result;
    },
    getGeometricInfos: function (geom) {
        var area = this.getArea(geom);
        var perimeter = this.getPerimeter(geom);

        return {
            area: area,
            perimeter: perimeter
        };
    },
    getTypeGeometry: function (geom) {
        if (geom instanceof ol.geom.Point) {
            return 'Point';
        } else if (geom instanceof ol.geom.LineString) {
            return 'Ligne';
        } else if (geom instanceof ol.geom.Polygon) {
            return 'Polygone';
        } else if (geom instanceof ol.geom.MultiPoint) {
            return 'Multi-Points';
        } else if (geom instanceof ol.geom.MultiLineString) {
            return 'Multi-Lignes';
        } else if (geom instanceof ol.geom.MultiPolygon) {
            return 'Multi-Polygones';
        }
        return null;
    },
    getFeatureAttributes: function (feature) {
        var attributes = _.clone(feature.getProperties());
        delete attributes[feature.getGeometryName()];
        delete attributes.selected;
        delete attributes.boundedBy;
        delete attributes.state;
        return attributes;
    },
    gotoFeatureBounds: function (boundsAndScales) {
        //Utils.gotoFeatureBounds(boundsAndScales, this.olMap); //KO ne fonctionne pas
        if (!(boundsAndScales instanceof Array) && boundsAndScales.data) {
            //cas type_widget
            boundsAndScales = boundsAndScales.data;
        } else if (!(boundsAndScales instanceof Array)) {
            //cas type_popup, type_inplace
            boundsAndScales = eval(boundsAndScales);
        }
        var bounds = boundsAndScales[0];
        if (!(bounds instanceof Array) && bounds.data) {
            bounds = bounds.data;
        }
        var layerMinScale = boundsAndScales[1];
        var layerMaxScale = boundsAndScales[2];

        var xmin = bounds[0], ymin = bounds[1], xmax = bounds[2], ymax = bounds[3];

        var mapType = this.olMap.get('mapType');
        var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;

        var extent = [xmin, ymin, xmax, ymax];
        if ((xmax - xmin) < Descartes.MIN_BBOX && (ymax - ymin) < Descartes.MIN_BBOX) {
            var newXmin = xmin + (xmax - xmin) / 2 - Descartes.MIN_BBOX / 2;
            var newXmax = xmin + (xmax - xmin) / 2 + Descartes.MIN_BBOX / 2;
            var newYmin = ymin + (ymax - ymin) / 2 - Descartes.MIN_BBOX / 2;
            var newYmax = ymin + (ymax - ymin) / 2 + Descartes.MIN_BBOX / 2;
            extent = [newXmin, newYmin, newXmax, newYmax];
        }
        this.olMap.getView().fit(extent, {
            constrainResolution: constrainResolution,
            minResolution: this.olMap.getView().getMinResolution(),
            maxResolution: this.olMap.getView().getMaxResolution()
        });

        var currentRes = this.olMap.getView().getResolution();
        var unit = this.olMap.getView().getProjection().getUnits();
        var newRes = currentRes;
        if (layerMinScale !== null) {
            var layerMinRes = this.getResolutionForScale(layerMinScale, unit);
            if (layerMinRes < currentRes) {
                newRes = layerMinRes;
            }
        }
        if (layerMaxScale !== null) {
            var layerMaxRes = this.getResolutionForScale(layerMaxScale, unit);
            if (layerMaxRes > currentRes) {
                newRes = layerMaxRes;
            }
        }

        if (newRes !== currentRes) {
            this.olMap.getView().setResolution(newRes);
        }

        return extent;

    },

    CLASS_NAME: 'Descartes.Tool.Edition.Information.IntersectInformation'
});

module.exports = Class;

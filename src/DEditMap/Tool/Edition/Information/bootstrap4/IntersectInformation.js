/* global MODE, Descartes */
var ol = require('openlayers');
var _ = require('lodash');
var $ = require('jquery');

var Utils = Descartes.Utils;
var IntersectInformation = require('../IntersectInformation');

var template = require('./templates/IntersectInformation.ejs');

var Class = Utils.Class(IntersectInformation, {

    highlightId: null,

    createOverlay: function (feature) {
        this.highlightId = Utils.createUniqueID();

        var attributes = _.clone(feature.getProperties());
        delete attributes[feature.getGeometryName()];
        delete attributes.selected;
        delete attributes.boundedBy;
        delete attributes.state;

        var geom = feature.getGeometry();

        var geomInfo = {};
        if (this.showInfos.geometricCalculs) {
            geomInfo.perimeter = this.getPerimeter(geom);
            if (geom instanceof ol.geom.Polygon || geom instanceof ol.geom.MultiPolygon) {
                geomInfo.area = this.getArea(geom);
            }
        }

        var info = this.getIntersectedFeaturesInfo(feature);

        var nbIntersection = 0;
        _.each(info.layers, function (layer) {
            nbIntersection += layer.layerDatas.length;
        });

        var intersectMsg = this.getMessage('INTERSECT_PREFIX');
        intersectMsg += nbIntersection;
        intersectMsg += this.getMessage('INTERSECT_SUFFIX');

        var html = template({
            id: this.highlightId,
            title: this.getMessage('TITLE'),
            intersectMsg: intersectMsg,
            identifierLabel: this.getMessage('IDENTIFIER'),
            areaLabel: this.getMessage('AREA'),
            perimeterLabel: this.getMessage('PERIMETER'),
            showInfos: this.showInfos,
            featureId: feature.getId(),
            attributes: attributes,
            objetSelectMsg: this.getMessage('TITLE_OBJET_SELECT'),
            noAttributesLabel: this.getMessage('NO_ATTRIBUTES'),
            geomInfo: geomInfo
        });

        var div = document.createElement('div');

        div.innerHTML = html.trim();
        return div.firstChild;
    },
    afterHighlight: function () {
        $('#' + this.highlightId + '_close').click(function () {
            this.unselectAll();
        }.bind(this));
    },
    /*_initializeSupportTempLayer: function () {
     },*/

    CLASS_NAME: 'Descartes.Tool.Edition.Information.IntersectInformation'
});

module.exports = Class;

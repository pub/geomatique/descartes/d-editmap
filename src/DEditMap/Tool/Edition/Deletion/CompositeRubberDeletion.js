/* global MODE, Descartes */
var _ = require('lodash');
var jsts = require('jsts');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Deletion = require('../Deletion');
var FeatureState = require('../../../Model/FeatureState');
var EditionManager = require('../../../Core/EditionManager');
var LayerConstants = Descartes.Layer;

require('../css/compositeRubberDeletion.css');

/**
 * Class: Descartes.Tool.Edition.Deletion.CompositeRubberDeletion
 * Outil gomme de suppression.
 * Cet outil permet de supprimer la géométrie selectionné dans un objet selectionné.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Deletion>
 */
var Class = Utils.Class(Deletion, {

    _parser: new jsts.io.OL3Parser(),

    /**
     * Methode: setAvailable
     * l'outils n'est disponible que pour les geometries composite
     *
     * Paramètres:
     * available  - {boolean} indiquant le nouveau statut de disponibilité
     */
    setAvailable: function () {
        if (!_.isNil(this.editionLayer) && this.editionLayer.isCompositeGeometry()) {
            Deletion.prototype.setAvailable.apply(this, arguments);
        } else {
            Deletion.prototype.setAvailable.apply(this, [false]);
        }
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Deletion.prototype.activate.apply(this, arguments);

            this.editionLayer.showCompositeSelectionLayer();
            //Puis on y ajoute les objets composites décomposés
            this.editionLayer.splitCompositeFeatures();

            this.editionLayer.compositeSelectionLayer.getSource().on('removefeature', this.refresh, this);
            this.editionLayer.getSaveStrategy().events.register('success', this, this.deactivate);
            this.interaction.getFeatures().clear();
            this.underActivation = false;
        }
    },

    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Deletion.prototype.deactivate.apply(this, arguments);

            this.editionLayer.compositeSelectionLayer.getSource().un('removefeature', this.refresh, this);
            this.editionLayer.getSaveStrategy().events.unregister('success', this, this.deactivate);
            this.interaction.getFeatures().clear();

            this.editionLayer.clearCompositeSelectionLayer();
            this.editionLayer.hideCompositeSelectionLayer();
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        Deletion.prototype.initializeOLFeature.apply(this, [this.editionLayer.compositeSelectionLayer]);
        this.interaction.set('id', 'CompositeRubberDeletion_' + Utils.createUniqueID());
        this.olLayer = olLayer;
    },

    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     * La selection est supprimé de la couche.
     *
     * Paramètres:
     * feature - l'objet sélectionné.
     */
    onSelect: function (event) {
        _.each(event.deselected, function (feature) {
            var parentFeature = feature.get('parent');
            var parentState = parentFeature.get('state');
            if (parentState !== FeatureState.DELETE) {
                feature.set('state', parentState);
                feature.editionLayer = this.editionLayer;
            } else {
                if (this.parentCreation) {
                    feature.set('state', FeatureState.INSERT);
                    feature.editionLayer = this.editionLayer;
                } else {
                    feature.set('state', FeatureState.UPDATE);
                    feature.editionLayer = this.editionLayer;
                }
            }
            var style = Utils.getStyleByState(parentFeature);
            this.setFeatureStyle(feature, style);
            var children = parentFeature.get('children');
            _.each(children, function (child) {
                    child.set('state', parentState);
                    this.setFeatureStyle(child, style);
            }.bind(this));

            this.computeParentFeatureGeom(parentFeature);
        }.bind(this));

        _.each(event.selected, function (feature) {
            this.removeGeomFromParent(feature);
        }.bind(this));

        if (EditionManager.autoSave) {
            this.olLayer.getSource().get('saveStrategy').save();
        }
    },

    removeGeomFromParent: function (childFeatureToRemove) {
        childFeatureToRemove.set('state', FeatureState.DELETE);
        childFeatureToRemove.editionLayer = this.editionLayer;
        var parentFeature = childFeatureToRemove.get('parent');
        this.parentCreation = parentFeature.get('state') === FeatureState.INSERT;

        var style;
        if (this.parentCreation) {
            style = Utils.getStyleByState(childFeatureToRemove);
            this.setFeatureStyle(childFeatureToRemove, style);
        } else {
            var children = parentFeature.get('children');
            _.each(children, function (childFeature) {
                var style = Utils.getStyleByState(childFeature);
                if (childFeature !== childFeatureToRemove) {
                    if (childFeature.get('state') !== FeatureState.DELETE) {
                        childFeature.set('state', FeatureState.UPDATE);
                        childFeature.editionLayer = this.editionLayer;
                        style = Utils.getStyleByState(childFeature);
                        this.setFeatureStyle(childFeature, style);
                    }
                } else {
                    this.setFeatureStyle(childFeature, style);
                }
            }.bind(this));
        }

        this.computeParentFeatureGeom(parentFeature);
    },

    computeParentFeatureGeom: function (parentFeature) {
        var result = null;
        var childrenFeatures = parentFeature.get('children');

        for (var i = 0; i < childrenFeatures.length; i++) {
            var childFeature = childrenFeatures[i];
            var childState = childFeature.get('state');

            //on ne prend pas en compte les features supprimées pour calculer le résultat
            if (childState !== FeatureState.DELETE) {
                var jstsGeom = this._parser.read(childFeature.getGeometry().clone());

                //on ajoute ou on créé un géométrie composite
                if (result instanceof jsts.geom.GeometryCollection &&
                        !(jstsGeom instanceof jsts.geom.GeometryCollection)) {
                    //result.geometries.push(jstsGeom);
                    result = result.union(jstsGeom);
                } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                    result = new jsts.geom.MultiPolygon([jstsGeom], jstsGeom.getFactory());
                } else if (this.editionLayer.geometryType === LayerConstants.MULTI_LINE_GEOMETRY) {
                    result = new jsts.geom.MultiLineString([jstsGeom], jstsGeom.getFactory());
                } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY) {
                    result = new jsts.geom.MultiPoint([jstsGeom], jstsGeom.getFactory());
                }

            }
        }
        if (_.isNil(result)) {
            parentFeature.set('state', FeatureState.DELETE);
            parentFeature.editionLayer = this.editionLayer;
        } else {
            if (!_.isNil(parentFeature.getId())) {
                parentFeature.set('state', FeatureState.UPDATE);
            }
            result = this._parser.write(result);
            parentFeature.setGeometry(result);
            parentFeature.editionLayer = this.editionLayer;
        }
        var style = Utils.getStyleByState(parentFeature);
        this.setFeatureStyle(parentFeature, style);
    },

    CLASS_NAME: 'Descartes.Tool.Edition.Deletion.CompositeRubberDeletion'
});

module.exports = Class;

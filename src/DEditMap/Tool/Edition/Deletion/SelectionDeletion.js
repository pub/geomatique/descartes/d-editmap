/* global MODE, Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;
var Selection = require('../Selection');
var EditionLayer = require('../../../Model/EditionLayer');

require('../css/selectionDeletion.css');

/**
 * Class: Descartes.Tool.Edition.Deletion.SelectionDeletion
 *
 * Outil de suppression après une selection.
 * Cet outil permet de supprimer l'objet selectionné.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Selection>
 */
var Class = Utils.Class(Selection, {

    /**
     * c'est outil n'est pas actif ou non (bouton enfoncé ou pas)
     * Son état est lié uniquement à l'état disponible (available)
     */
    active: false,

    selectionEditionTool: null,

    setSelectionEditionTool: function (selectionEditionTool) {
        this.selectionEditionTool = selectionEditionTool;
        if (_.isNil(this.selectionEditionTool)) {
            alert('Attention l\'outil de sélection doit être présent pour utiliser l\'outil de suppression pour sélection');
        }
    },
    setCompositeSelectionEditionTool: function (compositeSelectionEditionTool) {
        this.compositeSelectionEditionTool = compositeSelectionEditionTool;
    },

    /**
     * Methode: activate
     * Les objets selectionnées dans la couche d'édition courante sont supprimés.
     */
    activate: function () {
        if (this.isAvailable()) {
            this.underActivation = true;
            if (this.editionLayer.getSelectedFeatures().length > 0 ||
                    this.editionLayer.getcompositeSelectionLayerSelectedFeatures().length > 0) {
                this.editionLayer.removeSelectedFeatures();
                this.selectionEditionTool.unhighlight();
                this.selectionEditionTool.unselectAll();
                if (!_.isNil(this.compositeSelectionEditionTool)) {
                    this.compositeSelectionEditionTool.unselectAll();
                    this.compositeSelectionEditionTool.deactivate();
                }
                this.editionLayer.getSaveStrategy().events.register('success', this, this.refresh);
            } else {
                alert(this.getMessage('ERROR_NO_SELECT_GEOMETRY'));
            }
            this.setAvailable(false);
            this.updateElement();
            this.underActivation = false;
        }
    },

    deactivate: function () {
        if (this.isAvailable()) {
            this.editionLayer.getSaveStrategy().events.register('success', this, this.refresh);
        }
    },

    refresh: function () {
        this.selectionEditionTool.unselectAll();
        if (!_.isNil(this.compositeSelectionEditionTool)) {
            this.compositeSelectionEditionTool.unselectAll();
        }
    },

    /*
     * Méthode: updateStateWithLayer
     * Met à jour l'état de l'outil en fonction des changements de couche en cours d'édition.
     * S'abonne au changement de selection du layer en cours d'édition et se désabonne si nécessaire.
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (editionLayer instanceof EditionLayer) {
            if (this.editionLayer !== editionLayer) {
                if (this.editionLayer !== null) {
                    this.unregisterToSelection(this.editionLayer.getFeatureOL_layers()[0]);
                }
                if (editionLayer.isUnderEdition()) {
                    this.registerToSelection(editionLayer.getFeatureOL_layers()[0]);
                    if (editionLayer.isCompositeGeometry()) {
                        this.registerToSelection(editionLayer.compositeSelectionLayer);
                        this.registerToSelection(editionLayer.compositeVectorLayer);
                    }
                }

                if (this.map !== null) {
                    this.editionLayer = editionLayer;
                }
            }
        } else if (this.editionLayer !== null) {
            if (Descartes.EditionManager.isGlobalEditonMode()) {
                this.unregisterToSelection(this.editionLayer.getFeatureOL_layers()[0]);
                if (this.editionLayer.isCompositeGeometry()) {
                    this.unregisterToSelection(this.editionLayer.compositeSelectionLayer);
                    this.unregisterToSelection(this.editionLayer.compositeVectorLayer);
                }
            } else {
                this.registerToSelection(this.editionLayer.getFeatureOL_layers()[0]);
                if (this.editionLayer.isCompositeGeometry()) {
                    this.registerToSelection(this.editionLayer.compositeSelectionLayer);
                    this.registerToSelection(this.editionLayer.compositeVectorLayer);
                }
            }
        }
        Selection.prototype.updateStateWithLayer.apply(this, arguments);
        this.featureSelectionChanged();
    },

    CLASS_NAME: 'Descartes.Tool.Edition.Deletion.SelectionDeletion'
});

module.exports = Class;

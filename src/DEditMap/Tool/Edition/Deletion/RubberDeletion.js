/* global MODE, Descartes */
var _ = require('lodash');

var Utils = Descartes.Utils;
var Deletion = require('../Deletion');
var FeatureState = require('../../../Model/FeatureState');
var EditionManager = require('../../../Core/EditionManager');

require('../css/rubberDeletion.css');

/**
 * Class: Descartes.Tool.Edition.Deletion.RubberDeletion
 * Outil gomme de suppression.
 * Cet outil permet de supprimer l'objet selectionné.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Deletion>
 */
var Class = Utils.Class(Deletion, {

    initialize: function () {
        if (!Descartes.EditionManager.autoSave) {
            this.defaultDeletionOptions.multiple = true;
        }
        Deletion.prototype.initialize.apply(this, arguments);
    },

    initializeOLFeature: function (olLayer) {
        Deletion.prototype.initializeOLFeature.apply(this, arguments);
        this.interaction.set('id', 'RubberDeletion_' + Utils.createUniqueID());
        this.olLayer = olLayer;
    },

    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Deletion.prototype.activate.apply(this, arguments);
            this.olLayer.getSource().on('removefeature', this.refresh, this);
            this.interaction.getFeatures().clear();
            this.interaction.getFeatures().extend(this.getFeaturesToDelete());
            this.underActivation = false;
        }
    },

    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Deletion.prototype.deactivate.apply(this, arguments);
            this.olLayer.getSource().un('removefeature', this.refresh, this);
            this.interaction.getFeatures().clear();
        }
    },

    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     * La selection est supprimé de la couche.
     *
     * Paramètres:
     * feature - l'objet sélectionné.
     */
    onSelect: function (event) {
        _.each(event.deselected, function (feature) {
            feature.set('state', feature.get('oldstate'));
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
        }.bind(this));
        _.each(event.selected, function (feature) {
            if (_.isNil(feature.getId())) {
                this.olLayer.getSource().removeFeature(feature);
            } else {
                feature.set('oldstate', feature.get('state'));
                feature.set('state', FeatureState.DELETE);
                feature.editionLayer = this.editionLayer;
                if (this.editionLayer &&
                        this.editionLayer.attributes &&
                        this.editionLayer.attributes.attributeId &&
                        this.editionLayer.attributes.attributeId.fieldName !== null) {
                             feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
                }
                this.setFeatureStyle(feature, 'delete');
            }
        }.bind(this));

        if (EditionManager.autoSave) {
            this.olLayer.getSource().get('saveStrategy').save();
        }
    },

    getFeaturesToDelete: function () {
        var result = [];
        this.olLayer.getSource().getFeatures().forEach(function (feature) {
            if (feature.get('state') === FeatureState.DELETE) {
                result.push(feature);
            }
        });
        return result;
    },
    /**
     * Met à jour la collection des features de l'interaction
     */
    refresh: function (e) {
        var feature = e.feature;
        var unselectFeature = false;
        this.interaction.getFeatures().forEach(function (selectedFeature) {
            unselectFeature = selectedFeature === feature;
        });
        if (unselectFeature) {
            this.interaction.getFeatures().clear();
        }
    },

    CLASS_NAME: 'Descartes.Tool.Edition.Deletion.RubberDeletion'
});

module.exports = Class;

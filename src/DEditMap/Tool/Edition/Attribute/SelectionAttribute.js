/* global MODE, Descartes */
var _ = require('lodash');
var log = require('loglevel');

var Utils = Descartes.Utils;
var Selection = require('../Selection');
var Edition = require('../../Edition');
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var AttributesEditor = require('../../../Action/AttributesEditor');

var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var Symbolizers = require('../../../Symbolizers');

require('../css/selectionAttribute.css');

/**
 * Class: Descartes.Tool.Edition.SelectionAttribut
 *
 * Outil de saisie des attributs après une sélection.
 * Cet outil permet de saisir les attributs de l'objet selectionné.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Selection, {

    /**
     * c'est outil n'est pas actif ou non (bouton enfoncé ou pas)
     * Son état est lié uniquement à l'état disponible (available)
     */
    active: false,

    selectionEditionTool: null,

    defaultOlStyles: null,
    createOlStyles: null,

    setSelectionEditionTool: function (selectionEditionTool) {
        this.selectionEditionTool = selectionEditionTool;
        if (_.isNil(this.selectionEditionTool)) {
            alert('Attention l\'outil de sélection doit être présent pour utiliser l\'outil de modification des attributs par sélection');
        }
    },

    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable()) {
            this.underActivation = true;
            if (this.editionLayer.getSelectedFeatures().length > 0) {
                var feature = this.editionLayer.getSelectedFeatures()[0];
                if (!this.isConforme(feature.getGeometry())) {
                    alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                    this.resetNonConformModif();
                    return false;
                }

                if (this.isActiveVerifTopoConformity() &&
                        !this.isConformeTopology(feature.getGeometry(), true)) {
                    this.resetNonConformModif();
                    return false;
                }

                if (this.isActiveVerifAppConformity() &&
                        !this.isConformeRulesApp(feature, true)) {
                    this.resetNonConformModif();
                    return false;
                }

                // Creation de la vue
                var viewOptions = {
                    view: AttributesEditorDialog,
                    feature: feature,
                    editionLayer: this.editionLayer
                };

                // Creation de l'action qui va ouvrir la vue
                var action = new AttributesEditor(null, this.map, viewOptions);
                action.events.register('saveObject', this, this.saveObject);
                action.events.register('cancel', this, this.cancel);

                this.selectionEditionTool.unhighlight();
                return true;
            } else {
                alert(this.getMessage('ERROR_NO_SELECT_GEOMETRY'));
                this.setAvailable(false);
            }
            this.underActivation = false;
        }
        return false;
    },

    /*
     * Méthode: updateStateWithLayer
     * Met à jour l'état de l'outil en fonction des changements de couche en cours d'édition.
     * S'abonne au changement de selection du layer en cours d'édition et se désabonne si nécessaire.
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (editionLayer instanceof Descartes.Layer.EditionLayer) {
            if (this.editionLayer !== editionLayer) {
                if (this.editionLayer !== null) {
                    this.unregisterToSelection(this.editionLayer.getFeatureOL_layers()[0]);
                }
                this.registerToSelection(editionLayer.getFeatureOL_layers()[0]);
                this.editionLayer = editionLayer;
                this.defaultOlStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.default);
                this.createOlStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.create);
            }
        } else if (this.editionLayer !== null) {
            if (Descartes.EditionManager.isGlobalEditonMode()) {
                this.unregisterToSelection(this.editionLayer.getFeatureOL_layers()[0]);
            } else {
                this.registerToSelection(this.editionLayer.getFeatureOL_layers()[0]);
            }
        }
        Selection.prototype.updateStateWithLayer.apply(this, arguments);
        this.featureSelectionChanged();
    },

    /**
     * Methode: saveObject
     * Enregistrement des attributs saisis.
     *
     * Listener appelé lorsque l'utilisateur clic sur OK.
     * Fait les modifications sur l'objet et sauvegarde les modification en sauvegarde automatique.
     * Sinon change l'aspect de l'objet
     *
     * Paramètres:
     * event - événment Descarte content l'objet à sauvegarder.
     */
    saveObject: function (event) {
        var feature = event.data[0];
        if (feature.get('state') !== FeatureState.INSERT) {
            feature.set('state', FeatureState.UPDATE);
        }

        feature.editionLayer = this.editionLayer;
        if (this.editionLayer && this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                   feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        olLayer.getSource().dispatchEvent('changefeature', {
            feature: feature,
            modified: true
        });

        //Sauvegarde en mode manuel.
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            } else {
                log.warn('Strategy de sauvegarde non affectée. Sauvegarde impossible');
            }
        } else {
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été modifiée
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.create);
            var olStyle = olStyles[feature.getGeometry().getType()];
            feature.setStyle(olStyle);
        }

        this.selectionEditionTool.unselectAll();

    },

    /**
     * Methode: cancel
     * Annulation des attributs saisies.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    cancel: function (feature) {
        /*var olLayer = this.editionLayer.getFeatureOL_layers()[0];

         var style = null;
         var state = feature.get('state');
         if (state === FeatureState.INSERT || state === FeatureState.UPDATE) {
         style = this.createOlStyles[feature.getGeometry().getType()];
         } else {
         style = this.defaultOlStyles[feature.getGeometry().getType()];
         }
         feature.setStyle(style);

         this.deactivate();*/
        this.selectionEditionTool.unselectAll();
        //OpenLayers.Util.removeItem(olLayer.selectedFeatures, feature);
        //olLayer.events.triggerEvent("featureunselected");
    },

    /*
     * Private
     */
    resetNonConformModif: function () {
        var feature = this.editionLayer.getSelectedFeatures()[0];
        if (!_.isNil(feature.getStyle())) {
            feature.setStyle(null);
        }
        this.selectionEditionTool.unselectAll();
    },

    CLASS_NAME: 'Descartes.Tool.Edition.SelectionAttribut'
});

module.exports = Class;

/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');
var log = require('loglevel');

var Utils = Descartes.Utils;
var Edition = require('../../' + MODE + '/Edition');
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var AttributesEditor = require('../../../Action/AttributesEditor');

var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var Symbolizers = require('../../../Symbolizers');

require('../css/attribute.css');

/**
 * Class: Descartes.Tool.Edition.Attribute
 * Outil de saisie des attributs.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /*
     * Propriete: defaultToolOptions
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires pour le fonctionnement de l'outil.
     */
    defaultToolOptions: {
        clickout: false,
        hover: false,
        toggle: false,
        multiple: false,
        toggleKey: 'ctrlKey',
        multipleKey: 'shiftKey'
    },
    /**
     * Propriete: toolOptions
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires pour le fonctionnement de l'outil.
     */
    toolOptions: {},
    /**
     * Select interaction
     */
    interaction: null,

    /**
     * Constructeur: Descartes.Tool.Edition.Attribute
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * toolOptions - {Object} Objet JSON stockant les options d'utilisation de l'outil.
     */
    initialize: function (options) {
        this.toolOptions = _.extend({}, this.defaultToolOptions);
        if (!_.isNil(options)) {
            _.extend(this.toolOptions, options.toolOptions);
            delete options.toolOptions;
        }

        Edition.prototype.initialize.apply(this, arguments);
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Edition.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.underActivation = false;
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Edition.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
        }
    },
    /*
     * Méthode initializeOLFeature
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;
        this.interaction = new ol.interaction.Select({
            layers: [olLayer],
            hitTolerance: 5,
            toggleCondition: ol.events.condition.singleClick,
            multi: this.toolOptions.multiple
        });
        this.interaction.set('id', 'Attribute_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    changeCursorPointerOnFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.olLayer;
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },

    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     * Affichage de la boite de dialogue pour la saisie des attributs.
     *
     * Paramètres:
     * event - l'événement de sélection l'objet sélectionné.
     */
    onSelect: function (event) {
        var features = event.selected;
        if (features.length > 0) {
            var feature = features[0];
            if (!this.isConforme(feature.getGeometry())) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                this.resetNonConformModif(feature);
                return false;
            }

            if (this.isActiveVerifTopoConformity() &&
                    !this.isConformeTopology(feature.getGeometry(), true)) {
                this.resetNonConformModif(feature);
                return false;
            }

            if (this.isActiveVerifAppConformity() &&
                    !this.isConformeRulesApp(feature, true)) {
                this.resetNonConformModif(feature);
                return false;
            }

            this.setFeatureStyle(feature, 'select');

            var featureState = feature.get('state');
            if (featureState !== FeatureState.DELETE) {

                // Creation de la vue
                var viewOptions = {
                    view: AttributesEditorDialog,
                    feature: feature,
                    editionLayer: this.editionLayer
                };

                // Creation de l'action qui va ouvrir la vue
                var action = new AttributesEditor(null, this.map, viewOptions);
                action.events.register('saveObject', this, this.saveObject);
                action.events.register('cancel', this, this.cancel);
                return true;
            }
        }
        return false;
    },

    /**
     * Methode: saveObject
     * Enregistrement des attributs saisis.
     *
     * Listener appelé lorsque l'utilisateur clic sur OK.
     * Fait les modifications sur l'objet et sauvegarde les modification en sauvegarde automatique.
     * Sinon change l'aspect de l'objet
     *
     * Paramètres:
     * event - Evénement Descartes contenant l'objet à sauvegarder.
     */
    saveObject: function (event) {
        var feature = event.data[0];
        var featureState = feature.get('state');
        if (featureState !== FeatureState.INSERT) {
            feature.set('state', FeatureState.UPDATE);
        }

        feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                     feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];

        olLayer.getSource().dispatchEvent('changefeature', {
            feature: feature,
            modified: true
        });
        //Sauvegarde en mode manuel.
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            } else {
                log.warn('Strategy de sauvegarde non affectée. Sauvegarde impossible');
            }
        } else {
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été modifiée
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.create);
            var olStyle = olStyles[feature.getGeometry().getType()];
            feature.setStyle(olStyle);
        }
        this.unselectAll();
    },

    /*
     * Private
     */
    resetNonConformModif: function (feature) {
        if (!_.isNil(feature.getStyle())) {
            feature.setStyle(null);
        }
        this.layer.drawFeature(feature, 'default');
        this.unselectAll();
    },

    /**
     * Methode: cancel
     * Annulation des attributs saisies.
     *
     */
    cancel: function () {
        this.unselectAll();
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        this.editionLayer.unselectAll();
        this.interaction.getFeatures().forEach(function (feature) {
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
        }, this);
        this.interaction.getFeatures().clear();
    },

    CLASS_NAME: 'Descartes.Tool.Edition.Attribute'
});

module.exports = Class;

/* global MODE */
var _ = require('lodash');

var CompositeGlobalModification = require('./Modification/CompositeGlobalModification');
var GlobalModification = require('./Modification/GlobalModification');
var MergeModification = require('./Modification/MergeModification');
var SelectionSubstractModification = require('./Modification/SelectionSubstractModification');
var TranslateModification = require('./Modification/TranslateModification');
var VerticeModification = require('./Modification/VerticeModification');
var GlobalModificationAnnotation = require('./Annotation/GlobalModificationAnnotation');
var VerticeModificationAnnotation = require('./Annotation/VerticeModificationAnnotation');
var AddTextAnnotation = require('./Annotation/AddTextAnnotation');
var StyleAnnotation = require('./Annotation/StyleAnnotation');

var namespace = {
	CompositeGlobalModification: CompositeGlobalModification,
	GlobalModification: GlobalModification,
	MergeModification: MergeModification,
	SelectionSubstractModification: SelectionSubstractModification,
	TranslateModification: TranslateModification,
	VerticeModification: VerticeModification,
	GlobalModificationAnnotation: GlobalModificationAnnotation,
	VerticeModificationAnnotation: VerticeModificationAnnotation,
	AddTextAnnotation: AddTextAnnotation,
	StyleAnnotation: StyleAnnotation
};

var Modification = require('./Modification');

_.extend(Modification, namespace);

module.exports = Modification;

/* global MODE */
var _ = require('lodash');

var IntersectInformation = require('./Information/IntersectInformation');


var namespace = {
	IntersectInformation: IntersectInformation
};

var Information = require('./Information');

_.extend(Information, namespace);

module.exports = Information;

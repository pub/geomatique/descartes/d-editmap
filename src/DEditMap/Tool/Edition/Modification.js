/* global MODE, Descartes */

var Utils = Descartes.Utils;
var Edition = require('../' + MODE + '/Edition');
var AttributesEditor = require('../../Action/AttributesEditor');
var AttributesEditorDialog = require('../../UI/' + MODE + '/AttributesEditorDialog');

require('./css/modification.css');

/**
 * Class: Descartes.Tool.Edition.Modification
 * Classe "abstraite" pour les outils d'édition de modification.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature, save, cancel) {
        feature.editAttribut = true;
        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.map, viewOptions);
        if (save) {
            action.events.register('saveObject', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
    },
    changeCursorPointerOnFeature: function (evt) {
        var map = this.olLayer.map;
        var hit = map.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.olLayer ||
                        aLayer === this.editionLayer.compositeSelectionLayer;
            }.bind(this)
        });
        map.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification'
});

module.exports = Class;

/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Edition = require('../' + MODE + '/Edition');
var Messages = require('../../Messages');
var FeatureState = require('../../Model/FeatureState');
var AttributesEditor = require('../../Action/AttributesEditor');
var MapPreview = require('../../Action/MapPreview');

var AttributesEditorDialog = require('../../UI/' + MODE + '/AttributesEditorDialog');
var MapPreviewDialog = require('../../UI/' + MODE + '/MapPreviewDialog');

require('./css/information.css');

/**
 * Class: Descartes.Tool.Edition.Information
 * Classe "abstraite" pour les outils affichant des informations
 * sur les couches d'édition
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition>
 */
var Class = Utils.Class(Edition, {

    /**
     * Propriete privée
     * Layer temporaire
     */
    _tempLayer: null,
    /**
     * Propriete privée _explodeMultiGeometryInTempLayer
     * Indique si les géométries multiple doivent être transformées
     * en géométrie simple dans le layer temporaire
     */
    _explodeMultiGeometryInTempLayer: false,

    /**
     * Methode: deactivate
     * Appeler lorsque l'outil se désactive
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            this._deactivateSupportLayers(true);
            Edition.prototype.deactivate.apply(this, arguments);
        }
    },
    /**
     * Méthode privée _activateSupportLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et
     * ajoute l'initialisation du layer temporaire.
     */
    _activateSupportLayers: function () {
        Edition.prototype._activateSupportLayers.apply(this, arguments);
        this._initializeSupportTempLayer();
    },
    /**
     * Méthode privée _deactivateSupportLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et
     * supprime le layer temporaire
     */
    _deactivateSupportLayers: function () {
        Edition.prototype._deactivateSupportLayers.apply(this, arguments);
        if (this._tempLayer !== null) {
            this.olMap.removeLayer(this._tempLayer);
            this._tempLayer = null;
        }
    },
    /**
     * Méthode privée _updateSupportsLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et
     * ajoute l'initialisation du layer temporaire.
     */
    _updateSupportsLayers: function () {
        Edition.prototype._updateSupportsLayers.apply(this, arguments);
        this._initializeSupportTempLayer();
    },
    changeCursorPointerOnFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    /**
     * Méthode privée _initializeSupportTempLayer
     * Initialize et ajoute la couche temporaire en fonction
     * des couches supports sélectionnées et du filtre sur les features facultatif.
     */
    _initializeSupportTempLayer: function (featureFilter) {
        if (this.olMap && this.isActive()) {
            var that = this;
            var charged = true;
            //récupération de toutes les features
            var features = [];
            for (var i = 0; i < this._supportLayers.length; i++) {
                var supportLayer = this._supportLayers[i];
                var featureToAddForSupportLayer = [];

                // Si une couche a été chargé après que la couche temporaire
                // ait été créée il faut ajouter les features à la fin du chargement.
                if (this.mapContent.getLayerByOLLayer(supportLayer) !== this.editionLayer) {
                    var supportCharged = supportLayer.get('charged');
                    if (!_.isNil(supportCharged) && supportCharged === false) {
                        charged = false;
                    }

                    supportLayer.getSource().on('featureloadend', function (event) {
                        if (this._tempLayer) {
                            var newFeatures = event.target.getFeatures();
                            if (!_.isNil(newFeatures)) {
                                var result = [];
                                for (var j = 0; j < newFeatures.length; j++) {
                                    var aFeature = newFeatures[j];
                                    var featuresToAdd = that._filterFeatureForTempLayer(aFeature, featureFilter);
                                    result = result.concat(featuresToAdd);
                                }
                                var descartesLayer;
                                if (result.length === 0) {
                                    //griser le bouton de support de l'arbre des couches
                                    descartesLayer = this.mapContent.getLayerByOLLayer(supportLayer);
                                    descartesLayer.supportEnable = false;
                                    this.mapContent.refreshOnlyContentManager();
                                } else {
                                    descartesLayer = this.mapContent.getLayerByOLLayer(supportLayer);
                                    descartesLayer.supportEnable = true;
                                    this._tempLayer.getSource().addFeatures(result);
                                }
                                supportLayer.set('charged', true);
                            }
                        }
                    }.bind(this), this);
                }

                var supportFeatures = supportLayer.getSource().getFeatures();
                for (var j = 0; j < supportFeatures.length; j++) {
                    var feature = supportFeatures[j];
                    var featuresToAdd = this._filterFeatureForTempLayer(feature, featureFilter);
                    featureToAddForSupportLayer = featureToAddForSupportLayer.concat(featuresToAdd);
                }
                if (featureToAddForSupportLayer.length === 0) {
                    //griser le bouton de support de l'arbre des couches.
                    var descartesLayer = this.mapContent.getLayerByOLLayer(supportLayer);
                    descartesLayer.supportEnable = false;
                    this.mapContent.refreshOnlyContentManager();
                }
                features = features.concat(featureToAddForSupportLayer);
            }

            //creation d'une couche temporaire
            if (this._tempLayer === null) {
                this._tempLayer = new ol.layer.Vector({
                    title: 'creationTemp',
                    source: new ol.source.Vector()
                });
                this.olMap.addLayer(this._tempLayer);

            } else {
                this._tempLayer.getSource().clear();
            }

            if (charged && features.length === 0) {
                alert(Messages.Descartes_EDITION_NOT_AVAILABLE_GEOMETRY);
            } else {
                this._tempLayer.getSource().addFeatures(features);
            }
        }
    },
    /**
     * Méthode _filterFeatureForTempLayer
     * Retourne l'ensemble des features eligibles à la couche temporaire.
     */
    _filterFeatureForTempLayer: function (feature, featureFilter) {
        //application du filtre sur une feature
        var result = [];
        if (_.isFunction(featureFilter)) {
            if (!featureFilter.apply(this, [feature])) {
                return result;
            }
        }

        if (feature.get('state') !== FeatureState.DELETE) {
            if (this._explodeMultiGeometryInTempLayer === true &&
                    this.isMultipleGeometry(feature)) {
                for (var k = 0; k < feature.geometry.components.length; k++) {
                    var polygonFeature = feature.clone();
                    polygonFeature.setId(feature.getId() + '_' + k);
                    this.setFeatureStyle(polygonFeature, 'support');
                    result.push(polygonFeature);
                }
            } else {
                var copy = feature.clone();
                copy.setId(feature.getId());
                this.setFeatureStyle(copy, 'support');
                result.push(copy);
            }
        }
        return result;
    },
    isMultipleGeometry: function (feature) {
        var geometry = feature.getGeometry();
        return (geometry instanceof ol.geom.MultiPolygon ||
                geometry instanceof ol.geom.MultiPoint ||
                geometry instanceof ol.geom.MultiLineString);
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature, save, cancel) {
        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.map, viewOptions);
        if (save) {
            action.events.register('saveObject', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
    },

    /*
     * Private
     */
    _showMapPreviewDialog: function (features, save, cancel, title, doNotSelect) {
        //pas de couche de fond pour le moment
        /*var wmsLayer = null;

         for (var i = 0; i < this.map.layers.length; i++) {
         var layer = this.map.layers[i];
         if (layer instanceof ol.layer.WMS) {
         wmsLayer = new OpenLayers.Layer.WMS(layer.name, layer.url, layer.params, layer.options);
         break;
         }
         }*/

        var viewOptions = {
            view: MapPreviewDialog,
            //layers: [wmsLayer], //pas de couche de fond pour le moment
            features: features,
            title: title,
            doNotSelect: doNotSelect
        };

        var action = new MapPreview(null, this.map, viewOptions);
        if (save) {
            action.events.register('save', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
        return action;
    },

    /*
     * Private
     */
    registerToSelection: function (editionLayer) {
        if (this.editionLayer !== editionLayer) {
            if (this.editionLayer !== null) {
                this.unregisterToOLSelection(this.editionLayer.getFeatureOL_layers()[0]);
            }
            if (editionLayer.isUnderEdition()) {
                this.registerToOLSelection(editionLayer.getFeatureOL_layers()[0]);
                if (editionLayer.isCompositeGeometry()) {
                    this.registerToOLSelection(editionLayer.compositeSelectionLayer);
                    this.registerToOLSelection(editionLayer.compositeVectorLayer);
                }
            }
        }
    },

    /**
     * Méthode privée: unregisterToSelection
     * Désabonne l'outil au changement de selection dans la couche passée en paramètre.
     */
    unregisterToSelection: function () {
        if (this.editionLayer !== null) {
            this.unregisterToOLSelection(this.editionLayer.getFeatureOL_layers()[0]);
            if (this.editionLayer.isCompositeGeometry()) {
                this.unregisterToOLSelection(this.editionLayer.compositeSelectionLayer);
                this.unregisterToOLSelection(this.editionLayer.compositeVectorLayer);
            }
        }
    },
    /**
     * Méthode privée: registerToOLSelection
     * Abonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    registerToOLSelection: function (olLayer) {
        var source = olLayer.getSource();
        var features = source.getFeatures();

        _.each(features, function (feature) {
            feature.on('change:selected', this.featureSelectionChanged, this);
        }.bind(this));

        source.on('addfeature', this.registerToAddFeature, this);
        source.on('removefeature', this.registerToRemoveFeature, this);
    },

    registerToAddFeature: function (e) {
        e.feature.on('change:selected', this.featureSelectionChanged, this);
    },

    registerToRemoveFeature: function (e) {
        e.feature.un('change:selected', this.featureSelectionChanged, this);
    },

    /**
     * Méthode privée: unregisterToOLSelection
     * Désabonne l'outil au changement de selection dans la couche passée en paramètre.
     *
     * Paramètres:
     * olLayer : Couche openlayer
     */
    unregisterToOLSelection: function (olLayer) {
        var source = olLayer.getSource();
        var features = source.getFeatures();
        _.each(features, function (feature) {
            feature.un('change:selected', this.featureSelectionChanged, this);
        }.bind(this));

        source.un('addfeature', this.registerToAddFeature, this);
        source.un('removefeature', this.registerToRemoveFeature, this);
    },
    /*
     * Méthode: featureSelectionChanged
     * A implémenter par les classes filles
     */
    featureSelectionChanged: function () {
    },

    CLASS_NAME: 'Descartes.Tool.Edition.Information'
});

module.exports = Class;

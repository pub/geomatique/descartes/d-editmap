/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var AttributesEditor = require('../../../Action/AttributesEditor');
var Messages = require('../../../Messages');
var Symbolizers = require('../../../Symbolizers');
var FeatureState = require('../../../Model/FeatureState');
var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var LayerConstants = Descartes.Layer;
var EditionLayerConstants = require('../../../Model/EditionLayerConstants');

require('ol-ext');
//require('ol-ext-css');
require('../../../OpenLayersExtras/ol-ext/DrawHole');

require('../css/drawCreation.css');


/**
 * Class: Descartes.Tool.Edition.Creation.DrawCreation
 * Outil de création par dessin.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {

    /*
     * private
     */
    createObjectId: null,
    /*
     * private
     */
    createAttributes: null,
    /*
     * private
     */
    compositeFeature: null,

    /**
     * Propriete: charMultiGeom
     * Char - touche du clavier utilisée pour créer directement des objets composites.
     * (Par défaut: la touche 'c')
     */
    charMultiGeom: 'C',
    /**
     * Interaction
     */
    interaction: null,

    /**
     * AutoTracing
     */
    autotracing: false,
    _drawing: false,
    autotracingFeature: null,
    autotracingPreviewLine: null,
    autotracingPreviewVector: null,
    autotracingStartPoint: null,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DrawCreation
     */
    initialize: function () {
        Creation.prototype.initialize.apply(this, arguments);
        this.charMultiGeom = this.charMultiGeom.toUpperCase();
        this._parser = new jsts.io.OL3Parser();
        if (this.geometryType) {
            this.element = Utils.htmlToElement(this.createElement());
        }

    },
    setSelectionEditionTool: function (selectionEditionTool) {
        this.selectionEditionTool = selectionEditionTool;
    },

    /**
     * Methode: activate
     * Appeler lorsque l'outil devient actif.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Creation.prototype.activate.apply(this, arguments);

            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.keydownListener = this.keyboardDownListener.bind(this);
            this.keyupListener = this.keyboardUpListener.bind(this);
            document.addEventListener('keydown', this.keydownListener);
            document.addEventListener('keyup', this.keyupListener);
            this.underActivation = false;
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this.olMap.on('pointermove', this.autotracingPointerMoveFct, this);
                this.olMap.on('click', this.autotracingClickFct, this);
                this.autotracingPreviewLine = new ol.Feature({
                  geometry: new ol.geom.LineString([])
                });
                var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['autotracing']);
                var styleFunction = function (feature, resolution) {
                    var featureStyleFunction = feature.getStyleFunction();
                    if (featureStyleFunction) {
                        return featureStyleFunction.call(feature, resolution);
                    } else {
                        var style = olStyles['LineString'];
                        return style;
                    }
                };
                this.autotracingPreviewVector = new ol.layer.Vector({
                  descartesLayerId: "descartesAutoTracingLayer",
                  source: new ol.source.Vector({
                     features: [this.autotracingPreviewLine]
                  }),
                  style: styleFunction
                });
                this.olMap.addLayer(this.autotracingPreviewVector);
            }
        }
    },
    /**
     * Methode: deactivate
     * Appeler lorsque l'outil est désactivé.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Creation.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
            if (!_.isNil(this.keydownListener)) {
                document.removeEventListener('keydown', this.keydownListener);
            }
            if (!_.isNil(this.keyupListener)) {
                document.removeEventListener('keyup', this.keyupListener);
            }
            if (!_.isNil(this.olMap)) {
                if (!_.isNil(this.interaction)) {
                    this.interaction.setActive(false);
                }
                if (!_.isNil(this.holeInteraction)) {
                    this.holeInteraction.setActive(false);
                }
            }
            this.compositeFeature = null;
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this.olMap.un('pointermove', this.autotracingPointerMoveFct, this);
                this.olMap.un('click', this.autotracingClickFct, this);
                this.olMap.removeLayer(this.autotracingPreviewVector);
                this.autotracingPreviewVector = null;
                this.autotracingPreviewLine = null;
                this.autotracingStartPoint = null;
                this.autotracingFeature = null;
                this._drawing = false;
        }
        }
    },
    isActive: function () {
        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        }
        if (!active && !_.isNil(this.holeInteraction)) {
            active = this.holeInteraction.getActive();
        }
        return active;
    },
    keyboardDownListener: function (evt) {
        switch (evt.keyCode) {
            case 18://alt
                evt.preventDefault();
                //toggle interactions
                this.toggleHoleMode();
                break;
            case 90: //z
                if (evt.metaKey || evt.ctrlKey) {
                    if (this.interaction.getActive()) {
                        this.interaction.removeLastPoint();
                    } else if (this.holeInteraction.getActive()) {
                        this.holeInteraction.removeLastPoint();
                    }
                }
                break;
            case 89: //y
                if (evt.metaKey || evt.ctrlKey) {
                    //TODO pas de moyen simple de récupérer la liste des points
                }
                break;
            case 27 : //esc
                if (this.interaction.getActive()) {
                    this.interaction.abortDrawing_();
                } else if (this.holeInteraction.getActive()) {
                    this.holeInteraction.abortDrawing_();
                }
                if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                    this._drawing = false;
                    this.autotracingFeature = null;
                    this.autotracingPreviewLine.getGeometry().setCoordinates([]);
                    this.autotracingStartPoint = null;
                }
                break;
            case this.charMultiGeom.charCodeAt(0): //c par défaut
                if (this.editionLayer.isCompositeGeometry() &&
                        this.editionLayer.getSelectedFeatures().length === 0 &&
                        _.isNil(this.compositeFeature)) {
                    this.compositeFeature = new ol.Feature({
                        state: FeatureState.INSERT
                    });
                    this.olLayer.getSource().addFeature(this.compositeFeature);

                }
                break;
        }
    },
    keyboardUpListener: function (evt) {
        switch (evt.keyCode) {
            case this.charMultiGeom.charCodeAt(0): //c par défaut
                if (this.editionLayer.isCompositeGeometry() &&
                        this.editionLayer.getSelectedFeatures().length === 0 &&
                        !_.isNil(this.compositeFeature.getGeometry())) {
                    this.saveCompositeFeature();
                    this.compositeFeature = null;
                }
                break;
        }
    },

    saveCompositeFeature: function () {
        if (this.compositeFeature) {
            if (this.editAttribut) {
                this.compositeFeature['editAttribut'] = this.editAttribut;
                // Creation de la vue
                var viewOptions = {
                    view: AttributesEditorDialog,
                    editionLayer: this.editionLayer,
                    feature: this.compositeFeature
                };

                // Creation de l'action qui va ouvrir la vue
                var action = new AttributesEditor(null, this.olMap, viewOptions);
                action.events.register('saveObject', this, function (e) {
                    var feature = e.data[0];
                    this.saveObject(feature);
                });
                action.events.register('cancel', this, function (e) {
                    var feature = e.data[0];
                    this.olLayer.getSource().removeFeature(feature);
                });
            } else {
                this.saveObject(this.compositeFeature);
            }
        }
    },

    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer {<OpenLayers.Control.DrawFeature.prototype>}
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;
        var olHandler;
        if (this.editionLayer.type === EditionLayerConstants.TYPE_GenericVector) {
           if (this.geometryType) {
                olHandler = this._getOLHandlerFromGeometryType(this.geometryType);
           } else {
                olHandler = this._getOLHandlerFromGeometryType(LayerConstants.POLYGON_GEOMETRY);
           }
        } else {
            olHandler = this._getOLHandlerFromGeometryType(this.editionLayer.geometryType);
        }
        var temporaryTypes = Symbolizers.getOlStyle(this.editionLayer.symbolizers['temporary']);

        if (!_.isNil(this.olMap)) {
            if (!_.isNil(this.interaction)) {
                this.olMap.removeInteraction(this.interaction);
            }
            if (!_.isNil(this.holeInteraction)) {
                this.olMap.removeInteraction(this.holeInteraction);
            }
        }

        var geomType;
        if (this.editionLayer.type === EditionLayerConstants.TYPE_GenericVector) {
            if (this.geometryType) {
                geomType = this.geometryType;
            } else {
                geomType = LayerConstants.POLYGON_GEOMETRY;
            }
        } else {
            geomType = this.editionLayer.geometryType;
        }
        if (_.endsWith(geomType, 'Line')) {
            geomType += 'String';
        }
        var style = temporaryTypes[geomType];
        this.interaction = new ol.interaction.Draw({
            source: olLayer.getSource(),
            //geometryName: 'geom', //TO FIX
            type: olHandler.type,
            style: style
        });
        this.interaction.set('id', 'DrawCreation_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawstart', function (e) {
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this._drawing = true;
            }
        }.bind(this));
        this.interaction.on('drawend', function (e) {
            var feature = e.feature;
            if (this.drawFeature(feature) === false) {
                setTimeout(function () {
                    olLayer.getSource().removeFeature(feature);
                }, 100);
            }
            if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
                this._drawing = false;
                this.autotracingFeature = null;
                this.autotracingPreviewLine.getGeometry().setCoordinates([]);
                this.autotracingStartPoint = null;
            }
        }.bind(this));

        this.interaction.on('finishdrawing', function (e) {
            if (Descartes.EditionManager.autoSave && _.isNil(this.compositeFeature) && !this.editAttribut) {
                this.interaction.source_.clear();
            }
        }.bind(this));

        if (this.snapping && this.autotracing && this.editionLayer.snapping && this.editionLayer.snapping.autotracing) {
            this.interaction.autotracingAppendCoordinates = function (coordinateExtension) {
                var mode = this.mode_;
                let coordinates = [];
                if (mode === ol.interaction.Draw.Mode_.LINE_STRING) {
                  coordinates = /** @type {LineCoordType} */ this.sketchCoords_;
                } else if (mode === ol.interaction.Draw.Mode_.POLYGON) {
                  coordinates = this.sketchCoords_ && this.sketchCoords_.length ? /** @type {PolyCoordType} */ (this.sketchCoords_)[0] : [];
                }

                // Remove last coordinate from sketch drawing (this coordinate follows cursor position)
                const ending = coordinates.pop();

                // Append coordinate list
                for (let i = 0; i < coordinateExtension.length; i++) {
                  this.autotracingAddToDrawing_(coordinateExtension[i]);
                }

                // Duplicate last coordinate for sketch drawing
                this.autotracingAddToDrawing_(ending);
            };
            this.interaction.autotracingAddToDrawing_ = function (coordinate) {
                  var geometry = /** @type {ol.geom.SimpleGeometry} */ (this.sketchFeature_.getGeometry());
                  var done;
                  var coordinates;
                  if (this.mode_ === ol.interaction.Draw.Mode_.LINE_STRING) {
                    this.finishCoordinate_ = coordinate.slice();
                    coordinates = this.sketchCoords_;
                    if (coordinates.length >= this.maxPoints_) {
                        if (this.freehand_) {
                           coordinates.pop();
                        } else {
                           done = true;
                        }
                    }
                    coordinates.push(coordinate.slice());
                    this.geometryFunction_(coordinates, geometry);
                   } else if (this.mode_ === ol.interaction.Draw.Mode_.POLYGON) {
                    coordinates = this.sketchCoords_[0];
                    if (coordinates.length >= this.maxPoints_) {
                      if (this.freehand_) {
                          coordinates.pop();
                      } else {
                          done = true;
                      }
                    }
                    coordinates.push(coordinate.slice());
                    if (done) {
                       this.finishCoordinate_ = coordinates[0];
                    }
                    this.geometryFunction_(this.sketchCoords_, geometry);
                  }
                  this.updateSketchFeatures_();
                  if (done) {
                     this.finishDrawing();
                  }
               };
        }

        this.holeInteraction = new ol.interaction.DrawHole({
            layers: [olLayer],
            style: new ol.style.Style({
                image: new ol.style.RegularShape({
                    fill: new ol.style.Fill({
                        color: 'red'
                    }),
                    points: 4,
                    radius1: 10,
                    radius2: 1
                }),
                fill: ol.style.Fill({
                    color: 'red'
                }),
                stroke: new ol.style.Stroke({
                    color: 'red'
                })
            })
        });
        this.holeInteraction.set('id', 'DrawHoleCreation_' + Utils.createUniqueID());
        this.holeInteraction.setActive(false);
        //pb avec l'interaction hole
        this.holeInteraction.un('drawend', this.holeInteraction._finishDrawing, this.holeInteraction);
        this.holeInteraction.on('drawstart', function (e) {
            this.holedFeature = e.target.getPolygon();
        }.bind(this));
        this.holeInteraction.on('drawend', this.onHoleDrawn.bind(this));


        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     * Retourne la liste des intéractions nécessaire au fonctionnament de l'outil en plus
     * de l'interaction principale.
     * @returns {Array}
     */
    getAdditionnalInterations: function () {
        return [this.holeInteraction];
    },
    /**
     * Methode privé: onHoleDrawn
     * permet de savoir quand l'utilisateur a dessiné un trou et de sauver la modification
     */
    onHoleDrawn: function (e) {
        var feature = e.feature;
        var geom = feature.getGeometry();

        var jstsGeom = this._parser.read(geom);
        var jstsHoledGeom = this._parser.read(this.holedFeature.getGeometry());
        var jstsResult = jstsHoledGeom.symDifference(jstsGeom);
        if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY && jstsResult instanceof jsts.geom.Polygon) {
            jstsResult = new jsts.geom.MultiPolygon([jstsResult], jstsResult.getFactory());
        }
        var result = this._parser.write(jstsResult);
        this.holedFeature.setGeometry(result);


        this.holedFeature.set('state', FeatureState.UPDATE);
        this.holedFeature.editionLayer = this.editionLayer;
        this.setFeatureStyle(this.holedFeature, 'modify');
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(this.holedFeature);
            }
        }
        this.toggleHoleMode();
    },
    toggleHoleMode: function () {
        this.holeInteraction.setActive(!this.holeInteraction.getActive());
        this.interaction.setActive(!this.interaction.getActive());
    },
    /*
     * Method: drawFeature
     * Idem que celle d'openLayers avec en plus l'ajout de l'attribut identifiant
     */
    drawFeature: function (feature) {
        var geometry = feature.getGeometry();
        var update = false;

        if (this.editionLayer.isCompositeGeometry()) {
            var tempGeom;
            if (this.editionLayer.getSelectedFeatures().length === 1) {
                //Si une geometrie est selectionné et que le layer est de type layer d'objets composite alors
                //On ajoute la nouvel geometrie é l'objet sélectionné
                var selectedFeature = this.editionLayer.getSelectedFeatures()[0];
                tempGeom = selectedFeature.getGeometry().clone();
                var appendedGeom = this.appendGeometry(tempGeom, geometry);
                selectedFeature.setGeometry(appendedGeom);
                if (!this.validFeature(selectedFeature)) {
                    selectedFeature.setGeometry(tempGeom);
                    setTimeout(function () {
                        var source = this.olLayer.getSource();
                        source.removeFeature(feature);
                    }.bind(this), 50);
                    return false;
                }

                var sketchFeature = feature;
                setTimeout(function () {
                    this.olLayer.getSource().removeFeature(sketchFeature);
                }.bind(this), 100);
                //la feature à sauvegarder est celle selectionée
                feature = selectedFeature;
                update = true;
                // un nouvel objet "créé" doit toujours rester dans l'état "INSERT" tant qu'il n'y a pas eu de sauvegarde
                if (feature.get('state') === FeatureState.INSERT) {
                    update = false;
                }
            } else if (!_.isNil(this.compositeFeature)) {
                tempGeom = this.compositeFeature.getGeometry();
                if (_.isNil(tempGeom)) {
                    tempGeom = geometry;
                } else {
                    tempGeom = this.compositeFeature.getGeometry().clone();
                    tempGeom = this.appendGeometry(tempGeom, geometry);
                }
                this.compositeFeature.setGeometry(tempGeom);
                if (!this.validFeature(this.compositeFeature)) {
                    setTimeout(function () {
                        var source = this.olLayer.getSource();
                        source.removeFeature(feature);
                        source.removeFeature(this.compositeFeature);
                        this.compositeFeature = null;
                    }.bind(this), 50);
                    return false;
                }
                this.setFeatureStyle(feature, 'create');
                this.setFeatureStyle(this.compositeFeature, 'create');

                setTimeout(function () {
                    var source = this.olLayer.getSource();
                    source.removeFeature(feature);
                    source.removeFeature(this.compositeFeature);
                    source.addFeature(this.compositeFeature);
                }.bind(this), 50);

                return true;
            } else if (!this.validFeature(feature)) {
                return false;
            }
        } else if (!this.validFeature(feature)) {
            return false;
        }

        var objectId = this.createObjectId;

        //ajout de l'identifiant et de la valeur s'il existe.
        if (objectId !== null && !update) {
            feature.set('idAttribut', objectId);
        }

        if (update) {
            feature.set('state', FeatureState.UPDATE);
            if (this.editionLayer &&
                    this.editionLayer.attributes &&
                    this.editionLayer.attributes.attributeId &&
                    this.editionLayer.attributes.attributeId.fieldName !== null) {
                         feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
            }
        } else {
            feature.set('state', FeatureState.INSERT);
        }

        if (this.editAttribut && this.createAttributes === null) {
            feature['editAttribut'] = this.editAttribut;
            // Creation de la vue
            var viewOptions = {
                view: AttributesEditorDialog,
                editionLayer: this.editionLayer,
                feature: feature
            };

            // Creation de l'action qui va ouvrir la vue
            var action = new AttributesEditor(null, this.olMap, viewOptions);
            action.events.register('saveObject', this, function (e) {
                var feature = e.data[0];
                this.saveObject(feature);
            });
            action.events.register('cancel', this, function (e) {
                var feature = e.data[0];
                this.olLayer.getSource().removeFeature(feature);
            });
        } else {
            if (!_.isNil(this.createAttributes)) {
                feature['editAttribut'] = true;
                for (var i = 0; i < this.createAttributes.length; i++) {
                    var attribut = this.createAttributes[i];
                    feature.set(attribut.fieldName, attribut.value);
                }
            }
            this.saveObject(feature);
        }

        return true;
    },
    validFeature: function (feature) {
        var geometry = feature.getGeometry();
        if (!this.isConforme(geometry)) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            return false;
        }
        if (this.editionLayer.isCompositeGeometry() && (!_.isNil(this.compositeFeature) || this.editionLayer.getSelectedFeatures().length === 1)) {
            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(geometry, true)) {
                return false;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(feature, true)) {
                return false;
            }
        } else {
            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(geometry)) {
                return false;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(feature)) {
                return false;
            }
        }
        return true;
    },
    /**
     * Methode: saveObject
     * Enregistrement de l'objet créé.
     *
     * Paramètres:
     * feature - l'objet à sauvegarder.
     */
    saveObject: function (feature) {
        this.createObjectId = null;
        this.createAttributes = null;
        feature.editionLayer = this.editionLayer;
        feature.modified = false;
        var style = Utils.getStyleByState(feature);
        this.setFeatureStyle(feature, style);
        //mode sauvegarde manuel
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }
        if (!_.isNil(this.selectionEditionTool)) {
            this.selectionEditionTool.unselectAll();
        }

        return true;
    },
    /**
     * Methode: getNewMultiGeometry
     * Retourne une multi-géométrie lorsque l'on en dessine une avec la touche 'C'
     *
     * paramètres:
     * feature: la feature dessinée que l'on ajoute à la nouvelle géométrie
     */
    getNewMultiGeometry: function (feature) {
        switch (this.editionLayer.geometryType) {
            case LayerConstants.MULTI_POINT_GEOMETRY:
                return new ol.geom.MultiPoint([feature.geometry.components[0]]);
            case LayerConstants.MULTI_LINE_GEOMETRY:
                return new ol.geom.MultiLineString([feature.geometry.components[0]]);
            case LayerConstants.MULTI_POLYGON_GEOMETRY:
                return new ol.geom.MultiPolygon([feature.geometry.components[0]]);
        }
        return null;
    },
    /**
     *
     * Attention, appendPolygon ne fonctionne pas.
     *
     * Paramètre :
     * multiGeom : la géométrie source
     * geom : la géométrie à ajouter
     * Retourne la nouvelle geometrie formée
     */
    appendGeometry: function (multiGeom, geom) {
        var coordinates;
        if (multiGeom instanceof ol.geom.MultiPolygon) {

            coordinates = [];
            _.each(multiGeom.getCoordinates(), function (coord) {
                coordinates.push(coord);
            });
            _.each(geom.getCoordinates(), function (coord) {
                coordinates.push(coord);
            });
            return new ol.geom.MultiPolygon(coordinates);
        } else if (multiGeom instanceof ol.geom.MultiPoint) {
            multiGeom.appendPoint(geom.getPoint(0));
            return multiGeom;
        } else if (multiGeom instanceof ol.geom.MultiLineString) {
            coordinates = [];
            _.each(multiGeom.getCoordinates(), function (coord) {
                coordinates.push(coord);
            });
            _.each(geom.getCoordinates(), function (coord) {
                coordinates.push(coord);
            });
            return new ol.geom.MultiLineString(coordinates);
        }
        return null;
    },
    changeCursorPointerOnFeature: function (evt) {
        if (this.holeInteraction.getActive()) {
            var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
                layerFilter: function (aLayer) {
                    return aLayer === this.editionLayer.getFeatureOL_layers()[0];
                }.bind(this)
            });
            this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
        }
    },
    autotracingPointerMoveFct: function (evt) {
        if (this.autotracingFeature && this._drawing) {
            var coord = null;
            var that = this;
            this.olMap.forEachFeatureAtPixel(
                evt.pixel,
                function (feature) {
                     if (that.autotracingFeature === feature) {
                         coord = that.olMap.getCoordinateFromPixel(evt.pixel);
                     }
                },
                {
                    hitTolerance: 10,
                    layerFilter: that.autotracingLayerFilterFct.bind(that)
                }
            );
            var previewCoords = [];
            if (coord) {
                var endPoint = this.autotracingFeature.getGeometry().getClosestPoint(coord);
                previewCoords = Descartes.Utils.getPartialRingCoords(this.autotracingFeature, this.autotracingStartPoint, endPoint);
            }
            this.autotracingPreviewLine.getGeometry().setCoordinates(previewCoords);
        }
    },
    autotracingClickFct: function (evt) {
      if (!this._drawing) {
          return;
       }

      var hit = false;
      var that = this;
      this.olMap.forEachFeatureAtPixel(
         evt.pixel,
         function (feature) {
             if (that.autotracingFeature && feature !== that.autotracingFeature) {
                that.autotracingFeature = feature;
                that.autotracingStartPoint = that.autotracingFeature.getGeometry().getClosestPoint(that.olMap.getCoordinateFromPixel(evt.pixel));
                return;
             }

             hit = true;

             var coord = that.olMap.getCoordinateFromPixel(evt.pixel);
             // second click on the tracing feature: append the ring coordinates
             if (feature === that.autotracingFeature) {
                 var endPoint = that.autotracingFeature.getGeometry().getClosestPoint(coord);
                 var appendCoords = Descartes.Utils.getPartialRingCoords(that.autotracingFeature, that.autotracingStartPoint, endPoint);
                 that.interaction.removeLastPoint();
                 that.interaction.autotracingAppendCoordinates(appendCoords);
             }
             // start tracing on the feature ring
             that.autotracingFeature = feature;
             that.autotracingStartPoint = that.autotracingFeature.getGeometry().getClosestPoint(coord);
        },
        {
             hitTolerance: 10,
             layerFilter: that.autotracingLayerFilterFct.bind(that)
         }
      );

      if (!hit) {
            // clear current tracing feature & preview
            this.autotracingPreviewLine.getGeometry().setCoordinates([]);
      }
    },
    autotracingLayerFilterFct: function (layer) {
        var rtn = false;
        var targets = this._getSnappingTargets(false);
        _.each(targets, function (target) {
            if (layer === target.layer && target.layer.autotracingEnable) {
                rtn = true;
            }
        });
        return rtn;
    },
    /**
     * Méthodes privées de gestion des couches supports
     * Pour l'outil draw, il n'y a rien à faire.
     */
    _activateSupportLayers: function () {},
    _deactivateSupportLayers: function () {},
    _updateSupportsLayers: function () {},
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.DrawCreation'
});
module.exports = Class;

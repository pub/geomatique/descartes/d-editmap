/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');
var log = require('loglevel');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var Symbolizers = require('../../../Symbolizers');

var AttributesEditor = require('../../../Action/AttributesEditor');
var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var DecartesEvent = Descartes.Utils.DescartesEvent;

require('../css/cloneCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.CloneCreation
 * Outil de création par clonage.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {
    /**
     * Propriete : confirm
     * Affiche une message de confirmation lors de la copie.
     */
    confirm: true,
    initializeOLFeature: function () {
        this.interaction = new ol.interaction.Select({
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            hitTolerance: 5,
            toggleCondition: ol.events.condition.click,
            multi: false
        });
        this.interaction.set('id', 'CloneCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (Descartes.EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }
            Creation.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);
            this.underActivation = false;
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            //this.unselectAll();
            Creation.prototype.deactivate.apply(this, arguments);
            if (Descartes.EditionManager.onlyAppMode) {
                this.editionLayer = null;
            }
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode global
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer.clone;
            } else {
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.clone;
        }
        //Creation.prototype.updateStateWithLayer.apply(this, arguments);
        //contrôle de la configuration
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        }
    },
    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     * La selection est clonée
     *
     * Paramètres:
     * feature - l'objet sélectionné à cloner.
     */
    onSelect: function (event) {
        var features = event.selected;
        if (features.length !== 1) {
            return false;
        }
        var feature = features[0];

        if (!this.isConforme(feature.getGeometry())) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            return false;
        }

        var clone = feature.clone();
        clone.set('state', FeatureState.INSERT);

        var initStyle = feature.getStyle();

        var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.select);
        var olStyle = olStyles[clone.getGeometry().getType()];
        feature.setStyle(olStyle);

        if (!this.confirm || confirm(this.getMessage('CONFIRM'))) {
            feature.setStyle(initStyle);

            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(feature.getGeometry())) {
                //feature.layer.redraw();
                this.unselectAll();
                return false;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(feature)) {
                //feature.layer.redraw();
                this.unselectAll();
                return false;
            }

            if (this.editAttribut) {
                this._showAttributesDialog(clone);
            } else {
                this.saveObject(clone);
            }
        } else {
            feature.setStyle(initStyle);
        }
        this.unselectAll();
        return true;
    },
    _initializeSupportTempLayer: function () {
        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        }

        if (this.olMap && active) {
            var charged = true;

            var cloneFeatures = [];
            for (var i = 0; i < this._supportLayers.length; i++) {
                var supportOlLayer = this._supportLayers[i];

                if (this.mapContent.getLayerByOLLayer(supportOlLayer) !== this.editionLayer) {
                    if (!_.isNil(supportOlLayer.charged) && supportOlLayer.charged === false) {
                        charged = false;
                    }

                    supportOlLayer.getSource().on('featureloadend', function (event) {
                        if (this._tempLayer) {
                            var newFeatures = event.target.getFeatures();
                            if (!_.isNil(newFeatures)) {
                                var result = [];
                                var descartesLayer;
                                for (var j = 0; j < newFeatures.length; j++) {
                                    var clone = newFeatures[j].clone();
                                    this.setFeatureStyle(clone, 'support');
                                    result.push(clone);

                                    //si aux moins un attribut dans la feature à cloner (en plus de la géométrie).
                                    if (clone.getProperties().length > 1) {
                                        //recherche d'un mapping
                                        var mappingList = null;
                                        if (!_.isNil(this._toolConfig) && !_.isNil(this._toolConfig.supportLayers)) {
                                            for (var k = 0; k < this._toolConfig.supportLayers.length; k++) {
                                                var supportLayerDef = this._toolConfig.supportLayers[k];
                                                descartesLayer = this.mapContent.getLayerByOLLayer(supportOlLayer);
                                                if (!_.isNil(descartesLayer) && supportLayerDef.id === descartesLayer.id) {
                                                    mappingList = supportLayerDef.attributes;
                                                }
                                            }
                                        }
                                        this._copyAttributes(clone, mappingList);
                                    }
                                }

                                if (result.length === 0) {
                                    //griser le bouton de support de l'arbre des couches
                                    descartesLayer = this.mapContent.getLayerByOLLayer(supportOlLayer);
                                    descartesLayer.supportEnable = false;
                                    this.mapContent.refreshOnlyContentManager();
                                } else {
                                    descartesLayer = this.mapContent.getLayerByOLLayer(supportOlLayer);
                                    descartesLayer.supportEnable = true;
                                    this.mapContent.refreshOnlyContentManager();
                                    this._tempLayer.getSource().addFeatures(result);
                                }
                                supportOlLayer.charged = true;
                            }
                        }
                    }.bind(this), this);
                }

                var supportFeatures = supportOlLayer.getSource().getFeatures();
                for (var j = 0; j < supportFeatures.length; j++) {
                    var clone = supportFeatures[j].clone();
                    this.setFeatureStyle(clone, 'support');
                    cloneFeatures.push(clone);

                    if (this.mapContent.getLayerByOLLayer(supportOlLayer) !== this.editionLayer) {
                        //si aux moins un attribut dans la feature à cloner.
                        if (Object.getOwnPropertyNames(clone.getProperties()).length > 1) {
                            //recherche d'un mapping
                            var mappingList = null;
                            if (!_.isNil(this._toolConfig) && !_.isNil(this._toolConfig.supportLayers)) {
                                for (var k = 0; k < this._toolConfig.supportLayers.length; k++) {
                                    var supportLayerDef = this._toolConfig.supportLayers[k];
                                    var descartesLayer = this.mapContent.getLayerByOLLayer(supportOlLayer);
                                    if (!_.isNil(descartesLayer) && supportLayerDef.id === descartesLayer.id) {
                                        mappingList = supportLayerDef.attributes;
                                        break;
                                    }
                                }
                            }
                            this._copyAttributes(clone, mappingList);
                        }
                    } else {
                        //On supprime les attributs qu'il ne faut pas cloner
                        if (this._toolConfig && this._toolConfig.attributsNotClonable) {
                            _.each(this._toolConfig.attributsNotClonable, function (notClonableAtt) {
                                clone.unset(notClonableAtt.fieldName);
                            });
                        }
                    }


                }
            }
            if (this._tempLayer === null) {
                this._tempLayer = new ol.layer.Vector({
                    title: 'creationTemp',
                    source: new ol.source.Vector()
                });
                this.olMap.addLayer(this._tempLayer);
            } else {
                this._tempLayer.getSource().clear();
            }
            if (charged && cloneFeatures.length === 0) {
                alert(this.getMessage('NO_OBJECT'));
            } else {
                this._tempLayer.getSource().addFeatures(cloneFeatures);
            }
        }
    },
    /**
     * Méthode _copyAttributes
     * Méthode privé qui permet de copier les attributs d'une feature à une autre
     * selon le mapping définit par la couche.
     */
    _copyAttributes: function (clone, mappingList) {
        var i, att, attributeValue;
        var resultAtt = [];
        for (var attribute in clone.getProperties()) {
            //si un mapping existe
            var found = false;
            if (!_.isNil(mappingList)) {
                for (i = 0; i < mappingList.length; i++) {
                    var mapping = mappingList[i];
                    if (mapping.from === attribute) {
                        attributeValue = clone.get(attribute);
                        att = {
                            label: mapping.to,
                            value: attributeValue
                        };
                        resultAtt.push(att);
                        log.info('add Attribute from mapping %s=%s', mapping.to, attributeValue);
                        found = true;
                        break;
                    }
                }
            }
            //si pas trouvé ou pas de mapping alors on cherche si la couche possède
            //un attribut ayant le même nom
            if (!found && this.editionLayer.attributes && this.editionLayer.attributes.attributesEditable) {
                for (i = 0; i < this.editionLayer.attributes.attributesEditable.length; i++) {
                    var attEditable = this.editionLayer.attributes.attributesEditable[i];
                    if (attEditable.fieldName === attribute) {
                        attributeValue = clone.get(attribute);

                        att = {
                            label: attribute,
                            value: attributeValue
                        };
                        resultAtt.push(att);
                        log.info('add Attribute %s=%s', attribute, attributeValue);
                        break;
                    }
                }
            }
        }

        var geoAtt = clone.getGeometryName();
        _.each(clone.getKeys(), function (key) {
            if (key !== geoAtt) {
                clone.unset(key);
            }
        });
        _.each(resultAtt, function (att) {
            clone.set(att.label, att.value);
        });
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature) {
        feature['editAttribut'] = true;

        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.olMap, viewOptions);
        action.events.register('saveObject', this, this.saveObjectAfterDialog);
        action.events.register('cancel', this, this.cancel);
    },
    saveObjectAfterDialog: function (e) {
        this.saveObject(e.data[0]);
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveObject: function (feature) {
        var objectId = this.createObjectId;
        if (!_.isNil(objectId)) {
            feature.set('idAttribut', objectId);
        }

        this.createObjectId = null;
        this.createAttributes = null;

        feature['editAttribut'] = true;
        feature.editionLayer = this.editionLayer;

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        if (feature.get('state') === FeatureState.INSERT) {
            olLayer.getSource().addFeature(feature);
        }

        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            saveStrategy.save();
        } else {
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été créée
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.create);
            var olStyle = olStyles[this.editionLayer.geometryType];
            feature.setStyle(olStyle);
        }

        this.deactivate();
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule le clonage.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    cancel: function (feature) {
        this.unselectAll();
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        this.editionLayer.unselectAll();
        this.interaction.getFeatures().clear();
    },
    changeCursorPointerOnTempFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this)
        });
        var self = this;
        var feature = this.olMap.forEachFeatureAtPixel(evt.pixel, function (f, layer) {
            if (layer === self._tempLayer) {
                _.each(self._tempLayer.getSource().getFeatures(), function (f) {
                    self.setFeatureStyle(f, 'support');
                });
                return f;
            }
            return null;
        });
        if (feature) {
            this.setFeatureStyle(feature, 'select');
        } else {
            _.each(this._tempLayer.getSource().getFeatures(), function (feature) {
                this.setFeatureStyle(feature, 'support');
            }.bind(this));
        }
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.CloneCreation'
});

module.exports = Class;

/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;

var AttributesEditor = require('../../../Action/AttributesEditor');
var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var BufferEditor = require('../../../Action/BufferEditor');
var DecartesEvent = Descartes.Utils.DescartesEvent;

/**
 * Class: Descartes.Tool.Edition.Creation.BufferCreation
 * Classe "abstraite" d'outil de création par extension / réduction d'une géométrie existante.
 * Les instances sont de type BufferHaloCreation ou BufferNormalcreation
 *
 * Hérite de:
 * - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {
    /**
     * Propriete: confirm
     * Affiche une message de confirmation
     * avant la transformation.
     */
    confirm: true,
    /**
     * Propriete : explodeMultiGeometry
     * active ou non la transformation les objets supports composites en objets supports simples
     */
    explodeMultiGeometry: true,

    /**
     * Propriete : aggregateResult
     * Si true, les buffers créés sont aggrégés lors de la validation (click sur la carte)
     */
    aggregateResult: false,

    /**
     * Propriete : metersProj définit la projection en mètres dont nous avons besoin pour convertir
     * la distance des mètres en degrés et vice-versa
     */
    metersProj: 'EPSG:3857',
    /**
     * Propriete : bufferingOpt définit les options à prendre en compte au moment de créer le buffer.
     * Contient la distance et, si buffer simple, le nombre de points nécessaire pour créer un arc de cercle.
     */
    bufferOptions: null,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.BufferCreation
     * Constructeur d'instances
     */
    initialize: function (options) {
        Creation.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
        this._parser = new jsts.io.OL3Parser();
    },
    /*
     * Methode privé: initializeOLFeature Initialise l'outil
     * openLayer {<ol.interaction.Select>}
     *
     */
    initializeOLFeature: function () {
        this.interaction = new ol.interaction.Select({
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            hitTolerance: 5,
            multi: true,
            toggleCondition: function (event) {
                if (event.type === 'singleclick') {
                    var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this._tempLayer;
                        }.bind(this)
                    });
                    return !_.isNil(features);
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'BufferCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (Descartes.EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }
            this._showBufferDialog();

            this._explodeMultiGeometryInTempLayer = true;
            if (!this.explodeMultiGeometry) {
                this._explodeMultiGeometryInTempLayer = false;
            }
            Creation.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            //this.unselectAll();
            Creation.prototype.deactivate.apply(this, arguments);
            if (Descartes.EditionManager.onlyAppMode) {
                this.editionLayer = null;
            }
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);
        }
    },

    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode global
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer[this.configKey];
            } else {
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer[this.configKey];
        }

        //contrôle de la configuration
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        } else if (Descartes.EditionManager.isGlobalEditonMode() &&
                editionLayer.geometryType !== LayerConstants.POLYGON_GEOMETRY &&
                editionLayer.geometryType !== LayerConstants.MULTI_POLYGON_GEOMETRY) {
            this.setAvailable(false);
            this.updateElement();
        } else if (!_.isNil(this.editionLayer) &&
                this.editionLayer.geometryType !== LayerConstants.POLYGON_GEOMETRY &&
                this.editionLayer.geometryType !== LayerConstants.MULTI_POLYGON_GEOMETRY) {
            this.setAvailable(false);
            this.updateElement();
        }
    },
    addAttributesToSupportFeatures: function (features, supportOlLayer) {
        var supportLayerId = supportOlLayer.get('descartesLayerId');
        _.each(features, function (feature) {
            feature.set('originLayerId', supportLayerId);
        });
    },
    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     *
     * Paramètres:
     * feature - l'objet sélectionné à cloner.
     */
    onSelect: function (event) {
        _.each(event.selected, function (feature) {
            this.setFeatureStyle(feature, 'select');
        }.bind(this));

        _.each(event.deselected, function (feature) {
            this.setFeatureStyle(feature, 'support');
        }.bind(this));


        if (event.selected.length === 0) {
            if (event.deselected.length > 0) {
                var clickout = true;
                if (event.deselected.length === 1) {
                    var features = this.olMap.forEachFeatureAtPixel(event.mapBrowserEvent.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this._tempLayer;
                        }.bind(this)
                    });
                    clickout = _.isNil(features);
                }

                if (clickout) {
                    this.clickoutFeature(event.deselected);
                }
            }
        }
    },
    /** Méthode clickoutFeature
     * Appelée lorsque l'utilisateur clic en dehors de toute géométrie
     */
    clickoutFeature: function (features) {
        if (this.bufferOptions) {
            this._bufferingSelection(features);
        }
    },
    /**
     * Méthode _bufferingSelection
     * Bufferise la selection courante du layer temporaire.
     */
    _bufferingSelection: function (features) {
        var result = [];
        var i, feature;

        for (i = 0; i < features.length; i++) {
            feature = features[i];
            result = result.concat(this.buffering(feature.getGeometry().clone(), this.getPonderation(feature)));
        }

        for (i = 0; i < result.length; i++) {
            if (!this.isConforme(result[i].getGeometry()) || _.isNil(result[i])) {
                this.unselectAll();
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                return;
            }
        }

        if (this.aggregateResult) {
            result = this.aggregateBuffers(result);
            //On teste la validité avant et après l'agrgégation.
            for (i = 0; i < result.length; i++) {
                feature = result[i];
                if (!this.isConforme(feature.getGeometry())) {
                    this.unselectAll();
                    alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                    return;
                }
                feature.set('state', FeatureState.INSERT);
            }
        } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
            for (i = 0; i < result.length; i++) {
                feature = result[i];
                var aGeometry = feature.getGeometry();
                var coordinates = aGeometry.getCoordinates();
                var clone = aGeometry.clone();
                if (aGeometry instanceof ol.geom.Polygon) {
                    clone = new ol.geom.MultiPolygon([coordinates]);
                }
                feature.setGeometry(clone);
                feature.set('state', FeatureState.INSERT);
            }
        }

        if (!this.confirm ||
                confirm('Etes-vous sûr de vouloir créer un nouvel objet en étendant ou réduisant ces objets ?')) {

            if (this.isActiveVerifTopoConformity()) {
                for (i = 0; i < result.length; i++) {
                    if (!this.isConformeTopology(result[i].getGeometry())) {
                        this.unselectAll();
                        return;
                    }
                }
            }

            if (this.isActiveVerifAppConformity()) {
                for (i = 0; i < result.length; i++) {
                    if (!this.isConformeRulesApp(result[i])) {
                        this.unselectAll();
                        return;
                    }
                }
            }

            if (this.editAttribut && result.length === 1) {
                feature = result[0];
                this._showAttributesDialog(feature);
            } else {
                this.saveObjects(result);
            }
        }
        this.unselectAll();
    },
    /**
     * Methode getPonderation
     * get Ponderation for a feature
     */
    getPonderation: function (feature) {
        var ponderation = null;
        if (!_.isNil(this._toolConfig.supportLayers)) {
            for (var i = 0; i < this._toolConfig.supportLayers.length; i++) {
                var supportLayerConfig = this._toolConfig.supportLayers[i];
                if (supportLayerConfig.attribute) {
                    if (supportLayerConfig.id.toLowerCase() === feature.get('originLayerId').toLowerCase()) {
                        ponderation = feature.get(supportLayerConfig.attribute);
                    }
                }
            }
        }
        return ponderation;
    },
    /**
     * Methode: aggregateBuffers
     * permet l'aggrégation des buffers créés.
     */
    aggregateBuffers: function (buffers) {
        var aggregationResult = null;
        var result = [];
        var i;

        //On parcourt les buffers créés afin de les agréger.
        for (i = 0; i < buffers.length; i++) {
            var jstsGeom = this._parser.read(buffers[i].getGeometry());
            if (aggregationResult === null) {
                aggregationResult = jstsGeom;
            } else {
                aggregationResult = aggregationResult.union(jstsGeom);
            }
        }

        //Si on a un buffer qui est une multi-géométrie si la supportLayer contient des polygones simples,
        // alors on sauvegarde plusieurs géométries (on ne peut pas sauvegarder un multi-polygone).
        var isMultiGeom = aggregationResult.getGeometryType().indexOf('Multi') !== -1;
        if (isMultiGeom && !this.editionLayer.isCompositeGeometry()) {
            for (i = 0; i < aggregationResult._geometries.length; i++) {
                var aggregationParsedResult = this._parser.write(aggregationResult._geometries[i]);
                var aFeature = new ol.Feature({
                    geometry: aggregationParsedResult,
                    state: FeatureState.INSERT
                });
                result.push(aFeature);
            }
        } else {
            aggregationResult = this._parser.write(aggregationResult);
            if (this.editionLayer.geometryType === Descartes.Layer.MULTI_POLYGON_GEOMETRY &&
                    aggregationResult instanceof ol.geom.Polygon) {
                var coordinates = aggregationResult.getCoordinates();
                aggregationResult = new ol.geom.MultiPolygon([coordinates]);
            }

            var newFeature = new ol.Feature({
                geometry: aggregationResult,
                state: FeatureState.INSERT
            });
            result.push(newFeature);
        }

        return result;
    },
    /**
     * Méthode privée: buffering Appelée lorsque l'on veut appliquer le buffer simple à une géométrie.
     * Est à surcharger dans les classes filles
     */
    buffering: function (newGeometry, ponderation) {
        return null;
    },
    /**
     * Méthode privé _showBufferDialog Affiche la boite de
     * dialogue proposant un formulaire pour le buffer
     */
    _showBufferDialog: function () {
        var action = new BufferEditor(null, this.olMap, {
            value: this._toolConfig.distance,
            isHalo: this.configKey === 'halo'
        });

        action.events.register('save', this, function (event) {
            this.bufferOptions = event.data[0];
        });
        action.events.register('cancel', this, this.cancel);
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature) {
        feature['editAttribut'] = true;

        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.olMap, viewOptions);
        action.events.register('saveObject', this, this.saveObjectAfterDialog);
        action.events.register('cancel', this, this.cancel);
    },
    saveObjectAfterDialog: function (e) {
        this.saveObjects(e.data);
    },
    /**
     * Méthode saveObject Permet de sauvegarde l'objet en base.
     */
    saveObject: function (feature) {
        this.saveObjects([feature]);
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveObjects: function (features) {
        var objectId = this.createObjectId;
        if (!_.isNil(objectId)) {
            _.each(features, function (feature) {
                feature.set('idAttribut', objectId);
            });
        }

        this.createObjectId = null;
        this.createAttributes = null;


        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        _.each(features, function (feature) {
            if (feature.get('state') === FeatureState.INSERT) {
                olLayer.getSource().addFeature(feature);
            }
            feature.editionLayer = this.editionLayer;
            this.setFeatureStyle(feature, 'create');
        }.bind(this));

        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            saveStrategy.save();
        }

        this.deactivate();
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule le clonage.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    cancel: function () {
        //this.unselectAll();
        this.deactivate();
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        if (!_.isNil(this.editionLayer)) {
            this.editionLayer.unselectAll();
        }
        if (!_.isNil(this.interaction)) {
            this.interaction.getFeatures().clear();
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.BufferCreation'
});

module.exports = Class;

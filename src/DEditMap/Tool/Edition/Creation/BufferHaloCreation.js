/* global MODE, Descartes, javascript */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var BufferCreation = require('./BufferCreation');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;

require('../css/bufferHaloCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.BufferHaloCreation
 * Outil de création par extension / réduction d'une géométrie existante à l'aide d'un buffer simple.
 *
 * Hérite de:
 * - <Descartes.Tool.Edition.Creation.BufferCreation>
 */
var Class = Utils.Class(BufferCreation, {
    /**
     * Propriete : bufferNormalMin
     * Si true, alors le buffer créé sera Normal Min (la distance sera calculée par rapport aux sommets)
     * Si false, le buffer sera Normal Max (la distance sera calculée par rapport aux côtés)
     */
    bufferNormalMin: false,
    /**
     * Clé de configuration à utiliser dans la configuration du layer.
     */
    configKey: 'halo',

    /**
     * Methode: buffering
     * surcharge de buffering, effectue un buffer simple.
     */
    buffering: function (newGeometry, ponderation) {
        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        var bufferPondere = this.bufferOptions.buffer;

        if (!_.isNil(ponderation)) {
            ponderation = Number(ponderation);
            if (_.isNumber(ponderation)) {
                bufferPondere *= ponderation;
            }
        }

        //Pour pouvoir convertir la distance que nous avons en mètre en degrés,
        //nous procédons à un changement de projection de la géom�trie à modifier.
        //Nous la passons dans une projection en mètres, nous lui appliquons le buffer
        //puis on la repasse dans la projection d'origine.
        //var projection = olLayer.map.getView().getProjection().getCode();
        //newGeometry.transform(projection, this.metersProj);

        var projection = olLayer.map.getView().getProjection();
        var units = projection.getUnits();

        var jstsGeometry = this._parser.read(newGeometry);
        if (_.isNil(this.bufferOptions.nbPoints)) {
            if (units === 'm') {
                jstsGeometry = jstsGeometry.buffer(bufferPondere / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(newGeometry.getExtent()), units) * projection.getMetersPerUnit()));
            } else if (units === 'degrees') {
                jstsGeometry = this._parser.read(newGeometry.clone().transform(projection, 'EPSG:2154'));
            jstsGeometry = jstsGeometry.buffer(bufferPondere);
            }
        } else {
            if (units === 'm') {
                jstsGeometry = jstsGeometry.buffer(bufferPondere / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(newGeometry.getExtent()), units) * projection.getMetersPerUnit()), this.bufferOptions.nbPoints);
            } else if (units === 'degrees') {
                jstsGeometry = this._parser.read(newGeometry.clone().transform(projection, 'EPSG:2154'));
                jstsGeometry = jstsGeometry.buffer(bufferPondere, this.nbPoints);
            }
        }

        var bufferedGeometry = this._parser.write(jstsGeometry);
        //bufferedGeometry.transform(this.metersProj, projection);
        if (units === 'degrees') {
            bufferedGeometry = bufferedGeometry.clone().transform('EPSG:2154', projection);
        }

        if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY &&
                bufferedGeometry instanceof ol.geom.Polygon) {
            var coordinates = bufferedGeometry.getCoordinates();
            bufferedGeometry = new ol.geom.MultiPolygon([coordinates]);
        }

        var newfeatures = [];
        if (this.editionLayer.geometryType === LayerConstants.POLYGON_GEOMETRY &&
                bufferedGeometry instanceof ol.geom.MultiPolygon) {
            for (var i = 0; i < bufferedGeometry.getPolygons().size(); i++) {
                var aFeature = new ol.Feature({
                    state: FeatureState.INSERT,
                    geometry: bufferedGeometry.getPolygons()[i]
                });
                newfeatures.push(aFeature);
            }
        } else {
            var newFeature = new ol.Feature({
                state: FeatureState.INSERT,
                geometry: bufferedGeometry
            });
            newfeatures.push(newFeature);
        }

        return newfeatures;

    },

    CLASS_NAME: 'Descartes.Tool.Edition.Creation.BufferHaloCreation'
});

module.exports = Class;

/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var AbstractSplit = require('./AbstractSplit');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;
var Messages = require('../../../Messages');
var DecartesEvent = Descartes.Utils.DescartesEvent;

require('../css/splitCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.SplitCreation
 * Outil de création par scission.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation.AbstractSplit>
 */
var Class = Utils.Class(AbstractSplit, {
    /**
     * split par point/ligne ou polygone
     * par defaut c'est par ligne
     */
    drawingType: LayerConstants.LINE_GEOMETRY,
    /*
     * Private
     */
    featureToModify: null,

    _drawType: null,

    setSelectionEditionTool: function (selectionEditionTool) {
        this.selectionEditionTool = selectionEditionTool;
    },
    setCompositeSelectionEditionTool: function (compositeSelectionEditionTool) {
        this.compositeSelectionEditionTool = compositeSelectionEditionTool;
    },
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.AggregationCreation
     * Constructeur d'instances
     */
    initialize: function (options) {
        _.extend(this, options);
        AbstractSplit.prototype.initialize.apply(this, arguments);

        //indique que les géométries multiples ne doivent pas être transformées
        //en géométrie simple dans le layer temporaire
        this._explodeMultiGeometryInTempLayer = false;

        switch (this.drawingType) {
            case LayerConstants.POINT_GEOMETRY:
                this._drawType = 'Point';
                break;
            case LayerConstants.POLYGON_GEOMETRY:
                this._drawType = 'Polygon';
                break;
            case LayerConstants.LINE_GEOMETRY:
                this._drawType = 'LineString';
                break;
            default :
                this._drawType = 'LineString';
        }

    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (Descartes.EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }
            AbstractSplit.prototype.activate.apply(this, arguments);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            if (Descartes.EditionManager.onlyAppMode && this.featureToModify) {
                this.setFeatureStyle(this.featureToModify, Utils.getStyleByState(this.featureToModify));
                this.featureToModify = null;
            }
            AbstractSplit.prototype.deactivate.apply(this, arguments);
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer.split;
                this.registerToSelection(editionLayer);
            } else {
                this.unregisterToSelection();
                this._toolConfig = null;
            }
        } else {
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.split;
            this.registerToSelection(this.editionLayer);
        }
        AbstractSplit.prototype.updateStateWithLayer.apply(this, arguments);

        //controle de la configuration et du type de géométrie
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
        } else if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode globale
            if (this.drawingType === Descartes.Layer.POINT_GEOMETRY &&
                    editionLayer.geometryType !== LayerConstants.LINE_GEOMETRY &&
                    editionLayer.geometryType !== LayerConstants.MULTI_LINE_GEOMETRY) {
                this.setAvailable(false);
            } else if (this.drawingType === Descartes.Layer.LINE_GEOMETRY &&
                    (editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                            editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY)) {
                this.setAvailable(false);
            } else if (this.drawingType === Descartes.Layer.POLYGON_GEOMETRY &&
                    (editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                            editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY)) {
                this.setAvailable(false);
            }
        } else {
            //mode individuel
            if (this.drawingType === Descartes.Layer.POINT_GEOMETRY &&
                    this.editionLayer.geometryType !== LayerConstants.LINE_GEOMETRY &&
                    this.editionLayer.geometryType !== LayerConstants.MULTI_LINE_GEOMETRY) {
                this.setAvailable(false);
            } else if (this.drawingType === Descartes.Layer.LINE_GEOMETRY &&
                    (this.editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                            this.editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY)) {
                this.setAvailable(false);
            } else if (this.drawingType === Descartes.Layer.POLYGON_GEOMETRY &&
                    (this.editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                            this.editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY)) {
                this.setAvailable(false);
            }
        }
        this.updateElement();
        this.featureSelectionChanged();
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer draw
     */
    initializeOLFeature: function () {
        if (!_.isNil(this.olMap) && !_.isNil(this.interaction)) {
            this.olMap.removeInteraction(this.interaction);
        }
        this.interaction = new ol.interaction.Draw({
            type: this._drawType
        });
        this.interaction.set('id', 'SplitCreation_' + this._drawType + '_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawend', this.onDrawEnd.bind(this));

        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    onDrawEnd: function (event) {
        var splitFeature = event.feature;
        var geom = splitFeature.getGeometry();
        var splitGeom = this._parser.read(geom);

        if (geom instanceof ol.geom.Point) {
            var coordinate = geom.getCoordinates();
            var coordinate1 = [coordinate[0] + 0.1, coordinate[1] + 0.1];
            var coordinate2 = [coordinate[0] - 0.1, coordinate[1] - 0.1];
            var newGeom = new ol.geom.LineString([coordinate1, coordinate2]);
            //Conversion du point en ligne
            splitGeom = this._parser.read(newGeom);
        }

        var features = this._tempLayer.getSource().getFeatures();

        var splittedGeoms = [];
        for (var i = 0; i < features.length; i++) {
            var aFeature = features[i];
            var aGeometry = this._parser.read(aFeature.getGeometry());
            if (!splitGeom.equalsTopo(aGeometry) &&
                    splitGeom.intersects(aGeometry)) {
                splittedGeoms = splittedGeoms.concat(this.split(splitFeature.getGeometry(), aFeature.getGeometry()));
            }
        }
        this.showOnMap(splittedGeoms);
    },
    split: function (splitGeom, aGeometry) {
        return this.splitGeometryWithGeometry(splitGeom, aGeometry);
    },
    showOnMap: function (splittedGeoms) {
        if (!_.isEmpty(splittedGeoms)) {

            var features = [];
            _.each(splittedGeoms, function (aGeometry) {
                features.push(new ol.Feature({
                    geometry: aGeometry
                }));
            });

            var message = this.getMessage('PREVIEW_MAP_TITLE_CREATION');
            if (!_.isNil(this.featureToModify)) {
                message = this.getMessage('PREVIEW_MAP_TITLE_MODIFICATION');
            }

            var multiple = this.editionLayer.isCompositeGeometry() || _.isNil(this.featureToModify);
            this._showMapPreviewDialog(features, this._save.bind(this), this._cancel.bind(this), message, false, multiple);
        }
    },
    _save: function (event) {
        var i;
        var features = event.data[0].getArray();
        if (_.isNil(this.featureToModify)) {
            if (this.isActiveVerifTopoConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeTopology(features[i].getGeometry())) {
                        return;
                    }
                }
            }
            if (this.isActiveVerifAppConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeRulesApp(features[i])) {
                        return;
                    }
                }
            }
            this._saveInCreationMode(features);
        } else {
            if (this.isActiveVerifTopoConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeTopologyFeatureToModify(features[i].getGeometry())) {
                        return;
                    }
                }
            }
            if (this.isActiveVerifAppConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeRulesAppFeatureToModify(features[i])) {
                        return;
                    }
                }
            }
            this._saveInModificationMode(features);
        }
    },
    _saveInCreationMode: function (features) {
        if (this.editionLayer.isCompositeGeometry()) {
            var coordinates = [];
            for (var i = 0; i < features.length; i++) {
                coordinates.push(features[i].getGeometry().getCoordinates());
            }

            var geometry = null;
            if (this.editionLayer.geometryType === LayerConstants.MULTI_LINE_GEOMETRY) {
                geometry = new ol.geom.MultiLineString(coordinates);
            } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                geometry = new ol.geom.MultiPolygon(coordinates);
            }

            var feature = new ol.Feature({
                geometry: geometry
            });
            if (this.editAttribut) {
                this._showAttributesDialog(feature, this.saveObjectsAfterDialog.bind(this), this._cancel.bind(this));
            } else {
                this.saveObjects(feature);
            }
        } else {
            if (this.editAttribut && features.length === 1) {
                this._showAttributesDialog(features[0], this.saveObjectsAfterDialog.bind(this), this._cancel.bind(this));
            } else {
                this.saveObjects(features);
            }
        }
    },
    _saveInModificationMode: function (features) {
        if (this.editionLayer.isCompositeGeometry()) {
            var coordinates = [];
            for (var i = 0; i < features.length; i++) {
                coordinates.push(features[i].getGeometry().getCoordinates());
            }

            var geometry = null;
            if (this.editionLayer.geometryType === LayerConstants.MULTI_LINE_GEOMETRY) {
                geometry = new ol.geom.MultiLineString(coordinates);
            } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                geometry = new ol.geom.MultiPolygon(coordinates);
            }

            if (this.editAttribut) {
                this.featureToModify.set('initGeom', this.featureToModify.getGeometry().clone());
            }
            this.featureToModify.setGeometry(geometry);

        } else {
            if (features.length !== 1) {
                alert(this.getMessage('WARNING_NO_ONE_GEOMETRY'));
                return;
            } else if (!this.isConforme(features[0].getGeometry())) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                return;
            } else {
                if (this.editAttribut) {
                    this.featureToModify.set('initGeom', this.featureToModify.getGeometry().clone());
                }
                this.featureToModify.setGeometry(features[0].getGeometry());
            }
        }
        if (this.editAttribut) {
            this._showAttributesDialog(this.featureToModify, this.saveObjectsAfterDialog.bind(this), this._cancel.bind(this));
        } else {
            this.saveObjects(this.featureToModify);
        }
    },
    saveObjectsAfterDialog: function (e) {
        this.saveObjects(e.data);
    },
    saveObjects: function (features) {
        if (!_.isNil(features.data)) {
            //cas après édition des attributs
            features = features.data;
        }
        if (!_.isArray(features)) {
            features = [features];
        }
        _.each(features, function (feature) {
            this.saveAnObject(feature);
        }.bind(this));

        if (Descartes.EditionManager.autoSave) {
            this.editionLayer.getSaveStrategy().save();
        }
        this.unselectAll();
        this.deactivate();
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveAnObject: function (feature) {
        feature.editionLayer = this.editionLayer;
        //feature.modified = false;

        if (_.isNil(this.featureToModify)) {
            //mode creation
            var objectId = this.createObjectId;
            if (!_.isNil(objectId)) {
                feature.set('idAttribut', objectId);
            }
            feature.set('state', FeatureState.INSERT);
            //this.createObjectId = null;
            //this.createAttributes = null;
            //feature.editionLayer = this.editionLayer;
            this.setFeatureStyle(feature, 'create');
            this.editionLayer.getFeatureOL_layers()[0].getSource().addFeature(feature);
        } else {
            //mode modification
            if (this.editionLayer &&
                    this.editionLayer.attributes &&
                    this.editionLayer.attributes.attributeId &&
                    this.editionLayer.attributes.attributeId.fieldName !== null) {
                          this.featureToModify.idAttribut = this.featureToModify.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
            }
            var state = this.featureToModify.get('state');
            if (state !== FeatureState.INSERT) {
                this.featureToModify.set('state', FeatureState.UPDATE);
            }
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été modifié
            this.setFeatureStyle(this.featureToModify, 'modify');
        }
    },
    _cancel: function (e) {
        //rollback sur la géométrie en mode modification lors du click sur annuler
        //de la boite de dialogue des attributs
        var feature = e.data[0];
        if (!_.isNil(feature) && this.editAttribut) {
            var initGeom = feature.get('initGeom');
            if (!_.isNil(initGeom)) {
                feature.setGeometry(initGeom);
            }
        }
        //recalcule de la couche temp (l'outil de split détruit la géométrie splitée)
        this._updateSupportsLayers();
    },
    /*
     * Méthode: featureSelectionChanged
     * Listener agissant sur l'état de l'outil lorsqu'un changement de selection
     * est fait dans le layer d'édition courant.
     */
    featureSelectionChanged: function () {
        if (!_.isNil(this.editionLayer) &&
                _.isNil(this._tempLayer) &&
                !_.isNil(this._toolConfig)) {
            if (this.editionLayer.getSelectedFeatures().length === 1) {
                this.featureToModify = this.editionLayer.getSelectedFeatures()[0];
            } else {
                this.featureToModify = null;
            }
        } else if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
        }
    },
    unselectAll: function () {
        if (!_.isNil(this.selectionEditionTool)) {
            this.selectionEditionTool.unselectAll();
        }
        if (!_.isNil(this.compositeSelectionEditionTool)) {
            this.compositeSelectionEditionTool.unselectAll();
        }
        this.featureToModify = null;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.SplitCreation'
});

module.exports = Class;

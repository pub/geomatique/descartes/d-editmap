/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var Symbolizers = require('../../../Symbolizers');

var AttributesEditor = require('../../../Action/AttributesEditor');
var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var DecartesEvent = Descartes.Utils.DescartesEvent;

require('../css/copyCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.CopyCreation
 * Outil de création par copie.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {
    /**
     * Propriete : confirm
     * Affiche une message de confirmation lors de la copie.
     */
    confirm: true,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.CopyCreation
     * Constructeur d'instances
     */
    initialize: function () {
        Creation.prototype.initialize.apply(this, arguments);
        //indique que les géométries multiples doivent être transformées
        //en géométrie simple dans le layer temporaire
        this._explodeMultiGeometryInTempLayer = true;
    },
    initializeOLFeature: function (olLayer) {
        this.interaction = new ol.interaction.Select({
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            hitTolerance: 5,
            toggleCondition: ol.events.condition.click,
            multi: false
        });
        this.interaction.set('id', 'CopyCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (Descartes.EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }
            Creation.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            //this.unselectAll();
            Creation.prototype.deactivate.apply(this, arguments);
            if (Descartes.EditionManager.onlyAppMode) {
                this.editionLayer = null;
            }
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode global
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer.copy;
            } else {
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.copy;
        }
        //Creation.prototype.updateStateWithLayer.apply(this, arguments);
        //contrôle de la configuration
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        }
    },
    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     * La selection est clonée
     *
     * Paramètres:
     * feature - l'objet sélectionné à cloner.
     */
    onSelect: function (event) {
        var features = event.selected;
        if (features.length !== 1) {
            return false;
        }
        var feature = features[0];

        if (!this.isConforme(feature.getGeometry())) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            return false;
        }

        var clone = feature.clone();
        for (var i = 0; i < clone.getKeys().length; i++) {
            var key = clone.getKeys()[i];
            if (key !== clone.getGeometryName()) {
                clone.unset(key);
            }
        }
        clone.set('state', FeatureState.INSERT);

        var initStyle = feature.getStyle();

        if (this.editionLayer.isCompositeGeometry()) {
            var simpleGeom = feature.getGeometry();
            var coordinate = simpleGeom.getCoordinates();
            var multiGeom = null;
            if (simpleGeom instanceof ol.geom.Polygon) {
                multiGeom = new ol.geom.MultiPolygon([coordinate]);
            } else if (simpleGeom instanceof ol.geom.LineString) {
                multiGeom = new ol.geom.MultiLineString([coordinate]);
            } else if (simpleGeom instanceof ol.geom.Point) {
                multiGeom = new ol.geom.MultiPoint([coordinate]);
            }

            if (multiGeom !== null) {
                clone.setGeometry(multiGeom);
            }
        }
        this.setFeatureStyle(feature, 'select');

        if (!this.confirm || confirm(this.getMessage('CONFIRM'))) {
            feature.setStyle(initStyle);

            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(feature.getGeometry())) {
                //feature.layer.redraw();
                this.unselectAll();
                return false;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(feature)) {
                //feature.layer.redraw();
                this.unselectAll();
                return false;
            }

            if (this.editAttribut) {
                this._showAttributesDialog(clone);
            } else {
                this.saveObject(clone);
            }
        } else {
            feature.setStyle(initStyle);
            //feature.layer.redraw();
        }
        this.unselectAll();
        return true;
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature) {
        feature['editAttribut'] = true;

        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.olMap, viewOptions);
        action.events.register('saveObject', this, this.saveObjectAfterDialog);
        action.events.register('cancel', this, this.cancel);
    },
    saveObjectAfterDialog: function (e) {
        this.saveObject(e.data[0]);
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveObject: function (feature) {
        var objectId = this.createObjectId;
        if (!_.isNil(objectId)) {
            feature.set('idAttribut', objectId);
        }

        this.createObjectId = null;
        this.createAttributes = null;

        feature.editionLayer = this.editionLayer;

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        if (feature.get('state') === FeatureState.INSERT) {
            olLayer.getSource().addFeature(feature);
        }

        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            saveStrategy.save();
        } else {
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été créée
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers.create);
            var olStyle = olStyles[this.editionLayer.geometryType];
            feature.setStyle(olStyle);
        }

        this.deactivate();
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule le clonage.
     *
     */
    cancel: function () {
        this.unselectAll();
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        this.editionLayer.unselectAll();
        this.interaction.getFeatures().clear();
    },
    changeCursorPointerOnTempFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this)
        });
        var self = this;
        var feature = this.olMap.forEachFeatureAtPixel(evt.pixel, function (f, layer) {
            if (layer === self._tempLayer) {
                _.each(self._tempLayer.getSource().getFeatures(), function (f) {
                    self.setFeatureStyle(f, 'support');
                });
                return f;
            }
            return null;
        });
        if (feature) {
            this.setFeatureStyle(feature, 'select');
        } else {
            _.each(this._tempLayer.getSource().getFeatures(), function (feature) {
                this.setFeatureStyle(feature, 'support');
            }.bind(this));
        }
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.CopyCreation'
});

module.exports = Class;

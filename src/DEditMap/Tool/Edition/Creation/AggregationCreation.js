/* global MODE, Descartes */

var jsts = require('jsts');
var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var LayerConstants = Descartes.Layer;
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var DecartesEvent = Descartes.Utils.DescartesEvent;

var EditionLayer;
var EditionManager;

require('../css/aggregationCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.AggregationCreation
 * Outil de création par agrégation d'un objet multi-géométries à support.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {

    /**
     * _parser Jsts parser
     */
    _parser: null,

    /**
     * Propriété featureToModify
     */
    featureToModify: null,

    /**
     * Propriété hitTolerance
     */
    hitTolerance: 5,

    setSelectionEditionTool: function (selectionEditionTool) {
        this.selectionEditionTool = selectionEditionTool;
    },
    setCompositeSelectionEditionTool: function (compositeSelectionEditionTool) {
        this.compositeSelectionEditionTool = compositeSelectionEditionTool;
    },

    /**
     * Constructeur: Descartes.Tool.Edition.Creation.AggregationCreation
     * Constructeur d'instances
     */
    initialize: function (options) {
        EditionManager = require('../../../Core/EditionManager');
        EditionLayer = require('../../../Model/EditionLayer');
        Creation.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
        this._parser = new jsts.io.OL3Parser();
        //indique que les géométries multiples doivent être transformées en géométrie simple
        //dans le layer temporaire
        this._explodeMultiGeometryInTempLayer = true;
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }

            Creation.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);

            if (EditionManager.onlyAppMode && this.featureToModify) {
                this.setFeatureStyle(this.featureToModify, 'modify');
            }
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            if (EditionManager.onlyAppMode && this.featureToModify) {
                 this.setFeatureStyle(this.featureToModify, Utils.getStyleByState(this.featureToModify));
                 this.featureToModify = null;
            }
            Creation.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (EditionManager.isGlobalEditonMode()) {
            if (editionLayer instanceof EditionLayer) {
                this._toolConfig = editionLayer.aggregate;
                this.registerToSelection(editionLayer);
            } else {
                this._toolConfig = null;
                this.unregisterToSelection();
            }
        } else {
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.aggregate;
            this.registerToSelection(this.editionLayer);
        }
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        //controle de la configuration
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        }
    },
    featureFilter: function (aFeature) {
        //Traitement spécifique au mode modification
        if (!_.isNil(this.featureToModify) && !_.isNil(this.featureToModify.getGeometry())) {
            //on ne garde pas la géométrie de la feature à modifier
            if (aFeature === this.featureToModify) {
                return false;
            } else if (!this.editionLayer.isCompositeGeometry()) {
                //on ne garde que les géométries intersectant la géométrie à modifier
                var geometryToModify = this._parser.read(this.featureToModify.getGeometry());
                var aGeometry = this._parser.read(aFeature.getGeometry());
                if (geometryToModify.intersects(aGeometry)) {
                    return true;
                }
                return false;
            }
        }
        return true;
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer {<OpenLayers.Control.SelectFeature.prototype>}
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.interaction = new ol.interaction.Select({
            hitTolerance: this.hitTolerance,
            multi: true,
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            toggleCondition: function (event) {
                if (event.type === 'singleclick') {
                    var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this._tempLayer;
                        }.bind(this),
                        hitTolerance: this.hitTolerance
                    });
                    return !_.isNil(features);
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'AggregationCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    onSelect: function (event) {
        _.each(event.selected, function (feature) {
            this.setFeatureStyle(feature, 'select');
        }.bind(this));

        _.each(event.deselected, function (feature) {
            this.setFeatureStyle(feature, 'support');
        }.bind(this));

        if (event.selected.length === 0 && event.deselected.length > 0) {
            var clickout = true;
            if (event.deselected.length === 1) {
                var features = this.olMap.forEachFeatureAtPixel(event.mapBrowserEvent.pixel, function (feature) {
                    return feature;
                }, {
                    layerFilter: function (aLayer) {
                        return aLayer === this._tempLayer;
                    }.bind(this)
                });
                clickout = _.isNil(features);
            }

            if (clickout) {
                if (event.deselected.length > 1 ||
                        (event.deselected.length === 1 && !_.isNil(this.featureToModify))) {
                    this.aggregate(event.deselected);
                } else {
                    alert(this.getMessage('WARNING_AT_LEAST_TWO_GEOMETRY'));
                }
            }
        }
    },

    aggregate: function (features) {
        var result = this.aggregateFeatures(features);
        if (!_.isNil(result)) {
            if (!this.isConforme(result)) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                this.unselectAll();
                return;
            }

            var feature = new ol.Feature({
                state: FeatureState.INSERT,
                geometry: result
            });
            this.setFeatureStyle(feature, 'create');

            if (this.isActiveVerifTopoConformity()) {
                if (_.isNil(this.featureToModify) && !this.isConformeTopology(result)) {
                    this.unselectAll();
                    return;
                } else if (!_.isNil(this.featureToModify) && !this.isConformeTopologyFeatureToModify(result)) {
                    this.unselectAll();
                    return;
                }
            }

            if (this.isActiveVerifAppConformity()) {
                if (_.isNil(this.featureToModify) && !this.isConformeRulesApp(feature)) {
                    this.unselectAll();
                    return;
                } else if (!_.isNil(this.featureToModify) && !this.isConformeRulesAppFeatureToModify(feature)) {
                    this.unselectAll();
                    return;
                }
            }

            var message = this.getMessage('PREVIEW_MAP_TITLE_CREATION');
            if (!_.isNil(this.featureToModify)) {
                message = this.getMessage('PREVIEW_MAP_TITLE_MODIFICATION');
            }
            this._showMapPreviewDialog([feature], this.onConfirm.bind(this), this.onCancel.bind(this), message, true);
        }
    },

    /**
     * Méthode aggregateFeatures
     * Agrège la selection courante du layer temporaire.
     */
    aggregateFeatures: function (features) {
        if ((_.isNil(this.featureToModify) && features.length < 2) ||
                !_.isNil(this.featureToModify) && features.length === 0) {
            alert(this.getMessage('WARNING_AT_LEAST_TWO_GEOMETRY'));
            return null;
        }

        var result = null;

        if (!_.isNil(this.featureToModify) && !_.isNil(this.featureToModify.getGeometry())) {
            result = this._parser.read(this.featureToModify.getGeometry().clone());
        } else if (!_.isNil(this.featureToModify) && _.isNil(this.featureToModify.getGeometry)) {//pb dezoom
            alert(this.getMessage('ERROR_ALTER_GEOMETRY'));
            this.deactivate();
            return null;
        }

        for (var i = 0; i < features.length; i++) {
            var feature = features[i];
            var jstsGeom = this._parser.read(feature.getGeometry().clone());
            if (result === null) {
                result = jstsGeom;
            } else {
                if (result.intersects(jstsGeom)) {
                    var tempResult = result.union(jstsGeom);
                    if (tempResult.isGeometryCollection() && !this.editionLayer.isCompositeGeometry()) {
                        alert(this.getMessage('WARNING_MULTIPLE_GEOMETRY'));
                        return null;
                    } else {
                        result = tempResult;
                    }
                } else if (this.editionLayer.isCompositeGeometry()) {
                    //on ajoute ou on créé un géométrie composite
                    if (result instanceof jsts.geom.GeometryCollection &&
                            !(jstsGeom instanceof jsts.geom.GeometryCollection)) {
                        //result.geometries.push(jstsGeom);
                        result = result.union(jstsGeom);
                    } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                        result = new jsts.geom.MultiPolygon([result, jstsGeom], result.getFactory());
                    } else if (this.editionLayer.geometryType === LayerConstants.MULTI_LINE_GEOMETRY) {
                        result = new jsts.geom.MultiLineString([result, jstsGeom], result.getFactory());
                    } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY) {
                        result = new jsts.geom.MultiPoint([result, jstsGeom], result.getFactory());
                    }
                } else {
                    alert(this.getMessage('WARNING_INTERSECT_GEOMETRY'));
                    return null;
                }
            }
        }

        if (!_.isNil(result)) {
            result = this._parser.write(result);
        }

        return result;
    },
    onConfirm: function (event) {
        var features = event.data;
        if (this.featureToModify === null) {
            this._saveInCreationMode(features);
        } else {
            this._saveInModificationMode(features);
        }
    },
    /**
     * Méthode privée _saveInCreationMode
     * Créée et sauvegarde les features passées en paramètre.
     */
    _saveInCreationMode: function (features) {
        var feature;
        if (this.editionLayer.isCompositeGeometry()) {
            feature = features[0].clone();
            //Le cas peut se présenter ou l'on veut un multi polygone mais que feature soit un polygone
            if (feature.getGeometry() instanceof ol.geom.Polygon) {
                var simpleGeom = feature.getGeometry();
                var coordinate = simpleGeom.getCoordinates();
                feature.setGeometry(new ol.geom.MultiPolygon([coordinate]));
            }

            feature.set('state', FeatureState.INSERT);
            if (this.editAttribut) {
                this._showAttributesDialog(feature, this.saveObjectAfterDialog, this.onCancel);
            } else {
                this.saveObject(feature);
            }
        } else {
            for (var i = 0; i < features.length; i++) {
                feature = features[i].clone();
                feature.set('state', FeatureState.INSERT);
                if (this.editAttribut) {
                    this._showAttributesDialog(feature, this.saveObjectAfterDialog, this.onCancel);
                } else {
                    this.saveObject(feature);
                }
            }
        }
    },
    _saveInModificationMode: function (features) {
        var feature = features[0];
        if (this.editionLayer.isCompositeGeometry()) {

            //Le cas peut se présenter ou l'on veut un multi polygone mais que feature soit un polygone
            var geom = feature.getGeometry();
            if (feature.getGeometry() instanceof ol.geom.Polygon) {
                var simpleGeom = feature.getGeometry();
                var coordinate = simpleGeom.getCoordinates();
                geom = new ol.geom.MultiPolygon([coordinate]);
            }

            this.featureToModify.setGeometry(geom);
        } else {
            if (features.length !== 1) {
                alert(this.getMessage('WARNING_UPDATE_INTO_MULTIPLE_GEOMETRY'));
                return;
            } else {
                if (this.editAttribut) {
                    this.featureToModify.set('initGeom', this.featureToModify.getGeometry().clone());
                }
                this.featureToModify.setGeometry(feature.getGeometry());
            }
        }

        if (this.editAttribut) {
            this._showAttributesDialog(this.featureToModify, this.saveObjectAfterDialog, this.onCancel);
        } else {
            this.saveObject(this.featureToModify);
        }
    },
    saveObjectAfterDialog: function (e) {
        this.saveObject(e.data[0]);
    },
    saveObject: function (feature) {
        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        var source = olLayer.getSource();

        feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                       feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }
        if (!_.isNil(this.featureToModify)) {
            if (!_.isNil(this.featureToModify.getId())) {
                feature.set('state', FeatureState.UPDATE);
                this.setFeatureStyle(this.featureToModify, 'modify');
            } else {
                source.removeFeature(this.featureToModify);
                feature.set('state', FeatureState.INSERT);
                this.setFeatureStyle(feature, 'create');
            }
        } else {
            feature.set('state', FeatureState.INSERT);
            this.setFeatureStyle(feature, 'create');
        }
        source.addFeature(feature);

        if (EditionManager.autoSave) {
            var saveStrategy = source.get('saveStrategy');
            saveStrategy.save();
        }

        this.deactivate();
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule l'aggregation.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    onCancel: function (e) {
        //rollback sur la géométrie en mode modification lors du click sur annuler
        //de la boite de dialogue des attributs
        var feature = e.data[0];
        if (!_.isNil(feature) && this.editAttribut) {
            var initGeom = feature.get('initGeom');
            if (!_.isNil(initGeom)) {
                feature.setGeometry(initGeom);
            }
        }
        //recalcule de la couche temp (l'outil de split détruit la géométrie splitée)
        this._updateSupportsLayers();
    },

    unselectAll: function () {
        this.interaction.getFeatures().clear();
        if (!_.isNil(this.selectionEditionTool)) {
            this.selectionEditionTool.unselectAll();
        }
        if (!_.isNil(this.compositeSelectionEditionTool)) {
            this.compositeSelectionEditionTool.unselectAll();
        }
    },
    /*
     * Méthode: featureSelectionChanged
     * Listener agissant sur l'état de l'outil lorsqu'un changement
     * de selection est fait dans le layer d'édition courant.
     */
    featureSelectionChanged: function () {
        if (!_.isNil(this.editionLayer) && _.isNil(this._tempLayer) && !_.isNil(this._toolConfig)) {
            if (this.editionLayer.getSelectedFeatures().length === 1) {
                this.featureToModify = this.editionLayer.getSelectedFeatures()[0];
            } else {
                this.featureToModify = null;
            }
        } else if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.AggregationCreation'
});

module.exports = Class;

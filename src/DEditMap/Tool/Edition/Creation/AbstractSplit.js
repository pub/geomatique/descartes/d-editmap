/* global MODE, Descartes */
var jsts = require('jsts');
var _ = require('lodash');
var ol = require('openlayers');
var log = require('loglevel');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var Messages = require('../../../Messages');

/**
 * Class: Descartes.Tool.Edition.Creation.AbstractSplit
 * Classe "abstraite" pour les outils de de scission.
 * Les instances sont de type SplitCreation ou DivideCreation
 *
 * Hérite de:
 * - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {
    /**
     * Propriete _parser
     * Jsts Parser
     */
    _parser: null,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.AbstractSplit
     * Constructeur d'instances
     */
    initialize: function () {
        Creation.prototype.initialize.apply(this, arguments);
        this._parser = new jsts.io.OL3Parser();
    },
    /**
     * Méthode splitGeometryWithGeometry
     * Permet de scinder une géométie (target) à l'aide d'une autre (splitGeom)
     */
    splitGeometryWithGeometry: function (splitGeom, target) {
        if (splitGeom instanceof ol.geom.Point) {
            var coordinate = splitGeom.getCoordinates();
            var coordinate1 = [coordinate[0] + 0.1, coordinate[1] + 0.1];
            var coordinate2 = [coordinate[0] - 0.1, coordinate[1] - 0.1];
            splitGeom = new ol.geom.LineString([coordinate1, coordinate2]);
        }

        if (this.canSplit(splitGeom, target)) {
            if (splitGeom instanceof ol.geom.LineString) {
                return this.splitGeometryWithLine(splitGeom, target);
            } else if (splitGeom instanceof ol.geom.Polygon) {
                return this.splitGeometryWithPolygon(splitGeom, target);
            } else if (Utils.isMultipleGeometry(splitGeom)) {
                //décomposition de la géométrie de split en plusieurs géométrie simple.
                var result = [];
                var geoms = Utils.getSubGeom(splitGeom);
                for (var i = 0; i < geoms.length; i++) {
                    var aSplitGeom = geoms[i];
                    var partResult = this.splitGeometryWithGeometry(aSplitGeom, target);
                    if (partResult !== null) {
                        result = result.concat(partResult);
                    }
                }
                return result;
            } else {
                log.info('Split not yet implemented');
            }
            log.error('Cannot split %s with %s', target.CLASS_NAME, splitGeom.CLASS_NAME);
        }
        return null;
    },
    /**
     * Méthode splitGeometryWithPolygon
     * Permet de scinder une géométrie (target) à l'aide d'un polygone
     */
    splitGeometryWithPolygon: function (splitPolygon, target) {
        if (target instanceof ol.geom.Polygon) {
            return this.splitPolygonWithPolygon(splitPolygon, target);
        } else if (target instanceof ol.geom.LineString) {
            return this.splitLineWithPolygon(splitPolygon, target);
        } else if (Utils.isMultipleGeometry(target)) {
            var result = [];
            var geoms = Utils.getSubGeom(target);
            for (var i = 0; i < geoms.length; i++) {
                var aTargetGeometry = geoms[i];
                var partResult = this.splitGeometryWithPolygon(splitPolygon, aTargetGeometry);
                if (partResult !== null) {
                    result = result.concat(partResult);
                }
            }
            return result;
        }
        return null;
    },
    /**
     * Méthode splitGeometryWithLine
     * Permet de scinder une géométrie (target) à l'aide d'une ligne
     */
    splitGeometryWithLine: function (splitLine, target) {
        if (target instanceof ol.geom.Polygon) {
            return this.splitPolygonWithLine(splitLine, target);
        } else if (target instanceof ol.geom.LineString) {
            return this.splitLineWithLine(splitLine, target);
        } else if (Utils.isMultipleGeometry(target)) {
            var result = [];
            var geoms = Utils.getSubGeom(target);
            for (var i = 0; i < geoms.length; i++) {
                var aTargetGeometry = geoms[i];
                var partResult = this.splitGeometryWithLine(splitLine, aTargetGeometry);
                if (partResult !== null) {
                    result = result.concat(partResult);
                }
            }
            return result;
        }
        return null;
    },
    /**
     * Méthode splitPolygonWithLine
     * Permet de scinder un polygon (polygon) à l'aide d'une ligne (splitLine)
     */
    splitPolygonWithLine: function (splitLine, polygon) {
        var jstsSplitGeom = this._parser.read(splitLine);
        var targetPolygon = this._parser.read(polygon);

        var union = targetPolygon.getExteriorRing().union(jstsSplitGeom);

        var result = [];
        if (targetPolygon.intersects(jstsSplitGeom) ||
                this.featureToModify !== null) {
            var polygonizer = new jsts.operation.polygonize.Polygonizer();
            polygonizer.add(union);

            var polygons = polygonizer.getPolygons().toArray();
            _.each(polygons, function (aPolygon) {
                var difference = aPolygon.difference(targetPolygon);
                //si le resultat est contenu dans le polygon de départ.
                if (difference.isEmpty()) {
                    var geometry = this._parser.write(aPolygon);
                    result.push(geometry);
                }
            }.bind(this));
        }
        return result;
    },
    /**
     * Méthode splitLineWithLine
     * Permet de scinder une ligne (line) à l'aide d'une autre ligne (splitLine)
     */
    splitLineWithLine: function (splitLine, line) {
        var jstsSplitLine = this._parser.read(splitLine);
        var targetLine = this._parser.read(line);

        var result = [];
        var diff = targetLine.difference(jstsSplitLine);
        if (!diff.equalsTopo(targetLine)) {
            var geometry;
            if (diff instanceof jsts.geom.MultiLineString) {
                var geometries = diff._geometries;

                for (var i = 0; i < geometries.length; i++) {
                    geometry = this._parser.write(geometries[i]);
                    result.push(geometry);
                }
            } else {
                geometry = this._parser.write(diff);
                result.push(geometry);
            }
        } else if (this.featureToModify !== null) {
            result.push(this._parser.write(targetLine));
        }
        return result;
    },
    /**
     * Méthode splitPolygonWithPolygon
     * Permet de scinder un polygone (polygon) à l'aide d'un autre polygone (splitPolygon)
     */
    splitPolygonWithPolygon: function (splitPolygon, polygon) {
        var jstsSplitPolygon = this._parser.read(splitPolygon);
        var result = [];
        if (jstsSplitPolygon.isValid()) {
            var targetPolygon = this._parser.read(polygon);

            //la partie commune doit tjs être proposée
            if (targetPolygon.intersects(jstsSplitPolygon)) {
                var intersection = targetPolygon.intersection(jstsSplitPolygon);
                result.push(this._parser.write(intersection));
            } else if (this.featureToModify !== null) {
                result.push(this._parser.write(targetPolygon));
            }

            var diff = targetPolygon.difference(jstsSplitPolygon);
            if (!diff.equalsTopo(targetPolygon)) {
                var geometry;
                if (diff instanceof jsts.geom.MultiPolygon) {
                    var geometries = diff._geometries;
                    for (var i = 0; i < geometries.length; i++) {
                        geometry = this._parser.write(geometries[i]);
                        result.push(geometry);
                    }
                } else {
                    geometry = this._parser.write(diff);
                    result.push(geometry);
                }
            }
        } else {
            alert(Messages.Descartes_EDITION_NOT_VALID_GEOMETRY);
        }
        return result;
    },
    /**
     * Méthode splitLineWithPolygon
     * Permet de scinder une ligne (line) à l'aide d'un polygone (splitPolygon)
     */
    splitLineWithPolygon: function (splitPolygon, line) {
        var jstsSplitPolygon = this._parser.read(splitPolygon);
        var result = [];
        if (jstsSplitPolygon.isValid()) {
            var jstsLine = this._parser.read(line);

            //On ajoute toujours la partie commune
            if (jstsSplitPolygon.intersects(jstsLine)) {
                var intersection = jstsSplitPolygon.intersection(jstsLine);
                result.push(this._parser.write(intersection));
            }

            var diff = jstsLine.difference(jstsSplitPolygon);
            if (!diff.equalsTopo(jstsLine)) {
                var geometry;
                if (diff instanceof jsts.geom.MultiLineString) {
                    var geometries = diff._geometries;
                    for (var i = 0; i < geometries.length; i++) {
                        geometry = this._parser.write(geometries[i]);
                        result.push(geometry);
                    }
                } else {
                    geometry = this._parser.write(diff);
                    result.push(geometry);
                }
            } else if (!_.isNil(this.featureToModify)) {
                result.push(this._parser.write(jstsLine));
            }
        } else {
            alert(Messages.Descartes_EDITION_NOT_VALID_GEOMETRY);
        }
        return result;
    },
    featureFilter: function (feature) {
        if (!_.isNil(this.featureToModify)) {
            return this.featureToModify === feature;
        }
        return true;
    },
    /**
     * Méthode canSplit
     * retourn true si la géométrie (target) peut être scinder par la géométrie (splitGeom),
     * false sinon
     */
    canSplit: function (splitGeom, target) {
        if (splitGeom instanceof ol.geom.LineString ||
                splitGeom instanceof ol.geom.MultiLineString ||
                splitGeom instanceof ol.geom.Polygon ||
                splitGeom instanceof ol.geom.MultiPolygon) {
            if (target instanceof ol.geom.LineString ||
                    target instanceof ol.geom.MultiLineString ||
                    target instanceof ol.geom.Polygon ||
                    target instanceof ol.geom.MultiPolygon) {
                return true;
            }
        }
        return false;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.AbstractSplit'
});

module.exports = Class;

/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var LayerConstants = Descartes.Layer;
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');

var HomotheticEditor = require('../../../Action/HomotheticEditor');

var AttributesEditor = require('../../../Action/AttributesEditor');
var AttributesEditorDialog = require('../../../UI/' + MODE + '/AttributesEditorDialog');
var DecartesEvent = Descartes.Utils.DescartesEvent;

require('../css/homotheticCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.HomotheticCreation
 * Outil de création par homothétie.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {
    /**
     * Propriete : confirm
     * Affiche une message de confirmation avant la transformation.
     */
    confirm: true,
    /**
     * Propriete: explodeMultiGeometry
     * active ou non la transformation les objets supports composites en objets supports simples
     */
    explodeMultiGeometry: true,

    /**
     * Propriete : centerClick
     * Si true, l'utilisateur doit cliquer sur la carte poru définir son centre d'homothétie.
     */
    centerClick: false,

    /**
     * Propriete : centerCoord
     * Si true, les coordonées du centre d'homothétie sont demandées à l'utilisateurs via une fenêtre.
     */
    centerCoord: false,

    defaultHandlerOptions: {
        'single': true,
        'double': false
    },
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.HomotheticCreation
     * Constructeur d'instances
     */
    initialize: function (options) {
        Creation.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
        this._parser = new jsts.io.OL3Parser();
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (Descartes.EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }

            this._explodeMultiGeometryInTempLayer = true;
            if (!this.explodeMultiGeometry && this.editionLayer.isCompositeGeometry()) {
                this._explodeMultiGeometryInTempLayer = false;
            }
            Creation.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            //this.unselectAll();
            Creation.prototype.deactivate.apply(this, arguments);
            if (Descartes.EditionManager.onlyAppMode) {
                this.editionLayer = null;
            }
            if (!_.isNil(this.drawInteraction)) {
                this.olMap.removeInteraction(this.drawInteraction);
                this.drawInteraction = null;
                this.updateElement();
            }
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);
        }
    },
    isActive: function () {
        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        }
        if (!active && !_.isNil(this.drawInteraction)) {
            active = this.drawInteraction.getActive();
        }
        return active;
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];

        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }

        if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode globale
            if (editionLayer) {
                this._toolConfig = editionLayer.homothetic;
            } else {
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.homothetic;
        }

        //controle de la configuration et du type de géométrie
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        } else if (Descartes.EditionManager.isGlobalEditonMode() &&
                editionLayer.geometryType !== LayerConstants.POLYGON_GEOMETRY &&
                editionLayer.geometryType !== LayerConstants.MULTI_POLYGON_GEOMETRY) {
            this.setAvailable(false);
            this.updateElement();
        } else if (!_.isNil(this.editionLayer) &&
                this.editionLayer.geometryType !== LayerConstants.POLYGON_GEOMETRY &&
                this.editionLayer.geometryType !== LayerConstants.MULTI_POLYGON_GEOMETRY) {
            //mode individuel
            this.setAvailable(false);
            this.updateElement();
        }

    },
    /*
     * Methode privé: initializeOLFeature Initialise l'outil
     * openLayer {<ol.interaction.Select>}
     *
     */
    initializeOLFeature: function () {
        this.interaction = new ol.interaction.Select({
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            hitTolerance: 5,
            multi: false
        });
        this.interaction.set('id', 'HomotheticCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    onSelect: function (event) {
        this.feature = null;
        if (event.selected.length === 1) {
            var feature = event.selected[0];
            this.setFeatureStyle(feature, 'select');
            this.newGeometry = feature.getGeometry().clone();
            this._showHomotheticDialog();
        }
    },
    _showHomotheticDialog: function () {
        var options = {
            coord: (this.centerCoord && !this.centerClick && !this.appCenter),
            click: (this.centerClick && !this.appCenter)
        };

        var action = new HomotheticEditor(null, this.olMap, options);
        action.events.register('save', this, this._choseCenter);
        action.events.register('cancel', this, this.cancel);
    },
    _choseCenter: function (event) {
        var data = event.data[0];
        this.buffer = data.buffer;
        if (this.centerClick && !this.appCenter) {
            this.interaction.setActive(false);
            this.drawInteraction = new ol.interaction.Draw({
                type: 'Point'
            });
            this.drawInteraction.on('drawend', this._onClickOnMap, this);
            //il faut désactiver le snapping afin
            //qu'openlayer le prenne en ocmpte pour l'outil draw
            this._deactivateSnapping();
            this.olMap.addInteraction(this.drawInteraction);
            this._activateSnapping();

        } else {
            this._homothetie(data);
        }
    },
    _onClickOnMap: function (event) {
        var coordinates = event.feature.getGeometry().getCoordinates();
        //on revient à l'état initial
        this.olMap.removeInteraction(this.drawInteraction);
        this.drawInteraction = null;
        this.interaction.setActive(true);

        this._homothetie({
            center: coordinates
        });
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule l'action.
     */
    cancel: function () {
        this.unselectAll();
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        if (!_.isNil(this.editionLayer)) {
            this.editionLayer.unselectAll();
            this.interaction.getFeatures().forEach(function (feature) {
                //var style = Utils.getStyleByState(feature);
                this.setFeatureStyle(feature, 'support');
            }, this);
        }
        this.interaction.getFeatures().clear();
    },
    _homothetie: function (data) {
        var origin;
        if (this.appCenter) {
            origin = new ol.geom.Point(this.appCenter.x, this.appCenter.y);
        } else if (data.center) {
            origin = data.center;
        } else {
            //calcul du centroid
            var jstsGeom = this._parser.read(this.newGeometry);
            var centroid = jsts.algorithm.Centroid.getCentroid(jstsGeom);
            origin = [centroid.x, centroid.y];
        }

        this.newGeometry.scale(this.buffer, this.buffer, origin);


        if (this.editionLayer.geometryType === Descartes.Layer.MULTI_POLYGON_GEOMETRY &&
                this.newGeometry instanceof ol.geom.Polygon) {
            this.newGeometry = new ol.geom.MultiPolygon([this.newGeometry.getCoordinates()]);
        }

        var newFeature = new ol.Feature({
            state: FeatureState.INSERT,
            geometry: this.newGeometry
        });

        //contrôle si les géométries générées par l'homothétie sont conformes
        if (!this.isConforme(this.newGeometry)) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            this.unselectAll();
            return;
        }

        if (!this.confirm ||
                confirm('Etes-vous sûr de vouloir créer un nouvel objet en étendant ou réduisant cet objet ?')) {
            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(this.newGeometry)) {
                this.unselectAll();
                return;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(newFeature)) {
                this.unselectAll();
                return;
            }

            if (this.editAttribut) {
                this._showAttributesDialog(newFeature);
            } else {
                this.saveObject(newFeature);
            }
        }
        this.unselectAll();
    },
    /**
     * Méthode privé showAttributesDialog
     * Affiche la boite de dialogue des attributs
     */
    _showAttributesDialog: function (feature) {
        feature['editAttribut'] = true;

        // Creation de la vue
        var viewOptions = {
            view: AttributesEditorDialog,
            feature: feature,
            editionLayer: this.editionLayer
        };

        // Creation de l'action qui va ouvrir la vue
        var action = new AttributesEditor(null, this.olMap, viewOptions);
        action.events.register('saveObject', this, this.saveObjectAfterDialog);
        action.events.register('cancel', this, this.cancel);
    },
    saveObjectAfterDialog: function (e) {
        this.saveObject(e.data[0]);
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveObject: function (feature) {
        var objectId = this.createObjectId;
        if (!_.isNil(objectId)) {
            feature.set('idAttribut', objectId);
        }

        feature.editionLayer = this.editionLayer;
        this.createObjectId = null;
        this.createAttributes = null;

        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        if (feature.get('state') === FeatureState.INSERT) {
            olLayer.getSource().addFeature(feature);
        }

        this.setFeatureStyle(feature, 'create');
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = olLayer.getSource().get('saveStrategy');
            saveStrategy.save();
        }

        this.deactivate();
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.HomotheticCreation'
});

module.exports = Class;

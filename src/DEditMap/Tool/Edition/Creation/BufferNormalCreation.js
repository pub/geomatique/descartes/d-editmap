/* global MODE, Descartes, javascript */

var ol = require('openlayers');
var _ = require('lodash');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var BufferCreation = require('./BufferCreation');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;

require('../css/bufferNormalCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.BufferNormalCreation
 * Outil de création par extension / réduction d'une géométrie existante à l'aide d'un buffer simple.
 *
 * Hérite de:
 * - <Descartes.Tool.Edition.Creation.BufferCreation>
 */
var Class = Utils.Class(BufferCreation, {
    /**
     * Propriete : bufferNormalMin
     * Si true, alors le buffer créé sera Normal Min (la distance sera calculée par rapport aux sommets)
     * Si false, le buffer sera Normal Max (la distance sera calculée par rapport aux côtés)
     */
    bufferNormalMin: false,
    /**
     * Clé de configuration à utiliser dans la configuration du layer.
     */
    configKey: 'buffer',
    /**
     * Methode: buffering
     * surcharge de buffering, effectue un buffer simple.
     */
    buffering: function (newGeometry, ponderation) {
        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        var bufferPondere = this.bufferOptions.buffer;
        var i;

        if (!_.isNil(ponderation)) {
            ponderation = Number(ponderation);
            if (_.isNumber(ponderation)) {
                bufferPondere *= ponderation;
            }
        }

        //Pour pouvoir convertir la distance que nous avons en mètre en degrés,
        //nous procédons à un changement de projection de la géométrie à modifier.
        //Nous la passons dans une projection en mètres, nous lui appliquons le buffer
        //puis on la repasse dans la projection d'origine.
        //var projection = olLayer.map.getView().getProjection().getCode();
        //newGeometry.transform(projection, this.metersProj);

        var projection = olLayer.map.getView().getProjection();
        var units = projection.getUnits();

        var jstsGeometry = this._parser.read(newGeometry);
        if (units === 'm') {
            bufferPondere = bufferPondere / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(newGeometry.getExtent()), units) * projection.getMetersPerUnit());
        } else if (units === 'degrees') {
            jstsGeometry = this._parser.read(newGeometry.clone().transform(projection, 'EPSG:2154'));
        }
        if (this.bufferNormalMin) {
            if (jstsGeometry instanceof jsts.geom.MultiPolygon) {
                for (i = 0; i < jstsGeometry._geometries.length; i++) {
                    jstsGeometry._geometries[i] = this.bufferingNormalMin(jstsGeometry._geometries[i], bufferPondere);
                }
                var bufferedGeom = this._parser.write(jstsGeometry);

                var aggregationResult = null;
                for (var j = 0; j < bufferedGeom.getPolygons().length; j++) {
                    var jstsGeom = this._parser.read(bufferedGeom.getPolygons()[j]);

                    if (aggregationResult === null) {
                        aggregationResult = jstsGeom;
                    } else {
                        aggregationResult = aggregationResult.union(jstsGeom);
                    }
                }
                jstsGeometry = aggregationResult;
            } else {
                jstsGeometry = this.bufferingNormalMin(jstsGeometry, bufferPondere);
            }
        } else {
            //JOIN_MITRE donne un buffer normal sans halo
            var params = new jsts.operation.buffer.BufferParameters();
            params.setJoinStyle(jsts.operation.buffer.BufferParameters.JOIN_MITRE);
            jstsGeometry = jsts.operation.buffer.BufferOp.bufferOp(jstsGeometry, bufferPondere, params);
        }

        var bufferedGeometry = this._parser.write(jstsGeometry);
        //bufferedGeometry.transform(this.metersProj, olLayer.map.getView().getProjection().getCode());
        if (units === 'degrees') {
            bufferedGeometry = bufferedGeometry.clone().transform('EPSG:2154', projection);
        }

        if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY &&
                bufferedGeometry instanceof ol.geom.Polygon) {
            var coordinates = bufferedGeometry.getCoordinates();
            bufferedGeometry = new ol.geom.MultiPolygon([coordinates]);
        }

        var newfeatures = [];

        //S'il est un MultiPolygon, on transforme le buffer en array de polygones
        if (this.editionLayer.geometryType === LayerConstants.POLYGON_GEOMETRY &&
                bufferedGeometry instanceof ol.geom.MultiPolygon) {
            for (i = 0; i < bufferedGeometry.getPolygons().size(); i++) {
                var aFeature = new ol.Feature({
                    geometry: bufferedGeometry.getPolygons()[i],
                    state: FeatureState.INSERT
                });
                newfeatures.push(aFeature);
            }
        } else {
            var newFeature = new ol.Feature({
                geometry: bufferedGeometry,
                state: FeatureState.INSERT
            });
            newfeatures.push(newFeature);
        }
        return newfeatures;
    },

    /**
     * Methode : bufferNormalMin
     * calcule le polygone correspondant au buffer normal min, qui est créé
     * en calculant la distance aux sommets et non aux côtés.
     */
    bufferingNormalMin: function (jstsGeometry, distBuffer) {
        var shell = jstsGeometry._shell._points;
        var buffer = this.calcExtremPointsAndBuffer(jstsGeometry, distBuffer, shell._coordinates);

        if (_.isNil(buffer)) {
            return new jsts.geom.Polygon(new jsts.geom.LinearRing([]));
        }

        var holes = jstsGeometry._holes;
        var buffHoles = [];
        var islands = [];

        for (var i = 0; i < holes.length; i++) {
            var points = holes[i].getCoordinates();
            var buffHole = this.calcExtremPointsAndBuffer(jstsGeometry, distBuffer, points, true);

            if (!_.isNil(buffHole)) {
                if (buffHole._holes) {
                    for (var n = 0; n < buffHole._holes.length; n++) {
                        islands.push(new jsts.geom.Polygon(buffHole._holes[n]));
                    }
                }
                if (buffHole instanceof jsts.geom.MultiPolygon) {
                    for (var pol = 0; pol < buffHole._geometries.lenght; pol++) {
                        buffHoles.push(buffHole._geometries[pol]._shell);
                    }
                } else {
                    buffHoles.push(buffHole._shell);
                }
            }
        }

        if (buffHoles.length > 0) {
            if (buffer instanceof jsts.geom.MultiPolygon) {
                buffer = this.testHoleMultiPolygones(buffer, buffHoles);
                buffer._geometries = buffer._geometries.concat(islands);
            } else {
                if (buffer._holes) {
                    buffer._holes = buffer._holes.concat(buffHoles);
                } else {
                    buffer._holes = buffHoles;
                }

                if (islands.length > 0) {
                    islands.push(buffer);
                    buffer = new jsts.geom.MultiPolygon(islands, buffer.getFactory());
                }
            }
        }
        return buffer;
    },

    /**
     * Methode : calcExtremPointsAndBuffer
     * Calcule les points situés aux extrémités x et y de la feature, puis appelle la fonction qui crée le buffer
     */
    calcExtremPointsAndBuffer: function (jstsGeometry, distBuffer, points, isHole) {
        var maxX = points[0];
        var minX = points[0];
        var maxY = points[0];
        var minY = points[0];

        for (var p = 0; p < points.length; p++) {
            if (points[p].x > maxX.x) {
                maxX = points[p];
            }
            if (points[p].x < minX.x) {
                minX = points[p];
            }
            if (points[p].y > maxY.y) {
                maxY = points[p];
            }
            if (points[p].y < minY.y) {
                minY = points[p];
            }
        }

        var extremePoints = {
            'maxX': maxX,
            'minX': minX,
            'maxY': maxY,
            'minY': minY
        };

        return this.calculateBufferGeometry(jstsGeometry, distBuffer, points, extremePoints, isHole);
    },

    /**
     * Methode : calculateBufferGeometry
     * calcule le buffer pour une liste de points donnée.
     * Les points peuvent être le "shell" (contour) de la géométrie ou l'un de ses trous.
     */
    calculateBufferGeometry: function (jstsGeometry, distBuffer, points, extremePoints, isHole) {
        var buffCoords = [];
        var buffDistance;
        var factory = jstsGeometry.getFactory();
        for (var i = 0; i < (points.length - 1); i++) {
            var vertice = points[i];
            var base;

            //On calcule le point qui, avec le sommet de l'angle qui nous interesse, forme la bissectrice de cet angle
            if (i === 0) {
                base = jsts.geom.Triangle.angleBisector(points[points.length - 2], points[i], points[i + 1]);
            } else {
                base = jsts.geom.Triangle.angleBisector(points[i - 1], points[i], points[i + 1]);
            }

            var distance = points[i].distance(base);

            //On crée des points de tests pour savoir de quel côté de l'angle on est à "l'extérieur" du polygone,
            //en fonction de quoi on détermine la valeur de buffDistance
            var length = 0.00001;
            var testAx = vertice.x + (vertice.x - base.x) / distance * (length);
            var testAy = vertice.y + (vertice.y - base.y) / distance * (length);
            var testBx = vertice.x + (vertice.x - base.x) / distance * (-length);
            var testBy = vertice.y + (vertice.y - base.y) / distance * (-length);

            var coordA = new jsts.geom.Coordinate(testAx, testAy);
            var coordB = new jsts.geom.Coordinate(testBx, testBy);

            var pointA = factory.createPoint(coordA);
            if (jstsGeometry.contains(pointA)) {
                buffDistance = -distBuffer;
            }
            var pointB = factory.createPoint(coordB);
            if (jstsGeometry.contains(pointB)) {
                buffDistance = distBuffer;
            }

            //On crée le point situé sur la bissectrice de l'angle, à la distance désirée
            var bufferX = vertice.x + (vertice.x - base.x) / distance * (buffDistance);
            var bufferY = vertice.y + (vertice.y - base.y) / distance * (buffDistance);

            var buffCoord = new jsts.geom.Coordinate(bufferX, bufferY);

            //S'il s'agit d'un point faisant partie des extremités x ou y,
            //on enregistre son point "bufferisé" pour faire des tests plus tard
            if (extremePoints) {
                for (var p in extremePoints) {
                    if (vertice.x === extremePoints[p].x && vertice.y === extremePoints[p].y) {
                        extremePoints[p].image = buffCoord;
                    }
                }
            }
            buffCoords.push(buffCoord);
        }

        //Pour que le polygone JSTS du buffer soit valide, on fait en sorte que son dernier point
        //soit égal à son premier. (pour que le polygone soit bien fermé)
        buffCoords.push(buffCoords[0]);
        var linearRing = factory.createLinearRing(buffCoords);
        var buffer = factory.createPolygon(linearRing);

        //Si le polygone obtenu se recoupe à un endroit,
        //il faut effectuer dessus un buffer(0) afin d'obtenir seulement "l'enveloppe" du polygone.
        if (!(buffer.isSimple() && buffer.isValid())) {
            //Dans le cas d'un trou, si la géométrie obtenue possède une intersection avec la géométrie originale,
            //le buffer(0) n'a pas bien marcher, il faut le faire soi-même avec validateHoles
            if (distBuffer > 0 && isHole && buffer.buffer(0).intersects(jstsGeometry)) {
                buffer = this.validateHoles(buffer, jstsGeometry);
            } else {
                buffer = buffer.buffer(0);
            }
        } else if (extremePoints && !(!isHole && buffDistance > 0) &&
                (this.testWithGraph(jstsGeometry, buffer._shell.getCoordinates(), isHole) ||
                        this.testReversedBuffer(extremePoints, buffCoords))) {
            buffer = null;
        }

        return buffer;
    },

    /**
     * Methode : testWithGraph
     * Methode utilisée par la fonction buffer de jsts pour vérifier si le buffer obtenu
     * est cohérent par rapport à la géométrie initiale
     * Return true si il y a une incohérence
     */
    testWithGraph: function (jstsGeometry, buffCoords, isHole) {
        return false;
        /*var segStr = new jsts.noding.BasicSegmentString(buffCoords, [0, 1, 2, 0]);
         //var segStr = new jsts.noding.NodedSegmentString(buffCoords, new jsts.geomgraph.Label(0, 1, 2, 0));
         var strList = new javascript.util.ArrayList();
         strList.add(segStr);
         var bufferBuilder = new jsts.operation.buffer.BufferBuilder({
         quadrantSegments: 1,
         joinStyle: 2,
         endCapStyle: 1,
         mitreLimit: 5,
         _isSingleSided: false
         });
         bufferBuilder.computeNodedEdges(strList, jstsGeometry.getPrecisionModel);

         var graph = new jsts.geomgraph.PlanarGraph(new jsts.operation.overlay.OverlayNodeFactory());
         graph.addEdges(bufferBuilder.edgeList.getEdges());

         var subgraphList = bufferBuilder.createSubgraphs(graph);
         var polyBuilder = new jsts.operation.overlay.PolygonBuilder(jstsGeometry.getFactory());
         bufferBuilder.buildSubgraphs(subgraphList, polyBuilder);
         var resultPolyList = polyBuilder.getPolygons();

         var result = true;

         if (resultPolyList.size() <= 0) {
         result = false;
         }

         //Le résultat est inversé dans le cas d'un trou
         if (isHole) {
         return result;
         }
         return !result;*/
    },

    /**
     * Methode : testReversedBuffer
     * La méthode testWithGraph semble limité dans le cas où la géométrie issue du buffer est complètement "inversée"
     * par rapport à la géométrie originale.
     * Par exemple si tous les points du buffer sont au sud de l'image du point le plus au nord de la géométrie originale.
     * Return True si le buffer est inversé
     */
    testReversedBuffer: function (extremePoints, buffCoords) {
        var boolXmax = false;
        var boolXmin = false;
        var boolYmax = false;
        var boolYmin = false;

        for (var i in buffCoords) {
            boolXmax = boolXmax || (buffCoords[i].x < extremePoints['maxX'].image.x);
            boolXmin = boolXmin || (buffCoords[i].x > extremePoints['minX'].image.x);
            boolYmax = boolYmax || (buffCoords[i].y < extremePoints['maxY'].image.y);
            boolYmin = boolYmin || (buffCoords[i].y > extremePoints['minY'].image.y);
        }

        return !(boolXmax && boolXmin && boolYmax && boolYmin);
    },

    /**
     * Methode : testHoleMultiPolygones
     * Dans le cas où un buffer aurait créé plusieurs géométries différentes (buffer avec distance négative),
     * si on a calculé des trous, cette méthode permet de savoir dans quelle nouvelle géométrie se situe
     * chacun des trous calculés.
     */
    testHoleMultiPolygones: function (buffer, bufferHoles) {
        for (var i = 0; i < buffer._geometries.lenght; i++) {
            var polygon = buffer._geometries[i];
            for (var b = 0; b < bufferHoles.size(); b++) {
                if (polygon.intersects(bufferHoles[b])) {
                    if (polygon.holes) {
                        polygon.holes = polygon.holes.concat(bufferHoles[b]);
                    } else {
                        polygon.holes = bufferHoles[b];
                    }
                }
            }
        }
        return buffer;
    },

    /**
     * Methode : validateHoles
     * Rend valide un buffer de trou qui s'auto-intersecte
     * (utilisé quand la méthode buffer(0) est insuffisante)
     */
    validateHoles: function (geom, original) {
        var polygons = this.getValidatedPolygons(geom);
        var polygonsToRemove = [];

        if (polygons instanceof jsts.geom.MultiPolygon) {
            for (var i = 0; i < polygons._geometries.lenght; i++) {
                var currGeom = polygons._geometries[i];
                if (currGeom.intersects(original)) {
                    polygonsToRemove.push(i);
                }
            }

            for (var j = 0; j < polygonsToRemove.size(); j++) {
                polygons._geometries.splice(polygonsToRemove[j], 1);
            }
        }
        return polygons;
    },
    getValidatedPolygons: function (geom) {
        var polygonizer = new jsts.operation.polygonize.Polygonizer();
        this.addLineString(geom.getExteriorRing(), polygonizer);
        for (var n = 0; n < geom.getNumInteriorRing(); n++) {
            this.addLineString(geom.getInteriorRingN(n), polygonizer);
        }
        return this.toPolygonGeometry(polygonizer.getPolygons(), geom.getFactory());

    },
    addLineString: function (lineString, polygonizer) {
        if (lineString instanceof jsts.geom.LinearRing) {
            lineString = lineString.getFactory().createLineString(lineString.getCoordinateSequence());
        }
        var point = lineString.getFactory().createPoint(lineString.getCoordinateN(0));
        var toAdd = lineString.union(point);
        polygonizer.add(toAdd);
    },
    toPolygonGeometry: function (polygons, factory) {
        switch (polygons.size()) {
            case 0:
                return null;
            case 1:
                return polygons.iterator().next();
            default:
                return factory.createMultiPolygon(polygons.a);
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.BufferNormalCreation'
});

module.exports = Class;

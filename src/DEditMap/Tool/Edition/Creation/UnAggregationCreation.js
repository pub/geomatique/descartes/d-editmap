/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Creation = require('../Creation');
var LayerConstants = Descartes.Layer;
var Messages = require('../../../Messages');
var FeatureState = require('../../../Model/FeatureState');
var DecartesEvent = Descartes.Utils.DescartesEvent;

var EditionLayer;
var EditionManager;

require('../css/unaggregationCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.UnAggregationCreation
 * Outil de création par désagrégation d'un objet multi-géométries support.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation>
 */
var Class = Utils.Class(Creation, {

    /**
     * Propriete : confirm
     * Affiche une message de confirmation lors de la désagrégation.
     */
    confirm: true,
    /**
     * Propriété hitTolerance
     */
    hitTolerance: 5,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.AggregationCreation
     * Constructeur d'instances
     */
    initialize: function (options) {
        EditionManager = require('../../../Core/EditionManager');
        EditionLayer = require('../../../Model/EditionLayer');
        Creation.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
    },
    /**
     * Methode: setAvailable
     * l'outils n'est disponible que pour les geometries composite
     *
     * Paramètres:
     * available  - {boolean} indiquant le nouveau statut de disponibilité
     */
    setAvailable: function (available) {
        var atLeastOneCompositeInLayerSupport = false;

        if (EditionManager.onlyAppMode && !_.isNil(this.editionLayer)) {
            this._toolConfig = this.editionLayer.unaggregate;
        }

        if (this._toolConfig) {
            if (this._toolConfig.supportLayersIdentifier) {
                for (var i = 0; i < this._toolConfig.supportLayersIdentifier.length; i++) {
                    var supportId = this._toolConfig.supportLayersIdentifier[i];
                    var supportLayer = this.mapContent.getLayerById(supportId);
                    var sameGeometryType = this.editionLayer.isSameGeometryType(supportLayer);
                    if (supportLayer && sameGeometryType && supportLayer.isCompositeGeometry()) {
                        atLeastOneCompositeInLayerSupport = true;
                        break;
                    } else {
                        var message = "Attention problème de configuration :\nla couche " + this._toolConfig.supportLayersIdentifier[i];
                        if (supportLayer === null) {
                            message += " n'existe pas.";
                        } else if (sameGeometryType === false) {
                            message += " n'est pas du même type que la couche en cours d'édition.";
                        } else {
                            message += " de type simple ne peut etre support pour la désaggrégation.";
                        }
                        alert(message);
                        //suppression de l'id possant problème.
                        _.pull(this._toolConfig.supportLayersIdentifier, supportId);
                    }
                }
            }
            if (this._toolConfig.enable && this.editionLayer.isCompositeGeometry()) {
                atLeastOneCompositeInLayerSupport = true;
            }
        }

        if (this.editionLayer &&
                (this.editionLayer.isCompositeGeometry() || atLeastOneCompositeInLayerSupport)) {
            Creation.prototype.setAvailable.apply(this, arguments);
        } else {
            Creation.prototype.setAvailable.apply(this, [false]);
        }
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }

            Creation.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Creation.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);

            if (EditionManager.onlyAppMode) {
                this._toolConfig = null;
            }
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (this.editionLayer !== null &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (EditionManager.isGlobalEditonMode()) {
            if (editionLayer instanceof EditionLayer) {
                this._toolConfig = editionLayer.unaggregate;
            } else {
                this._toolConfig = null;
            }
        } else {
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.unaggregate;
        }
        Creation.prototype.updateStateWithLayer.apply(this, arguments);
        //controle de la configuration
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer {<ol.interaction.Select>}
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisé par l'outil.
     */
    initializeOLFeature: function (olLayer) {

        this.interaction = new ol.interaction.Select({
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            hitTolerance: this.hitTolerance,
            multi: true,
            toggleCondition: function (event) {
                if (event.type === 'singleclick') {
                    var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this._tempLayer;
                        }.bind(this),
                        hitTolerance: this.hitTolerance
                    });
                    return !_.isNil(features);
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'UnAggregationCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    onSelect: function (event) {
        _.each(event.selected, function (feature) {
            this.setFeatureStyle(feature, 'select');
        }.bind(this));

        _.each(event.deselected, function (feature) {
            this.setFeatureStyle(feature, 'support');
        }.bind(this));

        if (event.selected.length === 0) {
            if (event.deselected.length > 0) {
                var clickout = true;
                if (event.deselected.length === 1) {
                    var features = this.olMap.forEachFeatureAtPixel(event.mapBrowserEvent.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this._tempLayer;
                        }.bind(this)
                    });
                    clickout = _.isNil(features);
                }
                if (clickout) {
                    this.clickoutFeature(event.deselected);
                }
            }
        }
    },

    clickoutFeature: function (features) {
        var result = this.unaggregateSelection(features);

        var i, feature;
        for (i = 0; i < result.length; i++) {
            feature = result[i];
            if (!this.isConforme(feature.getGeometry())) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                this.unselectAll();
                return;
            }
        }

        if (!this.confirm ||
                confirm("Etes-vous sûr de vouloir désaggréger ces objets pour en créer " + result.length + " ?")) {
            if (this.isActiveVerifTopoConformity()) {
                for (i = 0; i < result.length; i++) {
                    if (!this.isConformeTopology(result[i].getGeometry())) {
                        this.unselectAll();
                        return;
                    }
                }
            }
            if (this.isActiveVerifAppConformity()) {
                for (i = 0; i < result.length; i++) {
                    if (!this.isConformeRulesApp(result[i])) {
                        this.unselectAll();
                        return;
                    }
                }
            }
            if (this.editAttribut) {
                for (i = 0; i < result.length; i++) {
                    feature = result[i];
                    this._showAttributesDialog(feature, this.saveObjectsAfterDialog, this.onCancel);
                }
            } else {
                this.saveObjects(result);
            }
        }
    },
    /**
     * Méthode unaggregateSelection
     * Désagrège la selection courante du layer temporaire.
     */
    unaggregateSelection: function (features) {
        var result = [];
        for (var i = 0; i < features.length; i++) {
            var feature = features[i];
            result = result.concat(this.unAggregate(feature));
        }
        return result;
    },

    /**
     * Méthode unaggregate
     * Désagrège la geometrie de la feature passé en paramètre en un tableau de géométrie.
     */
    unAggregate: function (feature) {
        var result = [];
        var geometries = [];
        var geometry = feature.getGeometry();

        if (geometry instanceof ol.geom.MultiPolygon) {
            geometries = geometry.getPolygons();
        } else if (geometry instanceof ol.geom.MultiPoint) {
            geometries = geometry.getPoints();
        } else if (geometry instanceof ol.geom.MultiLineString) {
            geometries = geometry.getLineStrings();
        }

        for (var j = 0; j < geometries.length; j++) {
            var aGeometry = geometries[j];
            var coordinates = aGeometry.getCoordinates();
            var clone = aGeometry.clone();
            if (this.editionLayer.isCompositeGeometry()) {
                switch (this.editionLayer.geometryType) {
                    case LayerConstants.MULTI_POINT_GEOMETRY:
                        clone = new ol.geom.MultiPoint([coordinates]);
                        break;
                    case LayerConstants.MULTI_LINE_GEOMETRY:
                        clone = new ol.geom.MultiLineString([coordinates]);
                        break;
                    case LayerConstants.MULTI_POLYGON_GEOMETRY:
                        clone = new ol.geom.MultiPolygon([coordinates]);
                        break;
                }
            }
            var aFeature = new ol.Feature({
                geometry: clone,
                state: FeatureState.INSERT
            });
            result.push(aFeature);
        }

        return result;
    },
    saveObjectsAfterDialog: function (e) {
        this.saveObjects(e.data);
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveObject: function (feature) {
        this.saveObjects([feature]);
    },
    saveObjects: function (features) {
        var olLayer = this.editionLayer.getFeatureOL_layers()[0];
        var source = olLayer.getSource();
        for (var i = 0; i < features.length; i++) {
            var feature = features[i];
            this.setFeatureStyle(feature, 'create');
            source.addFeature(feature);
        }

        if (EditionManager.autoSave) {
            var saveStrategy = source.get('saveStrategy');
            saveStrategy.save();
        }
        this.unselectAll();
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule l'aggregation.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    onCancel: function (feature) {
        this.unselectAll();
    },

    unselectAll: function () {
        this.interaction.getFeatures().clear();
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.UnAggregationCreation'
});

module.exports = Class;

/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Messages = require('../../../Messages');
var AbstractSplit = require('./AbstractSplit');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;
var DecartesEvent = Descartes.Utils.DescartesEvent;

var EditionLayer;

require('../css/divideCreation.css');

/**
 * Class: Descartes.Tool.Edition.Creation.DivideCreation
 * Outil de création par division
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Creation.AbstractSplit>
 */
var Class = Utils.Class(AbstractSplit, {
    /**
     * Propriete : confirm
     * Affiche une message de confirmation lors de la copie.
     */
    //confirm: false,

    /**
     * Propriété featureToModify
     * L'objet à modifier dans le cas d'une modification.
     */
    featureToModify: null,

    _explodeMultiGeometryInTempLayer: true,
    /**
     * Constructeur: Descartes.Tool.Edition.Creation.DivideCreation
     * Constructeur d'instances
     */
    initialize: function (options) {
        EditionLayer = require('../../../Model/EditionLayer');
        AbstractSplit.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
    },
    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            if (Descartes.EditionManager.onlyAppMode) {
                this.updateStateWithLayer(new DecartesEvent('layerUnderEdition', [this.editionLayer]));
            }
            AbstractSplit.prototype.activate.apply(this, arguments);
            this.olMap.on('pointermove', this.changeCursorPointerOnTempFeature, this);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            if (Descartes.EditionManager.onlyAppMode && this.featureToModify) {
                this.setFeatureStyle(this.featureToModify, Utils.getStyleByState(this.featureToModify));
                this.featureToModify = null;
            }
            AbstractSplit.prototype.deactivate.apply(this, arguments);
            this.olMap.un('pointermove', this.changeCursorPointerOnTempFeature, this);
        }
    },
    /*
     * Private
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];

        if (!_.isNil(this.editionLayer) &&
                this.editionLayer !== editionLayer &&
                this.editionLayer.getSelectedFeatures().length > 0 &&
                this.layer) {
            this.unselectAll();
        }
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            //mode globale
            if (editionLayer instanceof EditionLayer) {
                this._toolConfig = editionLayer.divide;
                this.registerToSelection(editionLayer);
            } else {
                this.unregisterToSelection();
                this._toolConfig = null;
            }
        } else {
            //mode individuel
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.divide;
            this.registerToSelection(this.editionLayer);
        }

        AbstractSplit.prototype.updateStateWithLayer.apply(this, arguments);
        //controle de la configuration
        if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
            this.updateElement();
        }
        this.featureSelectionChanged();
    },
    /**
     * Méthode privée : _featureFilter
     * Permet de filtrer les features des couches supports.
     */
    featureFilter: function (aFeature) {
        if (!_.isNil(this.featureToModify) && !_.isNil(this.featureToModify.getGeometry())) {
            if (aFeature !== this.featureToModify) {
                var jstsToModify = this._parser.read(this.featureToModify.getGeometry());
                var jstsToCheck = this._parser.read(aFeature.getGeometry());
                if (jstsToModify.intersects(jstsToCheck)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'outil openLayer de sélection
     *
     * Paramètres:
     * olLayer - couche openlayers
     */
    initializeOLFeature: function (olLayer) {
        this.interaction = new ol.interaction.Select({
            layers: function (aLayer) {
                return aLayer === this._tempLayer;
            }.bind(this),
            hitTolerance: 5,
            toggleCondition: ol.events.condition.click,
            multi: false
        });
        this.interaction.set('id', 'DivideCreation_' + Utils.createUniqueID());
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     * Methode: onSelect
     * Appelée lorsqu'une selection est faite par l'utilisateur.
     *
     * Paramètres:
     * event - événement contenant les features sélectionnées et non sélectionnées
     */
    onSelect: function (event) {
        if (event.selected.length === 1) {
            this.divide(event.selected[0]);
        }
    },
    divide: function (feature) {
        var message = this.getMessage('PREVIEW_MAP_TITLE_CREATION');
        if (!_.isNil(this.featureToModify)) {
            message = this.getMessage('PREVIEW_MAP_TITLE_MODIFICATION');
        }

        if (!this.isConforme(feature.getGeometry())) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            return;
        }

        var i;
        var result = [];
        if (_.isNil(this.featureToModify)) {
            //creation
            var sourceGeom = this._parser.read(feature.getGeometry());
            for (i = 0; i < this._supportLayers.length; i++) {
                var supportLayer = this._supportLayers[i];
                var supportFeatures = supportLayer.getSource().getFeatures();
                for (var j = 0; j < supportFeatures.length; j++) {
                    var aSupportFeature = supportFeatures[j];
                    var aGeometry = this._parser.read(aSupportFeature.getGeometry());
                    if (!sourceGeom.equalsTopo(aGeometry) &&
                            sourceGeom.intersects(aGeometry)) {
                        var splitResult = this.splitGeometryWithGeometry(feature.getGeometry(), aSupportFeature.getGeometry());
                        if (splitResult !== null) {
                            result = result.concat(splitResult);
                        }
                    }
                }
            }
        } else {
            //modification
            var aSplitResult = this.splitGeometryWithGeometry(feature.getGeometry(), this.featureToModify.getGeometry());
            if (!_.isNil(aSplitResult)) {
                result = result.concat(aSplitResult);
            }
        }

        var featureList = [];
        for (i = 0; i < result.length; i++) {
            var geometry = result[i];
            if (this.editionLayer.isSameGeometryType(geometry)) {
                var aFeature = new ol.Feature({
                    geometry: geometry
                });
                featureList.push(aFeature);
            }
        }

        if (featureList.length > 0) {
            var multiple = this.editionLayer.isCompositeGeometry() || _.isNil(this.featureToModify);
            this._showMapPreviewDialog(featureList, this._save.bind(this), this.cancel.bind(this), message, false, multiple);
        } else {
            alert(this.getMessage('WARNING_NO_RESULT'));
        }

        this.unselectAll();
    },
    _save: function (event) {
        var i;
        var features = event.data[0].getArray();
        if (_.isNil(this.featureToModify)) {
            if (this.isActiveVerifTopoConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeTopology(features[i].getGeometry())) {
                        return;
                    }
                }
            }
            if (this.isActiveVerifAppConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeRulesApp(features[i])) {
                        return;
                    }
                }
            }
            this._saveInCreationMode(features);
        } else {
            if (this.isActiveVerifTopoConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeTopologyFeatureToModify(features[i].getGeometry())) {
                        return;
                    }
                }
            }
            if (this.isActiveVerifAppConformity()) {
                for (i = 0; i < features.length; i++) {
                    if (!this.isConformeRulesAppFeatureToModify(features[i])) {
                        return;
                    }
                }
            }
            this._saveInModificationMode(features);
        }
    },
    /**
     * Méthode privée _saveInCreationMode
     * Créée et sauvegarde les features passées en paramètre.
     */
    _saveInCreationMode: function (features) {
        if (this.editionLayer.isCompositeGeometry()) {
            var coordinates = [];
            for (var i = 0; i < features.length; i++) {
                coordinates.push(features[i].getGeometry().getCoordinates());
            }

            var geometry = null;
            if (this.editionLayer.geometryType === LayerConstants.MULTI_LINE_GEOMETRY) {
                geometry = new ol.geom.MultiLineString(coordinates);
            } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                geometry = new ol.geom.MultiPolygon(coordinates);
            }

            var feature = new ol.Feature({
                geometry: geometry
            });
            if (this.editAttribut) {
                this._showAttributesDialog(feature, this.saveObjectsAfterDialog.bind(this), this.cancel.bind(this));
            } else {
                this.saveObjects(feature);
            }
        } else {
            if (this.editAttribut && features.length === 1) {
                this._showAttributesDialog(features[0], this.saveObjectsAfterDialog.bind(this), this.cancel.bind(this));
            } else {
                this.saveObjects(features);
            }
        }
    },
    _saveInModificationMode: function (features) {
        if (this.editionLayer.isCompositeGeometry()) {
            var coordinates = [];
            for (var i = 0; i < features.length; i++) {
                coordinates.push(features[i].getGeometry().getCoordinates());
            }

            var geometry = null;
            if (this.editionLayer.geometryType === LayerConstants.MULTI_LINE_GEOMETRY) {
                geometry = new ol.geom.MultiLineString(coordinates);
            } else if (this.editionLayer.geometryType === LayerConstants.MULTI_POLYGON_GEOMETRY) {
                geometry = new ol.geom.MultiPolygon(coordinates);
            }
            if (this.editAttribut) {
                this.featureToModify.set('initGeom', this.featureToModify.getGeometry().clone());
            }
            this.featureToModify.setGeometry(geometry);

        } else {
            if (features.length !== 1) {
                alert(this.getMessage('WARNING_NO_COMPOSITE'));
                return;
            } else {
                if (this.editAttribut) {
                    this.featureToModify.set('initGeom', this.featureToModify.getGeometry().clone());
                }
                this.featureToModify.setGeometry(features[0].getGeometry());
            }
        }
        if (this.editAttribut) {
            this._showAttributesDialog(this.featureToModify, this.saveObjectsAfterDialog.bind(this), this.cancel.bind(this));
        } else {
            this.saveObjects(this.featureToModify);
        }
    },
    saveObjectsAfterDialog: function (e) {
        this.saveObjects(e.data);
    },
    saveObjects: function (features) {
        if (!_.isArray(features)) {
            features = [features];
        }
        _.each(features, function (feature) {
            this.saveAnObject(feature);
        }.bind(this));

        if (Descartes.EditionManager.autoSave) {
            this.editionLayer.getSaveStrategy().save();
        }
        this.deactivate();
    },
    /**
     * Méthode saveObject
     * Permet de sauvegarde l'objet en base.
     */
    saveAnObject: function (feature) {
        feature.editionLayer = this.editionLayer;
        //feature.modified = false;

        if (_.isNil(this.featureToModify)) {
            //mode creation
            var objectId = this.createObjectId;
            if (!_.isNil(objectId)) {
                feature.set('idAttribut', objectId);
            }
            feature.set('state', FeatureState.INSERT);
            //this.createObjectId = null;
            //this.createAttributes = null;
            //feature.editionLayer = this.editionLayer;
            this.setFeatureStyle(feature, 'create');
            this.editionLayer.getFeatureOL_layers()[0].getSource().addFeature(feature);
        } else {
            //mode modification
            var state = this.featureToModify.get('state');
            if (state !== FeatureState.INSERT) {
                this.featureToModify.set('state', FeatureState.UPDATE);
            }
            //changement de couleur de la feature pour indique dans le mode manuel que la feature a été modifié
            this.setFeatureStyle(this.featureToModify, 'modify');
        }
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    cancel: function (e) {
        this.unselectAll();

        //rollback sur la géométrie en mode modification lors du click sur annuler
        //de la boite de dialogue des attributs
        var feature = e.data[0];
        if (!_.isNil(feature) && this.editAttribut) {
            var initGeom = feature.get('initGeom');
            if (!_.isNil(initGeom)) {
                feature.setGeometry(initGeom);
            }
        }
        //recalcule de la couche temp (l'outil de split détruit la géométrie splitée)
        this._updateSupportsLayers();
    },
    /**
     * Méthode: unselectAll
     * Déselection de toutes les features sélectionnées.
     */
    unselectAll: function () {
        this.editionLayer.unselectAll();
        this.interaction.getFeatures().clear();
    },
    /*
     * Méthode: featureSelectionChanged
     * Listener agissant sur l'état de l'outil lorsqu'un changement de selection est fait
     * dans le layer d'édition courant.
     */
    featureSelectionChanged: function () {
        if (!_.isNil(this.editionLayer) &&
                _.isNil(this._tempLayer) &&
                !_.isNil(this._toolConfig)) {
            if (this.editionLayer.getSelectedFeatures().length === 1) {
                this.featureToModify = this.editionLayer.getSelectedFeatures()[0];
            } else {
                this.featureToModify = null;
            }
        } else if (_.isNil(this._toolConfig)) {
            this.setAvailable(false);
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Creation.DivideCreation'
});

module.exports = Class;

/* global MODE */
var _ = require('lodash');

var CompositeRubberDeletion = require('./Deletion/CompositeRubberDeletion');
var RubberDeletion = require('./Deletion/RubberDeletion');
var RubberAnnotation = require('./Annotation/RubberAnnotation');
var EraseAnnotation = require('./Annotation/EraseAnnotation');
var SelectionDeletion = require('./Deletion/SelectionDeletion');

var namespace = {
	CompositeRubberDeletion: CompositeRubberDeletion,
	RubberDeletion: RubberDeletion,
	RubberAnnotation: RubberAnnotation,
	EraseAnnotation: EraseAnnotation,
	SelectionDeletion: SelectionDeletion
};

var Deletion = require('./Deletion');

_.extend(Deletion, namespace);

module.exports = Deletion;

/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');

var Utils = Descartes.Utils;
var Modification = require('../Modification');
var FeatureState = require('../../../Model/FeatureState');
var Symbolizers = require('../../../Symbolizers');

require('../css/verticeModification.css');

/**
 * Class: Descartes.Tool.Edition.Modification.VerticeModification
 * Outil de modification de sommet.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Modification>
 */
var Class = Utils.Class(Modification, {

    /**
     * Propriete: defaultMode
     * {Object} Objet JSON stockant le comportement par défaut de l'outil.
     *
     * Par defaut, la création de sommet est activée
     *
     * createVertices - {boolean} activation ou non de la création de sommet.
     */
    defaultMode: {
        createVertices: true,
        deleteCondition: function (mapBrowserEvent) {
            return ol.events.condition.singleClick(mapBrowserEvent) /*&&
                   mapBrowserEvent.originalEvent.ctrlKey*/;
        }
    },

    /**
     * Propriété hitTolerance
     */
    hitTolerance: 5,

    /**
     * Constructeur: Descartes.Tool.Edition.Modification.VerticeModification
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * createVertices - {boolean} activation ou non de la création de sommet.
     */
    initialize: function () {
        _.merge(this, this.defaultMode);
        Modification.prototype.initialize.apply(this, arguments);
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Modification.prototype.activate.apply(this, arguments);
            if (!_.isNil(this.olLayer) && !_.isNil(this.olLayer.map)) {
                var map = this.olLayer.map;
                map.on('pointermove', this.changeCursorPointerOnFeature, this);
            }
            this.underActivation = false;

            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(false);
              }
            });
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            if (!_.isNil(this.olLayer) && !_.isNil(this.olLayer.map)) {
                var map = this.olLayer.map;
                map.un('pointermove', this.changeCursorPointerOnFeature, this);
            }
            if (Descartes.EditionManager.autoSave) {
                this.checkForModificationToSave();
            }
            Modification.prototype.deactivate.apply(this, arguments);
            this.interaction.getFeatures().clear();
            if (!_.isNil(this.featureToModify)) {
                var style = Utils.getStyleByState(this.featureToModify);
                this.setFeatureStyle(this.featureToModify, style);
            }

            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(true);
              }
            });

        }
    },
    checkForModificationToSave: function () {
        if (this.editionLayer.isUnderEdition()) {
            if (this.editionLayer.isDirty()) {
                var confirm = function () {
                    this.editionLayer.getSaveStrategy().save();
                    //Inutile de concerver l'état des features.
                    //l'état est conservé coté serveur.
                    this.editionLayer.cancelChange();
                }.bind(this);

                var lose = function () {
                    this.editionLayer.cancelChange();
                    this.editionLayer.refresh();
                }.bind(this);

                this.editionLayer.confirmOrLoseModification(confirm, lose);
            }
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'interaction d'openlayers
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisée par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;

        if (!_.isNil(this.olMap)) {
            if (!_.isNil(this.interaction)) {
                this.olMap.removeInteraction(this.interaction);
            }
            if (!_.isNil(this.modifyInteraction)) {
                this.olMap.removeInteraction(this.modifyInteraction);
            }
        }

        var insertVertexCondition = ol.events.condition.always;
        if (this.createVertices === false) {
            insertVertexCondition = ol.events.condition.never;
        }

        this.interaction = new ol.interaction.Select({
            layers: [olLayer],
            toggleCondition: function (event) {
                if (event.type === 'singleclick') {
                    var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                        return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this.olLayer;
                        }.bind(this),
                        hitTolerance: this.hitTolerance
                    });
                    if (!_.isNil(features)) {
                        return features === this.featureToModify;
                    }
                    return false;
                }
                return false;
            }.bind(this)
        });
        this.interaction.on('select', this.onSelection.bind(this));
        this.interaction.setActive(false);

        var style;
        if (this.createVertices === true) {
           style = new ol.style.Style({
                image: new ol.style.RegularShape({
                    radius: 5,
                    radius2: 0,
                    points: 4,
                    angle: 0,
                    stroke: new ol.style.Stroke({
                        color: 'black',
                        width: 3
                      }),
                    fill: new ol.style.Fill({
                      color: 'black'
                    })
                  })
            });
        } else {
            style = new ol.style.Style();
        }

        this.modifyInteraction = new ol.interaction.Modify({
            insertVertexCondition: insertVertexCondition,
            style: style,
            deleteCondition: this.deleteCondition,
            features: this.interaction.getFeatures()
        });
        this.modifyInteraction.set('id', 'VerticeModification_' + Utils.createUniqueID());
        this.modifyInteraction.on('modifystart', this.onModificationStart, this);
        this.modifyInteraction.on('modifyend', this.onModificationEnd, this);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    /**
     * Retourne la liste des intéractions nécessaire au fonctionnament de l'outil en plus
     * de l'interaction principale.
     * @returns {Array}
     */
    getAdditionnalInterations: function () {
        return [this.modifyInteraction];
    },

    onSelection: function (event) {
        _.each(event.deselected, function (feature) {
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
        }.bind(this));

        if (event.deselected.length > 0) {
            if (event.deselected[0] === this.featureToModify) {
                var sameGeom = true;
                if (!_.isNil(this.initialGeom)) {
                    var initGeom = this._parser.read(this.initialGeom);
                    var newGeom = this._parser.read(this.featureToModify.getGeometry());
                    sameGeom = initGeom.equalsTopo(newGeom);
                }

                if (!sameGeom) {
                    if (this.editAttribut) {
                        this._showAttributesDialog(this.featureToModify, this.saveFeatureAfterDialog.bind(this), this.cancel.bind(this));
                    } else {
                        this.saveFeature(this.featureToModify);
                    }
                    this.interaction.getFeatures().clear();
                } else {
                    this.interaction.getFeatures().remove(this.featureToModify);
                }
            }
        }

        _.each(event.selected, function (feature) {
            this.featureToModify = feature;
            //this.setFeatureStyle(feature, 'select');
            var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['select']);
            var olStyle = olStyles[feature.getGeometry().getType()];
            var styles = [
                olStyle,
                new ol.style.Style({
                  image: new ol.style.RegularShape({
                    radius: 5,
                    radius2: 0,
                    points: 4,
                    angle: 0,
                    stroke: new ol.style.Stroke({
                        color: 'black',
                        width: 3
                      }),
                    fill: new ol.style.Fill({
                      color: 'black'
                    })
                  }),
                  geometry: function (feature) {
                    // return the coordinates of the first ring of the polygon
                    var coordinates, i, len;
                    var allCoordinates = [];
                    var geom = feature.getGeometry();
                    if (feature.getGeometry() instanceof ol.geom.Point || feature.getGeometry() instanceof ol.geom.MultiPoint) {
                        return null;
                    } else if (feature.getGeometry() instanceof ol.geom.LineString) {
                        coordinates = feature.getGeometry().getCoordinates();
                        geom = new ol.geom.MultiPoint(coordinates);
                    } else if (feature.getGeometry() instanceof ol.geom.Polygon) {
                        coordinates = feature.getGeometry().getCoordinates()[0];
                        geom = new ol.geom.MultiPoint(coordinates);
                    } else if (feature.getGeometry() instanceof ol.geom.MultiLineString) {
                        coordinates = feature.getGeometry().getCoordinates();
                        for (i = 0, len = coordinates.length; i < len; i++) {
                            allCoordinates = allCoordinates.concat(coordinates[i]);
                        }
                        geom = new ol.geom.MultiPoint(allCoordinates);
                    } else if (feature.getGeometry() instanceof ol.geom.MultiPolygon) {
                        coordinates = feature.getGeometry().getCoordinates();
                        for (i = 0, len = coordinates.length; i < len; i++) {
                            allCoordinates = allCoordinates.concat(coordinates[i][0]);
                        }
                        geom = new ol.geom.MultiPoint(allCoordinates);
                    }
                    return geom;
                  }
                })
              ];
            feature.setStyle(styles);
        }.bind(this));

    },
    /**
     * Lorsqu'une modification commence, on abonne toutes les features à la
     * méthode onModification afin de sauvegarder la feature en cours de
     * modification.
     */
    onModificationStart: function (e) {
        this.initialState = this.featureToModify.get('state');
        this.initialGeom = this.featureToModify.getGeometry().clone();
    },
    /**
     * Lorsqu'une modification est faite alors on récupère la feature modifée
     * via this.featureToModify et on modife le style, l'état et on sauvegarde
     * en mode sauvegarde automatique.
     */
    onModificationEnd: function (e) {
        if (!_.isNil(this.featureToModify)) {

            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(this.featureToModify.getGeometry(), true)) {
                this.featureToModify.setGeometry(this.initialGeom);
                return;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(this.featureToModify, true)) {
                this.featureToModify.setGeometry(this.initialGeom);
                return;
            }
            if (!_.isNil(this.featureToModify.getId())) {
                this.featureToModify.set('state', FeatureState.UPDATE);
            }
       }
    },
    saveFeatureAfterDialog: function (e) {
        this.saveFeature(e.data[0]);
    },
    saveFeature: function (feature) {
        feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                     feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }
        this.featureToModify = null;
        this.initialState = null;
        this.initialGeom = null;
    },
    cancel: function () {
        if (!_.isNil(this.featureToModify)) {
            if (!_.isNil(this.initialGeom)) {
                this.featureToModify.setGeometry(this.initialGeom);
            }
            this.featureToModify.set('state', this.initialState);

            var style = Utils.getStyleByState(this.featureToModify);
            this.setFeatureStyle(this.featureToModify, style);

            this.featureToModify = null;
            this.initialState = null;
            this.initialGeom = null;
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.VerticeModification'
});

module.exports = Class;

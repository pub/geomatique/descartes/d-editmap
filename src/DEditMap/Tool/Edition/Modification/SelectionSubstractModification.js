/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Selection = require('../Selection');
var FeatureState = require('../../../Model/FeatureState');
var Messages = require('../../../Messages');
var MapPreview = require('../../../Action/MapPreview');
var MapPreviewDialog = require('../../../UI/' + MODE + '/MapPreviewDialog');
var Symbolizers = require('../../../Symbolizers');

require('../css/selectionSubstractModification.css');
/**
 * Class: Descartes.Tool.Edition.Modification.SelectionSubstractModification
 * Outil de modification par soustraction.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Selection>
 */
var Class = Utils.Class(Selection, {

    /**
     * Propriété privée _parser
     *  Jsts parser
     */
    _parser: null,
    /**
     * Propriété privée : _tempLayer
     * Layer temporaire
     */
    _tempLayer: null,
    /**
     * Propriété privée : _toolConfig
     * Configuration de l'outil pour le layer en cours d'�dition
     */
    _toolConfig: null,
    /**
     * propriete featureToModify
     * La feature à modifier
     */
    featureToModify: null,
    /**
     * Constructeur: Descartes.Tool.Edition.Modification
     * Constructeur d'instances
     */
    initialize: function (options) {
        Selection.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
        this._parser = new jsts.io.OL3Parser();
        //indique que les géométries multiples doivent tre transformées
        //en géométrie simple dans le layer temporaire
        this._explodeMultiGeometryInTempLayer = true;
    },
    setSelectionEditionTool: function (selectionEditionTool) {
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            this.selectionEditionTool = selectionEditionTool;
            if (_.isNil(this.selectionEditionTool)) {
                 alert('Attention l\'outil de sélection doit être présent pour utiliser l\'outil de modification pour soustraction');
            }
        }
    },
    /**
     * Methode: activate
     * Les objets selectionnées dans la couche d'édition courante sont supprimés.
     */
    activate: function () {
        if (this.editionLayer.getSelectedFeatures().length > 0 ||
                !_.isNil(this.featureToModify)) {
            if (this.isAvailable() && !this.isActive()) {
                this.underActivation = true;
                if (!_.isNil(this.interaction)) {
                    this.olMap.removeInteraction(this.interaction);
                }
                Selection.prototype.activate.apply(this, arguments);

                if (Descartes.EditionManager.onlyAppMode && this.featureToModify) {
                    this.setFeatureStyle(this.featureToModify, 'select');
                }

                this.interaction = new ol.interaction.Select({
                    hitTolerance: this.hitTolerance,
                    multi: true,
                    layers: function (layer) {
                        return layer === this._tempLayer;
                    }.bind(this),
                    toggleCondition: function (event) {
                        if (event.type === 'singleclick') {
                            var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                                return feature;
                            }, {
                                layerFilter: function (aLayer) {
                                    return aLayer === this._tempLayer;
                                }.bind(this),
                                hitTolerance: this.hitTolerance
                            });
                            return !_.isNil(features);
                        }
                        return false;
                    }.bind(this)
                });
                this.interaction.on('select', this.onSelect.bind(this));
                this.interaction.set('id', 'SelectionSubstractModification_' + Utils.createUniqueID());
                this.olMap.addInteraction(this.interaction);
                this.underActivation = false;
            }
        } else {
            alert(this.getMessage('ERROR_NO_SELECT_GEOMETRY'));
            this.featureToModify = null;
            this.setAvailable(false);
            this.updateElement();
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            this.interaction.getFeatures().clear();
            if (Descartes.EditionManager.onlyAppMode) {
                this.setFeatureStyle(this.featureToModify, Utils.getStyleByState(this.featureToModify));
                this.featureToModify = null;
            }
            Selection.prototype.deactivate.apply(this, arguments);
        }
    },
    unselectAll: function () {
        if (!_.isNil(this.selectionEditionTool)) {
            this.selectionEditionTool.unselectAll();
        }
        Selection.prototype.unselectAll.apply(this, arguments);
    },
    /**
     * Appelée lors d'une selection
     */
    onSelect: function (event) {
        _.each(event.selected, function (feature) {
            this.setFeatureStyle(feature, 'select');
        }.bind(this));

        _.each(event.deselected, function (feature) {
            this.setFeatureStyle(feature, 'support');
        }.bind(this));

        if (event.selected.length === 0 && event.deselected.length > 0) {
            var clickout = true;
            if (event.deselected.length === 1) {
                var features = this.olMap.forEachFeatureAtPixel(event.mapBrowserEvent.pixel, function (feature) {
                    return feature;
                }, {
                    layerFilter: function (aLayer) {
                        return aLayer === this._tempLayer;
                    }.bind(this)
                });
                clickout = _.isNil(features);
            }
            if (clickout) {
                this.onClickOut(event.deselected);
            }
        }
    },
    /**
     * Méthode onClickOut
     * Appelée lorsque l'utilisateur clic en dehors de toute géométrie.
     */
    onClickOut: function (features) {
        var result = this.substractSelection(features);
        if (!_.isNil(result)) {
            var feature = new ol.Feature({
                geometry: result
            });
            this.setFeatureStyle(feature, 'default');
            this._showMapPreviewDialog(
                    [feature],
                    this._confirm,
                    this.cancel,
                    this.getMessage('PREVIEW_MAP_TITLE_MESSAGE'),
                    true);
        }
    },
    substractSelection: function (features) {
        if (_.isNil(features) || features.length < 1) {
            alert(this.getMessage('WARNING_NO_GEOMETRY'));
        } else {
            var geometryToModify = this._parser.read(this.featureToModify.getGeometry().clone());
            for (var i = 0; i < features.length; i++) {
                var feature = features[i];
                var jstsGeom = this._parser.read(feature.getGeometry().clone());

                var tempDiff = geometryToModify.difference(jstsGeom);
                if (!tempDiff.equalsTopo(geometryToModify)) {
                    geometryToModify = tempDiff;
                }
            }

            var result = this._parser.write(geometryToModify);
            if (geometryToModify.getNumGeometries() > 1 &&
                    !this.editionLayer.isCompositeGeometry()) {
                alert(this.getMessage('WARNING_MULTIPLE_GEOMETRY'));
                return null;
            }

            if (!this.isConforme(result)) {
                this.interaction.getFeatures().clear();
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                return null;
            }

            var aFeature = new ol.Feature({
                geometry: result
            });

            if (this.isActiveVerifTopoConformity() &&
                    !this.isConformeTopologyFeatureToModify(result)) {
                this.interaction.getFeatures().clear();
                return null;
            }

            if (this.isActiveVerifAppConformity() &&
                    !this.isConformeRulesAppFeatureToModify(aFeature)) {
                this.interaction.getFeatures().clear();
                return null;
            }

            return result;
        }
        return null;
    },
    /*
     * Méthode: featureSelectionChanged
     * Listener agissant sur l'état de l'outil lorsqu'un changement de selection
     * est fait dans le layer d'édition courant.
     */
    featureSelectionChanged: function () {
        if (!_.isNil(this.editionLayer) && !_.isNil(this._toolConfig)) {
            var hasSelection = this.editionLayer.getSelectedFeatures().length !== 0;
            if (this.editionLayer.isCompositeGeometry()) {
                var hasCompositeSelection = this.editionLayer.getcompositeSelectionLayerSelectedFeatures().length !== 0;
                if (hasCompositeSelection) {
                    this.featureToModify = this.editionLayer.getcompositeSelectionLayerSelectedFeatures()[0];
                    this.setAvailable(true);
                } else if (hasSelection) {
                    this.featureToModify = this.editionLayer.getSelectedFeatures()[0];
                    this.setAvailable(true);
                } else {
                    this.featureToModify = null;
                    this.deactivate();
                    this.setAvailable(false);
                }
            } else {
                if (hasSelection) {
                    this.featureToModify = this.editionLayer.getSelectedFeatures()[0];
                    this.setAvailable(true);
                } else {
                    this.featureToModify = null;
                    this.deactivate();
                    this.setAvailable(false);
                }
            }
        } else if (_.isNil(this._toolConfig)) {
            this.deactivate();
            this.setAvailable(false);
        }
        this.updateElement();
    },
    setAvailable: function () {
        if (_.isNil(this.featureToModify)) {
            Selection.prototype.setAvailable.apply(this, [false]);
        } else {
            Selection.prototype.setAvailable.apply(this, [true]);
        }
    },
    /**
     * Méthode privée _activateSupportLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et ajoute l'initialisation du layer temporaire.
     */
    _activateSupportLayers: function () {
        Selection.prototype._activateSupportLayers.apply(this, arguments);
        this._initializeSupportTempLayer();
    },
    /**
     * Méthode privée _updateSupportsLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et ajoute l'initialisation du layer temporaire.
     */
    _updateSupportsLayers: function () {
        Selection.prototype._updateSupportsLayers.apply(this, arguments);
        this._initializeSupportTempLayer();
    },
    /**
     * Méthode privée _deactivateSupportLayers
     * Surcharge de la méthode de Descartes.Tool.Edition et supprime le layer temporaire
     */
    _deactivateSupportLayers: function () {
        Selection.prototype._deactivateSupportLayers.apply(this, arguments);
        if (!_.isNil(this._tempLayer)) {
            this.olMap.removeLayer(this._tempLayer);
            this._tempLayer = null;
        }
    },
    _initializeSupportTempLayer: function () {
        if (this.olMap && this.isActive()) {
            var features = [];
            var charged = true;
            //récupération de toutes les features
            var selectedGeometry = this._parser.read(this.featureToModify.getGeometry());
            for (var i = 0; i < this._supportLayers.length; i++) {
                var supportLayer = this._supportLayers[i];

                // Si une couche a été chargé après que la couche temporaire ait été créée
                // il faut ajouter les features à la fin du chargement.
                if (this.mapContent.getLayerByOLLayer(supportLayer) !== this.editionLayer) {
                    if (!_.isNil(supportLayer.charged) && supportLayer.charged === false) {
                        charged = false;
                    }

                    supportLayer.getSource().on('featureloadend', function (event) {
                       if (this._tempLayer) {
                            var newFeatures = event.target.getFeatures();
                            if (!_.isNil(newFeatures)) {
                                var result = [];
                                for (var j = 0; j < newFeatures.length; j++) {
                                    var aFeature = newFeatures[j];
                                    result = result.concat(this._filterFeatureForTempLayer(aFeature, selectedGeometry));
                                }
                                this._tempLayer.getSource().addFeatures(result);
                            }
                            supportLayer.charged = true;
                       }
                    }.bind(this), this);
                }

                var supportFeatures = supportLayer.getSource().getFeatures();
                for (var j = 0; j < supportFeatures.length; j++) {
                    var feature = supportFeatures[j];
                    features = features.concat(this._filterFeatureForTempLayer(feature, selectedGeometry));
                }

            }


            //creation d'une couche temporaire
            if (_.isNil(this._tempLayer)) {
                this._tempLayer = new ol.layer.Vector({
                    title: 'selectionSubstractTemp',
                    source: new ol.source.Vector()
                });
                this.olMap.addLayer(this._tempLayer);
            } else {
                this._tempLayer.getSource().clear();
            }

            if (charged && features.length === 0) {
                alert(Messages.Descartes_EDITION_NOT_AVAILABLE_GEOMETRY);
            } else {
                this._tempLayer.getSource().addFeatures(features);
            }
        }
    },
    _filterFeatureForTempLayer: function (feature, selectedGeometry) {
        var result = [];
        var state = feature.get('state');
        if (state !== FeatureState.DELETE) {
            var aGeometry = this._parser.read(feature.getGeometry());
            if (selectedGeometry.intersects(aGeometry) && !aGeometry.equalsTopo(selectedGeometry)) {
                if (this._explodeMultiGeometryInTempLayer === true &&
                        Utils.isMultipleGeometry(feature.getGeometry())) {
                    var subGeoms = Utils.getSubGeom(feature.getGeometry());
                    for (var k = 0; k < subGeoms.length; k++) {
                        var subGeom = subGeoms[k].clone();
                        var polygonFeature = new ol.Feature(feature.getProperties());

                        polygonFeature.setGeometry(subGeom);
                        this.setFeatureStyle(polygonFeature, 'support');
                        result.push(polygonFeature);
                    }
                } else {
                    var clone = feature.clone();
                    this.setFeatureStyle(clone, 'support');
                    result.push(clone);
                }
            }
        }
        return result;
    },
    /*
     * Méthode: updateStateWithLayer
     * Met à jour l'état de l'outil en fonction des changements de couche en cours d'édition.
     * S'abonne au changement de selection du layer en cours d'édition et se désabonne si nécessaire.
     */
    updateStateWithLayer: function (event) {
        var editionLayer = event.data[0];
        if (Descartes.EditionManager.isGlobalEditonMode()) {
            this._toolConfig = null;
            if (editionLayer instanceof Descartes.Layer.EditionLayer) {
                this._toolConfig = editionLayer.substract;
                if (this.editionLayer !== editionLayer) {
                    if (!_.isNil(this.editionLayer)) {
                        this.unregisterToSelection(this.editionLayer.getFeatureOL_layers()[0]);
                    }
                    if (editionLayer.isUnderEdition()) {
                        this.registerToSelection(editionLayer.getFeatureOL_layers()[0]);
                        if (editionLayer.isCompositeGeometry()) {
                            this.registerToSelection(editionLayer.compositeSelectionLayer);
                            this.registerToSelection(editionLayer.compositeVectorLayer);
                        }
                    }

                    if (!_.isNil(this.olMap)) {
                        this.editionLayer = editionLayer;
                    }
                }
            } else if (!_.isNil(this.editionLayer)) {
                this.unregisterToSelection(this.editionLayer.getFeatureOL_layers()[0]);
                if (this.editionLayer.isCompositeGeometry()) {
                    this.unregisterToSelection(this.editionLayer.compositeSelectionLayer);
                    this.unregisterToSelection(this.editionLayer.compositeVectorLayer);
                }
            }
        } else {
            if (this.editionLayer === null) {
                //prise en compte du cas où aucun layer n'a été indiqué.
                //dans ce cas, on prend le premier layer.
                this.editionLayer = editionLayer;
            }
            this._toolConfig = this.editionLayer.substract;
            this.registerToSelection(this.editionLayer.getFeatureOL_layers()[0]);
            if (this.editionLayer.isCompositeGeometry()) {
                this.registerToSelection(this.editionLayer.compositeSelectionLayer);
                this.registerToSelection(this.editionLayer.compositeVectorLayer);
            }
        }
        Selection.prototype.updateStateWithLayer.apply(this, arguments);
    },
    _showMapPreviewDialog: function (features, save, cancel, title, doNotSelect) {
        var olSelectStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['select']);
        var featureSelectStyle = olSelectStyles[features[0].getGeometry().getType()];

        var viewOptions = {
            view: MapPreviewDialog,
            //layers: [wmsLayer], //pas de couche de fond pour le moment
            features: features,
            featureSelectStyle: featureSelectStyle,
            title: title,
            doNotSelect: doNotSelect
        };

        var action = new MapPreview(null, this.olMap, viewOptions);
        if (save) {
            action.events.register('save', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
        return action;
    },
    _confirm: function (event) {
        var mergedFeature = event.data[0];
        var mergedGeom = mergedFeature.getGeometry();

        if (!_.isNil(this.featureToModify.getId())) {
            if (this.editAttribut) {
                this.featureToModify.set('initGeom', this.featureToModify.getGeometry().clone());
            }
        }
        this.featureToModify.setGeometry(mergedGeom);

        if (this.editAttribut) {
            this._showAttributesDialog(this.featureToModify, this.saveObjects.bind(this), this.cancel.bind(this));
        } else {
            this.saveObjects();
        }
    },
    /**
     * Méthode saveObjects
     * Permet de sauvegarder des objets en base.
     */
    saveObjects: function () {
        this.createObjectId = null;
        this.createAttributes = null;

        if (this.featureToModify) {
            this.featureToModify.editionLayer = this.editionLayer;
            if (this.editionLayer &&
                    this.editionLayer.attributes &&
                    this.editionLayer.attributes.attributeId &&
                    this.editionLayer.attributes.attributeId.fieldName !== null) {
                           this.featureToModify.idAttribut = this.featureToModify.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
            }
            if (!_.isNil(this.featureToModify.getId())) {
                this.featureToModify.set('state', FeatureState.UPDATE);
                this.setFeatureStyle(this.featureToModify, 'modify');
            }
        }

        this.deactivate();
        this.unselectAll();
        //mode sauvegarde manuel
        if (Descartes.EditionManager.autoSave) {
            this.editionLayer.getSaveStrategy().save();
        }
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule le clonage.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    cancel: function (e) {
        var feature = e.data[0];
        if (!_.isNil(feature) && this.editAttribut) {
            var initGeom = feature.get('initGeom');
            if (!_.isNil(initGeom)) {
                feature.setGeometry(initGeom);
            }
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.SelectionSubstractModification'
});
module.exports = Class;

/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Selection = require('../Selection');
var FeatureState = require('../../../Model/FeatureState');
var Messages = require('../../../Messages');
var LayerConstants = Descartes.Layer;
var MapPreview = require('../../../Action/MapPreview');
var MapPreviewDialog = require('../../../UI/' + MODE + '/MapPreviewDialog');
var Symbolizers = require('../../../Symbolizers');


require('../css/mergeModification.css');

/**
 * Class: Descartes.Tool.Edition.Modification.MergeModification
 * Outil de modification par fusion.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Selection>
 */
var Class = Utils.Class(Selection, {

    /**
     * _parser Jsts parser
     */
    _parser: null,

    /**
     * Affichage des attributs dans la carte
     */
    showAttributes: true,

    multiple: true,

    /**
     * Constructeur: Descartes.Tool.Edition.Selection
     * Constructeur d'instances
     */
    initialize: function () {
        Selection.prototype.initialize.apply(this, arguments);
        this._parser = new jsts.io.OL3Parser();
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Selection.prototype.activate.apply(this, arguments);
            this.underActivation = false;
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            //this.unselectAll();
            Selection.prototype.deactivate.apply(this, arguments);
        }
    },
    updateStateWithLayer: function (event) {
        Selection.prototype.updateStateWithLayer.apply(this, arguments);

        var editionLayer = event.data[0];
        if (editionLayer && (editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY)) {
            this.setAvailable(false);
            this.updateElement();
        }
    },
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;
        this.interaction = new ol.interaction.Select({
            hitTolerance: this.hitTolerance,
            layers: function (layer) {
                if (!_.isNil(this.editionLayer)) {
                    for (var i = 0; i < this.editionLayer.OL_layers.length; i++) {
                        if (this.editionLayer.OL_layers[i] === layer) {
                            return true;
                        }
                    }
                }
                return false;
            }.bind(this),
            multi: true,
            toggleCondition: function (event) {
                if (event.type === 'singleclick') {
                    var features = event.map.forEachFeatureAtPixel(event.pixel, function (feature) {
                       return feature;
                    }, {
                        layerFilter: function (aLayer) {
                            return aLayer === this.olLayer;
                        }.bind(this)
                    });
                    return !_.isNil(features);
                }
                return false;
            }.bind(this)
        });
        this.interaction.on('select', this.toggleFeatureSelected.bind(this));
        this.interaction.on('select', this.onSelect.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    /**
     *
     */
    onSelect: function (event) {
        if (event.selected.length === 0) {
            if (event.deselected.length > 1) {
                this.onClickOut(event.deselected);
            }
        }
    },
    onClickOut: function (features) {
        this.selectedFeatures = features;
        if (!this.confirm || confirm(this.getMessage('CONFIRM_MERGE'))) {
            var copies = [];
            for (var i = 0; i < features.length; i++) {
                var intersection = this.checkIntersection(features[i], features);

                if (intersection) {
                    var copy = features[i].clone();
                    copy.unset('children');
                    copies.push(copy);
                }
            }

            if (copies.length === 0) {
                alert(this.getMessage('WARNING_NO_INTERSECTION'));
                this.cancel();
            } else {
                this._showMapPreviewDialog(copies,
                        this._showResult,
                        this.cancel,
                        this.getMessage('PREVIEW_MAP_TITLE_MESSAGE'),
                        false);
            }
        }
    },
    checkIntersection: function (featureToCheck, features) {
        var featureGeom = featureToCheck.getGeometry();
        if (!this.isConforme(featureGeom)) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            return false;
        }
        var jstsGeom = this._parser.read(featureGeom);

        for (var j = 0; j < features.length; j++) {
            var anotherFeatureGeom = features[j].getGeometry();
            if (anotherFeatureGeom !== featureGeom) {
                var anotherJstsGeom = this._parser.read(anotherFeatureGeom);

                if (!this.isConforme(anotherFeatureGeom)) {
                    alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                    return false;
                }

                if (jstsGeom.intersects(anotherJstsGeom)) {
                    return true;
                }
            }
        }
        return false;
    },
    /**
     * Private
     */
    _showMapPreviewDialog: function (features, save, cancel, title, doNotSelect) {
        var wmsSource = null;
        var layers = this.olMap.getLayers();
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            var source = layer.getSource();
            if (layer instanceof ol.source.ImageWMS) {
                wmsSource = source;
                break;
            }
        }

        /*var wmsLayer = new ol.layer.Image({
         source: wmsSource
         });*/
        var olStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['default']);
        var featureStyle = olStyles[features[0].getGeometry().getType()];

        var olSelectStyles = Symbolizers.getOlStyle(this.editionLayer.symbolizers['select']);
        var featureSelectStyle = olSelectStyles[features[0].getGeometry().getType()];

        var viewOptions = {
            view: MapPreviewDialog,
            //layers: [wmsLayer], //pas de couche de fond pour le moment
            features: features,
            featureStyle: featureStyle,
            featureSelectStyle: featureSelectStyle,
            title: title,
            doNotSelect: doNotSelect,
            showAttributes: this.showAttributes,
            multiple: false
        };

        var action = new MapPreview(null, this.olMap, viewOptions);
        if (save) {
            action.events.register('save', this, save);
        }
        if (cancel) {
            action.events.register('cancel', this, cancel);
        }
        return action;
    },
    _showResult: function (event) {
        this.featureToModify = event.data[0].getArray()[0];
        var mergeGeom = this.mergeGeometries(this.selectedFeatures);

        if (_.isNil(mergeGeom)) {
            return;
        }

        if (!this.isConforme(mergeGeom)) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            this.unselectAll();
            return;
        }

        var feature = new ol.Feature({
            geometry: mergeGeom
        });

        if (!_.isNil(mergeGeom)) {
            var message = this.getMessage('CONFIRM_MERGE_RESULT');
            this._showMapPreviewDialog([feature], this._confirm, this.cancel, message, true);
        }
    },
    _confirm: function (event) {
        var mergedFeature = event.data[0];
        var mergedGeom = mergedFeature.getGeometry();
        var selectedGeoms = [];
        for (var j = 0; j < this.selectedFeatures.length; j++) {
            selectedGeoms.push(this.selectedFeatures[j].getGeometry());
        }
        if (this.isActiveVerifTopoConformity() &&
                !this.isConformeTopology(mergedGeom, true, selectedGeoms)) {
            this.unselectAll();
            return;
        }

        if (this.isActiveVerifAppConformity() &&
                !this.isConformeRulesApp(mergedFeature, true)) {
            this.unselectAll();
            return;
        }

        var modifiedFeature = null;
        var jstsGeom = this._parser.read(this.featureToModify.getGeometry());
        for (var i = 0; i < this.selectedFeatures.length; i++) {
            var aFeature = this.selectedFeatures[i];
            if (this.editionLayer &&
                    this.editionLayer.attributes &&
                    this.editionLayer.attributes.attributeId &&
                    this.editionLayer.attributes.attributeId.fieldName !== null) {
                         aFeature.idAttribut = aFeature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
            }
            var aJstsGeom = this._parser.read(aFeature.getGeometry());
            if (jstsGeom.equalsTopo(aJstsGeom)) {
                modifiedFeature = aFeature;
                modifiedFeature.editionLayer = this.editionLayer;
                aFeature.editionLayer = this.editionLayer;
                aFeature.setGeometry(mergedGeom);
                if (!_.isNil(aFeature.getId())) {
                    //dans le cas où la feature existe en base, on la modifie.
                    //dans le cas contraire, on laisse l'état de la feature à 'CREATE'
                    this.setFeatureStyle(aFeature, 'modify');
                    aFeature.set('state', FeatureState.UPDATE);
                }
            } else {
                if (_.isNil(aFeature.getId())) {
                    //cas feature non existante en base.
                    //on la supprime juste de la carte.
                    var olLayer = this.editionLayer.getFeatureOL_layers()[0];
                    olLayer.getSource().removeFeature(aFeature);
                } else {
                    aFeature.editionLayer = this.editionLayer;
                    this.setFeatureStyle(aFeature, 'delete');
                    aFeature.set('state', FeatureState.DELETE);
                }
            }
        }

        if (this.editAttribut) {
            this._showAttributesDialog(modifiedFeature, this.saveObjects.bind(this));
        } else {
            this.saveObjects();
        }
    },
    /**
     * Méthode saveObjects
     * Permet de sauvegarder des objets en base.
     */
    saveObjects: function () {
        this.createObjectId = null;
        this.createAttributes = null;

        this.unselectAll();

        //mode sauvegarde manuel
        if (Descartes.EditionManager.autoSave) {
            this.editionLayer.getSaveStrategy().save();
        }
        this.deactivate();
    },
    /**
     * Methode: cancel
     * Appelée lorsque l'utilisateur annule le clonage.
     *
     * Paramètres:
     * feature - l'objet en cours de traitement.
     */
    cancel: function () {
        this.unselectAll();
    },
    /**
     * Méthode mergeGeometriesSelection
     * Fusion des géométries selectionnées en une seule.
     */
    mergeGeometries: function (selectedFeatures) {
        if (_.isNil(selectedFeatures) || selectedFeatures.length < 2) {
            alert(this.getMessage('WARNING_AT_LEAST_TWO_GEOM'));
            return null;
        }
        var resultGeom, feature, tempResult;
        for (var i = 0; i < selectedFeatures.length; i++) {
            feature = selectedFeatures[i];
            var jstsGeom = this._parser.read(feature.getGeometry().clone());

            if (_.isNil(resultGeom)) {
                resultGeom = jstsGeom;
            } else if (resultGeom.intersects(jstsGeom)) {
                //Dans le cas des lignes, il faut utiliser l'operation lineMerge de jsts
                //et non pas la fonction union, qui crée des géométries multiples non désirées
                if (this.editionLayer.geometryType === LayerConstants.LINE_GEOMETRY) {


                    var lineMerger = new jsts.operation.linemerge.LineMerger();
                    //pb avec jsts 1.6.0, il faut passer par addEdge
                    //lineMerger.add(resultGeom);
                    //lineMerger.add(jstsGeom);
                    if (resultGeom instanceof jsts.geom.LineString) {
                        if (lineMerger._factory === null) {
                            lineMerger._factory = resultGeom.getFactory();
                        }
                        lineMerger._graph.addEdge(resultGeom);
                    } else {
                        lineMerger.add(resultGeom);
                    }

                    lineMerger._graph.addEdge(jstsGeom);

                    if (lineMerger.getMergedLineStrings() === 0) {
                        alert(this.getMessage('WARNING_NO_GEOM_RESULT'));
                        return null;
                    }

                    if (lineMerger.getMergedLineStrings() > 1) {
                        alert(this.getMessage('WARNING_MULTI_GEOM_RESULT'));
                        return null;
                    }
                    tempResult = lineMerger.getMergedLineStrings().get(0);
                } else {
                    tempResult = resultGeom.union(jstsGeom);
                }

                if (tempResult.isGeometryCollection() &&
                        !this.editionLayer.isCompositeGeometry()) {
                    alert(this.getMessage('WARNING_MULTI_GEOM_RESULT'));
                    return null;
                }
                resultGeom = tempResult;

            } else if (this.editionLayer.isCompositeGeometry() &&
                    jstsGeom instanceof jsts.geom.GeometryCollection) {
                resultGeom.geometries = resultGeom._geometries.concat(jstsGeom._geometries);
            } else {
                alert(this.getMessage('WARNING_NO_INTERSECTION'));
                return null;
            }
        }
        if (!_.isNil(resultGeom)) {
            resultGeom = this._parser.write(resultGeom);
            if (!this.isConforme(resultGeom)) {
                alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
                return null;
            }
        }
        return resultGeom;
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.MergeModification'
});

module.exports = Class;

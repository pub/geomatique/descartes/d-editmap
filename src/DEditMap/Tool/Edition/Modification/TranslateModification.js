/* global MODE, Descartes */
var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Modification = require('../Modification');
var FeatureState = require('../../../Model/FeatureState');

require('../css/translateModification.css');

/**
 * Class: Descartes.Tool.Edition.Modification.TranslateModification
 * Outil de modification par translation.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Modification>
 */
var Class = Utils.Class(Modification, {

    /**
     * Propriete: editAttribut
     * {boolean} activation ou non de la demande d'édition des attributs
     * lors de la creation de nouvelles géométries. (Par défaut: false)
     */
    editAttribut: false,
    /**
     * Constructeur: Descartes.Tool.Edition.Modification.GlobalModification
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * rotation - {boolean} activation ou non de la rotation.
     * resize - {boolean} activation ou non du redimentionnement.
     * keepAspectRatio - {boolean} garder ou non les proportions lors du redimentionnement.
     * move - {boolean} activation ou non de la translation.
     */
    initialize: function () {
        Modification.prototype.initialize.apply(this, arguments);
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Modification.prototype.activate.apply(this, arguments);
            this.underActivation = false;
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'interaction d'openlayers
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisée par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;

        if (!_.isNil(this.interaction) && !_.isNil(this.olMap)) {
            this.olMap.removeInteraction(this.interaction);
        }

        this.interaction = new ol.interaction.Translate({
            layers: [olLayer]
        });
        this.interaction.set('id', 'TranslateModification_' + Utils.createUniqueID());
        this.interaction.on('translatestart', this.onModificationStart.bind(this));
        this.interaction.on('translateend', this.onModificationEnd.bind(this));
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    onModificationStart: function (e) {
        this.initialGeom = e.features.item(0).getGeometry().clone();
    },

    onModificationEnd: function (e) {
        if (e.features.getLength() === 1) {
            var feature = e.features.item(0);

            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(feature.getGeometry(), true)) {
                feature.setGeometry(this.initialGeom);
                return;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(feature, true)) {
                feature.setGeometry(this.initialGeom);
                return;
            }

            if (!_.isNil(feature.getId())) {
                this.setFeatureStyle(feature, 'modify');
                feature.set('state', FeatureState.UPDATE);
            }

            if (this.editAttribut) {
                this._showAttributesDialog(feature, this.saveFeatureAfterDialog.bind(this));
            } else {
                this.saveFeature(feature);
            }
        }
    },
    saveFeatureAfterDialog: function (e) {
        this.saveFeature(e.data[0]);
    },
    saveFeature: function (feature) {
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.TranslateModification'
});

module.exports = Class;

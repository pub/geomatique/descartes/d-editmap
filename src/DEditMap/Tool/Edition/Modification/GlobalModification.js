/* global MODE, Descartes */
var ol = require('openlayers');
var _ = require('lodash');

var Utils = Descartes.Utils;
var Modification = require('../Modification');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;

require('ol-ext');
// require('ol-ext-css');
require('../../../OpenLayersExtras/ol-ext/TransformInteraction');

require('../css/globalModification.css');

/**
 * Class: Descartes.Tool.Edition.Modification.GlobalModification
 * Outil de modification par rotation, translation, homothetie.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Modification>
 */
var Class = Utils.Class(Modification, {

    /**
     * Propriete: editAttribut
     * {boolean} activation ou non de la demande d'édition des attributs
     * lors de la creation de nouvelles géométries. (Par défaut: false)
     */
    editAttribut: false,
    /**
     * Propriete: defaultMode
     * {Object} Objet JSON stockant le comportement par défaut de l'outil.
     *
     * Par defaut, la rotation, le redimentionnement en gardant les proportions et la translation sont actif.
     *
     * rotation - {boolean} activation ou non de la rotation.
     * resize - {boolean} activation ou non du redimentionnement.
     * keepAspectRatio - {boolean} garder ou non les proportions lors du redimentionnement.
     * move - {boolean} activation ou non de la translation.
     */
    defaultMode: {
        rotation: true,
        resize: true,
        keepAspectRatio: true,
        move: true
    },
    /**
     * Constructeur: Descartes.Tool.Edition.Modification.GlobalModification
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * rotation - {boolean} activation ou non de la rotation.
     * resize - {boolean} activation ou non du redimentionnement.
     * keepAspectRatio - {boolean} garder ou non les proportions lors du redimentionnement.
     * move - {boolean} activation ou non de la translation.
     */
    initialize: function () {
        _.merge(this, this.defaultMode);
        Modification.prototype.initialize.apply(this, arguments);
    },
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Modification.prototype.activate.apply(this, arguments);
            this.underActivation = false;

            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(false);
              }
            });
        }
    },
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            this.deActivating = true;
            if (Descartes.EditionManager.autoSave) {
                this.checkForModificationToSave();
            }
            Modification.prototype.deactivate.apply(this, arguments);
            this.deActivating = false;
            this.feature = null;

            //changement de style a la selection
            if (!_.isNil(this.featureToModify)) {
                var style = Utils.getStyleByState(this.featureToModify);
                this.setFeatureStyle(this.featureToModify, style);
                this.featureToModify = null;
            }

            var interactions = this.olMap.getInteractions();
            interactions.forEach(function (interaction) {
              if (interaction instanceof ol.interaction.DragPan) {
                   interaction.setActive(true);
              }
            });
        }
    },

    checkForModificationToSave: function () {
        if (this.editionLayer.isUnderEdition()) {
            if (this.editionLayer.isDirty()) {
                var confirm = function () {
                    this.editionLayer.getSaveStrategy().save();
                    //Inutile de concerver l'état des features.
                    //l'état est conservé coté serveur.
                    this.editionLayer.cancelChange();
                }.bind(this);

                var lose = function () {
                    this.editionLayer.cancelChange();
                    this.editionLayer.refresh();
                }.bind(this);

                this.editionLayer.confirmOrLoseModification(confirm, lose);
            }
        }
    },
    /*
     * Methode privé: initializeOLFeature
     * Initialise l'interaction d'openlayers
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisée par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;

        if (!_.isNil(this.interaction) && !_.isNil(this.olMap)) {
            this.olMap.removeInteraction(this.interaction);
        }

        var aspectRatioCondition = ol.events.condition.shiftKeyOnly;
        if (this.keepAspectRatio === true) {
            aspectRatioCondition = ol.events.condition.always;
        }

        if (this.editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                this.editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY) {
            this.interaction = new ol.interaction.Translate({
                layers: [olLayer]
            });
            this.interaction.on('translatestart', this.onModificationStart.bind(this));
            this.interaction.on('translateend', this.onTranslationForPoint.bind(this));
        } else {

            this.interaction = new ol.interaction.Transform({
                layers: [olLayer],
                translate: this.move,
                translateFeature: false,
                scale: this.resize,
                stretch: false,
                rotate: this.rotation,
                keepAspectRatio: aspectRatioCondition
            });
            this.interaction.set('id', 'GlobalModification_' + Utils.createUniqueID());
            if (this.rotation) {
                this.interaction.on('rotatestart', this.onModificationStart.bind(this));
                this.interaction.on('rotateend', this.onModification.bind(this));
            }
            if (this.move) {
                this.interaction.on('translatestart', this.onModificationStart.bind(this));
                this.interaction.on('translateend', this.onModification.bind(this));
            }
            if (this.resize) {
                this.interaction.on('scalestart', this.onModificationStart.bind(this));
                this.interaction.on('scaleend', this.onModification.bind(this));
            }
            this.interaction.on('select', this.saveModification.bind(this));
        }
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },
    onModificationStart: function (e) {
        var feature = e.feature;
        if (!_.isNil(e.features)) {
            feature = e.features.item(0);
        }
        this.setFeatureStyle(feature, 'select');
        this.initialState = feature.get('state');
        this.initialGeom = feature.getGeometry().clone();
    },
    saveModification: function (e) {
        if (!this.underActivation && !this.deActivating) {

            //changement de style a la selection
            if (!_.isNil(this.featureToModify)) {
                var style = Utils.getStyleByState(this.featureToModify);
                this.setFeatureStyle(this.featureToModify, style);
            }
            if (!_.isNil(e.feature)) {
                this.featureToModify = e.feature;
                this.setFeatureStyle(this.featureToModify, 'select');
            }
            //
            var feature = e.feature;
            if (_.isNil(feature) && !_.isNil(this.feature) ||
                    (!_.isNil(feature) && !_.isNil(this.feature) && feature !== this.feature)) {
                if (this.editAttribut) {
                    this._showAttributesDialog(this.feature, this.saveFeatureAfterDialog.bind(this), this.cancel.bind(this));
                } else {
                    this.saveFeature(this.feature);
                }
            }
        }
    },
    saveFeatureAfterDialog: function (e) {
        this.saveFeature(e.data[0]);
    },
    saveFeature: function (feature) {
        var style = Utils.getStyleByState(feature);
        this.setFeatureStyle(feature, style);
        feature.editionLayer = this.editionLayer;
        if (this.editionLayer &&
                this.editionLayer.attributes &&
                this.editionLayer.attributes.attributeId &&
                this.editionLayer.attributes.attributeId.fieldName !== null) {
                     feature.idAttribut = feature.getProperties()[this.editionLayer.attributes.attributeId.fieldName];
        }
        if (Descartes.EditionManager.autoSave) {
            var saveStrategy = this.olLayer.getSource().get('saveStrategy');
            if (!_.isNil(saveStrategy)) {
                saveStrategy.save(feature);
            }
        }
        this.feature = null;
        this.interaction.setActive(false);
        this.interaction.setActive(true);
    },

    onModification: function (e) {
        this.feature = e.feature;

        if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(this.feature.getGeometry(), true)) {
            this.feature.setGeometry(this.initialGeom);
            return;
        }

        if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(this.feature, true)) {
            this.feature.setGeometry(this.initialGeom);
            return;
        }

        if (!_.isNil(this.feature.getId())) {
            this.feature.set('state', FeatureState.UPDATE);
        }

    },
    onTranslationForPoint: function (e) {
        if (e.features.getLength() === 1) {
            var feature = e.features.item(0);

            if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(feature.getGeometry(), true)) {
                feature.setGeometry(this.initialGeom);
                return;
            }

            if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(feature, true)) {
                feature.setGeometry(this.initialGeom);
                return;
            }

            //this.setFeatureStyle(feature, 'select');
            if (!_.isNil(feature.getId())) {
                feature.set('state', FeatureState.UPDATE);
            }

            if (this.editAttribut) {
                this._showAttributesDialog(feature, this.saveFeatureAfterDialog.bind(this));
            } else {
                this.saveFeature(feature);
            }
        }
    },
    cancel: function () {
        if (!_.isNil(this.featureToModify)) {
            if (!_.isNil(this.initialGeom)) {
                this.featureToModify.setGeometry(this.initialGeom);
            }
            this.featureToModify.set('state', this.initialState);

            var style = Utils.getStyleByState(this.featureToModify);
            this.setFeatureStyle(this.featureToModify, style);

            this.featureToModify = null;
            this.initialState = null;
            this.initialGeom = null;
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.GlobalModification'
});

module.exports = Class;

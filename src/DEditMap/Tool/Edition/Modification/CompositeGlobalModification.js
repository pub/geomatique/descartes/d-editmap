/* global MODE, Descartes */
var ol = require('openlayers');
var _ = require('lodash');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var GlobalModification = require('./GlobalModification');
var FeatureState = require('../../../Model/FeatureState');
var LayerConstants = Descartes.Layer;
var Messages = require('../../../Messages');

require('ol-ext');
// require('ol-ext-css');

require('../css/compositeGlobalModification.css');

/**
 * Class: Descartes.Tool.Edition.Modification.CompositeGlobalModification
 * Outil de modification d'uen géométrie d'un objet par rotation, translation, homothetie.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Modification.GlobalModification>
 */
var Class = Utils.Class(GlobalModification, {

    _parser: new jsts.io.OL3Parser(),

    /**
     * Methode: setAvailable
     * l'outils n'est disponible que pour les geometries composite
     *
     * Paramètres:
     * available  - {boolean} indiquant le nouveau statut de disponibili�
     */
    setAvailable: function () {
        if (this.editionLayer && this.editionLayer.isCompositeGeometry()) {
            GlobalModification.prototype.setAvailable.apply(this, arguments);
        } else {
            GlobalModification.prototype.setAvailable.apply(this, [false]);
        }
    },
    /**
     * Methode: activate
     * Appeler lorsque l'outil devient actif.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            GlobalModification.prototype.activate.apply(this, arguments);
            this.editionLayer.getSaveStrategy().events.register('success', this, this.deactivate);

            this.editionLayer.showCompositeSelectionLayer();
            //Puis on y ajoute les objets composites décomposés
            this.editionLayer.splitCompositeFeatures();
            this.underActivation = false;
        }
    },

    /**
     * Methode: deactivate
     * Appeler lorsque l'outil est désactivé.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            this.deActivating = true;
            this.editionLayer.clearCompositeSelectionLayer();
            //on réaffiche la couche avec les objets composites regroupés
            this.editionLayer.hideCompositeSelectionLayer();
            this.editionLayer.getSaveStrategy().events.unregister('success', this, this.deactivate);
            this.feature = null;
            if (!_.isNil(this.featureToModify)) {
                var parentFeature = this.featureToModify.get('parent');
                if (!_.isNil(parentFeature)) {
                     var style = Utils.getStyleByState(parentFeature);
                     this.setFeatureStyle(parentFeature, style);
                    //il faut modifier les autres features filles
                     var children = parentFeature.get('children');
                     _.each(children, function (child) {
                             this.setFeatureStyle(child, style);
                     }.bind(this));
                }
                this.featureToModify = null;
            }
            GlobalModification.prototype.deactivate.apply(this, arguments);
            this.deActivating = false;
        }
    },

    /*
     * Methode privé: initializeOLFeature
     * Initialise l'interaction d'openlayers
     *
     * Paramètres:
     * olLayer - {<OpenLayers.Layer>} Couche utilisée par l'outil.
     */
    initializeOLFeature: function (olLayer) {
        this.olLayer = olLayer;

        if (!_.isNil(this.interaction) && !_.isNil(this.olMap)) {
            this.olMap.removeInteraction(this.interaction);
        }

        var aspectRatioCondition = ol.events.condition.shiftKeyOnly;
        if (this.keepAspectRatio === true) {
            aspectRatioCondition = ol.events.condition.always;
        }

        if (this.editionLayer.geometryType === LayerConstants.POINT_GEOMETRY ||
                this.editionLayer.geometryType === LayerConstants.MULTI_POINT_GEOMETRY) {
            this.interaction = new ol.interaction.Translate({
                layers: [this.editionLayer.compositeSelectionLayer]
            });
            this.interaction.on('translatestart', this.onTranslateModificationStart.bind(this));
            this.interaction.on('translateend', this.onTranslationForPoint.bind(this));
        } else {
            this.interaction = new ol.interaction.Transform({
                layers: [this.editionLayer.compositeSelectionLayer],
                translate: this.move,
                translateFeature: false,
                scale: this.resize,
                stretch: false,
                rotate: this.rotation,
                keepAspectRatio: aspectRatioCondition
            });
            this.interaction.set('id', 'GlobalCompositeModification_' + Utils.createUniqueID());
            if (this.rotation) {
                this.interaction.on('rotatestart', this.onModificationStart.bind(this));
                this.interaction.on('rotateend', this.onModification.bind(this));
            }
            if (this.move) {
                this.interaction.on('translatestart', this.onModificationStart.bind(this));
                this.interaction.on('translateend', this.onModification.bind(this));
            }
            if (this.resize) {
                this.interaction.on('scalestart', this.onModificationStart.bind(this));
                this.interaction.on('scaleend', this.onModification.bind(this));
            }
            this.interaction.on('select', this.saveModification.bind(this));
        }
        this.interaction.setActive(false);

        //dans le mode piloté par la carte, il faut initialiser l'outil qu'une seule fois.
        if (!Descartes.EditionManager.isGlobalEditonMode() && !this._OLToolInitialize) {
            this._OLToolInitialize = true;
        }
    },

    saveModification: function (e) {
        if (!this.underActivation && !this.deActivating) {

            var feature = e.feature;

            //changement de style a la selection
            if (!_.isNil(this.featureToModify)) {
                var pFeature = this.featureToModify.get('parent');
                if (!_.isNil(pFeature)) {
                     var style = Utils.getStyleByState(pFeature);
                     this.setFeatureStyle(pFeature, style);
                    //il faut modifier les autres features filles
                     var child = pFeature.get('children');
                     _.each(child, function (c) {
                             this.setFeatureStyle(c, style);
                     }.bind(this));
                }
            }
            if (!_.isNil(feature)) {
                this.setFeatureStyle(feature, 'select');
                this.featureToModify = feature;
                this.setFeatureStyle(this.featureToModify, 'select');
                var parentFeature = feature.get('parent');
                if (!_.isNil(parentFeature)) {
                     this.setFeatureStyle(parentFeature, 'select');
                    //il faut modifier les autres features filles
                     var children = parentFeature.get('children');
                     _.each(children, function (child) {
                         if (child !== feature) {
                             this.setFeatureStyle(child, 'select');
                         }
                     }.bind(this));
                }
            }
            if (_.isNil(feature) && !_.isNil(this.feature) ||
                    (!_.isNil(feature) && !_.isNil(this.feature) && feature !== this.feature)) {
                if (this.editAttribut) {
                    this._showAttributesDialog(this.feature, this.saveFeatureAfterDialog.bind(this), this.cancel.bind(this));
                } else {
                    this.saveFeature(this.feature);
                }
            }
        }
    },

    onModificationStart: function (e) {
        var feature = e.feature;
        this.initialGeom = feature.getGeometry().clone();
        this.initialGeomJsts = this._parser.read(this.initialGeom);
    },

    onModification: function (e) {
        var feature = e.feature;
        var parentFeature = feature.get('parent');
        var parentGeom = parentFeature.getGeometry();

        var subGeoms = Utils.getSubGeom(parentGeom);

        var coordinates = [];
        for (var i = 0; i < subGeoms.length; i++) {
            var subGeom = subGeoms[i];
            var coordinate = subGeom.getCoordinates();
            var subGeomJsts = this._parser.read(subGeom);

            if (subGeomJsts.equalsExact(this.initialGeomJsts)) {
                coordinate = feature.getGeometry().getCoordinates();
            }
            coordinates.push(coordinate);
        }

        var multiGeom = null;
        if (parentGeom instanceof ol.geom.MultiPolygon) {
            multiGeom = new ol.geom.MultiPolygon(coordinates);
        } else if (parentGeom instanceof ol.geom.MultiLineString) {
            multiGeom = new ol.geom.MultiLineString(coordinates);
        } else if (parentGeom instanceof ol.geom.MultiPoint) {
            multiGeom = new ol.geom.MultiPoint(coordinates);
        }

        if (!this.isConforme(multiGeom)) {
            alert(Messages.Descartes_EDITION_NOT_CONFORM_GEOMETRY);
            feature.setGeometry(this.initialGeom);
            return;
        }

        if (this.isActiveVerifTopoConformity() && !this.isConformeTopology(multiGeom, true, this.initialGeom, true)) {
            feature.setGeometry(this.initialGeom);
            return;
        }

        if (this.isActiveVerifAppConformity() && !this.isConformeRulesApp(parentFeature, true)) {
            feature.setGeometry(this.initialGeom);
            return;
        }

        if (multiGeom !== null) {
            parentFeature.setGeometry(multiGeom);
            parentFeature.editionLayer = this.editionLayer;
            if (!_.isNil(parentFeature.getId())) {
                parentFeature.set('state', FeatureState.UPDATE);
            }
            var style = Utils.getStyleByState(parentFeature);
            this.setFeatureStyle(parentFeature, style);
            this.setFeatureStyle(feature, style);

            //il faut modifier les autres features filles
            var children = parentFeature.get('children');
            _.each(children, function (child) {
                if (child !== feature) {
                    this.setFeatureStyle(child, style);
                }
            }.bind(this));

        }
        this.feature = parentFeature;
    },
    onTranslateModificationStart: function (e) {
        if (e.features.getLength() === 1) {
            var feature = e.features.item(0);
            this.initialGeom = feature.getGeometry();
            this.initialGeomJsts = this._parser.read(this.initialGeom);

            this.setFeatureStyle(feature, 'select');
            this.featureToModify = feature;
            this.setFeatureStyle(this.featureToModify, 'select');
            var parentFeature = feature.get('parent');
            if (!_.isNil(parentFeature)) {
                 this.setFeatureStyle(parentFeature, 'select');
                //il faut modifier les autres features filles
                 var children = parentFeature.get('children');
                 _.each(children, function (child) {
                     if (child !== feature) {
                         this.setFeatureStyle(child, 'select');
                     }
                 }.bind(this));
            }
        }
    },
    onTranslationForPoint: function (e) {
        if (e.features.getLength() === 1) {
            var feature = e.features.item(0);

            this.onModification({
                feature: feature
            });

            if (this.editAttribut) {
                this._showAttributesDialog(this.feature, this.saveFeatureAfterDialog.bind(this));
            } else {
                this.saveFeature(this.feature);
            }
        }
    },
    CLASS_NAME: 'Descartes.Tool.Edition.Modification.CompositeGlobalModification'
});

module.exports = Class;

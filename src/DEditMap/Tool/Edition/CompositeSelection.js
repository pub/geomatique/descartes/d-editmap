/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');
var jsts = require('jsts');

var Utils = Descartes.Utils;
var Selection = require('./' + MODE + '/Selection');

require('./css/compositeSelection.css');

/**
 * Class: Descartes.Tool.Edition.CompositeSelection
 * Outil de sélection d'un élément d'un objet composite pour l'édition.
 *
 * Hérite de:
 *  - <Descartes.Tool.Edition.Selection>
 */
var Class = Utils.Class(Selection, {
    /**
     * Propriete _parser
     * Jsts Parser
     */
    _parser: null,

    /**
     * Constructeur: Descartes.Tool.Edition.Selection
     * Constructeur d'instances
     */
    initialize: function () {
        this._parser = new jsts.io.OL3Parser();

        Selection.prototype.initialize.apply(this, arguments);
        this.interaction = new ol.interaction.Select({
            //multi: true,
            toggleCondition: ol.events.condition.singleClick,
            hitTolerance: this.hitTolerance,
            layers: function (layer) {
                if (!_.isNil(this.editionLayer)) {
                    return layer === this.editionLayer.compositeSelectionLayer;
                }
                return false;
            }.bind(this)
        });
        this.interaction.set('id', 'compositeSelectionInteraction');
        this.interaction.on('select', this.toggleFeatureSelected.bind(this));
        this.interaction.setActive(false);
    },

    /**
     * Methode: setAvailable
     * l'outils n'est disponible que pour les geometries composite
     *
     * Paramètres:
     * available  - {boolean} indiquant le nouveau statut de disponibili�
     */
    setAvailable: function () {
        if (this.editionLayer && this.editionLayer.isCompositeGeometry()) {
            Selection.prototype.setAvailable.apply(this, arguments);
        } else {
            Selection.prototype.setAvailable.apply(this, [false]);
        }
    },

    /**
     * Methode: activate
     * Permet d'activer l'outil.
     */
    activate: function () {
        if (this.isAvailable() && !this.isActive()) {
            this.underActivation = true;
            Selection.prototype.activate.apply(this, arguments);

            this.unselectAll();
            this.editionLayer.showCompositeSelectionLayer();
            this.registerToSelection(this.editionLayer.compositeSelectionLayer);
            //Puis on y ajoute les objets composites décomposés
            this.editionLayer.splitCompositeFeatures();
            this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);
            this.underActivation = false;
        }
    },
    /**
     * Methode: deactivate
     * Permet de désactiver l'outil.
     */
    deactivate: function () {
        if (this.isAvailable() && this.isActive()) {
            Selection.prototype.deactivate.apply(this, arguments);

            this.unselectAll();
            //on réaffiche la couche avec les objets composites regroupés
            this.editionLayer.hideCompositeSelectionLayer();
            this.unregisterToSelection(this.editionLayer.compositeSelectionLayer);
            this.olMap.un('pointermove', this.changeCursorPointerOnFeature, this);
        }
    },
    unselectAll: function () {
        Selection.prototype.unselectAll.apply(this, arguments);
        if (!_.isNil(this.editionLayer)) {
            var featureSelected = this.editionLayer.getcompositeSelectionLayerSelectedFeatures();

            _.each(featureSelected, function (feature) {
                feature.set('selected', false);
                var style = Utils.getStyleByState(feature);
                this.setFeatureStyle(feature, style);
                if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
                    if (!this.sendAppResults) {
                        this.unhighlight();
                    }
                }
            }.bind(this));
        }
        this.interaction.getFeatures().clear();
    },

    toggleFeatureSelected: function (event) {
        event.deselected = this.editionLayer.getcompositeSelectionLayerSelectedFeatures();

        _.each(event.deselected, function (feature) {
            feature.set('selected', false);
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
            if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
                if (!this.sendAppResults) {
                    this.unhighlight();
                }
            }
            if (!this.multiple) {
                this.interaction.getFeatures().remove(feature);
            }
        }.bind(this));

        _.each(event.selected, function (feature) {
            feature.set('selected', true);
            this.setFeatureStyle(feature, 'select');
            if (this.showInfos.attributes || this.showInfos.geometricCalculs || this.showInfos.identifier) {
                if (this.sendAppResults) {
                    this.sendInformations(feature);
                } else {
                    this.highlight(feature);
                }
            }
        }.bind(this));
    },
    featureSelectionChanged: function (event) {
        if (this.editionLayer !== null) {
            if (!_.isNil(event) && !_.isNil(event.target)) {
                var feature = event.target;
                var selected = feature.get('selected');

                if (selected === true) {
                    this.featureCompositeSelectionSelected(feature);
                } else {
                    this.featureCompositeSelectionUnSelected(feature);
                }
            }
        }
    },
    /**
     * Methode: featureCompositeSelectionSelected
     * Gestion du style lorsqu'une geométrie d'un objet composite est sélectionné
     */
    featureCompositeSelectionSelected: function (feature) {
        //this.setFeatureStyle(feature, 'select');

        var selectedGeom = this._parser.read(feature.getGeometry());

        var otherFeatures = this.editionLayer.compositeSelectionLayer.getSource().getFeatures();

        var parent = feature.get('parent');
        var subGeoms = Utils.getSubGeom(parent.getGeometry());

        //on retrouve les autres features correspondantes dans le layer temporaire
        _.each(subGeoms, function (subGeom) {
            var aGeom = this._parser.read(subGeom);
            if (!selectedGeom.equalsTopo(aGeom)) {
                _.each(otherFeatures, function (aFeature) {
                    var featureGeom = this._parser.read(aFeature.getGeometry());
                    if (featureGeom.equalsTopo(aGeom)) {
                        this.setFeatureStyle(aFeature, 'temporary');
                    }
                }.bind(this));
            }
        }.bind(this));
    },
    /**
     * Methode: featureCompositeSelectionUnSelected
     * Gestion du style lorsqu'une geométrie d'un objet composite est désélectionnée
     */
    featureCompositeSelectionUnSelected: function () {
        var allFeatures = this.editionLayer.compositeSelectionLayer.getSource().getFeatures();
        _.each(allFeatures, function (feature) {
            var style = Utils.getStyleByState(feature);
            this.setFeatureStyle(feature, style);
        }.bind(this));
    },
    changeCursorPointerOnFeature: function (evt) {
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                return aLayer === this.editionLayer.compositeSelectionLayer;
            }.bind(this)
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },
    CLASS_NAME: 'Descartes.Tool.Edition.CompositeSelection'
});

module.exports = Class;

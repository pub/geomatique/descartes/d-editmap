
/**
 * Constantes utilisées pour les outils d'édition.
 */
module.exports = {
    /**
     * Constante: SNAPPING_DEFAULT_TOLERANCE
     * Valeur par défaut de la tolérance pour le magnétisme.
     */
    SNAPPING_DEFAULT_TOLERANCE: 15
};

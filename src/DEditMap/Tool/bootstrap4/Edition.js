/* global Descartes */
var _ = require('lodash');

var Utils = Descartes.Utils;
var Edition = require('../Edition');

var template = require('./templates/EditionTool.ejs');

var Class = Utils.Class(Edition, {
    initialize: function () {
        this.btnCss = Descartes.UIBootstrap4Options.btnCssToolBar;
        Edition.prototype.initialize.apply(this, arguments);
    },
    createElement: function () {
        var elementClass = this.displayClass + 'ItemActive';

        var geometryType;
        if (this.geometryType) {
            geometryType = this.geometryType;
        } else if (!_.isNil(this.drawingType)) {
            geometryType = this.drawingType;
        } else if (!_.isNil(this.editionLayer) && !_.isNil(this.editionLayer.geometryType)) {
            geometryType = this.editionLayer.geometryType;
        }

        var element = template({
            id: this.id,
            displayClass: elementClass,
            btnCss: this.btnCss,
            active: this.isActive(),
            available: this.isAvailable(),
            title: this.title,
            geometryType: geometryType
        });
        return element;
    },
    CLASS_NAME: 'Descartes.Tool.Edition'
});

module.exports = Class;

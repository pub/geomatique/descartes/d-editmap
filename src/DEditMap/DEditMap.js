/* global MODE */
var _ = require('lodash');

//Action
var AttributesEditor = require('./Action/AttributesEditor');
var BookmarksManager = require('./Action/BookmarksManager_overloaded');
var BufferEditor = require('./Action/BufferEditor');
var HomotheticEditor = require('./Action/HomotheticEditor');
var MapContentManager = require('./Action/MapContentManager_overloaded');
var MapPreview = require('./Action/MapPreview');

//Button
var ToolBarOpener = require('./Button/ToolBarOpener_overloaded');
var ExportVectorLayer = require('./Button/ContentTask/' + MODE + '/ExportVectorLayer_overloaded');
var RemoveLayer = require('./Button/ContentTask/' + MODE + '/RemoveLayer_overloaded');
var RemoveGroup = require('./Button/ContentTask/RemoveGroup_overloaded');

//Core
var EditionManager = require('./Core/EditionManager');

//Map
var MapNamespace = require('./Map/MapNamespace');

//Model
var LayerNamespace = require('./Model/LayerNamespace');
var MapContent = require('./Model/MapContent_overloaded');
var FeatureState = require('./Model/FeatureState');
var Group = require('./Model/Group_overloaded');

//Strategy
var StrategyNamespace = require('./Strategy/StrategyNamespace');

//Tool
var EditionNamespace = require('./Tool/EditionNamespace');

//ToolBar
var ToolBarNamespace = require('./Toolbar/ToolBarNamespace');

//UI
var AbstractAttributesEditor = require('./UI/' + MODE + '/AbstractAttributesEditor');
var AttributesEditorDialog = require('./UI/' + MODE + '/AttributesEditorDialog');
var AttributesEditorInPlace = require('./UI/' + MODE + '/AttributesEditorInPlace');
var BufferEditorDialog = require('./UI/' + MODE + '/BufferEditorDialog');
var HomotheticEditorDialog = require('./UI/' + MODE + '/HomotheticEditorDialog');
var LayersTree = require('./UI/' + MODE + '/LayersTree_overloaded');
var LayersTreeSimple = require('./UI/' + MODE + '/LayersTreeSimple_overloaded');
var MapPreviewDialog = require('./UI/' + MODE + '/MapPreviewDialog');

//Utils
var UtilsDEditMap = require('./Utils/UtilsDEditMap');

//Messages
var Messages = require('./Messages');

//Symbolizers
var Symbolizers = require('./Symbolizers');

var DEditMapConstants = require('./DEditMapConstants');

var DEditMap = {
    Action: {AttributesEditor: AttributesEditor, BufferEditor: BufferEditor, HomotheticEditor: HomotheticEditor, MapPreview: MapPreview},
    Action_overloaded: {BookmarksManager: BookmarksManager, MapContentManager: MapContentManager},
    Button_overloaded: {ToolBarOpener: ToolBarOpener},
    ButtonContentTask_overloaded: {ExportVectorLayer: ExportVectorLayer, RemoveLayer: RemoveLayer, RemoveGroup: RemoveGroup},
    EditionManager: EditionManager,
    Map_overloaded: MapNamespace,
    Layer_overloaded: LayerNamespace,
    Group_overloaded: Group,
    MapContent_overloaded: MapContent,
    FeatureState: FeatureState,
    Strategy: StrategyNamespace,
    Tool: {Edition: EditionNamespace},
    ToolBar_overloaded: ToolBarNamespace,
    UI: {AbstractAttributesEditor: AbstractAttributesEditor, AttributesEditorDialog: AttributesEditorDialog, AttributesEditorInPlace: AttributesEditorInPlace, BufferEditorDialog: BufferEditorDialog, HomotheticEditorDialog: HomotheticEditorDialog, MapPreviewDialog: MapPreviewDialog},
    UI_overloaded: {LayersTree: LayersTree, LayersTreeSimple: LayersTreeSimple},
    Utils: {UtilsDEditMap: UtilsDEditMap},
    Messages: Messages,
    Symbolizers: Symbolizers
};

_.extend(DEditMap, DEditMapConstants);

module.exports = DEditMap;

# D-EditMap, composant Javascript

Le composant Javascript d-editmap est le module d'édition de Descartes.

Ce composant est basé sur la librairie JSTS.
 
## Exemples de démonstration

https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-editmap
 
## Paquet NPM

Le composant Javascript d-editmap est disponible sous forme de paquet NPM sous l'appellation `@descartes/d-editmap`.

### Utilisation dans une application tierce

#### Utilisation via NPM

##### Installation

Pour installer le composant JS d-editmap dans un projet NPM :
```bash
$ npm install @descartes/d-editmap
```

Note : le paquet est disponible sur différents dépôts, il faut donc faire pointer le scope `@descartes` sur le dépôt souhaité:
- dépôt public gitlab-forge (pour les versions diffusées au sein du ministère MTE) :  
  https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm 
- dépôt privé (pour les versions en cours de développement) :  
  https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/18174/packages/npm 
- dépôts public NPM (pour les versions diffusées "grand public" à accès CDN)
  https://registry.npmjs.org

Pour que NPM puisse se connecter aux dépôts, il faut renseigner le fichier `.npmrc` :
```
@descartes:registry=https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm
...
```

##### Inclusion du composant

Tout d'abord, le chargement de la librairie principale Descartes est nécessaire:
le paquet `@descartes/d-map` doit être inclus dans l'application. 

Pour utiliser le composant JS d-editmap, il suffit d'inclure les fichiers suivants dans la page HTML de l'application (les chemins relatifs doivent être respectés) :
* `css/d-editmap.css`
* `d-editmap.js`


#### Utilisation via CDN

Tout d'abord, le chargement de la librairie principale Descartes est nécessaire:
les urls CDN de `@descartes/d-map` doit être incluses dans l'application. 

Pour utiliser le composant JS D-Map, il suffit d'inclure ensuite les fichiers suivants dans la page HTML de l'application  :
* `https://cdn.jsdelivr.net/npm/@descartes/d-editmap@X.X.X/dist/css/d-editmap.css`
* `https://cdn.jsdelivr.net/npm/@descartes/d-editmap@X.X.X/dist/d-editmap.js`

Note : l'utilisation via CDN n'est pas recommandé en production.


### Utilisation du composant en cours de développement

#### Pre-requis

Installation sur son poste:
* NODE 16.14.0
* NPM 8.5.2

#### Utilisation de link

Il est possible de travailler simultanément sur le composant d-editmap et sur une application tierce, afin de vérifier
par exemple le bon comportement d'une évolution de d-editmap.

Pour cela, on commence par demander à npm de créer une référence locale globale du paquet, en se positionnant à la racine du projet d-editmap :
```
npm link
```

Ensuite, il faut indiquer à NPM que le paquet `@descartes/d-editmap` doit être résolu localement, en se positionnant dans le projet tierce :
```
$ npm link @descartes/d-editmap
```

Une fois les développements terminés, pour revenir à un état normal, en se positionnant dans le projet tierce :
```
npm unlink --no-save @descartes/d-editmap
npm install
```

Finalement, on peut demander à npm de nettoyer la référence locale globale, en se positionnant à la racine du projet d-editmap :
```
npm unlink
```

#### Compilation

La compilation du composant JS de d-editmap se fait à l'aide de Webpack. Par ailleurs le composant possède les dépendances suivantes :
* jsts
* 

Pour compiler le composant :
```bash
$ npm run build
```

Lors de la compilation, les fichiers suivants sont générés dans le répertoire `dist` à la racine de ce module :
* `d-editmap.js` et `d-editmap.css` : contiennent le code source et les feuilles de style utilisées par d-editmap
  > ces fichiers sont générés par Webpack
* tous les autre fichiers (images, polices, ...) sont des ressources requises pour le bon fonctionnement du composant

#### Développement du composant

Pour lancer les exemples d-editmap :
```bash
$ npm run dev
```

Si besoin, pour spécifier un port : `npm run dev -- --port=XXXX`. 
Le port par défaut est `4201`.

L'index des exemples est accessible sur http://localhost:4201. Les exemples se rafraichissent automatiquement en cas de changement dans les fichiers source.

> Note : Le répertoire `dist` ainsi que les répertoires `/demo` et `/node_modules` sont rendus accessibles à la racine du serveur.

#### Publication

La publication du paquet NPM se fait via GITLAB-CI à la création d'un TAG.

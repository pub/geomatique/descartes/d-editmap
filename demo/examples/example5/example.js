var editionLayer;

function load() {

	initExampleMapContentEditionSelect();

    Descartes.Log.setLevel('debug');
	chargeCouchesGroupes();
	chargeEditionCouchesGroupes();
	chargeEditionCouchesGroupesGEOJSON();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesWFS();
	loadContext();	

}

function doMap(){

	 var autoSave = true;
	 if (saveType === "manuel") {
		autoSave = false;
	 }

	 //Configuration du gestionnaire d'édition
	 var configuration = {
		//autoSave: autoSave,
        //globalEditionMode: false, //mode global piloté par les outils
        onlyAppMode:true,
        displaySaveMsg: false, //désactivation de la confirmation de la sauvegarde
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
     };
     if (withVerifConformite) {
		configuration.verifTopoConformity={
	    	  enable: true //verification de la conformite topologique pour toutes les couches d'édition
	    };
	 }
	 Descartes.EditionManager.configure(configuration);     
		
	var contenuCarte = new Descartes.MapContent({editable: true, editInitialItems: true});
	
	/*
	contenuCarte.addItem(coucheAnnotations);
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
    */
    
    /* 
	contenuCarte.addItem(coucheParkingsWfs);
	contenuCarte.addItem(coucheStationsWfs);
	contenuCarte.addItem(coucheEauWfs);
	contenuCarte.addItem(coucheNatureWfs);
	*/
    
    contenuCarte.populate(context.items);
    editionLayer = contenuCarte.getLayerById("coucheGeneric");
    
    
    initBounds = [context.bbox.xMin, context.bbox.yMin, context.bbox.xMax, context.bbox.yMax];
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					maxExtent: maxBounds,
					displayExtendedOLExtent: true,
					minScale:2150000,
					maxScale:100,
					autoSize: true
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	/*carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
    carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_BUFFER_HALO_SELECTION,
        args: {
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.LINE_BUFFER_HALO_SELECTION,
        args: {
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	*/
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.DISPLAY_LAYERSTREE_SIMPLE});
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.SHARE_LINK_MAP});
	
	 //Ajout d'un barre d'outils d'édition
	 var editionTools = [];
	 
	 if(geomType === "composite") {
	 	editionTools.push({type: Descartes.Map.EDITION_SELECTION});
	 }
	 
	 if (mapContentType !== "EditionGenericVide") {
	 	editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION});
	 } else {
		 /*editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION}
	     );
		 editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION}
	     );*/
	     editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION}
	     );	 
     } 
	 editionTools.push({type: Descartes.Map.EDITION_GLOBAL_MODIFICATION});
	 if(geomType === "composite") {
	 	editionTools.push({type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION});
	 }
	 editionTools.push({type: Descartes.Map.EDITION_VERTICE_MODIFICATION});
	 //editionTools.push({type: Descartes.Map.EDITION_ATTRIBUTE});
	 editionTools.push({type: Descartes.Map.EDITION_RUBBER_DELETION});
		               	      
	 if(geomType === "composite") {
		editionTools.push({type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION});
	 }
	 
	 if (saveType === "manuel") {
		editionTools.push({type: Descartes.Map.EDITION_SAVE});
	 }
	 
	 carte.addEditionToolBar('editionToolBar', editionTools);
	
	 carte.addContentManager(
		'layersTree',
		[
			{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL},
			{type : Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL}
		],
		{
			toolBarDiv: "managerToolBar"
		}
	);
    carte.show();

	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	//carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});


	// Ajout des assistants
	//carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	//carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	//	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	//carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	//carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout asssitant localisation à l'adresse
    //carte.addAction({type: Descartes.Action.LocalisationAdresse, div: 'localisationAdresse'});
    
    var actions =  [{ 
        type:"AttributesEditor", 
        div:'attributesEditor', 
        options:{ 
            size: 30, 
            editionLayer: editionLayer
        }
    }];
	carte.addActions(actions);
    
    
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
    
	// Ajout de la minicarte
	carte.addMiniMap("https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?LAYERS=c_natural_Valeurs_type");

	// Ajout du gestionnaire de requete
	/*var gestionnaireRequetes = carte.addRequestManager('Requetes');
	
	var laCoucheBati = carte.mapContent.getLayerById("coucheBati");
	var requeteBati = new Descartes.Request(laCoucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);

	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	*/
	// Ajout du gestionnaire d'info-bulle
	/*var laCoucheParkings = carte.mapContent.getLayerById("coucheParkings");
	var laCoucheStations = carte.mapContent.getLayerById("coucheStations");
	carte.addToolTip('ToolTip', [
		{layer: laCoucheParkings, fields: ['name']},
		{layer: laCoucheStations, fields: ['name']}
	]);
	*/
	// Ajout du gestionnaire de contextes
	//carte.addBookmarksManager('Bookmarks', 'exemple-descartes',{behavior : Descartes.Action.BookmarksManager.BEHAVIOR_MANAGED_BY_CREATOR});

	// Ajout du gestionnaire de localisation rapide
	//carte.addDefaultGazetteer('Gazetteer', "93", Descartes.Action.DefaultGazetteer.DEPARTEMENT);

	if (saveType !== "manuel") {
		carte.addOpenLayersInteractions([
        	{type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
        	{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);
	}

    editionLayer.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer);
	
	// Ajout des select infos-bulle
    var selectToolTipLayers = [{layer: editionLayer, fields: ['Nom']}];
    carte.addSelectToolTip(selectToolTipLayers);
}


function launchCreation(editionLayer,objectId,activeTool,optionsTool) {
    var json = {
        editionLayer: editionLayer,
        objectId: objectId,
        activeTool: activeTool
    };
    if(optionsTool){
    	json.optionsTool=optionsTool;
    }
    Descartes.EditionManager.createObject(json);
}

function selectForEdition(editionLayer,objectId,activateTool) {

    var json = {
        editionLayer: editionLayer,
        objectId: objectId,
        activeTool: activateTool
    };
    Descartes.EditionManager.selectForEdition(json);
  
}
function selectForDeletion(editionLayer,objectId) {

    var json = {
        editionLayer: editionLayer,
        objectId: objectId
    };
    Descartes.EditionManager.selectForDeletion(json);
    
    carte.OL_map.zoomToMaxExtent();
   
}

 function selection(editionLayer,objectId) {

    var json = {
        editionLayer: editionLayer,
        objectId: objectId
    };
    Descartes.EditionManager.select(json);
 
}

function init_selects_editionLayer (e) {
	if(e && e.target && e.target.getFeatures()){
		select(e.target.getFeatures(),"select_modif_global",editionLayer,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION);
		select(e.target.getFeatures(),"select_modif_vertice",editionLayer,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
		select(e.target.getFeatures(),"select_visu",editionLayer,selection); 
		select(e.target.getFeatures(),"select_sup",editionLayer,selectForDeletion);
	}
}

function select(features,selectid,editionLayer,fct,outil) {
	  
  var div = document.getElementById(selectid);
  
  if(div){
	  if(div.childNodes){
		 for (var i=(div.childNodes.length-1) ; i>=0 ; i--) {
			div.removeChild(div.childNodes[i]);
		 } 
	  }

	  var optionsSelect = [];
	  optionsSelect.push({value:"",text:"Sélectionner un identifiant"});
	  if(features){
		 for (var i=0, len=features.length ; i < len ; i++) {
			 	var feature = features[i];
				var option = {value:feature.getId(),text:feature.getId().substring(feature.getId().indexOf(".")+1)};
				optionsSelect.push(option);
		 }
	  }
	  
	  optionsSelect = optionsSelect.sort(function(a, b) {
		 return a.text - b.text;
      });
	  
	  var selectElement = createSelectInputWithLabel(selectid,editionLayer.title,optionsSelect);
	  selectElement.onchange = function() {
		 if(this.lastChild[this.lastChild.selectedIndex].value !== ""){
			 if(outil!==null){
				 fct(editionLayer,this.lastChild[this.lastChild.selectedIndex].value,outil);  
			 } else {
				 fct(editionLayer,this.lastChild[this.lastChild.selectedIndex].value);
			 }
			
		 }
		 
	  };

	  div.appendChild(selectElement); 
  }
  
}

function createLabelSpan(labelText, options) {
	var defaultOptions = Descartes.Utils.extend({}, options);
	var labelElement = document.createElement('label');
	labelElement.innerHTML = labelText;
	if (options.separator === undefined || options.separator !== false) {
		labelElement.innerHTML += "&nbsp;:";
	}
	if (defaultOptions.labelClassName !== undefined) {
		labelElement.className = defaultOptions.labelClassName;
	}
	return labelElement;
}
	
function createSelectInput(selectName, values, options) {
	var defaultOptions = {size:1};
	defaultOptions = Descartes.Utils.extend(defaultOptions, options);
	var selectElement = document.createElement('select');
	selectElement.name = selectName;
	selectElement.id = selectName;
	selectElement.size = defaultOptions.size;
	if (defaultOptions.selectClassName !== undefined) {
		selectElement.className = defaultOptions.selectClassName;
	}
	for (var i=0, len=values.length ; i<len ; i++) {
		var optionElement = document.createElement('option');
		optionElement.value = values[i].value.toString();
		optionElement.selected = (values[i].value == defaultOptions.value);
		optionElement.innerHTML = values[i].text;
		selectElement.appendChild(optionElement);
	}
	return selectElement;
}
	
 function createSelectInputWithLabel(selectName, labelText, values, options) {
	var defaultOptions = Descartes.Utils.extend({}, options);

	var globalElement = document.createElement('div');
	if (defaultOptions.globalClassName !== undefined) {
		globalElement.className = defaultOptions.globalClassName;
	}
	globalElement.appendChild(createLabelSpan(labelText, defaultOptions));
	globalElement.appendChild(createSelectInput(selectName, values, defaultOptions));
	return globalElement;
}
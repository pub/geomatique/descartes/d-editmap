var coucheNatureSpecWms, coucheBatiSpecWms, coucheRoutesSpecWms, coucheFerSpecWms, coucheEauSpecWms, coucheToponymesSpecWms, coucheParkingsSpecWms, coucheStationsSpecWms;
var coucheNature, coucheBati, coucheRoutes, coucheFer, coucheEau, coucheToponymes, coucheParkings, coucheStations
var coucheNatureSpecWfs, coucheEauSpecWfs, coucheParkingsSpecWfs, coucheStationsSpecWfs;
var coucheNatureWfs, coucheEauWfs, coucheParkingsWfs, coucheStationsWfs;
var groupeInfrasSpec, groupeFondsSpec, groupeInfras, groupeFonds;

function chargeCouchesGroupes() {

	/************************************************************************
	 * 
	 * COUCHES WMS
	 * 
	 ***********************************************************************/

    var type = Descartes.Layer.TYPE_WMS;

	coucheNatureSpecWms = {
		title: "Espaces naturels (WMS " + moteurCarto + ")",
		type: type, 
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type",
				featureServerUrl: serveur,
				featureName: "c_natural_Valeurs_type",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"					}
			],
		options:{
			id:'coucheNature',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable: true,
			activeToQuery: true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POLYGONE_GEOMETRY
		}
	};

	coucheBatiSpecWms = {
		title: "Constructions (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				featureServerUrl: serveur,
				featureName: "c_buildings2",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		options:{
			id:'coucheBati',
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POLYGONE_GEOMETRY
		}
	};

	coucheRoutesSpecWms = {
		title: "Routes (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_roads",
				featureServerUrl: serveur,
				featureName: "c_roads",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"				}
			],
		options:{
			id:'coucheRoutes',
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_roads"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.LINE_GEOMETRY
		}
	};

	coucheFerSpecWms = {
		title: "Chemins de fer (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_railways",
				featureServerUrl: serveur,
				featureName: "c_railways",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"					}
			],
		options:{
			id:'coucheFer',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_railways"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.LINE_GEOMETRY
		}
	};

	coucheEauSpecWms = {
		title: "Cours d'eau (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_waterways_Valeurs_type",
				featureServerUrl: serveur,
				featureName: "c_waterways_Valeurs_type",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"			}
			],
		options:{
			id:'coucheEau',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable: false,
			activeToQuery: false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_waterways_Valeurs_type"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.LINE_GEOMETRY
		}
	};

	coucheToponymesSpecWms = {
		title: "Lieux (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_places_Etiquettes"			}
			],
		options:{
			id:'coucheToponymes',
			maxScale: null,
			minScale: 190000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend:  [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_places_Etiquettes"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes"
		}
	};
	
	coucheStationsSpecWms = {
		title: "Stations essence (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_stations",
				featureServerUrl: serveur,
				featureName: "c_stations",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		options:{
			id:'coucheStations',
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POINT_GEOMETRY
		}
	};

	coucheParkingsSpecWms = {
		title: "Parkings (WMS " + moteurCarto + ")", 
		type: type,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_parkings",
				featureServerUrl: serveur,
				featureName: "c_parkings",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		options:{
			id:'coucheParkings',
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POINT_GEOMETRY
		}
	};
	
	//coucheNature = new Descartes.Layer.WMS(coucheNatureSpecWms.title,coucheNatureSpecWms.definition,coucheNatureSpecWms.options);
	//coucheBati = new Descartes.Layer.WMS(coucheBatiSpecWms.title,coucheBatiSpecWms.definition,coucheBatiSpecWms.options);
	//coucheRoutes = new Descartes.Layer.WMS(coucheRoutesSpecWms.title,coucheRoutesSpecWms.definition,coucheRoutesSpecWms.options);
	//coucheFer = new Descartes.Layer.WMS(coucheFerSpecWms.title,coucheFerSpecWms.definition,coucheFerSpecWms.options);
	//coucheEau = new Descartes.Layer.WMS(coucheEauSpecWms.title,coucheEauSpecWms.definition,coucheEauSpecWms.options);
	//coucheToponymes = new Descartes.Layer.WMS(coucheToponymesSpecWms.title,coucheToponymesSpecWms.definition,coucheToponymesSpecWms.options);
	//coucheStations = new Descartes.Layer.WMS(coucheStationsSpecWms.title,coucheStationsSpecWms.definition,coucheStationsSpecWms.options);
	//coucheParkings = new Descartes.Layer.WMS(coucheParkingsSpecWms.title,coucheParkingsSpecWms.definition,coucheParkingsSpecWms.options);

	/************************************************************************
	 * 
	 * COUCHES KML
	 * 
	 ***********************************************************************/
	
	var typeKML = Descartes.Layer.TYPE_KML;
	
	coucheParkingsSpecKml = {
        title: "Parkings (KML)",
        type: typeKML,
        definition: [
            {
                serverUrl: "../commons/parkings.kml",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "coucheParkings",
            visible: true,
            opacity: 100,
            minScale: null,
            maxScale: null,
            legend: [
                "../commons/parkings.png"
            ],
            attribution: "©Descartes",
            geometryType: "Point",
            queryable: false,
            attributes: {
                attributesAlias: [
					{fieldName: 'name', label: 'Nom'}
				]
            },
            symbolizersFunction: function(feature) {
	        	var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#AEDFEA'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf041', // fa-map-marker
	                    font: 'normal 18px FontAwesome',
	                    fill: new ol.style.Fill({color: '#AEDFEA'})
	                  })
	              });
	              return style;
		    }
        }
    };
    
    coucheStationsSpecKml = {
        title: "Stations essences (KML)",
        type: typeKML,
        definition: [
            {
                serverUrl: "../commons/stationsessences.kml",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "coucheStations",
            visible: true,
            opacity: 100,
            minScale: null,
            maxScale: null,
            legend: [
                "../commons/stationsessences.png"
            ],
            attribution: "©Descartes",
            geometryType: "Point",
            queryable: false,
            attributes: {
                attributesAlias: [
					{fieldName: 'name', label: 'Nom'}
				]
            },
            symbolizersFunction: function(feature) {
	        	/*var style = new ol.style.Style({
	                //text: new ol.style.Text({
	                //	text: feature.get("operator"),
	                //	font: 'normal 18px',
	                //    fill: new ol.style.Fill({color: 'black'}),
	                //    offsetX: 0,
	                //    offsetY: 15
	                }),
	                image: new ol.style.Icon({
	                    src: 'data/marker.png',
	                    anchorOrigin: "bottom-right"
	                })
	            });
	            return style;*/
	            var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#EAAED0'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf276', // fa-map-pin
	                    font: 'normal 18px FontAwesome',
	                    fill: new ol.style.Fill({color: '#EAAED0'})
	                  })
	              });
	              return style;
		    }

        }
    };

	/************************************************************************
	 * 
	 * COUCHES GeoJson
	 * 
	 ***********************************************************************/
	
	var typeGeoJson = Descartes.Layer.TYPE_GeoJSON;

	coucheParkingsSpecGeoJson = {
        title: "Parkings (GeoJson)",
        type: typeGeoJson,
        definition: [
            {
                serverUrl: "../commons/parkings.geojson",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "coucheParkings",
            visible: true,
            opacity: 100,
            minScale: null,
            maxScale: null,
            legend: [
                "../commons/parkings.png"
            ],
            attribution: "©Descartes",
            geometryType: "Point",
            queryable: false,
            attributes: {
                attributesAlias: [
					{fieldName: 'name', label: 'Nom'}
				]
            },
            symbolizersFunction: function(feature) {
	        	var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#AEDFEA'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf041', // fa-map-marker
	                    font: 'normal 18px FontAwesome',
	                    fill: new ol.style.Fill({color: '#AEDFEA'})
	                  })
	              });
	              return style;
		    }
        }
    };
    
    coucheStationsSpecGeoJson = {
        title: "Stations essences (GeoJson)",
        type: typeGeoJson,
        definition: [
            {
                serverUrl: "../commons/stationsessences.geojson",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "coucheStations",
            visible: true,
            opacity: 100,
            minScale: null,
            maxScale: null,
            legend: [
                "../commons/stationsessences.png"
            ],
            attribution: "©Descartes",
            geometryType: "Point",
            queryable: false,
            attributes: {
                attributesAlias: [
					{fieldName: 'name', label: 'Nom'}
				]
            },
            symbolizersFunction: function(feature) {
	        	/*var style = new ol.style.Style({
	                //text: new ol.style.Text({
	                //	text: feature.get("operator"),
	                //	font: 'normal 18px',
	                //    fill: new ol.style.Fill({color: 'black'}),
	                //    offsetX: 0,
	                //    offsetY: 15
	                }),
	                image: new ol.style.Icon({
	                    src: 'data/marker.png',
	                    anchorOrigin: "bottom-right"
	                })
	            });
	            return style;*/
	            var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#EAAED0'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf276', // fa-map-pin
	                    font: 'normal 18px FontAwesome',
	                    fill: new ol.style.Fill({color: '#EAAED0'})
	                  })
	              });
	              return style;
		    }

        }
    };

	coucheFestivalsSpecGeoJson = {
        title: "Festivals (GeoJson)",
        type: typeGeoJson,
        definition: [
            {
                serverUrl: "../commons/festivals.geojson",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "festivals",
            visible: false,
            opacity: 100,
            minScale: null,
            maxScale: null,
            legend: [
                "../commons/festivals.png"
            ],
            attribution: "©OpenStreetMap",
            geometryType: "Point",
            queryable: false,
            _loaderWithRemoveFeatures: true,
            attributes: {
                attributesAlias: [
					{fieldName: 'nom du festival', label: 'Nom'},
					{fieldName: 'discipline dominante', label: 'Type'}
				]
            },
            symbolizersFunction: function(feature) {
	        	var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#D180C5'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf0c0', // fa-users
	                    font: 'normal 14px FontAwesome',
	                    fill: new ol.style.Fill({color: '#D180C5'})
	                  })
	              });
	              return style;
		    }
        }
    };

	coucheStationsRechargesSpecGeoJson = {
        title: "Stations de recharge de véhicules électriques (GeoJson)",
        type: typeGeoJson,
        definition: [
            {
                serverUrl: "../commons/stationsrecharges.geojson",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "stationsrecharges",
            visible: false,
            opacity: 100,
            minScale: null,
            maxScale: null,
            legend: [
                "../commons/stationsrecharges.png"
            ],
            attribution: "©OpenStreetMap",
            geometryType: "Point",
            queryable: false,
            _loaderWithRemoveFeatures: true,
            attributes: {
                attributesAlias: [
					{fieldName: 'operator', label: 'Opérateur'}
				]
            },
            symbolizersFunction: function(feature) {
	        	/*var style = new ol.style.Style({
	                //text: new ol.style.Text({
	                //	text: feature.get("operator"),
	                //	font: 'normal 18px',
	                //    fill: new ol.style.Fill({color: 'black'}),
	                //    offsetX: 0,
	                //    offsetY: 15
	                }),
	                image: new ol.style.Icon({
	                    src: 'data/marker.png',
	                    anchorOrigin: "bottom-right"
	                })
	            });
	            return style;*/
	            var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#1ECD48'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf1e6', // fa-plug
	                    font: 'normal 14px FontAwesome',
	                    fill: new ol.style.Fill({color: '#1ECD48'})
	                  })
	              });
	              return style;
		    }

        }
    };

	/************************************************************************
	 * 
	 * COUCHES WFS
	 * 
	 ***********************************************************************/
	
	var typeWFS = Descartes.Layer.TYPE_WFS;
	
	coucheNatureSpecWfs = {
		title: "Espaces naturels (WFS " + moteurCarto + ")", 
		type: typeWFS,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type",
				serverVersion: "1.1.0",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				imageServerUrl: serveur,
				imageLayerName: "c_natural_Valeurs_type",
				imageServerVersion: "1.3.0"					
			}
		],
		options:{
			id:'coucheNature',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable: true,
			activeToQuery: true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POLYGONE_GEOMETRY
		}
	};
	
	coucheEauSpecWfs = {
		title: "Cours d'eau (WFS " + moteurCarto + ")", 
		type: typeWFS,
		definition:[
			{			
				serverUrl: serveur,
				layerName: "c_waterways_Valeurs_type",
				serverVersion: "1.1.0",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				imageServerUrl: serveur,
				imageLayerName: "c_waterways_Valeurs_type",
				imageServerVersion: "1.3.0"
			}
		],
		options:{
			id:'coucheEau',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable: true,
			activeToQuery: true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.LINE_GEOMETRY
		}
	};
	
	coucheParkingsSpecWfs = {
		title: "Parkings (WFS " + moteurCarto + ")", 
		type: typeWFS,
		definition:	[
				{
				serverUrl: serveur,
				layerName: "c_parkings",
				serverVersion: "1.1.0",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				imageServerUrl: serveur,
				imageLayerName: "c_parkings",
				imageServerVersion: "1.3.0"
  			}
		],
		options:{
			id:'coucheParkings',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POINT_GEOMETRY,
			legend: [
                "../commons/parkings.png"
            ],
            symbolizersFunction: function(feature) {
	        	var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#6BAAEE'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf041', // fa-map-marker
	                    font: 'normal 18px FontAwesome',
	                    fill: new ol.style.Fill({color: '#6BAAEE'})
	                  })
	              });
	              return style;
		    }
		}
	};

	coucheStationsSpecWfs = {
		title: "Stations essence (WFS " + moteurCarto + ")", 
		type: typeWFS,
		definition:	[
				{
				serverUrl: serveur,
				layerName: "c_stations",
				serverVersion: "1.1.0",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				imageServerUrl: serveur,
				imageLayerName: "c_stations",
				imageServerVersion: "1.3.0"
  			}
		],
		options:{
			id:'coucheStations',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable: true,
			activeToQuery: true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POINT_GEOMETRY,
			legend: [
                "../commons/stationsessences.png"
            ],
            symbolizersFunction: function(feature) {
	        	/*var style = new ol.style.Style({
	                //text: new ol.style.Text({
	                //	text: feature.get("operator"),
	                //	font: 'normal 18px',
	                //    fill: new ol.style.Fill({color: 'black'}),
	                //    offsetX: 0,
	                //    offsetY: 15
	                }),
	                image: new ol.style.Icon({
	                    src: 'data/marker.png',
	                    anchorOrigin: "bottom-right"
	                })
	            });
	            return style;*/
	            var style = new ol.style.Style({
	                image: new ol.style.Circle({
	                  radius: 1,
	                  fill: new ol.style.Fill({
	                    color: '#E2AEEA'
	                  })
	                }),
	                text: new ol.style.Text({
	                    text: '\uf276', // fa-map-pin
	                    font: 'normal 18px FontAwesome',
	                    fill: new ol.style.Fill({color: '#E2AEEA'})
	                  })
	              });
	              return style;
		    }
		}
	};

	coucheBatiSpecWfs = {
		title: "Constructions (WFS " + moteurCarto + ")", 
		type: typeWFS,
		definition: [
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				serverVersion: "1.1.0",
				featureGeometryName: featureGeometryName,
				//internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: featureNameSpace,
				imageServerUrl: serveur,
				imageLayerName: "c_buildings",
				imageServerVersion: "1.3.0"
			}
		],
		options:{
			id:'coucheBati',
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;descartes",
			geometryType: Descartes.Layer.POLYGONE_GEOMETRY
		}
	};

	//coucheNatureWfs = new Descartes.Layer.WFS(coucheNatureSpecWfs.title,coucheNatureSpecWfs.definition,coucheNatureSpecWfs.options);
	//coucheEauWfs = new Descartes.Layer.WFS(coucheEauSpecWfs.title,coucheEauSpecWfs.definition,coucheEauSpecWfs.options);
	//coucheStationsWfs = new Descartes.Layer.WFS(coucheStationsSpecWfs.title,coucheStationsSpecWfs.definition,coucheStationsSpecWfs.options);
	//coucheParkingsWfs = new Descartes.Layer.WFS(coucheParkingsSpecWfs.title,coucheParkingsSpecWfs.definition,coucheParkingsSpecWfs.options);
	//coucheBatiWfs = new Descartes.Layer.WFS(coucheBatiSpecWfs.title,coucheBatiSpecWfs.definition,coucheBatiSpecWfs.options);

	/************************************************************************
	 * 
	 * COUCHES CLUSTER
	 * 
	 ***********************************************************************/
	
	var typeClusterGeoJson = Descartes.Layer.ClusterLayer.TYPE_GeoJSON;

	coucheParkingsSpecClusterGeoJson = {
        title: "Parkings - bleu (GeoJson)",
        type: typeClusterGeoJson,
        definition: [
            {
                serverUrl: "../commons/parkings.geojson",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "coucheParkings",
            visible: true,
            opacity: 100,
            minScale: null,
            maxScale: null,
            /*legend: [
                "../commons/parkings.png"
            ],*/
            attribution: "©Descartes",
            geometryType: "Point",
            queryable: false,
            attributes: {
                attributesAlias: [
					{fieldName: 'name', label: 'Nom'}
				]
            },
            cluster: {
    	        symbolizersClusterPoint: {
    			    'Point': {
    			    	 pointRadius: 20,
    	                 graphicName: 'square',
    	                 points: 4,
    	                 angle: Math.PI / 4,
    	                 fillColor: '#AEDFEA',
    	                 fillOpacity: 1,
    	                 strokeWidth: 1,
    	                 strokeOpacity: 1,
    	                 strokeColor: '#fff'
    			    }
    		    }
   	        },
   	        symbolizers:{
    	          "Point": {
					pointRadius: 4, //6
					graphicName: "circle",
					fillColor: "#AEDFEA",
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#AEDFEA"
				  }
			}
        }
    };
    
    coucheStationsSpecClusterGeoJson = {
        title: "Stations essences - rose (GeoJson)",
        type: typeClusterGeoJson,
        definition: [
            {
                serverUrl: "../commons/stationsessences.geojson",
                internalProjection: "EPSG:4326"
            }
        ],
        options: {
            id: "coucheStations",
            visible: true,
            opacity: 100,
            minScale: null,
            maxScale: null,
            /*legend: [
                "../commons/stationsessences.png"
            ],*/
            attribution: "©Descartes",
            geometryType: "Point",
            queryable: false,
            attributes: {
                attributesAlias: [
					{fieldName: 'name', label: 'Nom'}
				]
            },
            cluster: {
    	        symbolizersClusterPoint: {
    			    'Point': {
    			    	 pointRadius: 20,
    	                 graphicName: 'square',
    	                 points: 4,
    	                 angle: Math.PI / 4,
    	                 fillColor: '#EAAED0',
    	                 fillOpacity: 1,
    	                 strokeWidth: 1,
    	                 strokeOpacity: 1,
    	                 strokeColor: '#fff'
    			    }
    		    }
   	        },
   	        symbolizers:{
    	          "Point": {
					pointRadius: 4, //6
					graphicName: "circle",
					fillColor: "#EAAED0",
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#EAAED0"
				  }
			}

        }
    };

	/************************************************************************
	 * 
	 * COUCHES WMTS
	 * 
	 ***********************************************************************/
	
	 var typeWMTS = Descartes.Layer.TYPE_WMTS;
	 
	 var matrixIdsWMTS = [
        'EPSG:2154:0',
        'EPSG:2154:1',
        'EPSG:2154:2',
        'EPSG:2154:3',
        'EPSG:2154:4',
        'EPSG:2154:5',
        'EPSG:2154:6',
        'EPSG:2154:7',
        'EPSG:2154:8',
        'EPSG:2154:9',
        'EPSG:2154:10',
        'EPSG:2154:11',
        'EPSG:2154:12',
        'EPSG:2154:13',
        'EPSG:2154:14',
        'EPSG:2154:15',
        'EPSG:2154:16',
        'EPSG:2154:17',
        'EPSG:2154:18',
        'EPSG:2154:19',
        'EPSG:2154:20',
        'EPSG:2154:21'
        
    ];
	                            
   var originsWMTS = [
      [-357823.2365, 46112025.0],
      [-357823.2365, 26074517.0],
      [-357823.2365, 16055763.0],
      [-357823.2365, 11046386.0],
      [-357823.2365, 8541697.0],
      [-357823.2365, 7289353.0],
      [-357823.2365, 7289353.0],
      [-357823.2365, 7289353.0],
      [-357823.2365, 7289353.0],
      [-357823.2365, 7289353.0],
      [-357823.2365, 7250217.0],
      [-357823.2365, 7230649.0],
      [-357823.2365, 7230649.0],
      [-357823.2365, 7235541.0],
      [-357823.2365, 7233095.0],
      [-357823.2365, 7231872.0],
      [-357823.2365, 7231261.0],
      [-357823.2365, 7230955.0],
      [-357823.2365, 7230802.0],
      [-357823.2365, 7230802.0],
      [-357823.2365, 7230764.0],
      [-357823.2365, 7230745.0]
  ];
	                                     
   var projectionWMTS = new Descartes.Projection('EPSG:2154');
   var extentWMTS = [-357823.2365, 6093283.21, 1212610.74, 46112025.0];
   var resolutionsWMTS =  [156543.033928041, 78271.51696402048, 39135.758482010235, 19567.87924100512, 9783.93962050256, 4891.96981025128, 2445.98490512564, 1222.99245256282, 611.49622628141, 305.7481131407048, 152.8740565703525, 76.43702828517624, 38.21851414258813, 19.10925707129406, 9.554628535647032, 4.777314267823516, 2.388657133911758, 1.194328566955879, 0.5971642834779395, 0.2985821417389697, 0.1492910708694849, 0.0746455354347424];        
	                          
   coucheOSMWmtsSpec = {
   		 title: 'OSM (WMTS)', 
   		 type: typeWMTS,
   		 definition:[
   		  	{
   		  		serverUrl: 'http://osm.geobretagne.fr/gwc01/service/wmts?',
   		  		layerName: 'osm:map'
   		  	}
   		 ],
   		 options:{
   			 extent: extentWMTS,
   			 matrixSet: "EPSG:2154",
   			 projection: projectionWMTS,
   			 matrixIds: matrixIdsWMTS,
   			 origins: originsWMTS,
   			 resolutions: resolutionsWMTS,
   			 tileSize: [256, 256],
   			 visible: false,
   			 format: 'image/png',
   			 opacity: 50,
			 queryable: false
   		 }
    };
   
    //coucheOSMWmts = new Descartes.Layer.WMTS(coucheOSMWmtsSpec.title, coucheOSMWmtsSpec.definition, coucheOSMWmtsSpec.options);

	var matrixIdsWMTSGeoRef = [
         'EPSG2154_256px:0',
         'EPSG2154_256px:1',
         'EPSG2154_256px:2',
         'EPSG2154_256px:3',
         'EPSG2154_256px:4',
         'EPSG2154_256px:5',
         'EPSG2154_256px:6',
         'EPSG2154_256px:7',
         'EPSG2154_256px:8',
         'EPSG2154_256px:9',
         'EPSG2154_256px:10',
         'EPSG2154_256px:11'
     ];
    
 	 var originsWMTSGeoRef = [
       [70000, 7207600.0],
       [70000, 7310000.0],
       [70000, 7182000.0],
       [70000, 7182000.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130160.0],
       [70000, 7130032.0],
       [70000, 7130032.0]
   ];
             
    var projectionWMTSGeoRef = new Descartes.Projection('EPSG:2154');
    //var extentWMTSGeoRef = projectionWMTSGeoRef.getExtent();
    //var extentWMTSGeoRef = [70000, 6030000, 1270000, 7130000];
    var extentWMTSGeoRef = [-101991.9, 6023917.0, 1528303.1, 7110780.4];

    var resolutionsWMTSGeoRef = [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5];

    coucheGeoRefWmtsSpec = {
   		 title: 'GéoRef (WMTS)', 
   		 type: typeWMTS,
   		 definition:[
   		  	{
   		  		serverUrl: 'https://georef.application.developpement-durable.gouv.fr/cache/service/wmts?',
   		  		layerName: 'georef:plan2154'
   		  	}
   		 ],
   		 options:{
   			 extent: extentWMTSGeoRef,
   			 matrixSet: "EPSG2154_256px",
   			 projection: projectionWMTSGeoRef,
   			 matrixIds: matrixIdsWMTSGeoRef,
   			 origins: originsWMTSGeoRef,
   			 resolutions: resolutionsWMTSGeoRef,
   			 tileSize: [256, 256],
   			 visible: false,
   			 format: 'image/png',
   			 opacity: 50,
			 queryable: false
   		 }
    };

    coucheGeoRefWmts = new Descartes.Layer.WMTS(coucheGeoRefWmtsSpec.title, coucheGeoRefWmtsSpec.definition, coucheGeoRefWmtsSpec.options);
   
 
 	coucheGeoRefWmsSpec = {
		title : "Géoref (WMS)",
		type: type,
		definition: [
			{
				serverUrl: "https://georef.application.developpement-durable.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		options: {
			maxScale: 100,
			minScale: 10000001,
			alwaysVisible: false,
			visible: false,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 50,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};

    coucheGeoRefWms = new Descartes.Layer.WMS(coucheGeoRefWmsSpec.title, coucheGeoRefWmsSpec.definition, coucheGeoRefWmsSpec.options);

   
	/************************************************************************
	 * 
	 * COUCHES AUTRES
	 * 
	 ***********************************************************************/
      
   	var typeOSM = Descartes.Layer.TYPE_OSM;
	
	coucheOSMPreconfSpec = {
		title: "OSM préconfigurée (TILE)", 
		type: typeOSM,
		options: {
			visible: true,
			opacity: 50
		}
	};
	
	//coucheOSMPreconf = new Descartes.Layer.OSM(coucheOSMPreconfSpec.title);
	
   	var typeXYZ = Descartes.Layer.TYPE_XYZ;
	
	coucheGeoRefXYZSpec = {
		title: "GéoRef (XYZ)", 
		type: typeXYZ,
		definition:[
   		  	{
   		  		serverUrl: '../commons/georef_zxy_tiles_local/{z}/{x}/{y}.jpeg'
   		  	}
   		 ],		
		options: {
			visible: true,
			maxScale: 537500,
			opacity: 50
		}
	};
	
	//coucheGeoRefXYZPreconf = new Descartes.Layer.XYZ(coucheGeoRefXYZSpec.title, coucheGeoRefXYZSpec.definition);

	/************************************************************************
	 * 
	 * GROUPES
	 * 
	 ***********************************************************************/

   	groupeInfrasSpec = {
		title: "Infrastructures", 
		options : {opened : false}
	};

	groupeInfras = new Descartes.Group(groupeInfrasSpec.title,groupeInfrasSpec.options);
	
	groupeFondsSpec = {
		title: "Fonds de carte", 
		options : {opened : false}
	};
	
	groupeFonds = new Descartes.Group(groupeFondsSpec.title,groupeFondsSpec.options);

}

//Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)
//Descartes.setGeoRefWebServiceInstance('preprod'); //integration ou preprod (default: prod)
//var descartesUrlRoot = "https://preprod.descartes.din.developpement-durable.gouv.fr";
var descartesUrlRoot = "https://descartes.din.developpement-durable.gouv.fr";

var urlServiceBouchon = Descartes.getWebServiceRoot()+"edition";
var urlServiceBouchonKml = Descartes.getWebServiceRoot()+"editionKML";
var urlServiceBouchonGeoJSON = Descartes.getWebServiceRoot()+"editionGeoJSON";

var projection = 'EPSG:2154';
var initBounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
var maxBounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];

var urlParams = new URLSearchParams(window.location.search);
var moteurCarto = urlParams.get('moteurCarto');
var mapContentType = urlParams.get('mapContentType');
var saveType = urlParams.get('saveType');
var geomType = urlParams.get('geomType');
var withSnapping = urlParams.get('withSnapping');
var withVerifConformite = urlParams.get('withVerifConformite');
var fctAvanced = urlParams.get('fctAvanced');

if (fctAvanced) {
	projection = 'EPSG:4326';
	initBounds = [4.02, 42.60, 7.91, 45.37];
	maxBounds = [4.02, 42.60, 7.91, 45.37];
}
var serveur;
var featureGeometryName;
var featureNameSpace;

switch (moteurCarto) {
  case 'carto2':
    serveur = "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?";
	featureGeometryName = "the_geom";
	featureNameSpace = "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d";
    break;
  case 'd-mapserver':
    serveur = descartesUrlRoot + "/mapserver?";
    featureGeometryName = "ms:geometry";
    featureNameSpace = "";
    break;
  case 'd-geoserver':
  	serveur = descartesUrlRoot + "/geoserver/ows?";
	featureGeometryName = "the_geom";
	featureNameSpace = "descartes";
    break;
  case 'd-qgisserver':
    serveur = descartesUrlRoot + "/qgisserver?";
	featureGeometryName = "geometry";
	featureNameSpace = "";  
    break;
  case 'd-mapserver-local':
	serveur = "http://localhost:8082/mapserver?";
	featureGeometryName = "ms:geometry";
	featureNameSpace = "";
    break;
  case 'd-geoserver-local':
	serveur = "http://localhost:8081/geoserver/ows?";
	featureGeometryName = "the_geom";
	featureNameSpace = "descartes";
    break;
  case 'd-qgisserver-local':
	serveur = "http://localhost:8083/qgisserver?";
	featureGeometryName = "geometry";
	featureNameSpace = "";   
    break;
  default:
	serveur = descartesUrlRoot + "/mapserver?";
    featureGeometryName = "ms:geometry";
    featureNameSpace = "";
}

var context = {};
function loadContext() {
	
	// constitution de l'objet JSON de contexte
	var contextFile = (new Descartes.Url(this.location.href)).getParamValue("context");
	
	if (contextFile !== "") {
		// contexte sauvegardé
		var xhr = new XMLHttpRequest();
        xhr.open('GET', Descartes.CONTEXT_MANAGER_SERVER+"?" + contextFile);
        xhr.onload = function () {
            if (xhr.status === 200) {
            	getContext(xhr);
            } else {
            	stopMap();
            }
        };
        xhr.send();
	
	} else {
		context.bbox = {xMin:initBounds[0], yMin:initBounds[1], xMax:initBounds[2], yMax:initBounds[3]};
		//context.size = {w:600 , h:400};
		context.items = [];
		
		if (mapContentType === "Annotation") {
			context.items[0] = Descartes.Utils.extend(coucheAnnotationsSpec, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheToponymesSpecWms, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(groupeInfrasSpec, {itemType:"Group", items:[]});
			context.items[2].items[0] = Descartes.Utils.extend(coucheParkingsSpecWms, {itemType:"Layer"});
			context.items[2].items[1] = Descartes.Utils.extend(coucheStationsSpecWms, {itemType:"Layer"});
			context.items[2].items[2] = Descartes.Utils.extend(coucheRoutesSpecWms, {itemType:"Layer"});
			context.items[2].items[3] = Descartes.Utils.extend(coucheFerSpecWms, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheEauSpecWms, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(coucheBatiSpecWms, {itemType:"Layer"});
			context.items[5] = Descartes.Utils.extend(coucheNatureSpecWms, {itemType:"Layer"});
			context.items[6] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[6].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[6].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[6].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[6].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[6].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionGenericVide") {
			context.items[0] = Descartes.Utils.extend(coucheEditionGenericVectorVide1Spec, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[1].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[1].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[1].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[1].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[1].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionGeoJson") {
			if (withSnapping) {
				context.items[0] = Descartes.Utils.extend(geojsonCouchePointsSnapping, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(geojsonCoucheLignesSnapping, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(geojsonCouchePolygonesSnapping, {itemType:"Layer"});
			} else if (geomType !== "composite") {
				context.items[0] = Descartes.Utils.extend(geojsonCouchePoints, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(geojsonCoucheLignes, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(geojsonCouchePolygones, {itemType:"Layer"});
			} else {
				context.items[0] = Descartes.Utils.extend(geojsonCoucheMultiPoints, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(geojsonCoucheMultiLignes, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(geojsonCoucheMultiPolygones, {itemType:"Layer"});
			}
			context.items[3] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[3].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[3].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[3].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[3].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[3].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionKml") {
			if (withSnapping) {
				context.items[0] = Descartes.Utils.extend(kmlCouchePointsSnapping, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(kmlCoucheLignesSnapping, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(kmlCouchePolygonesSnapping, {itemType:"Layer"});
			} else if (geomType !== "composite") {
				context.items[0] = Descartes.Utils.extend(kmlCouchePoints, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(kmlCoucheLignes, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(kmlCouchePolygones, {itemType:"Layer"});
			} else {
				context.items[0] = Descartes.Utils.extend(kmlCoucheMultiPoints, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(kmlCoucheMultiLignes, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(kmlCoucheMultiPolygones, {itemType:"Layer"});	
			}
			context.items[3] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[3].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[3].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[3].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[3].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[3].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionWfs") {
			if (withSnapping) {
				context.items[0] = Descartes.Utils.extend(couchePointsSnapping, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(coucheLignesSnapping, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(couchePolygonesSnapping, {itemType:"Layer"});
			} else if (geomType !== "composite") {
				context.items[0] = Descartes.Utils.extend(couchePoints, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(coucheLignes, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(couchePolygones, {itemType:"Layer"});
			} else {
		    	context.items[0] = Descartes.Utils.extend(coucheMultiPoints, {itemType:"Layer"});
				context.items[1] = Descartes.Utils.extend(coucheMultiLignes, {itemType:"Layer"});
				context.items[2] = Descartes.Utils.extend(coucheMultiPolygones, {itemType:"Layer"});	
			}
			context.items[3] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[3].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[3].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[3].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[3].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[3].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionAvanceeGeoJson") {
			context.items[0] = Descartes.Utils.extend(geojsonCouchePolygonesFctAvanced4, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(geojsonCoucheMultiPolygonesFctAvanced4, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(geojsonCoucheClonePolygones4, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(geojsonCoucheCloneMultiPolygones4, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[4].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[4].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[4].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[4].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[4].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionAvanceeKml") {
			context.items[0] = Descartes.Utils.extend(kmlCouchePolygonesFctAvanced4, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(kmlCoucheMultiPolygonesFctAvanced4, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(kmlCoucheClonePolygones4, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(kmlCoucheCloneMultiPolygones4, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[4].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[4].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[4].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[4].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[4].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else if (mapContentType === "EditionAvanceeWfs") {
			context.items[0] = Descartes.Utils.extend(couchePolygonesFctAvanced4, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheMultiPolygonesFctAvanced4, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(coucheClonePolygones4, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheCloneMultiPolygones4, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[4].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[4].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[4].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		} else {
			context.items[0] = Descartes.Utils.extend(coucheAnnotationsSpec, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheToponymesSpecWms, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(groupeInfrasSpec, {itemType:"Group", items:[]});
			context.items[2].items[0] = Descartes.Utils.extend(coucheParkingsSpecWms, {itemType:"Layer"});
			context.items[2].items[1] = Descartes.Utils.extend(coucheStationsSpecWms, {itemType:"Layer"});
			context.items[2].items[2] = Descartes.Utils.extend(coucheRoutesSpecWms, {itemType:"Layer"});
			context.items[2].items[3] = Descartes.Utils.extend(coucheFerSpecWms, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheEauSpecWms, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(coucheBatiSpecWms, {itemType:"Layer"});
			context.items[5] = Descartes.Utils.extend(coucheNatureSpecWms, {itemType:"Layer"});
			context.items[6] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[6].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[6].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[6].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[6].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[6].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});
		}

		doMap();
	}
}

function getContext(transport) {
	
	var response = transport.responseText;
		if (response.charAt(0) !== "{") {
			//alert("La carte est disponible jusqu'au " + response.substring(0, response.indexOf("{")));
			response = response.substring(response.indexOf("{"));
		}
	context = JSON.parse(response);
	doMap();
	
}

function stopMap() {
	alert("Impossible de recharger la carte: problÃ¨me lors du chargement du fichier de contexte");
}

function exampleMapContentSelectOnChange(selectItem) {
   var params = selectItem.value;
   var url = window.location.origin + window.location.pathname + "?" + params;
   if (url) {
          window.location = url; 
      }
}
function initExampleMapContentAnnotationSelect() {
	var value = "moteurCarto=d-mapserver";
	if (moteurCarto) {
		value = "moteurCarto="+moteurCarto;
	} else {
		moteurCarto = "d-mapserver"
	}
	if (mapContentType) {
		value += "&mapContentType="+mapContentType;
	} else {
		value += "&mapContentType=Annotation";
	}
	var select = document.querySelector('#exampleMapContentSelect');
	var options = Array.from(select.options);
	var optionToSelect = options.find(item => item.value === value);
	optionToSelect.selected = true;

}

function initExampleMapContentEditionSelect() {
	moteurCarto = "d-mapserver";
	var value = "mapContentType=EditionGenericVide";
	if (mapContentType) {
		value = "mapContentType="+mapContentType;
	}
	if (saveType) {
		value += "&saveType="+saveType;
	}
	if (geomType) {
		value += "&geomType="+geomType;
	}
	if (withSnapping) {
		value += "&withSnapping="+withSnapping;
	}
	if (withVerifConformite) {
		value += "&withVerifConformite="+withVerifConformite;
	}
	if (fctAvanced) {
		value += "&fctAvanced="+fctAvanced;
	}
	var select = document.querySelector('#exampleMapContentSelect');
	var options = Array.from(select.options);
	var optionToSelect = options.find(item => item.value === value);
	optionToSelect.selected = true;

}
var kmlCouchePoints, kmlCoucheLignes, kmlCouchePolygones, kmlCoucheBase, groupeFonds, groupeEditionKML;
var kmlCoucheClonePoints, kmlCoucheCloneLignes, kmlCoucheClonePolygones;
var kmlCoucheMultiPoints, kmlCoucheMultiLignes, kmlCoucheMultiPolygones, groupeEditionKMLMulti;
var kmlCoucheCloneMultiPoints, kmlCoucheCloneMultiLignes, kmlCoucheCloneMultiPolygones;

function chargeEditionCouchesGroupesKML() {
	
	var attribution = "&#169;Descartes(kml)";
	
	// couche de type "point"
	var fichierKmlPoints = descartesUrlRoot + "/datas/kml/points.kml";
	var geometryTypeLayerPoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var fichierKmlLines = descartesUrlRoot + "/datas/kml/lines.kml";
	var geometryTypeLayerLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var fichierKmlPolygons = descartesUrlRoot + "/datas/kml/polygons.kml";
	var geometryTypeLayerPolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	// couche de type "multipoint"
	var fichierKmlMultiPoints = descartesUrlRoot + "/datas/kml/multipoints.kml";
	var geometryTypeLayerMultiPoints =  Descartes.Layer.MULTI_POINT_GEOMETRY;
	
	// couche de type "multiligne"
	var fichierKmlMultiLines = descartesUrlRoot + "/datas/kml/multilines.kml";
	var geometryTypeLayerMultiLignes =  Descartes.Layer.MULTI_LINE_GEOMETRY;
	
	// couche de type "multipolygone"
	var fichierKmlMultiPolygons = descartesUrlRoot + "/datas/kml/multipolygons.kml";
	var geometryTypeLayerMultiPolygones =  Descartes.Layer.MULTI_POLYGON_GEOMETRY;
	
	// Pour clonage
	// couche de type "point"
	var fichierKmlClonePoints = descartesUrlRoot + "/datas/kml/clonepoints.kml";
	var geometryTypeLayerClonePoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var fichierKmlCloneLines = descartesUrlRoot + "/datas/kml/clonelines.kml";
	var geometryTypeLayerCloneLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var fichierKmlClonePolygons = descartesUrlRoot + "/datas/kml/clonepolygons.kml";
	var geometryTypeLayerClonePolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	// couche de type "multipoint"
	var fichierKmlCloneMultiPoints = descartesUrlRoot + "/datas/kml/clonemultipoints.kml";
	var geometryTypeLayerCloneMultiPoints =  Descartes.Layer.MULTI_POINT_GEOMETRY;
	
	// couche de type "multiligne"
	var fichierKmlCloneMultiLines = descartesUrlRoot + "/datas/kml/clonemultilines.kml";
	var geometryTypeLayerCloneMultiLignes =  Descartes.Layer.MULTI_LINE_GEOMETRY;
	
	// couche de type "multipolygone"
	var fichierKmlCloneMultiPolygons = descartesUrlRoot + "/datas/kml/clonemultipolygons.kml";
	var geometryTypeLayerCloneMultiPolygones =  Descartes.Layer.MULTI_POLYGON_GEOMETRY;
	
	/*****************************************************
	  couches pour affichage simple
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
		
	kmlCouchePoints = {
	    title: "Ma couche KML de points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints,
	        attribution: attribution
	    }
	};
	
	kmlCouchePoints2 = {
	    title: "Ma couche KML de points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 10000,
	        minEditionScale: 4000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints,
	        attribution: attribution
	    }
	};
	
	kmlCouchePoints3 = {
	    title: "Ma couche KML de points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlPoints
	                 }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 50000, 
	        minScale: 2000000, 
	        maxEditionScale: 50000,
	        minEditionScale: 2000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints,
	        attribution: attribution
	    }
	};

	kmlCoucheLignes = {
	    title: "Ma couche KML de lignes",
	    type: 11,
	    definition: [
	        {
           	 serverUrl: fichierKmlLines
	        }
	    ],
	    options: {
	    	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	kmlCouchePolygones = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		       }
		   ],
		   options: {
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
		
	kmlCoucheMultiPoints = {
	    title: "Ma couche KML de  multi points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlMultiPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPoints2 = {
	    title: "Ma couche KML de multi points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlMultiPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 10000,
	        minEditionScale: 4000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPoints3 = {
	    title: "Ma couche KML de multi points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlMultiPoints
	                 }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 50000, 
	        minScale: 2000000, 
	        maxEditionScale: 50000,
	        minEditionScale: 2000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints,
	        attribution: attribution
	    }
	};

	kmlCoucheMultiLignes = {
	    title: "Ma couche KML de multi lignes",
	    type: 11,
	    definition: [
	        {
           	 		serverUrl: fichierKmlMultiLines
	        }
	    ],
	    options: {
	    	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPolygones = {
	    title: "Ma couche KML de multi polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlMultiPolygons
	        }
	    ],
	    options: {
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};
	
	/*****************************************************
	  couches pour la modification des styles d'affichage
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	kmlCouchePointsStyle = {
	    title: "Ma couche KML de points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlPoints
	                 }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
					           "Point": {
					               fillColor: "blue",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "blue",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
				           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheLignesStyle = {
	    title: "Ma couche KML de lignes",
	    type: 11,
	    definition: [
	        {
           	 serverUrl: fichierKmlLines
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
	        	"default":   {//pour affichage
					           "Line": {
					               fillColor: "red",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "red",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	kmlCouchePolygonesStyle = {
	    title: "Ma couche KML de polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlPolygons
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPolygones,
	        attribution: attribution
	    }
	};	
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	kmlCoucheMultiPointsStyle = {
	    title: "Ma couche KML de multi points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlMultiPoints
	                 }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
					           "MultiPoint": {
					               fillColor: "orange",
					               graphicName:"star",
					               points: 5,
					               radius: 8,
					               radius2: 4,
					               angle: 0,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "orange",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 8
				           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiLignesStyle = {
	    title: "Ma couche KML de multi lignes",
	    type: 11,
	    definition: [
	        {
           	 serverUrl: fichierKmlMultiLines
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
	        	"default":   {//pour affichage
					           "MultiLine": {
					               fillColor: "purple",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "purple",
					               strokeOpacity: 1,
					               strokeWidth: 6,
					               strokeLinecap: "round",
					               strokeDashstyle: "sold"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPolygonesStyle = {
	    title: "Ma couche KML de multi polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlMultiPolygons
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
	            		      "MultiPolygon": {
					               fillColor: "pink",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "pink",
					               strokeOpacity: 1,
					               strokeWidth: 8,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};	
	
	/***************************************************
	  couches pour la modification des styles d'édition
	 ***************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	kmlCouchePointsStyle2 = {
	    title: "Ma couche KML de points",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlPoints
	                 }
	    ],
	    options: {
	        symbolizers: {
				"temporary":   {//pour DrawCreation
					           "Point": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           }
				}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheLignesStyle2 = {
	    title: "Ma couche KML de lignes",
	    type: 11,
	    definition: [
	        {
           	 serverUrl: fichierKmlLines
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
		       "temporary":   {//pour DrawCreation
					           "Line": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	kmlCouchePolygonesStyle2 = {
	    title: "Ma couche KML de polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlPolygons
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"temporary":   {//pour DrawCreation
					           "Polygon": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
				}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPolygones,
	        attribution: attribution
	    }
	};	
	
	kmlCouchePointsStyle3 = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		                }
		   ],
		   options: {
		       symbolizers: {
					"temporary":   {//pour DrawCreation
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
					},
					"select":   { //pour GlobalModication
						           "Point": {
						               fillColor: "green",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
					}
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
		
		kmlCoucheLignesStyle3 = {
		   title: "Ma couche KML de lignes",
		   type: 11,
		   definition: [
		       {
               	 serverUrl: fichierKmlLines
				}
		   ],
		   options: {
		   	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		           ]
		       },
		       symbolizers: {
			       "temporary":   {//pour DrawCreation
						           "Line": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
			       },
				   "select":   {//pour GlobalModication
						           "Line": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
				   },
				   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
				   }
		       },    
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes,
	           attribution: attribution
		   }
		};
		
		kmlCouchePolygonesStyle3 = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		       symbolizers: {
		       	"temporary":   {//pour DrawCreation
						           "Polygon": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					},
					"select":   {//pour GlobalModication
				    		      "Polygon": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					},
				   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 6
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
				   }
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};	
		kmlCouchePointsStyle4 = {
			   title: "Ma couche KML de points",
			   type: 11,
			   definition: [
			                {	            	
			               	 serverUrl: fichierKmlPoints
			               	 
			                }
			   ],
			   options: {
			       symbolizers: {
						"temporary":   {//pour DrawCreation
							           "Point": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"create":   {//pour DrawCreation, GlobalModication
							           "Point": {
							               fillColor: "black",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"select":   { //pour GlobalModication
							           "Point": {
							               fillColor: "green",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"delete":   { //pour RubberDeletion
				           "Point": {
				               fillColor: "grey",
				               graphicName:"square",
				               points:4,
				               radius:4,
				               angle:Math.PI / 4,
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "grey",
				               strokeOpacity: 1,
				               strokeWidth: 1,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid",			            
				               pointRadius: 4
				           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPoints,
	               attribution: attribution
			   }
			};
			
			kmlCoucheLignesStyle4 = {
			   title: "Ma couche KML de lignes",
			   type: 11,
			   definition: [
			       {
	                	 serverUrl: fichierKmlLines
	                	 
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "temporary":   {//pour DrawCreation
							           "Line": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
				       },
				       "create":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
					   "select":   {//pour GlobalModication
							           "Line": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
					   "modify":   {//pour GlobalModication, VerticeModification
								   	"Point": {
							       		fillColor: "green",
							               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           },
							           "VirtualPoint": { //pour VerticeModification
							           	 cursor: "pointer",
							                graphicName: "cross",
							                fillColor:"yellow",
							                fillOpacity:1,
							                pointRadius:4,
							                strokeColor:"yellow",
							                strokeDashstyle:"solid",
							                strokeOpacity:1,
							                strokeWidth:1
							           }
					   },
					   "delete":   {//pour RubberDeletion
							           "Line": {
							               fillColor: "grey",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerLignes,
	               attribution: attribution
			   }
			};
			
			kmlCouchePolygonesStyle4 = {
			   title: "Ma couche KML de polygones",
			   type: 11,
			   definition: [
			                {
			               	 serverUrl: fichierKmlPolygons
			               	 
			       }
			   ],
			   options: {
			       symbolizers: {
			       	"temporary":   {//pour DrawCreation
							           "Polygon": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"select":   {//pour GlobalModication
					    		      "Polygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
					   "modify":   {//pour GlobalModication, VerticeModification
								   	"Point": {
							       		fillColor: "green",
							               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 6
							           },
							           "VirtualPoint": { //pour VerticeModification
							           	 cursor: "pointer",
							                graphicName: "cross",
							                fillColor:"yellow",
							                fillOpacity:1,
							                pointRadius:4,
							                strokeColor:"yellow",
							                strokeDashstyle:"solid",
							                strokeOpacity:1,
							                strokeWidth:1
							           }
					   },
						"delete":   {//pour RubberDeletion
					    		      "Polygon": {
							               fillColor: "grey",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones,
	               attribution: attribution
			   }
			};	
			
		kmlCouchePointsStyle5 = {
			   title: "Ma couche KML de points",
			   type: 11,
			   definition: [
			                {	            	
			               	 serverUrl: fichierKmlPoints
			               	 
			                }
			   ],
			   options: {
			       symbolizers: {
						"create":   {//pour DrawCreation, GlobalModication
							           "Point": {
							               fillColor: "black",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"modify":   {//pour DrawCreation, GlobalModication
					           "Point": {
					               fillColor: "black",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "black",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPoints,
	               attribution: attribution
			   }
			};
			
			kmlCoucheLignesStyle5 = {
			   title: "Ma couche KML de lignes",
			   type: 11,
			   definition: [
			       {
	                	 serverUrl: fichierKmlLines
	                	 
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "create":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
				       "modify":   {//pour DrawCreation, GlobalModication
				           "Line": {
				               fillColor: "black",
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "black",
				               strokeOpacity: 1,
				               strokeWidth: 4,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid"
				           }
				       }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerLignes,
	               attribution: attribution
			   }
			};
			
			kmlCouchePolygonesStyle5 = {
			   title: "Ma couche KML de polygones",
			   type: 11,
			   definition: [
			                {
			               	 serverUrl: fichierKmlPolygons
			               	 
			       }
			   ],
			   options: {
			       symbolizers: {
						"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"modify":   {//pour DrawCreation, GlobalModication
			    		      "Polygon": {
					               fillColor: "black",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "black",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						}
			       },
			       clone: {
			           supportLayers: [{
		                   id: "kmlCoucheMultiPolygones",
		                   attributes: [{
		                           from: "d_attrib_2",
		                           to: "d_attrib_2"
		                       },
		                       {
		                           from: "d_attrib_3",
		                           to: "d_attrib_3"
		                       }
		                   ]
		               }]
			       },
			       copy: {
			   		supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
			       },
			       buffer: {
			       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
			       	distance:20000/*,
			           enable: true*/
			       }, 
			       halo: {
			       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
			       	distance:20000/*,
			           enable: true*/
			       }, 
			       homothetic: {
			           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
			           enable: true*/
			       }, 
			       split: {
			           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
			           enable: true*/
			       }, 
			       divide: {
			           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
			           enable: true*/
			       },
			       aggregate: {
			           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
			           enable: true
			       },
			       substract: {
			           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
			           enable: true
			       },
			       intersect: {
			       	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
			           enable: true
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones,
	               attribution: attribution
			   }
			};	
			
			kmlCouchePointsStyle6 = {
				   title: "Ma couche KML de points",
				   type: 11,
				   definition: [
				                {	            	
				               	 serverUrl: fichierKmlPoints
				               	 
				                }
				   ],
				   options: {
				   	id: "kmlCouchePoints",
				       symbolizers: {
				       	"default":   {//pour affichage
					           "Point": {
					               fillColor: "blue",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "blue",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
						           }
					       	},
					       	"create":   {//pour DrawCreation, GlobalModication
						           "Point": {
						               fillColor: "black",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
				       		},
				       		"temporary":   {//pour DrawCreation
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
				       		},
								"select":   { //pour GlobalModication
									           "Point": {
									               fillColor: "green",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"delete":   { //pour RubberDeletion
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
				       },
				       copy: {
				       	supportLayersIdentifier: ["kmlCoucheMultiPoints"],
				       	enable:true
				       },
				       clone: {
				       	supportLayers: [{
			                   id: "kmlCoucheMultiPoints",
			                   attributes: [{
				                       from: "d_attrib_2",
				                       to: "d_attrib_2"
				                   },
				                   {
				                       from: "d_attrib_3",
				                       to: "d_attrib_3"
				                   }
			                   ]
			               }],
				       	enable:true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["kmlCoucheMultiPoints"]
				       },
				       intersect: {
				       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerPoints,
	                   attribution: attribution
				   }
				};
				
				kmlCoucheLignesStyle6 = {
				   title: "Ma couche KML de lignes",
				   type: 11,
				   definition: [
				       {
		               	 serverUrl: fichierKmlLines
		               	 
						}
				   ],
				   options: {
				   	id: "kmlCoucheLignes",
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: {
				       	"default":   {//pour affichage
					           "Line": {
					               fillColor: "red",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "red",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
								           }
					       },
					       "create":   {//pour DrawCreation, GlobalModication
						           "Line": {
						               fillColor: "black",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
						   },
						   "temporary":   {//pour DrawCreation
					           "Line": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   },
						   "select":   {//pour GlobalModication
					           "Line": {
					               fillColor: "green",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   },
						   "modify":   {//pour GlobalModication, VerticeModification
						   	"Point": {
					       		fillColor: "green",
					               graphicName:"cross",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           },
					           "VirtualPoint": { //pour VerticeModification
					           	 cursor: "pointer",
					                graphicName: "cross",
					                fillColor:"yellow",
					                fillOpacity:1,
					                pointRadius:4,
					                strokeColor:"yellow",
					                strokeDashstyle:"solid",
					                strokeOpacity:1,
					                strokeWidth:1
					           }
						   },
						   "delete":   {//pour RubberDeletion
					           "Line": {
					               fillColor: "grey",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   }
				       },  
				       copy: {
				       	supportLayersIdentifier: ["kmlCoucheMultiLignes"],
				       	enable:true
				       },
				       clone: {
				       	supportLayers: [{
			                   id: "kmlCoucheMultiLignes",
			                   attributes: [{
				                       from: "d_attrib_2",
				                       to: "d_attrib_2"
				                   },
				                   {
				                       from: "d_attrib_3",
				                       to: "d_attrib_3"
				                   }
			                   ]
			               }],
				       	enable:true
				       },
				       aggregate: {
				       	supportLayersIdentifier: ["kmlCoucheMultiLignes"],
				       	enable:true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["kmlCoucheMultiLignes"]
				       },
				       split: {
				       	supportLayersIdentifier: ["kmlCoucheMultiLignes"]
				       },
				       divide: {
				       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"]
				       },
				       substract: {
				       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"]
				       },
				       intersect: {
				       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerLignes,
	                   attribution: attribution
				   }
				};
				
				kmlCouchePolygonesStyle6 = {
				   title: "Ma couche KML de polygones",
				   type: 11,
				   definition: [
				                {
				               	 serverUrl: fichierKmlPolygons
				               	 
				       }
				   ],
				   options: {
				   	id: "kmlCouchePolygones",
				       symbolizers: {
				       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
				       	},"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
							},
							"temporary":   {//pour DrawCreation
					           "Polygon": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
							},
							"select":   {//pour GlobalModication
				    		      "Polygon": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							},
						   "modify":   {//pour GlobalModication, VerticeModification
						   	"Point": {
					       		fillColor: "green",
					               graphicName:"cross",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 6
					           },
					           "VirtualPoint": { //pour VerticeModification
					           	 cursor: "pointer",
					                graphicName: "cross",
					                fillColor:"yellow",
					                fillOpacity:1,
					                pointRadius:4,
					                strokeColor:"yellow",
					                strokeDashstyle:"solid",
					                strokeOpacity:1,
					                strokeWidth:1
					           }
						   },
							"delete":   {//pour RubberDeletion
				    		      "Polygon": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							}
				       },
				       clone: {
				           supportLayers: [{
			                   id: "kmlCoucheMultiPolygones",
			                   attributes: [{
			                           from: "d_attrib_2",
			                           to: "d_attrib_2"
			                       },
			                       {
			                           from: "d_attrib_3",
			                           to: "d_attrib_3"
			                       }
			                   ]
			               }]
				       },
				       copy: {
				   		supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
				       },
				       buffer: {
				       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
				       	distance:20000/*,
				           enable: true*/
				       }, 
				       halo: {
				       	supportLayers: [{id:"kmlCoucheMultiPoints"},{id:"kmlCoucheMultiLignes"}],
				       	distance:20000/*,
				           enable: true*/
				       }, 
				       homothetic: {
				           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
				           enable: true*/
				       }, 
				       split: {
				           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
				           enable: true*/
				       }, 
				       divide: {
				           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
				           enable: true*/
				       },
				       aggregate: {
				           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
				           enable: true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
				       },
				       substract: {
				           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
				           enable: true
				       },
				       intersect: {
				       	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerPolygones,
	                   attribution: attribution
				   }
				};	
				kmlCouchePointsStyle7 = {
						   title: "Ma couche KML de points",
						   type: 11,
						   definition: [
						                {	            	
						               	 serverUrl: fichierKmlPoints
						               	 
						                }
						   ],
						   options: {
						       symbolizers: {
									"modify":   {//pour DrawCreation, GlobalModication
								           "Point": {
								               fillColor: "black",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
									}
						       },
						       alwaysVisible: false,
						       visible: true,
						       queryable: false,
						       activeToQuery: false,
						       sheetable: false,
						       opacity: 100,
						       opacityMax: 100,
						       legend: null,
						       metadataURL: null,
						       format: "image/png",
						       displayOrder: 1,
						       geometryType: geometryTypeLayerPoints,
	                           attribution: attribution
						   }
						};
						
						kmlCoucheLignesStyle7 = {
						   title: "Ma couche KML de lignes",
						   type: 11,
						   definition: [
						       {
				                	 serverUrl: fichierKmlLines
				                	 
								}
						   ],
						   options: {
						   	 attributes: {
						           /*attributeId: {
						               fieldName: "d_attrib_1"
						           },*/
						           attributesEditable: [
						               {fieldName: 'd_attrib_2', label: 'Un attribut'},
						           ]
						       },
						       symbolizers: {
							       "modify":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 2,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
							       }
						       },    
						       alwaysVisible: false,
						       visible: true,
						       queryable: false,
						       activeToQuery: false,
						       sheetable: false,
						       opacity: 100,
						       opacityMax: 100,
						       legend: null,
						       metadataURL: null,
						       format: "image/png",
						       displayOrder: 1,
						       geometryType: geometryTypeLayerLignes,
	                           attribution: attribution
						   }
						};
						
						kmlCouchePolygonesStyle7 = {
						   title: "Ma couche KML de polygones",
						   type: 11,
						   definition: [
						                {
						               	 serverUrl: fichierKmlPolygons
						               	 
						       }
						   ],
						   options: {
						       symbolizers: {
									"modify":   {//pour DrawCreation, GlobalModication
						    		      "Polygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 2,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
									}
						       },
						       clone: {
						           supportLayers: [{
					                   id: "kmlCoucheMultiPolygones",
					                   attributes: [{
					                           from: "d_attrib_2",
					                           to: "d_attrib_2"
					                       },
					                       {
					                           from: "d_attrib_3",
					                           to: "d_attrib_3"
					                       }
					                   ]
					               }]
						       },
						       copy: {
						   		supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
						       },
						       buffer: {
						       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
						       	distance:20000/*,
						           enable: true*/
						       }, 
						       halo: {
						       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
						       	distance:20000/*,
						           enable: true*/
						       }, 
						       homothetic: {
						           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
						           enable: true*/
						       }, 
						       split: {
						           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
						           enable: true*/
						       }, 
						       divide: {
						           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
						           enable: true*/
						       },
						       aggregate: {
						           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
						           enable: true
						       },
						       substract: {
						           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
						           enable: true
						       },
						       intersect: {
						       	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
						           enable: true
						       },
						       alwaysVisible: false,
						       visible: true,
						       queryable: false,
						       activeToQuery: false,
						       sheetable: false,
						       opacity: 100,
						       opacityMax: 100,
						       legend: null,
						       metadataURL: null,
						       format: "image/png",
						       displayOrder: 1,
						       geometryType: geometryTypeLayerPolygones,
	                           attribution: attribution
						   }
						};				
						
						kmlCouchePointsStyle8 = {
							    title: "Ma couche KML de points",
							    type: 11,
							    definition: [
							                 {	            	
							                	 serverUrl: fichierKmlPoints
							                 }
							    ],
							    options: {
							        symbolizers: {
										"temporary":   {//pour DrawCreation
											           "Point": {
											               fillColor: "grey",
											               graphicName:"square",
											               points:4,
											               radius:4,
											               angle:Math.PI / 4,
											               fillOpacity: 0.4,
											               hoverFillColor: "white",
											               hoverFillOpacity: 0.8,
											               strokeColor: "grey",
											               strokeOpacity: 1,
											               strokeWidth: 1,
											               strokeLinecap: "round",
											               strokeDashstyle: "solid",			            
											               pointRadius: 4
											           }
										},
										"autotracing":{
										    'Line': {
										        fillColor: 'grey',
										        fillOpacity: 0.4,
										        hoverFillColor: 'white',
										        hoverFillOpacity: 0.8,
										        strokeColor: 'grey',
										        strokeOpacity: 1,
										        strokeWidth: 5,
										        strokeLinecap: 'round',
										        strokeDashstyle: [5, 10]
										    }
										}
							        },
							        snapping : {
								           tolerance: 10,
								           enable: true,
								           autotracing: true
								        }, 
							        alwaysVisible: false,
							        visible: true,
							        queryable: false,
							        activeToQuery: false,
							        sheetable: false,
							        opacity: 100,
							        opacityMax: 100,
							        legend: null,
							        metadataURL: null,
							        format: "image/png",
							        displayOrder: 1,
			       					geometryType: geometryTypeLayerMultiPoints,
	               					attribution: attribution
							    }
							};
							
							kmlCoucheLignesStyle8 = {
							    title: "Ma couche KML de lignes",
							    type: 11,
							    definition: [
							        {
						           	 serverUrl: fichierKmlLines
									}
							    ],
							    options: {
							    	 attributes: {
							            /*attributeId: {
							                fieldName: "d_attrib_1"
							            },*/
							            attributesEditable: [
							                {fieldName: 'd_attrib_2', label: 'Un attribut'},
							            ]
							        },
							        symbolizers: {
								       "temporary":   {//pour DrawCreation
											           "Line": {
											               fillColor: "grey",
											               graphicName:"square",
											               points:4,
											               radius:4,
											               angle:Math.PI / 4,
											               fillOpacity: 0.4,
											               hoverFillColor: "white",
											               hoverFillOpacity: 0.8,
											               strokeColor: "grey",
											               strokeOpacity: 1,
											               strokeWidth: 1,
											               strokeLinecap: "round",
											               strokeDashstyle: "solid"
											           }
								       },
										"autotracing":{
										    'Line': {
										        fillColor: 'grey',
										        fillOpacity: 0.4,
										        hoverFillColor: 'white',
										        hoverFillOpacity: 0.8,
										        strokeColor: 'grey',
										        strokeOpacity: 1,
										        strokeWidth: 5,
										        strokeLinecap: 'round',
										        strokeDashstyle: [5, 10]
										    }
										}
							        },
							        snapping : {
								           tolerance: 10,
								           enable: true,
								           autotracing: true
								        }, 
							        alwaysVisible: false,
							        visible: true,
							        queryable: false,
							        activeToQuery: false,
							        sheetable: false,
							        opacity: 100,
							        opacityMax: 100,
							        legend: null,
							        metadataURL: null,
							        format: "image/png",
							        displayOrder: 1,
							        geometryType: geometryTypeLayerLignes,
	                                attribution: attribution
							    }
							};
							
							kmlCouchePolygonesStyle8 = {
							    title: "Ma couche KML de polygones",
							    type: 11,
							    definition: [
							                 {
							                	 serverUrl: fichierKmlPolygons
							        }
							    ],
							    options: {
							        symbolizers: {
							        	"temporary":   {//pour DrawCreation
											           "Polygon": {
											               fillColor: "grey",
											               graphicName:"square",
											               points:4,
											               radius:4,
											               angle:Math.PI / 4,
											               fillOpacity: 0.4,
											               hoverFillColor: "white",
											               hoverFillOpacity: 0.8,
											               strokeColor: "grey",
											               strokeOpacity: 1,
											               strokeWidth: 1,
											               strokeLinecap: "round",
											               strokeDashstyle: "solid"
											           }
										},
										"autotracing":{
										    'Line': {
										        fillColor: 'grey',
										        fillOpacity: 0.4,
										        hoverFillColor: 'white',
										        hoverFillOpacity: 0.8,
										        strokeColor: 'grey',
										        strokeOpacity: 1,
										        strokeWidth: 5,
										        strokeLinecap: 'round',
										        strokeDashstyle: [5, 10]
										    }
										}
							        },
							        snapping : {
								           tolerance: 10,
								           enable: true,
								           autotracing: true
								        }, 
							        alwaysVisible: false,
							        visible: true,
							        queryable: false,
							        activeToQuery: false,
							        sheetable: false,
							        opacity: 100,
							        opacityMax: 100,
							        legend: null,
							        metadataURL: null,
							        format: "image/png",
							        displayOrder: 1,
							        geometryType: geometryTypeLayerPolygones,
	                                attribution: attribution
							    }
							};	
			//---------------------------------------------------------//
			//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
			//---------------------------------------------------------//
			
			kmlCoucheMultiPointsStyle2 = {
			   title: "Ma couche KML de multi points",
			   type: 11,
			   definition: [
			                {	            	
			               	 serverUrl: fichierKmlMultiPoints
			               	 
			                }
			   ],
			   options: {
			       symbolizers: {
						"temporary":   {//pour DrawCreation
							           "MultiPoint": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiPoints,
	               attribution: attribution
			   }
			};
			
			kmlCoucheMultiLignesStyle2 = {
			   title: "Ma couche KML de multi lignes",
			   type: 11,
			   definition: [
			       {
			       	serverUrl: fichierKmlMultiLines
	                	 
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "temporary":   {//pour DrawCreation
							           "MultiLine": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
				       }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiLignes,
	               attribution: attribution
			   }
			};
			
			kmlCoucheMultiPolygonesStyle2 = {
			   title: "Ma couche KML de multipolygones",
			   type: 11,
			   definition: [
			                {
			               	 serverUrl: fichierKmlMultiPolygons
			               	 
			       }
			   ],
			   options: {
			       symbolizers: {
			       	"temporary":   {//pour DrawCreation
							           "MultiPolygon": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiPolygones,
	               attribution: attribution
			   }
			};	
			
			kmlCoucheMultiPointsStyle3 = {
				   title: "Ma couche KML de multi points",
				   type: 11,
				   definition: [
				                {	            	
				               	 serverUrl: fichierKmlMultiPoints
				               	 
				                }
				   ],
				   options: {
				       symbolizers: {
							"temporary":   {//pour DrawCreation
								           "MultiPoint": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							},
							"select":   { //pour GlobalModication
								           "MultiPoint": {
								               fillColor: "green",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPoints,
	                   attribution: attribution
				   }
				};
				
				kmlCoucheMultiLignesStyle3 = {
				   title: "Ma couche KML de multi lignes",
				   type: 11,
				   definition: [
				       {
				       	serverUrl: fichierKmlMultiLines
		               	 
						}
				   ],
				   options: {
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: {
					       "temporary":   {//pour DrawCreation
								           "MultiLine": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
					       },
						   "select":   {//pour GlobalModication
								           "Line": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           },
								           "MultiLine": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
						   },
						   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
						   }
				       },    
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiLignes,
	                   attribution: attribution
				   }
				};
				
				kmlCoucheMultiPolygonesStyle3 = {
				   title: "Ma couche KML de multi polygones",
				   type: 11,
				   definition: [
				                {
				               	 serverUrl: fichierKmlMultiPolygons
				               	 
				       }
				   ],
				   options: {
				       symbolizers: {
				       	"temporary":   {//pour DrawCreation
								           "MultiPolygon": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
							"select":   {//pour GlobalModication
						    		      "Polygon": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           },
								           "MultiPolygon": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
						   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 6
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
						   }
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPolygones,
	                   attribution: attribution
				   }
				};	
				
				kmlCoucheMultiPointsStyle4 = {
					   title: "Ma couche KML de multi points",
					   type: 11,
					   definition: [
					                {	            	
					               	 serverUrl: fichierKmlMultiPoints
					               	 
					                }
					   ],
					   options: {
					       symbolizers: {
								"temporary":   {//pour DrawCreation
									           "MultiPoint": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"create":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"select":   { //pour GlobalModication
									           "Point": {
									               fillColor: "green",
									               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPoints,
	                       attribution: attribution
					   }
					};
					
					kmlCoucheMultiLignesStyle4 = {
					   title: "Ma couche KML de multi lignes",
					   type: 11,
					   definition: [
					       {
					       	serverUrl: fichierKmlMultiLines
			               	 
							}
					   ],
					   options: {
					   	 attributes: {
					           /*attributeId: {
					               fieldName: "d_attrib_1"
					           },*/
					           attributesEditable: [
					               {fieldName: 'd_attrib_2', label: 'Un attribut'},
					           ]
					       },
					       symbolizers: {
						       "temporary":   {//pour DrawCreation
									           "MultiLine": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
						       },
						       "create":   {//pour DrawCreation, GlobalModication
									           "MultiLine": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
							   "select":   {//pour GlobalModication
									           "Line": {
									               fillColor: "green",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
							   "modify":   {//pour GlobalModication, VerticeModification
										   	"Point": {
									       		fillColor: "green",
									               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           },
									           "VirtualPoint": { //pour VerticeModification
									           	 cursor: "pointer",
									                graphicName: "cross",
									                fillColor:"yellow",
									                fillOpacity:1,
									                pointRadius:4,
									                strokeColor:"yellow",
									                strokeDashstyle:"solid",
									                strokeOpacity:1,
									                strokeWidth:1
									           }
							   },
							   "delete":   {//pour RubberDeletion
									           "MultiLine": {
									               fillColor: "grey",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   }
					       },    
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiLignes,
	                       attribution: attribution
					   }
					};
					
					kmlCoucheMultiPolygonesStyle4 = {
					   title: "Ma couche KML de multi polygones",
					   type: 11,
					   definition: [
					                {
					               	 serverUrl: fichierKmlMultiPolygons
					               	 
					       }
					   ],
					   options: {
					       symbolizers: {
					       	"temporary":   {//pour DrawCreation
									           "MultiPolygon": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"create":   {//pour DrawCreation, GlobalModication
							    		      "MultiPolygon": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"select":   {//pour GlobalModication
							    		      "Polygon": {
									               fillColor: "green",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
							   "modify":   {//pour GlobalModication, VerticeModification
										   	"Point": {
									       		fillColor: "green",
									               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 6
									           },
									           "VirtualPoint": { //pour VerticeModification
									           	 cursor: "pointer",
									                graphicName: "cross",
									                fillColor:"yellow",
									                fillOpacity:1,
									                pointRadius:4,
									                strokeColor:"yellow",
									                strokeDashstyle:"solid",
									                strokeOpacity:1,
									                strokeWidth:1
									           }
							   },
								"delete":   {//pour RubberDeletion
							    		      "MultiPolygon": {
									               fillColor: "grey",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								}
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPolygones,
	                       attribution: attribution
					   }
					};	
			kmlCoucheMultiPointsStyle5 = {
				   title: "Ma couche KML de multi points",
				   type: 11,
				   definition: [
				                {	            	
				               	 serverUrl: fichierKmlMultiPoints
				                }
				   ],
				   options: {
				       symbolizers: {
							"create":   {//pour DrawCreation, GlobalModication
								           "MultiPoint": {
								               fillColor: "black",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							},
							"modify":   {//pour DrawCreation, GlobalModication
						           "MultiPoint": {
						               fillColor: "black",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPoints,
	                   attribution: attribution
				   }
				};
				
				kmlCoucheMultiLignesStyle5 = {
				   title: "Ma couche KML de multi lignes",
				   type: 11,
				   definition: [
				       {
				       		serverUrl: fichierKmlMultiLines
		               	 
						}
				   ],
				   options: {
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: { 
					       "create":   {//pour DrawCreation, GlobalModication
								           "MultiLine": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
						   },
					       "modify":   {//pour DrawCreation, GlobalModication
					           "MultiLine": {
					               fillColor: "black",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "black",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
					       }
				       },    
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiLignes,
	                   attribution: attribution
				   }
				};
				
				kmlCoucheMultiPolygonesStyle5 = {
				   title: "Ma couche KML de multi polygones",
				   type: 11,
				   definition: [
				                {
				               	 serverUrl: fichierKmlMultiPolygons
				               	 
				       }
				   ],
				   options: {
				   	id: "kmlCoucheMultiPolygones",
				       symbolizers: {
							"create":   {//pour DrawCreation, GlobalModication
								           "MultiPolygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
							"modify":   {//pour DrawCreation, GlobalModication
						           "MultiPolygon": {
						               fillColor: "black",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPolygones,
	                   attribution: attribution
				   }
				};	
				
				kmlCoucheMultiPointsStyle6 = {
					   title: "Ma couche KML de multi points",
					   type: 11,
					   definition: [
					                {	            	
					               	 serverUrl: fichierKmlMultiPoints
					               	 
					                }
					   ],
					   options: {
					   	id: "kmlCoucheMultiPoints",
					       symbolizers: {
					       	"default":   {//pour affichage
						           "MultiPoint": {
						               fillColor: "orange",
						               graphicName:"star",
						               points: 5,
						               radius: 8,
						               radius2: 4,
						               angle: 0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "orange",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 8
						           }
					       	},
					       	"create":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"temporary":   {//pour DrawCreation
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								},
								"select":   { //pour GlobalModication
						           "Point": {
						               fillColor: "green",
						               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "MultiPoint": {
						               fillColor: "green",
						               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       copy: {
					       	supportLayersIdentifier: ["kmlCouchePoints"],
					       	enable:true
					       },
					       clone: {
					       	supportLayers: [{
				                   id: "kmlCouchePoints",
				                   attributes: [{
					                       from: "d_attrib_2",
					                       to: "d_attrib_2"
					                   },
					                   {
					                       from: "d_attrib_3",
					                       to: "d_attrib_3"
					                   }
				                   ]
				               }],
					       	enable:true
					       },
					       aggregate: {
					       	supportLayersIdentifier: ["kmlCouchePoints"],
					       	enable:true
					       },
					       intersect: {
					       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
					           enable: true
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPoints,
	                       attribution: attribution
					   }
					};
					
					kmlCoucheMultiLignesStyle6 = {
					   title: "Ma couche KML de multi lignes",
					   type: 11,
					   definition: [
					       {
					       	serverUrl: fichierKmlMultiLines
			               	 
							}
					   ],
					   options: {
					   	id: "kmlCoucheMultiLignes",
					   	 attributes: {
					           /*attributeId: {
					               fieldName: "d_attrib_1"
					           },*/
					           attributesEditable: [
					               {fieldName: 'd_attrib_2', label: 'Un attribut'},
					           ]
					       },
					       symbolizers: { 
					       	"default":   {//pour affichage
						           "MultiLine": {
						               fillColor: "purple",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "purple",
						               strokeOpacity: 1,
						               strokeWidth: 6,
						               strokeLinecap: "round",
						               strokeDashstyle: "sold"
						           }
					       	},
						       "create":   {//pour DrawCreation, GlobalModication
									           "MultiLine": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
						       "temporary":   {//pour DrawCreation
						           "MultiLine": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
						       },
							   "select":   {//pour GlobalModication
						           "Line": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           },
						           "MultiLine": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							   },
							   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
							   },
							   "delete":   {//pour RubberDeletion
						           "MultiLine": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							   }
					       },
					       copy: {
					       	supportLayersIdentifier: ["kmlCoucheLignes"],
					       	enable:true
					       },
					       clone: {
					       	supportLayers: [{
				                   id: "kmlCoucheLignes",
				                   attributes: [{
					                       from: "d_attrib_2",
					                       to: "d_attrib_2"
					                   },
					                   {
					                       from: "d_attrib_3",
					                       to: "d_attrib_3"
					                   }
				                   ]
				               }],
					       	enable:true
					       },
					       aggregate: {
					       	supportLayersIdentifier: ["kmlCoucheLignes"],
					       	enable:true
					       },
					       split: {
					       	supportLayersIdentifier: ["kmlCoucheLignes"]
					       },
					       divide: {
					       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"]
					       },
					       substract: {
					       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"]
					       },
					       intersect: {
					       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"]
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiLignes,
	                       attribution: attribution
					   }
					};
					
					kmlCoucheMultiPolygonesStyle6 = {
					   title: "Ma couche KML de multi polygones",
					   type: 11,
					   definition: [
					                {
					               	 serverUrl: fichierKmlMultiPolygons
					               	 
					       }
					   ],
					   options: {
					   	id: "kmlCoucheMultiPolygones",
					       symbolizers: {
					       	"default":   {//pour affichage
							           "MultiPolygon": {
							               fillColor: "pink",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               pointerEvents: "visiblePainted",
							               cursor: "pointer",
							               strokeColor: "pink",
							               strokeOpacity: 1,
							               strokeWidth: 8,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					       	},
					       	"temporary":   {//pour DrawCreation
						           "MultiPolygon": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					       	},
					       	"select":   {//pour GlobalModication
							           "MultiPolygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
								},
							   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 6
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
							   },
							   "create":   {//pour DrawCreation, GlobalModication
								           "MultiPolygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       clone: {
					           supportLayers: [{
				                   id: "kmlCouchePolygones",
				                   attributes: [{
				                           from: "d_attrib_2",
				                           to: "d_attrib_2"
				                       },
				                       {
				                           from: "d_attrib_3",
				                           to: "d_attrib_3"
				                       }
				                   ]
				               }]
					       },
					       copy: {
					   		supportLayersIdentifier: ["kmlCouchePolygones"]
					       },
					       buffer: {
					       	supportLayers: [{id:"kmlCouchePolygones"}],
					       	distance:20000/*,
					           enable: true*/
					       }, 
					       halo: {
					       	supportLayers: [{id:"kmlCoucheMultiPoints"},{id:"kmlCoucheMultiLignes"}],
					       	distance:20000/*,
					           enable: true*/
					       }, 
					       homothetic: {
					           supportLayersIdentifier: ["kmlCouchePolygones"]/*,
					           enable: true*/
					       }, 
					       split: {
					           supportLayersIdentifier: ["kmlCouchePolygones"]/*,
					           enable: true*/
					       }, 
					       divide: {
					           supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheLignes"]/*,
					           enable: true*/
					       },
					       aggregate: {
					           supportLayersIdentifier: ["kmlCouchePolygones"],
					           enable: true
					       },
					       substract: {
					           supportLayersIdentifier: ["kmlCouchePolygones"],
					           enable: true
					       },
					       intersect: {
					       	supportLayersIdentifier:["kmlCouchePolygones","kmlCoucheLignes","kmlCouchePoints"],
					           enable: true
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPolygones,
	                       attribution: attribution
					   }
					};
					kmlCoucheMultiPointsStyle7 = {
							   title: "Ma couche KML de multi points",
							   type: 11,
							   definition: [
							                {	            	
							               	 serverUrl: fichierKmlMultiPoints
							                }
							   ],
							   options: {
							       symbolizers: {
										"modify":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
										}
							       },
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerMultiPoints,
	                               attribution: attribution
							   }
							};
							
							kmlCoucheMultiLignesStyle7 = {
							   title: "Ma couche KML de multi lignes",
							   type: 11,
							   definition: [
							       {
							       		serverUrl: fichierKmlMultiLines
					               	 
									}
							   ],
							   options: {
							   	 attributes: {
							           /*attributeId: {
							               fieldName: "d_attrib_1"
							           },*/
							           attributesEditable: [
							               {fieldName: 'd_attrib_2', label: 'Un attribut'},
							           ]
							       },
							       symbolizers: { 
								       "modify":   {//pour DrawCreation, GlobalModication
								           "MultiLine": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 2,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
								       }
							       },    
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerMultiLignes,
	                               attribution: attribution
							   }
							};
							
							kmlCoucheMultiPolygonesStyle7 = {
							   title: "Ma couche KML de multi polygones",
							   type: 11,
							   definition: [
							                {
							               	 serverUrl: fichierKmlMultiPolygons
							               	 
							       }
							   ],
							   options: {
							   	id: "kmlCoucheMultiPolygones",
							       symbolizers: {
										"modify":   {//pour DrawCreation, GlobalModication
									           "MultiPolygon": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 2,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
										}
							       },
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerMultiPolygones,
	                               attribution: attribution
							   }
							};					
							kmlCoucheMultiPointsStyle8 = {
									   title: "Ma couche KML de multi points",
									   type: 11,
									   definition: [
									                {	            	
									               	 serverUrl: fichierKmlMultiPoints
									               	 
									                }
									   ],
									   options: {
									       symbolizers: {
												"temporary":   {//pour DrawCreation
													           "MultiPoint": {
													               fillColor: "grey",
													               graphicName:"square",
													               points:4,
													               radius:4,
													               angle:Math.PI / 4,
													               fillOpacity: 0.4,
													               hoverFillColor: "white",
													               hoverFillOpacity: 0.8,
													               strokeColor: "grey",
													               strokeOpacity: 1,
													               strokeWidth: 1,
													               strokeLinecap: "round",
													               strokeDashstyle: "solid",			            
													               pointRadius: 4
													           }
												},
												"autotracing":{
												    'Line': {
												        fillColor: 'grey',
												        fillOpacity: 0.4,
												        hoverFillColor: 'white',
												        hoverFillOpacity: 0.8,
												        strokeColor: 'grey',
												        strokeOpacity: 1,
												        strokeWidth: 5,
												        strokeLinecap: 'round',
												        strokeDashstyle: [5, 10]
												    }
												}
									       },
									        snapping : {
										           tolerance: 10,
										           enable: true,
										           autotracing: true
										        }, 
									       alwaysVisible: false,
									       visible: true,
									       queryable: false,
									       activeToQuery: false,
									       sheetable: false,
									       opacity: 100,
									       opacityMax: 100,
									       legend: null,
									       metadataURL: null,
									       format: "image/png",
									       displayOrder: 1,
									       geometryType: geometryTypeLayerMultiPoints,
	                                       attribution: attribution
									   }
									};
									
									kmlCoucheMultiLignesStyle8 = {
									   title: "Ma couche KML de multi lignes",
									   type: 11,
									   definition: [
									       {
									       	serverUrl: fichierKmlMultiLines
							                	 
											}
									   ],
									   options: {
									   	 attributes: {
									           /*attributeId: {
									               fieldName: "d_attrib_1"
									           },*/
									           attributesEditable: [
									               {fieldName: 'd_attrib_2', label: 'Un attribut'},
									           ]
									       },
									       symbolizers: {
										       "temporary":   {//pour DrawCreation
													           "MultiLine": {
													               fillColor: "grey",
													               graphicName:"square",
													               points:4,
													               radius:4,
													               angle:Math.PI / 4,
													               fillOpacity: 0.4,
													               hoverFillColor: "white",
													               hoverFillOpacity: 0.8,
													               strokeColor: "grey",
													               strokeOpacity: 1,
													               strokeWidth: 1,
													               strokeLinecap: "round",
													               strokeDashstyle: "solid"
													           }
										       },
												"autotracing":{
												    'Line': {
												        fillColor: 'grey',
												        fillOpacity: 0.4,
												        hoverFillColor: 'white',
												        hoverFillOpacity: 0.8,
												        strokeColor: 'grey',
												        strokeOpacity: 1,
												        strokeWidth: 5,
												        strokeLinecap: 'round',
												        strokeDashstyle: [5, 10]
												    }
												}
									       },   
									        snapping : {
										           tolerance: 10,
										           enable: true,
										           autotracing: true
										        }, 
									       alwaysVisible: false,
									       visible: true,
									       queryable: false,
									       activeToQuery: false,
									       sheetable: false,
									       opacity: 100,
									       opacityMax: 100,
									       legend: null,
									       metadataURL: null,
									       format: "image/png",
									       displayOrder: 1,
									       geometryType: geometryTypeLayerMultiLignes,
	                                       attribution: attribution
									   }
									};
									
									kmlCoucheMultiPolygonesStyle8 = {
									   title: "Ma couche KML de multipolygones",
									   type: 11,
									   definition: [
									                {
									               	 serverUrl: fichierKmlMultiPolygons
									               	 
									       }
									   ],
									   options: {
									       symbolizers: {
									       	"temporary":   {//pour DrawCreation
													           "MultiPolygon": {
													               fillColor: "grey",
													               graphicName:"square",
													               points:4,
													               radius:4,
													               angle:Math.PI / 4,
													               fillOpacity: 0.4,
													               hoverFillColor: "white",
													               hoverFillOpacity: 0.8,
													               strokeColor: "grey",
													               strokeOpacity: 1,
													               strokeWidth: 1,
													               strokeLinecap: "round",
													               strokeDashstyle: "solid"
													           }
												},
												"autotracing":{
												    'Line': {
												        fillColor: 'grey',
												        fillOpacity: 0.4,
												        hoverFillColor: 'white',
												        hoverFillOpacity: 0.8,
												        strokeColor: 'grey',
												        strokeOpacity: 1,
												        strokeWidth: 5,
												        strokeLinecap: 'round',
												        strokeDashstyle: [5, 10]
												    }
												}
									       },
									        snapping : {
										           tolerance: 10,
										           enable: true,
										           autotracing: true
										        }, 
									       alwaysVisible: false,
									       visible: true,
									       queryable: false,
									       activeToQuery: false,
									       sheetable: false,
									       opacity: 100,
									       opacityMax: 100,
									       legend: null,
									       metadataURL: null,
									       format: "image/png",
									       displayOrder: 1,
									       geometryType: geometryTypeLayerMultiPolygones,
	                                       attribution: attribution
									   }
									};							
				
	/***************************
	  couches pour le snapping
	 ****************************/
					
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	kmlCouchePointsSnapping = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },     
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
	
	kmlCouchePointsSnapping2 = {
		   title: "Ma couche KML de points",
		   type: 1,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		       /*snapping : {
		           tolerance: 10,
		           enable: true
		       },  */   
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheLignesSnapping = {
	    title: "Ma couche KML de lignes",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlLines
           	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"kmlCoucheLignes",
		   snapping : {
		   	snappingLayersIdentifier:["kmlCouchePoints","kmlCouchePolygones"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	kmlCouchePolygonesSnapping = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   	snapping : {
		           tolerance: 10,
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};

	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	kmlCoucheMultiPointsSnapping = {
		   title: "Ma couche KML de multi points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiPoints",
		       snapping : {
		           tolerance: 10,
		           enable: false
		       },     
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiLignesSnapping = {
	    title: "Ma couche KML de multi lignes",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlMultiLines
           	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"kmlCoucheMultiLignes",
		   snapping : {
		   	snappingLayersIdentifier:["kmlCoucheMultiPoints","kmlCoucheMultiPolygones"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPolygonesSnapping = {
	    title: "Ma couche KML de multi polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheMultiPolygones",
	    	snapping : {
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};
	
	
	/*********************************************
	  couches pour les outils d'édition "avancés"
	 *********************************************/
					
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	kmlCouchePointsFctAvanced = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
	
	kmlCouchePointsFctAvancedBis = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true,
		       	attributsNotClonable:[{fieldName: 'd_attrib_3'}]
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
	
	kmlCouchePointsFctAvanced2 = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCoucheMultiPoints"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPoints"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
	
	kmlCouchePointsFctAvanced3 = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCoucheMultiPoints"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheLignesFctAvanced = {
	    title: "Ma couche KML de lignes",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlLines
           	 
	        }
	    ],
	    options: {
	        id:"kmlCoucheLignes",
	        copy: {
	        	enable: true
	        },
	        clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheLignesFctAvancedBis = {
		   title: "Ma couche KML de lignes",
		   type: 11,
		   definition: [
		       {
		       	serverUrl: fichierKmlLines
               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes,
	           attribution: attribution
		   }
		};
	
	kmlCoucheLignesFctAvanced2 = {
		   title: "Ma couche KML de lignes",
		   type: 11,
		   definition: [
		       {
		       	serverUrl: fichierKmlLines
               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCoucheMultiLignes"]
		       },
		       split: {
		       	supportLayersIdentifier: ["kmlCoucheMultiLignes"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["kmlCouchePoints","kmlCoucheMultiLignes","kmlCouchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiLignes"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiLignes"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes,
	           attribution: attribution
		   }
		};
	
	kmlCoucheLignesFctAvanced3 = {
		   title: "Ma couche KML de lignes",
		   type: 11,
		   definition: [
		       {
		       	serverUrl: fichierKmlLines
               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheLignes",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCoucheMultiLignes"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiLignes"],
		       	enable:true
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiLignes"],
		       	enable:true
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes,
	           attribution: attribution
		   }
		};
	
	kmlCouchePolygonesFctAvanced = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   	copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       aggregate: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};
	
	kmlCouchePolygonesFctAvancedBis = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};
	
	kmlCouchePolygonesFctAvanced2 = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
		       },
		       split: {
		       	supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["kmlCoucheLignes","kmlCoucheMultiPolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};
	
	kmlCouchePolygonesFctAvanced3 = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		       	enable:true
		       },
		       halo: {
		       	supportLayers: [{id:"kmlCouchePoints"}, {id:"kmlCoucheLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePoints","kmlCoucheLignes"]
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
        		   "default":{// EDITION WFS
        		   	"Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
        		   }	
		       },*/
		       /*symbolizers: { //WFS
     		      "Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};

	kmlCouchePolygonesFctAvanced4 = {
		   title: "Couche Support KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   		attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePoints","kmlCoucheLignes"]
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
        		   "default":{// EDITION WFS
        		   	"Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
        		   }	
		       },*/
		       /*symbolizers: { //WFS
     		      "Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
	           attribution: attribution
		   }
		};
		
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	kmlCoucheMultiPointsFctAvanced = {
		   title: "Ma couche KML de multi points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiPoints", 
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       aggregate: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiPointsFctAvancedBis = {
		   title: "Ma couche KML de multi points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiPoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiPointsFctAvanced2 = {
		   title: "Ma couche KML de multi points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiPoints", 
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCouchePoints"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCouchePoints"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiPointsFctAvanced3 = {
		   title: "Ma couche KML de multi points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiPoints", 
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCouchePoints"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCouchePoints"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiLignesFctAvanced = {
	    title: "Ma couche KML de multi lignes",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlMultiLines
           	 
	        }
	    ],
	    options: {
	        id:"kmlCoucheMultiLignes",
	        copy: {
	        	enable: true
	        },
	        clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiLignesFctAvancedBis = {
		   title: "Ma couche KML de multi lignes",
		   type: 11,
		   definition: [
		       {
		       	serverUrl: fichierKmlMultiLines
               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiLignesFctAvanced2 = {
		   title: "Ma couche KML de multi lignes",
		   type: 11,
		   definition: [
		       {
		       	serverUrl: fichierKmlMultiLines
               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCoucheLignes"]
		       },
		       split: {
		       	supportLayersIdentifier: ["kmlCoucheLignes"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["kmlCoucheMultiPoints","kmlCoucheLignes","kmlCouchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCoucheLignes"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiLignesFctAvanced3 = {
		   title: "Ma couche KML de multi lignes",
		   type: 11,
		   definition: [
		       {
		       	serverUrl: fichierKmlMultiLines
               	 
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiLignes",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["kmlCoucheLignes"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCoucheLignes"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCoucheLignes","kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
		           enable: true*/
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiPolygonesFctAvanced = {
	    title: "Ma couche KML de multi polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheMultiPolygones",
	    	copy: {
	        	enable: true
	        },
	    	clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPolygonesFctAvancedBis = {
		   title: "Ma couche KML de multi polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		       	enable: true
		       },
		   	clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiPolygonesFctAvanced2 = {
		   title: "Ma couche KML de multi polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["kmlCouchePolygones"]
		       },
		       split: {
		       	supportLayersIdentifier: ["kmlCouchePolygones"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["kmlCoucheMultiLignes","kmlCouchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCouchePolygones"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones,
	           attribution: attribution
		   }
		};
	
	kmlCoucheMultiPolygonesFctAvanced3 = {
		   title: "Ma couche KML de multi polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["kmlCouchePolygones"],
		       	enable:true
		       },
		       halo: {
		       	supportLayers: [{id:"kmlCoucheMultiPoints"}, {id:"kmlCoucheMultiLignes"}],
		           distance: 20000/*,
		           enable: true*/
		       },
		       buffer: {
		       	supportLayers: [{id:"kmlCouchePolygones"}],
		           distance: 20000/*,
		           enable: true*/
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiLignes","kmlCoucheMultiPoints"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":{
	     		      "Polygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           },
			          "MultiPolygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones,
	           attribution: attribution
		   }
		};
	
		kmlCoucheMultiPolygonesFctAvanced4 = {
		   title: "Couche Support KML de multi polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiLignes","kmlCoucheMultiPoints"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":{
	     		      "Polygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           },
			          "MultiPolygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones,
	           attribution: attribution
		   }
		};
	
	/*****************************************************
	  couches pour le clonage
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
		
	kmlCoucheClonePoints = {
	    title: "Ma couche KML de points pour clonage",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlClonePoints
	                	 
	        }
	    ],
	    options: { 
	    	id:"kmlCoucheClonePoints",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCouchePoints"]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCouchePoints",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiPoints"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCouchePoints"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerClonePoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheCloneLignes = {
	    title: "Ma couche KML de lignes pour clonage",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlCloneLines
           	 	
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCoucheLignes"]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["kmlCoucheLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheLignes"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheLignes"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheLignes"]/*,
	            enable: true*/
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        substract: {
	            supportLayersIdentifier: ["kmlCouchePolygones"]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheCloneLignes2 = {
	    title: "Ma couche KML de lignes pour clonage",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlCloneLines
           	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["kmlCoucheLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheLignes"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheLignes"],
	            enable: true
	        },
	        snapping: {
	        	snappingLayersIdentifier:["kmlCouchePoints"],
	            tolerance: 10/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheLignes"]/*,
	            enable: true*/
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheClonePolygones = {
		   title: "Ma couche KML de polygones pour clonage",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlClonePolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCoucheClonePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
		               {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		   		supportLayersIdentifier: ["kmlCouchePolygones"]
		       },
		       halo: {
		       	supportLayers: [{id:"kmlCouchePoints"}, {id:"kmlCoucheLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"kmlCouchePolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "kmlCouchePolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_cloneattrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_cloneattrib_3"
	                        }
	                    ]
	                }]
		       },
		       homothetic: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       }, 
		       split: {
		           supportLayersIdentifier: ["kmlCouchePolygones"]/*,
		           enable: true*/
		       },
		       divide: {
		           supportLayersIdentifier: ["kmlCouchePolygones"]/*,
		           enable: true*/
		       },
		       intersect: {
		       	supportLayersIdentifier:["kmlCouchePolygones"],
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCouchePolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerClonePolygones,
	           attribution: attribution
		   }
		};
	
	kmlCoucheClonePolygones2 = {
	    title: "Ma couche KML de polygones pour clonage",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlClonePolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheClonePolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCouchePolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        homothetic: {
	            supportLayersIdentifier: ["kmlCouchePolygones"],
	            enable: true
	        }, 
	        split: {
	            supportLayersIdentifier: ["kmlCouchePolygones"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["kmlCouchePolygones"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCouchePolygones"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCouchePolygones"],
	            enable: true
	        },
	        snapping: {
	        	snappingLayersIdentifier:["kmlCouchePoints"],
	            tolerance: 10/*,
	            enable: true*/
	        },
	        /*symbolizers: {
	        	"default":   {//pour affichage
            		      "Polygon": {
				               fillColor: "yellow",
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "yellow",
				               strokeOpacity: 1,
				               strokeWidth: 4,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid"
				           }
	        	}
	        },*/
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerClonePolygones,
	        attribution: attribution
	    }
	};

	kmlCoucheClonePolygones4 = {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlClonePolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"kmlCoucheClonePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
		               {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		   		supportLayersIdentifier: ["kmlCouchePolygones"],
	            enable: true
		       },
		       halo: {
		       	supportLayers: [{id:"kmlCouchePoints"}, {id:"kmlCoucheLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"kmlCouchePolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "kmlCouchePolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_cloneattrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_cloneattrib_3"
	                        }
	                    ]
	                }]
		       },
		       homothetic: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       }, 
		       split: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       divide: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["kmlCouchePolygones"],
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["kmlCouchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCouchePolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerClonePolygones,
	           attribution: attribution
		   }
		};
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
		
	kmlCoucheCloneMultiPoints = {
	    title: "Ma couche KML de  multi points pour clonage",
	    type: 11,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierKmlCloneMultiPoints
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneMultiPoints",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheMultiPoints",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCoucheMultiPoints"]
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiPoints"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheMultiPoints"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPoints,
	        attribution: attribution
	    }
	};
	
	kmlCoucheCloneMultiLignes = {
	    title: "Ma couche KML de multi lignes pour clonage",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlCloneMultiLines
           	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneMultiLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheMultiLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
	                ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCoucheMultiLignes"]
	        },
	        split: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheMultiLignes"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheCloneMultiLignes2 = {
	    title: "Ma couche KML de multi lignes pour clonage",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlCloneMultiLines
           	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneMultiLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheMultiLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
	                ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        snapping: {
	        	//snappingLayersIdentifier:["kmlCoucheMultiLignes"],
	            tolerance: 10,
	            enable: true
	        }, 
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheMultiLignes"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheCloneMultiPolygones = {
	    title: "Ma couche KML de multi polygones pour clonage",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlCloneMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
	        },
	        buffer: {
	        	supportLayers: [{id:"kmlCoucheMultiPolygones"}]/*,
	            enable: true*/
	        }, 
	        halo: {
	        	supportLayers: [{id:"kmlCoucheMultiPoints"}, {id:"kmlCoucheMultiLignes"}]/*,
	            enable: true*/
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones,
	        attribution: attribution
	    }
	};
	
	kmlCoucheCloneMultiPolygones2 = {
	    title: "Ma couche KML de multi polygones pour clonage",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlCloneMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        homothetic: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
	            enable: true*/
	        },
	        snapping: {
	        	snappingLayersIdentifier:["kmlCoucheMultiLignes"],
	            tolerance: 10,
	            enable: true
	        }, 
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones,
	        attribution: attribution
	    }
	};

	kmlCoucheCloneMultiPolygones4 = {
	    title: "Ma couche KML de multi polygones",
	    definition: [
	                 {
	                	 serverUrl: fichierKmlCloneMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"kmlCoucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "kmlCoucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        buffer: {
	        	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
	            enable: true
	        }, 
	        halo: {
	        	supportLayers: [{id:"kmlCoucheMultiPoints"}, {id:"kmlCoucheMultiLignes"}],
	            enable: true
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        }, 
	        split: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        }, 
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones,
	        attribution: attribution
	    }
	};
	
	/***************************
	  couches pour exemples complets
	 ****************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	kmlCouchePointsFull = {
		   title: "Ma couche KML de points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlPoints
		       }
		   ],
		   options: {
		       id:"kmlCouchePoints",
		       attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		           ]
		       },
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "kmlCoucheMultiPoints",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_attrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_attrib_3"
	                        }
	                    ]
	                }]
		       },
		       copy: {
		   		supportLayersIdentifier: ["kmlCoucheMultiPoints"]
		       }, 
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPoints"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["kmlCouchePolygones","kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       maxScale: 1000, 
		       minScale: 40000, 
		       maxEditionScale: 100,
		       minEditionScale: 40000,
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
		       attribution: attribution
		   }
		};
	
	kmlCoucheLignesFull = {
	    title: "Ma couche KML de lignes",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlLines	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"kmlCoucheLignes",
		   snapping : {
		   	snappingLayersIdentifier:["kmlCouchePoints"],
	            tolerance: 10,
	            enable: true
	        },
	        clone: {
	            supportLayers: [{
                  id: "kmlCoucheMultiLignes",
                  attributes: [{
                          from: "d_attrib_2",
                          to: "d_attrib_2"
                      },
                      {
                          from: "d_attrib_3",
                          to: "d_attrib_3"
                      }
                  ]
              }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCoucheMultiLignes"]
	        }, 
	        split: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["kmlCoucheMultiLignes"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["kmlCouchePolygones","kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCouchePolygones","kmlCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	kmlCouchePolygonesFull= {
		   title: "Ma couche KML de polygones",
		   type: 11,
		   definition: [
		                {
		               	 serverUrl: fichierKmlPolygons
		       }
		   ],
		   options: {
		   	id:"kmlCouchePolygones",
		   	snapping : {
		           tolerance: 10,
		           enable: false
		       },
		       clone: {
		           supportLayers: [{
	                    id: "kmlCoucheMultiPolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_attrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_attrib_3"
	                        }
	                    ]
	                }]
		       },
		       copy: {
		   		supportLayersIdentifier: ["kmlCoucheMultiPolygones"]
		       },
		       buffer: {
		       	supportLayers: [{id:"kmlCoucheMultiPolygones"}],
		       	distance:20000/*,
		           enable: true*/
		       }, 
		       halo: {
		       	supportLayers: [{id:"kmlCouchePoints"}, {id:"kmlCoucheLignes"}],
		       	distance:20000/*,
		           enable: true*/
		       }, 
		       homothetic: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
		           enable: true*/
		       }, 
		       split: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
		           enable: true*/
		       }, 
		       divide: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"]/*,
		           enable: true*/
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["kmlCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
		       attribution: attribution
		   }
		};

	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	kmlCoucheMultiPointsFull = {
		   title: "Ma couche KML de multi points",
		   type: 11,
		   definition: [
		                {	            	
		               	 serverUrl: fichierKmlMultiPoints
		       }
		   ],
		   options: {
		       id:"kmlCoucheMultiPoints",
		       attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		           ]
		       },
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },     
		       maxScale: 1000, 
		       minScale: 40000, 
		       maxEditionScale: 100,
		       minEditionScale: 40000,
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
		       attribution: attribution
		   }
		};
	
	kmlCoucheMultiLignesFull = {
	    title: "Ma couche KML de multi lignes",
	    type: 11,
	    definition: [
	        {
	        	serverUrl: fichierKmlMultiLines
         	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"kmlCoucheMultiLignes",
		   snapping : {
		   	snappingLayersIdentifier:["kmlCoucheMultiPoints"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	kmlCoucheMultiPolygonesFull= {
	    title: "Ma couche KML de multi polygones",
	    type: 11,
	    definition: [
	                 {
	                	 serverUrl: fichierKmlMultiPolygons
	        }
	    ],
	    options: {
	    	id:"kmlCoucheMultiPolygones",
	    	snapping : {
	            tolerance: 10,
	            enable: false
	        },
	        clone: {
	            supportLayers: [{
                  id: "kmlCouchePolygones",
                  attributes: [{
                          from: "d_attrib_2",
                          to: "d_attrib_2"
                      },
                      {
                          from: "d_attrib_3",
                          to: "d_attrib_3"
                      }
                  ]
              }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["kmlCouchePolygones"]
	        },
	        buffer: {
	        	supportLayers: [{id:"kmlCouchePolygones"}],
	        	distance:20000/*,
	            enable: true*/
	        }, 
	        halo: {
	        	supportLayers: [{id:"kmlCoucheMultiPoints"}, {id:"kmlCoucheMultiLignes"}],
	        	distance:20000/*,
	            enable: true*/
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["kmlCouchePolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["kmlCouchePolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["kmlCouchePolygones"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["kmlCouchePolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["kmlCouchePolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["kmlCouchePolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};
	
	/***************************
	  couche de fond
	 ****************************/
	
	coucheBase = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://georef.application.i2/cartes/mapserv?",
					layerName: "fond_vecteur"
				}
			],
			options: {
				maxScale: 100,
				minScale: 10000001,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		};

		groupeFonds = {
		   title: "Fonds cartographiques",
		   options: {
		       opened: true
		   }
		};
		 groupeEditionKML = {
		     title: "Mes couches d'édition KML - objet simple",
		     options: {
		         opened: true
		     }
		 };
		 
		 groupeEditionKMLMulti = {
		     title: "Mes couches d'édition KML - objet composite",
		     options: {
		         opened: true
		     }
		 };
}
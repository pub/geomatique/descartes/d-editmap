function load() {

	initExampleMapContentEditionSelect();

    Descartes.Log.setLevel('debug');
	chargeCouchesGroupes();
	chargeEditionCouchesGroupes();
	chargeEditionCouchesGroupesGEOJSON();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesWFS();
	loadContext();	

}

function doMap(){

	 var autoSave = true;
	 if (saveType === "manuel") {
		autoSave = false;
	 }

	 //Configuration du gestionnaire d'édition
	 var configuration = {
		autoSave: autoSave,
        globalEditionMode: true, //mode global piloté par l'arbre des couches
        displaySaveMsg: false, //désactivation de la confirmation de la sauvegarde
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
     };
     if (withVerifConformite) {
		configuration.verifTopoConformity={
	    	  enable: true //verification de la conformite topologique pour toutes les couches d'édition
	    };
	 }
	 Descartes.EditionManager.configure(configuration);     
		
	var contenuCarte = new Descartes.MapContent({editable: true, editInitialItems: true});
	
	/*
	contenuCarte.addItem(coucheAnnotations);
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
    */
    
    /* 
	contenuCarte.addItem(coucheParkingsWfs);
	contenuCarte.addItem(coucheStationsWfs);
	contenuCarte.addItem(coucheEauWfs);
	contenuCarte.addItem(coucheNatureWfs);
	*/
    
    contenuCarte.populate(context.items);
    
    initBounds = [context.bbox.xMin, context.bbox.yMin, context.bbox.xMax, context.bbox.yMax];
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					maxExtent: maxBounds,
					displayExtendedOLExtent: true,
					minScale:2150000,
					maxScale:100,
					autoSize: true
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	/*carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
										  args: {
				                            resultLayerParams: {
				                                display: true
				                            }
    }});
    carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_BUFFER_HALO_SELECTION,
        args: {
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.LINE_BUFFER_HALO_SELECTION,
        args: {
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	*/
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.DISPLAY_LAYERSTREE_SIMPLE});
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.SHARE_LINK_MAP});
	
	 //Ajout d'un barre d'outils d'édition
	 var editionTools = [];
	 
	 if(geomType === "composite") {
	 	editionTools.push({type: Descartes.Map.EDITION_SELECTION});
	 }
	 
	 if (mapContentType !== "EditionGenericVide") {
	 	editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION,
			               args: {
			                   snapping: true
			               }});
	 } else {
		 editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION,
			               args: {
			                   geometryType: Descartes.Layer.POINT_GEOMETRY
			               }}
	     );
		 editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION,
			               args: {
			                   geometryType: Descartes.Layer.LINE_GEOMETRY
			               }}
	     );
	     editionTools.push({type: Descartes.Map.EDITION_DRAW_CREATION,
			               args: {
			                   geometryType: Descartes.Layer.POLYGON_GEOMETRY
			               }}
	     );	 
     } 
	 editionTools.push({type: Descartes.Map.EDITION_GLOBAL_MODIFICATION});
	 if(geomType === "composite") {
	 	editionTools.push({type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION});
	 }
	 editionTools.push({type: Descartes.Map.EDITION_VERTICE_MODIFICATION});
	 editionTools.push({type: Descartes.Map.EDITION_ATTRIBUTE});
	 editionTools.push({type: Descartes.Map.EDITION_RUBBER_DELETION});
	 if(geomType === "composite") {
		editionTools.push({type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION});
	 }
	 
	 if (saveType === "manuel") {
		editionTools.push({type: Descartes.Map.EDITION_SAVE});
	 }
	 
	 carte.addEditionToolBar('editionToolBar', editionTools);
	
	 carte.addContentManager(
		'layersTree',
		[
			{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL},
			{type : Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL}
		],
		{
			toolBarDiv: "managerToolBar"
		}
	);
    carte.show();

	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	//carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});


	// Ajout des assistants
	//carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	//carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	//	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	//carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	//carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout asssitant localisation à l'adresse
    //carte.addAction({type: Descartes.Action.LocalisationAdresse, div: 'localisationAdresse'});
    
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
    
	// Ajout de la minicarte
	carte.addMiniMap("https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?LAYERS=c_natural_Valeurs_type");

	// Ajout du gestionnaire de requete
	/*var gestionnaireRequetes = carte.addRequestManager('Requetes');
	
	var laCoucheBati = carte.mapContent.getLayerById("coucheBati");
	var requeteBati = new Descartes.Request(laCoucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);

	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	*/
	// Ajout du gestionnaire d'info-bulle
	/*var laCoucheParkings = carte.mapContent.getLayerById("coucheParkings");
	var laCoucheStations = carte.mapContent.getLayerById("coucheStations");
	carte.addToolTip('ToolTip', [
		{layer: laCoucheParkings, fields: ['name']},
		{layer: laCoucheStations, fields: ['name']}
	]);
	*/
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes',{behavior : Descartes.Action.BookmarksManager.BEHAVIOR_MANAGED_BY_CREATOR});

	// Ajout du gestionnaire de localisation rapide
	//carte.addDefaultGazetteer('Gazetteer', "93", Descartes.Action.DefaultGazetteer.DEPARTEMENT);

	if (saveType !== "manuel") {
		carte.addOpenLayersInteractions([
        	{type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
        	{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);
	}

	
}

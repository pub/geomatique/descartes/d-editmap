const path = require('path');
const fs = require('fs');
const exec = require('child_process').exec;
require('shelljs/global');
const utils = require('./utils.js');

// à compléter selon l'implémentation de nouveau mode
const availableModes = ['bootstrap4'];

const argv = require('yargs')
    .help('h')
    .alias('h', 'help')
    .option('ui', {
        type: 'string',
        description: 'UI mode to select',
        default: availableModes[0],
        choices: availableModes
    })
    .option('port', {
        type: 'number',
        default: 4201,
        description: 'Port on which to run dev server',
    }).argv;

console.log('Using UI mode:', argv.ui);

const root = path.resolve(__dirname, '../');
const nodeModules = path.resolve(root, 'node_modules');
const distPath = path.resolve(root, 'dist');
const vendorPath = path.resolve(distPath, 'vendor');

rm('-rf', distPath);
console.log('Dist folder emptied');

fs.mkdirSync(distPath);
//fs.mkdirSync(vendorPath);

/*console.log('Concatenating vendor files (JS and CSS)...');
const jsFiles = [

];
const cssFiles = [

];
utils.concatFiles(jsFiles, path.resolve(vendorPath, 'd-editmap-required.js'));
utils.concatFiles(cssFiles, path.resolve(vendorPath, 'd-editmap-required.css'));
*/
module.exports = {
    argv
};

const path = require('path');
const webpack = require('webpack');
const webpackConf = require('./webpack.config.js');
const webpackDevServer = require('webpack-dev-server');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const argv = require('./common').argv;

// get conf for current options
const conf = webpackConf(argv.ui, argv.georef);

// add entry to enable live reload & add bundle analyzer
conf.entry = ['webpack-dev-server/client?http://localhost:' + argv.port, conf.entry];
conf.plugins = conf.plugins.concat([
    new BundleAnalyzerPlugin({
        openAnalyzer: false,
        analyzerPort: 4101
    })
]);

// info du serveur JAVA ou PHP servant Descartes coté serveur
var descartesServer = {
    protocol: 'http',
    host: 'localhost',
    port: 8080,
    path: '/services/api/v1/'
};

const server = new webpackDevServer(webpack(conf), {
    proxy: {
        //on a donc un proxy de proxy.... (pour le dev uniquement)
        '/*/proxy': {
            target: descartesServer,
            pathRewrite: {
                '/.*/proxy': '/proxy'
            }
        },
        '/*/exportPDF': {
            target: descartesServer,
            pathRewrite: {
                '/.*/exportPDF': '/exportPDF'
            }
        },
        '/*/exportPNG': {
            target: descartesServer,
            pathRewrite: {
                '/.*/exportPNG': '/exportPNG'
            }
        },
        '/*/contextManager': {
            target: descartesServer,
            pathRewrite: {
                '/.*/contextManager': '/contextManager'
            }
        },
        '/*/getFeature': {
            target: descartesServer,
            pathRewrite: {
                '/.*/getFeature': '/getFeature'
            }
        },
        '/*/getFeatureInfo': {
            target: descartesServer,
            pathRewrite: {
                '/.*/getFeatureInfo': '/getFeatureInfo'
            }
        },
        '/*/getImage': {
            target: descartesServer,
            pathRewrite: {
                '/.*/getImage': '/getImage'
            }
        },
        '/*/edition': {
            target: descartesServer,
            pathRewrite: {
                '/.*/edition': '/edition'
            }
        },
        '/*/editionKML': {
            target: descartesServer,
            pathRewrite: {
                '/.*/editionKML': '/editionKML'
            }
        },
        '/*/editionGeoJSON': {
            target: descartesServer,
            pathRewrite: {
                '/.*/editionGeoJSON': '/editionGeoJSON'
            }
        }
    },
    inline: true,
    contentBase: [
        path.resolve(__dirname, '../node_modules' ),
        path.resolve(__dirname, '../dist' ),
        path.resolve(__dirname, '../demo')
    ],
    quiet: false,
    noInfo: false,
    publicPath: '/',
    stats: {
        colors: true
    },
    watchContentBase: true,
    port: argv.port
});

console.log('Starting webpack-dev-server...');
server.listen(argv.port, 'localhost', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Dev server started on http://localhost:' + argv.port);
    }
});
